package hk.yijian.view.toast;

import android.content.Context;

/**
 * Created by young on 2018/2/11.
 */

public class _TUtils {

    public static void _SToast(Context context,String str) {
        _Toast.makeText(context, ""+str, 0, _Toast.WARNING,_Toast.SCALE);
    }
    public static void _LToast(Context context,String str) {
        _Toast.makeText(context, ""+str, 1, _Toast.WARNING,_Toast.SCALE);
    }

}
