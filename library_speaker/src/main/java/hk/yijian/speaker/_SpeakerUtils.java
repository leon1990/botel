package hk.yijian.speaker;

import android.content.Context;
import android.util.Log;

/**
 * Created by young on 2018/1/26.
 */

public class _SpeakerUtils {

    private _SpeakerUtils() {
    }

    private static class _RoomCardUtilsLoader {
        private static final _SpeakerUtils INSTANCE = new _SpeakerUtils();
    }

    public static _SpeakerUtils getInstance() {
        return _RoomCardUtilsLoader.INSTANCE;
    }

    public void pauseSpeaker(Context context) {
        if(TextToSpeech.isSpeeching()){
            TextToSpeech.pause(context);
        }
    }

    public void openSpeaker(Context context,String str) {
        TextToSpeech.setOnText2SpeechListener(new OnText2SpeechListener() {
            @Override
            public void onCompletion() {
                Log.i("speak","onCompletion");
            }
            @Override
            public void onPrepared() {
                Log.i("speak","onPrepared");
            }
            @Override
            public void onError(Exception e, String s) {
                Log.i("speak","onError");
            }
            @Override
            public void onStart() {
                Log.i("speak","onStart");
            }
            @Override
            public void onLoadProgress(int i, int i1) {
                Log.i("speak","onLoadProgress");
            }
            @Override
            public void onPlayProgress(int i, int i1) {
                Log.i("speak","onPlayProgress---->"+ i);
            }
        });

        if(TextToSpeech.isSpeeching()){
            TextToSpeech.pause(context);
        }else{
            TextToSpeech.speech(context,str,true);
        }
    }

    public void closeSpeaker(Context context) {
        if(TextToSpeech.isSpeeching()){
            TextToSpeech.pause(context);
            TextToSpeech.shutUp(context);
        }
    }

}
