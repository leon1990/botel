package hk.oneroom.callhardware;

public class _CallPrinter {
    /**
     * 01/传入code参数初始化不同厂商设备
     * 02/传入code参数调用不同方法
     */
    private _CallPrinter() {
    }

    private static class _CallPrinterLoader {
        private static final _CallPrinter INSTANCE = new _CallPrinter();
    }

    public static _CallPrinter getInstance() {
        return _CallPrinter._CallPrinterLoader.INSTANCE;
    }

}
