package hk.oneroom.callhardware;

public class _CallRoomCard {

    private _CallRoomCard() {
    }

    private static class _CallRoomCardLoader {
        private static final _CallRoomCard INSTANCE = new _CallRoomCard();
    }

    public static _CallRoomCard getInstance() {
        return _CallRoomCard._CallRoomCardLoader.INSTANCE;
    }


}
