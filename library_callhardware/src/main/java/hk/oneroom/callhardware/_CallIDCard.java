package hk.oneroom.callhardware;

import android.content.Context;

public class _CallIDCard {

    private _CallIDCard() {
    }

    private static class _CallIDCardLoader {
        private static final _CallIDCard INSTANCE = new _CallIDCard();
    }

    public static _CallIDCard getInstance() {
        return _CallIDCard._CallIDCardLoader.INSTANCE;
    }


    /**
     * 初始化ID card
     * @param context 上下文
     * @param initcode 厂商code
     * @return
     */
    public boolean InitIDCard(Context context,int initcode) {

        boolean initBool = false;
        switch(initcode) {
            case 101://A厂商打印机
                initBool = false;
                break;
            case 102://B厂商打印机
                initBool = false;
                break;
            case 103://C厂商打印机
                initBool = false;
                break;

        }
        return initBool;
    }

    /**
     * 根据传入的code调用相应的方法
     * @param initcode
     * @param callcode
     */
    public void CallMethod(int initcode,int callcode) {

        if(initcode == 101){
            switch(initcode) {
                case 201://打印小票
                    break;
                case 202://关闭打印机
                    break;

            }
        }else if(initcode == 102){
            switch(initcode) {
                case 201://打印小票
                    break;
                case 202://关闭打印机
                    break;

            }
        }else if(initcode == 103){

            switch(initcode) {
                case 201://打印小票
                    break;
                case 202://关闭打印机
                    break;

            }
        }
    }


}
