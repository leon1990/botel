package hk.yijian.hardware;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;

import com.hdos.usbdevice.publicSecurityIDCardLib;

import java.io.UnsupportedEncodingException;

/**
 * Created by young on 2017/11/14.
 */

public class _IDCardUtils {

    //身份证识别器 01
    private static final String TAG = "hardwarelib_IDCardDevice";
    private publicSecurityIDCardLib iDCardDevice;
    private byte[] name = new byte[32];
    private byte[] sex = new byte[6];
    private byte[] birth = new byte[18];
    private byte[] nation = new byte[12];
    private byte[] address = new byte[72];
    private byte[] Department = new byte[32];
    private byte[] IDNo = new byte[38];
    private byte[] EffectDate = new byte[18];
    private byte[] ExpireDate = new byte[18];
    byte [] BmpFile = new byte[38556];
    boolean mInitIDCard = false;

    private _IDCardUtils() {

    }
    private static class _IDCardUtilsLoader {
        private static final _IDCardUtils INSTANCE = new _IDCardUtils();
    }

    public static _IDCardUtils getInstance() {
        return _IDCardUtils._IDCardUtilsLoader.INSTANCE;
    }

    public boolean InitIDCardDevice(Context context) {
        try {
            iDCardDevice = new publicSecurityIDCardLib(context);
            mInitIDCard = true;
            _Log.d(TAG,"iDCardDevice创建成功");
        }catch (Exception e){
            mInitIDCard = false;
            _Log.d(TAG,"iDCardDevice创建失败");
        }
        return mInitIDCard;
    }
    public int getSAMStatus() {
        if(mInitIDCard){
            _Log.d(TAG,"iDCardDevice_getSAMStatus："+iDCardDevice.getSAMStatus());
            return iDCardDevice.getSAMStatus();
        }else {
            _Log.d(TAG,"iDCardDevice_getSAMStatus：-1");
            return -1;
        }
    }

    public SparseArray<String> readIDCardInfo(String pkName) {
        int ret = -1;
        SparseArray<String> mIDCardmap = new SparseArray<String>();
        if(mInitIDCard){
            try {
                ret = iDCardDevice.readBaseMsgToStr(pkName,BmpFile,name,sex,nation, birth,
                        address, IDNo, Department, EffectDate, ExpireDate);
                _Log.d(TAG,"iDCardDevice_readBaseMsgToStr："+ret);
                mIDCardmap.put(0,""+ret);
                if(ret==0x90) {
                    try {
                        mIDCardmap.put(1,""+new String(name, "Unicode"));
                        mIDCardmap.put(2,""+new String(sex, "Unicode"));
                        mIDCardmap.put(3,""+new String(nation, "Unicode"));
                        mIDCardmap.put(4,""+new String(birth, "Unicode"));
                        mIDCardmap.put(5,""+new String(address, "Unicode"));
                        mIDCardmap.put(6,""+new String(IDNo, "Unicode"));
                        mIDCardmap.put(7,""+new String(Department, "Unicode"));
                        mIDCardmap.put(8,""+new String(EffectDate, "Unicode"));
                        mIDCardmap.put(9,""+new String(ExpireDate, "Unicode"));
                        _Log.d(TAG,"姓名:"+new String(name, "Unicode")+'\n'
                                +"性别:"+new String(sex, "Unicode")+'\n'
                                +"民族:"+ new String(nation, "Unicode")+"族"+'\n'
                                +"出生日期:"+new String(birth, "Unicode")+'\n'
                                +"住址:"+new String(address, "Unicode")+'\n'
                                +"身份证号码:"+new String(IDNo, "Unicode")+'\n'
                                +"签发机关:"+new String(Department, "Unicode")+'\n'
                                +"有效日期:"+ new String(EffectDate, "Unicode") +
                                "至" + new String(ExpireDate, "Unicode")+'\n');
                    } catch (UnsupportedEncodingException e) {// Auto-generated catch block
                        e.printStackTrace();
                    }
                } else{
                    _Log.d(TAG,"读基本信息失败:"+String.format("0x%02x", ret));
                    mIDCardmap.put(1,""+404);
                    mIDCardmap.put(2,""+404);
                    mIDCardmap.put(3,""+404);
                    mIDCardmap.put(4,""+404);
                    mIDCardmap.put(5,""+404);
                    mIDCardmap.put(6,""+404);
                    mIDCardmap.put(7,""+404);
                    mIDCardmap.put(8,""+404);
                    mIDCardmap.put(9,""+404);
                }

            }catch (Exception e){
                _Log.d(TAG,"Exception:"+e.getMessage());
            }

        }
        return mIDCardmap;
    }

    public Bitmap readIDCardImg() {
        if(mInitIDCard){
            _Log.d(TAG,"iDCardDevice_readIDCardImg：true");
            int[] colors =iDCardDevice.convertByteToColor(BmpFile);
            Bitmap b = Bitmap.createBitmap(colors, 102, 126, Bitmap.Config.ARGB_8888);
            //ImageSpan imgSpan = new ImageSpan(b);
            //SpannableString spanString = new SpannableString("icon");
            //spanString.setSpan(imgSpan, 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return b;
        }else {
            _Log.d(TAG,"iDCardDevice_readIDCardImg：false");
            return null;
        }
    }

}
