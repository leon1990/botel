package hk.yijian.hardware;

import android.util.SparseArray;

import K720_Package.K720_Serial;

/**
 * Created by young on 2017/11/8.
 */

public class _RoomCardUtils {

    //房卡发卡机 01
    private static final String TAG = "hardwarelib_RoomCard";
    public static int Port = 1;
    private static int KeyType = 1;
    private static int Sector = 1;
    private static int Block = 2;
    private static byte MacAddr = 0;
    int initState = 0;          //方法没打开

    private _RoomCardUtils() {
    }

    private static class _RoomCardUtilsLoader {
        private static final _RoomCardUtils INSTANCE = new _RoomCardUtils();
    }

    public static _RoomCardUtils getInstance() {
        return _RoomCardUtils._RoomCardUtilsLoader.INSTANCE;
    }

    /**
     *
     * @return
     */
    public int InitLink() {
        String StrPort = "/dev/ttyS"+Port;   // ttyS1  ttys4
        //String StrPort="/dev/ttyUSB0";     //串口转 USB
        int re = 0;
        byte i;
        String[] RecordInfo = new String[2];
        //int Baudate = 9600;
        try{
            re = K720_Serial.K720_CommOpen(StrPort);
            if (re == 0) {
                for (i = 0; i < 16; i++) {
                    re = K720_Serial.K720_AutoTestMac(i, RecordInfo);
                    if (re == 0) {
                        MacAddr = i;
                        break;
                    }
                }
                if (i == 16 && MacAddr == 0) {
                    K720_Serial.K720_CommClose();
                    //_Log.d(TAG, "设备连接失败，错误代码为：" + K720_Serial.ErrorCode(re, 0));
                    initState = 202;//设备连接失败
                    _Log.d(TAG, "设备连接失败，错误代码为：" + initState);
                } else {
                    //_Log.d(TAG, "设备连接成功，设备地址为：" + MacAddr);
                    initState = 200;//设备连接成功
                    _Log.d(TAG, "设备连接成功，设备地址为：" + initState);
                }
            } else{
                //_Log.d(TAG, "串口打开错误，错误代码为：" + K720_Serial.ErrorCode(re, 0));
                initState = 201;
                _Log.d(TAG, "串口打开错误，错误代码为：" + initState );
            }
        }catch (Exception e){
            initState = 203;
            _Log.d(TAG, "设备连接失败，错误代码为：" + initState);
        }
        return initState;
    }

    /**
     *
     * @return
     */
    public SparseArray<Integer> K720_SensorQuery() {
        String str="";
        int nRet;
        byte[] StateInfo = new byte[4];
        String[] RecordInfo = new String[2];
        SparseArray<Integer> K720map = new SparseArray<Integer>();
        if(initState == 200){
            nRet = K720_Serial.K720_SensorQuery(MacAddr, StateInfo, RecordInfo);
            K720map.put(0,nRet);
            K720map.put(1,(int)StateInfo[0]);
            K720map.put(2,(int)StateInfo[1]);
            K720map.put(3,(int)StateInfo[2]);
            K720map.put(4,(int)StateInfo[3]);
            if(nRet == 0) {
                switch(StateInfo[0]) {
                    case 0x39:
                        str += "机器状态：回收箱卡满/卡箱预满\r\n";
                        break;
                    case 0x38:
                        str += "机器状态：回收箱卡满\r\n";
                        break;
                    case 0x34:
                        str += "机器状态：命令不能执行，请点击“复位”\r\n";
                        break;
                    case 0x32:
                        str += "机器状态：准备卡失败，请点击“复位”\r\n";
                        break;
                    case 0x31:
                        str += "机器状态：卡箱预满\r\n";
                        break;
                    case 0x30:
                        str += "机器状态：空闲\r\n";
                        break;
                }
                switch(StateInfo[1]) {
                    case 0x38:
                        str += "动作状态：正在发卡\r\n";
                        break;
                    case 0x34:
                        str += "动作状态：正在收卡\r\n";
                        break;
                    case 0x32:
                        str += "动作状态：发卡出错，请点击“复位”\r\n";
                        break;
                    case 0x31:
                        str += "动作状态：收卡出错，请点击“复位”\r\n";
                        break;
                    case 0x30:
                        str += "动作状态：空闲\r\n";
                        break;
                }
                switch(StateInfo[2]) {
                    case 0x39:
                        str += "卡箱状态：发卡箱已满，无法再回收到发卡箱\r\n";
                        break;
                    case 0x38:
                        str += "卡箱状态：发卡箱已满，无法再回收到发卡箱/卡箱预空\r\n";
                        break;
                    case 0x34:
                        str += "卡箱状态：重叠卡\r\n";
                        break;
                    case 0x32:
                        str += "卡箱状态：卡堵塞\r\n";
                        break;
                    case 0x31:
                        str += "卡箱状态：卡箱预空\r\n";
                        break;
                    case 0x30:
                        str += "卡箱状态：卡箱为非预空状态\r\n";
                        break;
                }
                switch(StateInfo[3]) {
                    case 0x3E:
                        str += "卡片状态：只有一张卡在传感器2-3位置\r\n";
                        break;
                    case 0x3B:
                        str += "卡片状态：只有一张卡在传感器1-2位置\r\n";
                        break;
                    case 0x39:
                        str += "卡片状态：只有一张卡在传感器1位置\r\n";
                        break;
                    case 0x38:
                        str += "卡片状态：卡箱已空\r\n";
                        break;
                    case 0x37:
                        str += "卡片状态：卡在传感器1-2-3的位置\r\n";
                        break;
                    case 0x36:
                        str += "卡片状态：卡在传感器2-3的位置\r\n";
                        break;
                    case 0x35:
                        str += "卡片状态：卡在传感器取卡位置\r\n";
                        break;
                    case 0x34:
                        str += "卡片状态：卡在传感器3位置\r\n";
                        break;
                    case 0x33:
                        str += "卡片状态：卡在传感器1-2位置(读卡位置)\r\n";
                        break;
                    case 0x32:
                        str += "卡片状态：卡在传感器2位置\r\n";
                        break;
                    case 0x31:
                        str += "卡片状态：卡在传感器1位置(取卡位置)\r\n";
                        break;
                    case 0x30:
                        str += "卡片状态：空闲\r\n";
                        break;
                }
                _Log.d(TAG,str);
                _Log.d("hardwarelib_RoomCard_查询",str);
            }else {
                _Log.d(TAG,"查询状态失败！");
                _Log.d("hardwarelib_RoomCard_查询","查询状态失败");

                System.exit(0);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }else {
            _Log.d(TAG,"设备连接失败！__查询");
            _Log.d("hardwarelib_RoomCard_查询","设备连接失败！__查询");
        }
        return K720map;
    }

    public int K720_Query() {
        int nRet = 0;
        byte[] StateInfo = new byte[4];
        StateInfo[0] = 0x41;
        StateInfo[1] = 0x51;
        //StateInfo[2] = 0x53;
        //StateInfo[3] = 0x53;
        String[] RecordInfo = new String[2];
        nRet = K720_Serial.K720_SensorQuery(MacAddr, StateInfo, RecordInfo);
        if (nRet == 0){
            _Log.d("K720RoomCardUtils", "传感器状态查询成功，其值分别为：" +
                    Integer.toHexString(StateInfo[0] & 0xFF).toUpperCase() +
                    " " + Integer.toHexString(StateInfo[1] & 0xFF).toUpperCase() +
                    " " + Integer.toHexString(StateInfo[2] & 0xFF).toUpperCase() +
                    " " + Integer.toHexString(StateInfo[3] & 0xFF).toUpperCase());
        }else{
            _Log.d("K720RoomCardUtils", "传感器状态查询失败，错误代码为：" +
                    K720_Serial.ErrorCode(nRet, 0));
        }

        return nRet;

    }


    public int GetCardID() {
            // TODO Auto-generated method stub
            int nRet;
            String[] RecordInfo=new String[2];
            byte[] CardID=new byte[10];

            nRet = K720_Serial.K720_S50GetCardID(MacAddr, CardID, RecordInfo);
            if(nRet == 0) {
                _Log.d("K720RoomCardUtils", "获取S50卡号成功，其值为：" +
                        Integer.toHexString(CardID[0] & 0xFF).toUpperCase() +
                        Integer.toHexString(CardID[1] & 0xFF).toUpperCase() +
                        Integer.toHexString(CardID[2] & 0xFF).toUpperCase() +
                        Integer.toHexString(CardID[3] & 0xFF).toUpperCase());
            }else {
                _Log.d("K720RoomCardUtils", "获取S50卡号失败，错误代码为：" +
                        K720_Serial.ErrorCode(nRet, 0));
            }
            return nRet;

    }

    public void ToCardBox() {
        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0x44;
        SendBuf[1] = 0x42;
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 2, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils","回收到卡箱命令执行成功");

        } else{
            _Log.d("K720RoomCardUtils","回收到卡箱命令执行失败");

        }
    }

    public void FrontEndCaed() {
        // TODO Auto-generated method stub
        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0x46;
        SendBuf[1] = 0x43;
        SendBuf[2] = 0x38;
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 3, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils", "前端进卡命令执行成功");
        }else{
            _Log.d("K720RoomCardUtils", "前端进卡命令执行失败");
        }
    }

    public void OutRoomCardExit() {
        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0x46;
        SendBuf[1] = 0x43;
        SendBuf[2] = 0X30;
        //K720_SendCmd( MacAddr, “FC0”, 3, RecrodInfo)
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 3, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils","卡片移动到卡口位置成功");
        }else{
            _Log.d("K720RoomCardUtils","卡片移动到卡口位置失败");
        }
    }

    public void OutRoomTakeCardPlace2() {
        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0X44;
        SendBuf[1] = 0X43;
        //K720_SendCmd( MacAddr, “DC”, 2, RecrodInfo)
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 2, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils","卡片移动到取卡位置成功");
        }else{
            _Log.d("K720RoomCardUtils","卡片移动到取卡位置失败");
        }
    }

    public void OutRoomTakeCardPlace() {
        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0x46;
        SendBuf[1] = 0x43;
        SendBuf[2] = 0x34;
        //K720_SendCmd( MacAddr, “FC0”, 3, RecrodInfo)
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 3, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils","卡片移动到取卡位置成功");
        }else{
            _Log.d("K720RoomCardUtils","卡片移动到取卡位置失败");
        }
    }

    public int AcceptRoomCard() {
        int nRet = 404;
        byte[] SendBuf = new byte[2];
        SendBuf[0] = 0x43;
        SendBuf[1] = 0x50;
        String[] RecrodInfo = new String[2];
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 2, RecrodInfo);
        //nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 2, RecordInfo);
        if (nRet == 0){
            _Log.d("K720RoomCardUtils", "收卡成功"+nRet);
            return nRet;
        }else{
            _Log.d("K720RoomCardUtils", "收卡失败"+nRet);
            return nRet;
        }
    }


    public boolean OutCardToReadRegion() {//K720_SendCmd( MacAddr, “FC7”, 3, RecrodInfo)
        // TODO Auto-generated method stub

        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0x46;
        SendBuf[1] = 0x43;
        SendBuf[2] = 0x37;
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 3, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils","卡片移动到读卡位置成功");
            return true;
        }else{
            _Log.d("K720RoomCardUtils","卡片移动到读卡位置失败");
            return true;
        }


    }

    public void Reset() {
        // TODO Auto-generated method stub
        int nRet;
        byte[] SendBuf=new byte[3];
        String[] RecordInfo=new String[2];
        SendBuf[0] = 0x52;
        SendBuf[1] = 0x53;
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 2, RecordInfo);
        if(nRet == 0){
            _Log.d("K720RoomCardUtils_Reset","复位成功");
        }else{
            _Log.d("K720RoomCardUtils_Reset","复位失败");

        }
    }


    public void GetPermissions() {
        // TODO Auto-generated method stub
        int nRet;
        byte[] Password = new byte[10];
        String[] RecordInfo=new String[2];
        String str;
        //str = StrPassWord.getText().toString();
        str = "FFFFFFFFFFFF";
        if(str.length() != 12) {
            _Log.d("K720RoomCardUtils_Verification","密码长度错误，请重新输入...(长度为6个字节)，12个字母");
            return;
        }
        Password = StringToHex(str);
        nRet = K720_Serial.K720_S70LoadSecKey(MacAddr, (byte)(Sector), (byte)(KeyType + 0x30), Password, RecordInfo);
        if(nRet == 0) {
            _Log.d("K720RoomCardUtils_Verification","密码验证命令执行成功");
        } else{
            _Log.d("K720RoomCardUtils_Verification","密码验证命令执行失败");
        }

    }

    public String ReadCardData() {

        // TODO Auto-generated method stub
        String CardNo = "";
        int nRet;
        byte[] Data=new byte[20];
        String[] RecordInfo=new String[2];
        try {
            nRet = K720_Serial.K720_S70ReadBlock(MacAddr, (byte)Sector, (byte)Block, Data, RecordInfo);
            if(nRet == 0) {
                CardNo = ""+ByteArrayToString(Data, 16);
                _Log.d("K720RoomCardUtils_ReadCardData","读数据命令执行成功"+
                        ByteArrayToString(Data, 16));
            } else{
                _Log.d("K720RoomCardUtils_ReadCardData","读数据命令执行失败");
            }
        }catch (Exception e){
        }
        return CardNo;
    }

    public int WriteCardData(String str) {
        // TODO Auto-generated method stub
        int mWriteCode ;
        int nRet;
        byte[] Data = new byte[20];
        String[] RecordInfo = new String[2];
        if(str.length() != 32){
            _Log.d("K720RoomCardUtils_WriteCardData","数据长度错误，请重新输入...(长度为16个字节，32个数字)");
            mWriteCode = 1;
        }
        Data = StringToHex(str);
        nRet = K720_Serial.K720_S70WriteBlock(MacAddr, (byte)Sector, (byte)Block, Data, RecordInfo);
        if(nRet == 0) {
            _Log.d("K720RoomCardUtils_WriteCardData","写数据命令执行成功");
            mWriteCode = 2;
        } else{
            _Log.d("K720RoomCardUtils_WriteCardData","写数据命令执行失败");
            mWriteCode = 3;
        }
        return mWriteCode;
    }


    public static byte[] StringToHex(String str) {
        byte[] by=new byte[255];
        byte[] temp=str.getBytes();
        int length = str.length();
        for(int i = 0; i < length / 2; i++)
        {
            by[i] = String2Hex(temp[2 * i], temp[2 * i + 1]);
        }
        return by;
    }

    public static byte String2Hex(byte src0,byte src1) {
        byte _bo=Byte.decode("0x" + new String(new byte[] {src0})).byteValue();
        _bo = (byte)(_bo << 4);

        byte _b1=Byte.decode("0x" + new String(new byte[] {src1})).byteValue();

        byte by = (byte)(_bo ^ _b1);

        return by;
    }


    public static String ByteArrayToString(byte[] by, int length){//把数据以十六进制显示

        String str="";
        for(int i=0;i<length;i++)
        {
            String hex = Integer.toHexString(by[i] & 0xFF);
            if(hex.length() == 1)
                hex="0" + hex;
            str += hex.toUpperCase();
        }
        return str;
    }

    //TODO
    public void ResetState() {
        int nRet;
        byte[] SendBuf = new byte[3];
        String[] RecordInfo = new String[2];
        SendBuf[0] = 0x52;
        SendBuf[1] = 0x53;
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 2, RecordInfo);
        if (nRet == 0)
            _Log.d("K720RoomCardUtils", "复位成功");
        else
            _Log.d("K720RoomCardUtils", "复位失败");
    }

    public int SendCardOutSide() {
        // TODO Auto-generated method stub
        int nRet;
        byte[] SendBuf = new byte[3];
        String[] RecordInfo = new String[2];
        SendBuf[0] = 0x46;
        SendBuf[1] = 0x43;
        SendBuf[2] = 0x30;
        nRet = K720_Serial.K720_SendCmd(MacAddr, SendBuf, 3, RecordInfo);
        if(nRet == 0) {
            _Log.d("K720RoomCardUtils", "卡片移动到卡口位置成功");
        }else {
            _Log.d("K720RoomCardUtils", "卡片移动到卡口位置失败");
        }
        return nRet;

    }

    public int SearchRoomCard() {
        int nRet =202;
        String[] RecordInfo = new String[2];
        nRet = K720_Serial.K720_S50DetectCard(MacAddr, RecordInfo);
        if (nRet == 0){
            _Log.d("K720RoomCardUtils", "S50寻卡成功");
            return nRet;
        }else{
            _Log.d("K720RoomCardUtils", "S50寻卡失败，错误代码为：" +
                    K720_Serial.ErrorCode(nRet, 0));
            return 202;
        }
    }

    public void GetRoomCardNO() {
        int nRet;
        String[] RecordInfo = new String[2];
        byte[] CardID = new byte[10];
        nRet = K720_Serial.K720_S50GetCardID(MacAddr, CardID, RecordInfo);
        if (nRet == 0)
            _Log.d("K720RoomCardUtils", "获取S50卡号成功，其值为：" +
                    Integer.toHexString(CardID[0] & 0xFF).toUpperCase() +
                    Integer.toHexString(CardID[1] & 0xFF).toUpperCase() +
                    Integer.toHexString(CardID[2] & 0xFF).toUpperCase() +
                    Integer.toHexString(CardID[3] & 0xFF).toUpperCase());
        else
            _Log.d("K720RoomCardUtils", "获取S50卡号失败，错误代码为：" +
                    K720_Serial.ErrorCode(nRet, 0));
    }


    /**
     * K720_QueryMachineState
     * @return
     */
    public int K720_QueryMachineState() {
        int nRet;
        byte[] StateInfo = new byte[4];
        String[] RecordInfo = new String[2];
        int getStateCode = 0;
        if(initState == 200){
            try {
                nRet = K720_Serial.K720_SensorQuery(MacAddr, StateInfo, RecordInfo);
                if(nRet == 0) {
                    String str="";
                    switch(StateInfo[0]) {
                        case 0x39:
                            str += "机器状态：回收箱卡满/卡箱预满\r\n";
                            getStateCode = 1;
                            break;
                        case 0x38:
                            str += "机器状态：回收箱卡满\r\n";
                            getStateCode = 2;
                            break;
                        case 0x34:
                            str += "机器状态：命令不能执行，请点击“复位”\r\n";
                            getStateCode = 3;
                            break;
                        case 0x32:
                            str += "机器状态：准备卡失败，请点击“复位”\r\n";
                            getStateCode = 4;
                            break;
                        case 0x31:
                            str += "机器状态：卡箱预满\r\n";
                            getStateCode = 5;
                            break;
                        case 0x30:
                            str += "机器状态：空闲\r\n";
                            getStateCode = 6;
                            break;
                    }
                    _Log.d(TAG,"getStateCode="+getStateCode);
                    _Log.d(TAG,str);
                }
            }catch (Exception e) {
                _Log.d(TAG,"执行传感器状态异常"+getStateCode);
                getStateCode = 30;
            }
        }else{
            _Log.d(TAG,"执行传感器状态异常"+getStateCode);
            getStateCode = 31;
        }
        return getStateCode;
    }
    /**
     * K720_QueryActionState
     * @return
     */
    public int K720_QueryActionState() {

        int nRet;
        byte[] StateInfo = new byte[4];
        String[] RecordInfo = new String[2];
        int getStateCode = 0;
        if(initState == 200){
            try {
                nRet = K720_Serial.K720_SensorQuery(MacAddr, StateInfo, RecordInfo);
                if(nRet == 0) {
                    String str="";
                    switch(StateInfo[1]) {
                        case 0x38:
                            str += "动作状态：正在发卡\r\n";
                            getStateCode = 7;
                            break;
                        case 0x34:
                            str += "动作状态：正在收卡\r\n";
                            getStateCode = 8;
                            break;
                        case 0x32:
                            str += "动作状态：发卡出错，请点击“复位”\r\n";
                            getStateCode = 9;
                            break;
                        case 0x31:
                            str += "动作状态：收卡出错，请点击“复位”\r\n";
                            getStateCode = 10;
                            break;
                        case 0x30:
                            str += "动作状态：空闲\r\n";
                            getStateCode = 11;
                            break;
                    }
                    _Log.d(TAG,"getStateCode="+getStateCode);
                    _Log.d(TAG,str);
                }
            }catch (Exception e) {
                _Log.d(TAG,"执行传感器状态异常"+getStateCode);
                getStateCode = 30;
            }
        }else{
            _Log.d(TAG,"执行传感器状态异常"+getStateCode);
            getStateCode = 31;
        }
        return getStateCode;
    }
    /**
     * K720_QueryBoxState
     * @return
     */
    public int K720_QueryBoxState() {

        int nRet;
        byte[] StateInfo = new byte[4];
        String[] RecordInfo = new String[2];
        int getStateCode = 0;
        if(initState == 200){
            try {
                nRet = K720_Serial.K720_SensorQuery(MacAddr, StateInfo, RecordInfo);
                if(nRet == 0) {
                    String str="";

                    switch(StateInfo[2]) {
                        case 0x39:
                            str += "卡箱状态：发卡箱已满，无法再回收到发卡箱\r\n";
                            getStateCode = 12;
                            break;
                        case 0x38:
                            str += "卡箱状态：发卡箱已满，无法再回收到发卡箱/卡箱预空\r\n";
                            getStateCode = 13;
                            break;
                        case 0x34:
                            str += "卡箱状态：重叠卡\r\n";
                            getStateCode = 14;
                            break;
                        case 0x32:
                            str += "卡箱状态：卡堵塞\r\n";
                            getStateCode = 15;
                            break;
                        case 0x31:
                            str += "卡箱状态：卡箱预空\r\n";
                            getStateCode = 16;
                            break;
                        case 0x30:
                            str += "卡箱状态：卡箱为非预空状态\r\n";
                            getStateCode = 17;
                            break;
                    }
                    _Log.d(TAG,"getStateCode="+getStateCode);
                    _Log.d(TAG,str);
                }
            }catch (Exception e) {
                _Log.d(TAG,"执行传感器状态异常"+getStateCode);
                getStateCode = 30;
            }
        }else{
            _Log.d(TAG,"执行传感器状态异常"+getStateCode);
            getStateCode = 31;
        }
        return getStateCode;
    }
    /**
     * K720_QueryCardState
     * @return
     */
    public int K720_QueryCardState() {

        int nRet;
        byte[] StateInfo = new byte[4];
        String[] RecordInfo = new String[2];
        int getStateCode = 0;
        if(initState == 200){
            try {
                nRet = K720_Serial.K720_SensorQuery(MacAddr, StateInfo, RecordInfo);
                if(nRet == 0) {
                    String str="";
                    switch(StateInfo[3]) {
                        case 0x3E:
                            str += "卡片状态：只有一张卡在传感器2-3位置\r\n";
                            getStateCode = 18;
                            break;
                        case 0x3B:
                            str += "卡片状态：只有一张卡在传感器1-2位置\r\n";
                            getStateCode = 19;
                            break;
                        case 0x39:
                            str += "卡片状态：只有一张卡在传感器1位置\r\n";
                            getStateCode = 20;
                            break;
                        case 0x38:
                            str += "卡片状态：卡箱已空\r\n";
                            getStateCode = 21;
                            break;
                        case 0x37:
                            str += "卡片状态：卡在传感器1-2-3的位置\r\n";
                            getStateCode = 22;
                            break;
                        case 0x36:
                            str += "卡片状态：卡在传感器2-3的位置\r\n";
                            getStateCode = 23;
                            break;
                        case 0x35:
                            str += "卡片状态：卡在传感器取卡位置\r\n";
                            getStateCode = 24;
                            break;
                        case 0x34:
                            str += "卡片状态：卡在传感器3位置\r\n";
                            getStateCode = 25;
                            break;
                        case 0x33:
                            str += "卡片状态：卡在传感器1-2位置(读卡位置)\r\n";
                            getStateCode = 26;
                            break;
                        case 0x32:
                            str += "卡片状态：卡在传感器2位置\r\n";
                            getStateCode = 27;
                            break;
                        case 0x31:
                            str += "卡片状态：卡在传感器1位置(取卡位置)\r\n";
                            getStateCode = 28;
                            break;
                        case 0x30:
                            str += "卡片状态：空闲\r\n";
                            getStateCode = 29;
                            break;
                    }
                    _Log.d(TAG,"getStateCode="+getStateCode);
                    _Log.d(TAG,str);
                }
            }catch (Exception e) {
                _Log.d(TAG,"执行传感器状态异常"+getStateCode);
                getStateCode = 30;
            }
        }else{
            _Log.d(TAG,"执行传感器状态异常"+getStateCode);
            getStateCode = 31;
        }
        return getStateCode;
    }

}
