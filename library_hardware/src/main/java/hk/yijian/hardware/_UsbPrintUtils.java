package hk.yijian.hardware;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.util.Log;
import android.util.SparseArray;

import com.printsdk.cmd.PrintCmd;
import com.printsdk.usbsdk.UsbDriver;

import hk.yijian.hardware.printerTest.Constant;
import hk.yijian.hardware.printerTest.L;
import hk.yijian.hardware.printerTest.Utils;


/**
 * Created by young on 2017/11/8.
 */
public class _UsbPrintUtils {

    //小票打印机 01
    private static final String TAG = "hardwarelib_USB_Print";
    private UsbManager mUsbManager;
    UsbDriver mUsbDriver;
    private static final String ACTION_USB_PERMISSION =  "com.usb.sample.USB_PERMISSION";
    UsbDevice mUsbDev1;		//打印机1
    UsbDevice mUsbDev2;		//打印机2
    private final static int PID11 = 8211;
    private final static int PID13 = 8213;
    private final static int PID15 = 8215;
    private final static int VENDORID = 1305;
    private int cutter = 0;       // 默认0，  0 全切、1 半切
    static int Number = 1000;
    public String title = "", strData = "", num = "",codeStr = "";
    String normal = "",notConnectedOrNotPopwer = "",notMatch = "",
            printerHeadOpen = "", cutterNotReset = "", printHeadOverheated = "",
            blackMarkError = "",paperExh = "",paperWillExh = "",abnormal = "";
    // 通过系统语言判断Message显示
    String receive = "", state = ""; // 接收提示、状态类型


    private _UsbPrintUtils() {
    }

    private static class _UsbPrintUtilsLoader {
        private static final _UsbPrintUtils INSTANCE = new _UsbPrintUtils();
    }

    public static _UsbPrintUtils getInstance() {
        return _UsbPrintUtilsLoader.INSTANCE;
    }


    public boolean InitUsbPrint(Context context){
        boolean usbDriver;
        try {
            mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            mUsbDriver = new UsbDriver(mUsbManager, context);
            PendingIntent permissionIntent1 = PendingIntent.getBroadcast(context, 0,
                    new Intent(ACTION_USB_PERMISSION), 0);
            mUsbDriver.setPermissionIntent(permissionIntent1);

            // Broadcast listen for new devices
            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_USB_PERMISSION);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
            context.registerReceiver(mUsbReceiver, filter);

            if (Utils.isZh(context)) {
                receive = Constant.Receive_CN;
                state = Constant.State_CN;
                normal = Constant.Normal_CN;
                notConnectedOrNotPopwer = Constant.NoConnectedOrNoOnPower_CN;
                notMatch = Constant.PrinterAndLibraryNotMatch_CN;
                printerHeadOpen = Constant.PrintHeadOpen_CN;
                cutterNotReset = Constant.CutterNotReset_CN;
                printHeadOverheated = Constant.PrintHeadOverheated_CN;
                blackMarkError = Constant.BlackMarkError_CN;
                paperExh = Constant.PaperExhausted_CN;
                paperWillExh = Constant.PaperWillExhausted_CN;
                abnormal = Constant.Abnormal_CN;
            } else {
                receive = Constant.Receive_US;
                state = Constant.State_US;
                normal = Constant.Normal_US;
                notConnectedOrNotPopwer = Constant.NoConnectedOrNoOnPower_US;
                notMatch = Constant.PrinterAndLibraryNotMatch_US;
                printerHeadOpen = Constant.PrintHeadOpen_US;
                cutterNotReset = Constant.CutterNotReset_US;
                printHeadOverheated = Constant.PrintHeadOverheated_US;
                blackMarkError = Constant.BlackMarkError_US;
                paperExh = Constant.PaperExhausted_US;
                paperWillExh = Constant.PaperWillExhausted_US;
                abnormal = Constant.Abnormal_US;
            }
            printConnStatus();
            usbDriver = true;
            L.d(TAG,"USB_Driver_初始化成功！");
        }catch (Exception e){
            L.d(TAG,"USB_Driver_初始化失败！");
            usbDriver = false;
        }
        return usbDriver;
    }

    /**
     * Get UsbDriver(UsbManager) service
     * @return
     */
    public boolean printConnStatus() {

        boolean blnRtn = false;

        try{
            if (!mUsbDriver.isConnected()) {
                // UsbManager m_UsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                for (UsbDevice device : mUsbManager.getDeviceList().values()) { // USB线已经连接
                    if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
                        if (!mUsbManager.hasPermission(device)) {
                            break;
                        }
                        blnRtn = mUsbDriver.usbAttached(device);
                        if (blnRtn == false) {
                            break;
                        }
                        blnRtn = mUsbDriver.openUsbDevice(device);
                        if (blnRtn) {// 打开设备
                            if (device.getProductId() == PID11) {
                                mUsbDev1 = device;
                            } else {
                                mUsbDev2 = device;
                            }
                            setClean();// 清理缓存，初始化
                            //T.showShort(context, "USB_Driver_Success");
                            L.d(TAG,"USB_Driver_Success");
                            break;
                        } else {
                            //T.showShort(context, "USB_Driver_Failed");
                            L.d(TAG,"USB_Driver_Failed");
                            break;
                        }
                    }
                }
            } else {
                blnRtn = true;
            }
        }catch (Exception e){
            blnRtn = false;
            L.d(TAG,"USB_Driver_Failed");
        }
        return blnRtn;
    }


    /**
     * 2.小票打印
     */
    public void getPrintTicketData(Context context,SparseArray<String> map) {
        UsbDevice usbDev = mUsbDev1;
        getStrDataByLanguage(context);
        int iStatus = getPrinterStatus();
        if(checkStatus(iStatus)!=0)
            return;
        try{
			// mUsbDriver.write(PrintCmd.SetClean(),usbDev);  // 初始化，清理缓存

            // 小票标题
            mUsbDriver.write(PrintCmd.SetBold(0),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(1),usbDev);
            mUsbDriver.write(PrintCmd.SetSizetext(1, 1),usbDev);
            mUsbDriver.write(PrintCmd.PrintString(map.get(1), 0),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(0),usbDev);
            mUsbDriver.write(PrintCmd.SetSizetext(0, 0),usbDev);
            // 小票号码
            mUsbDriver.write(PrintCmd.SetBold(1),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(1),usbDev);
            mUsbDriver.write(PrintCmd.SetSizetext(1, 1),usbDev);
            mUsbDriver.write(PrintCmd.PrintString(map.get(2), 0),usbDev);
            mUsbDriver.write(PrintCmd.SetBold(0),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(0),usbDev);
            mUsbDriver.write(PrintCmd.SetSizetext(0, 0),usbDev);
            // 小票主要内容
            mUsbDriver.write(PrintCmd.PrintString(map.get(3), 0),usbDev);
            mUsbDriver.write(PrintCmd.PrintFeedline(2),usbDev); // 打印走纸2行

             /*
            // 二维码
            mUsbDriver.write(PrintCmd.SetAlignment(1),usbDev);
            mUsbDriver.write(PrintCmd.PrintQrcode(codeStr, 25, 6, 1),usbDev);
            // 【1】MS-D347,13 52指令二维码接口，环绕模式1
			//  mUsbDriver.write(PrintCmd.PrintQrcode(codeStr, 12, 2, 0),usbDev);
            // 【2】MS-D245,MSP-100二维码，左边距、size、环绕模式0
			//  mUsbDriver.write(PrintCmd.PrintQrCodeT500II(5,Constant.WebAddress_zh),usbDev);
            // 【3】MS-532II+T500II二维码接口
            mUsbDriver.write(PrintCmd.PrintFeedline(2),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(0),usbDev);

            mUsbDriver.write(PrintCmd.PrintFeedline(2),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(0),usbDev);
            // 日期时间
            mUsbDriver.write(PrintCmd.SetAlignment(2),usbDev);
            mUsbDriver.write(PrintCmd.PrintString(sdf.format(new Date()).toString() + "\n\n", 1),usbDev);
            mUsbDriver.write(PrintCmd.SetAlignment(0),usbDev);
            // 一维条码
            mUsbDriver.write(PrintCmd.SetAlignment(1),usbDev);
            mUsbDriver.write(PrintCmd.Print1Dbar(2, 100, 0, 2, 10, "A12345678Z"),usbDev);
            // 一维条码打印
            mUsbDriver.write(PrintCmd.SetAlignment(0),usbDev);

             //打印图片
            if("".endsWith(imgPath)){_Log.i("","imgPath为空！");return;}
            Bitmap bm = Utils.getBitmapData(imgPath);
            if(bm == null){return;}
            int[] data = Utils.getPixelsByBitmap(bm);
            mUsbDriver.write(PrintCmd.PrintDiskImagefile(data,bm.getWidth()/2,bm.getHeight()/2));
            */

            // 走纸换行、切纸、清理缓存
            setFeedCut(cutter,usbDev);
        } catch(Exception e) {
            L.d(TAG,"USB_打印失败"+e.getMessage());
            //System.out.println(e.getMessage());
        }
    }

    /**
     *  检测打印机状态
     */
    public int getPrinterStatus() {
        UsbDevice usbDev = mUsbDev1;
        int iRet = -1;
        try {
            byte[] bRead1 = new byte[1];
            byte[] bWrite1 = PrintCmd.GetStatus1();
            if(mUsbDriver.read(bRead1,bWrite1,usbDev)>0) {
                iRet = PrintCmd.CheckStatus1(bRead1[0]);
            }
            if(iRet!=0)
                return iRet;

            byte[] bRead2 = new byte[1];
            byte[] bWrite2 = PrintCmd.GetStatus2();
            if(mUsbDriver.read(bRead2,bWrite2,usbDev)>0) {
                iRet = PrintCmd.CheckStatus2(bRead2[0]);
            }

            if(iRet!=0)
                return iRet;

            byte[] bRead3 = new byte[1];
            byte[] bWrite3 = PrintCmd.GetStatus3();
            if(mUsbDriver.read(bRead3,bWrite3,usbDev)>0) {
                iRet = PrintCmd.CheckStatus3(bRead3[0]);
            }

            if(iRet!=0)
                return iRet;

            byte[] bRead4 = new byte[1];
            byte[] bWrite4 = PrintCmd.GetStatus4();
            if(mUsbDriver.read(bRead4,bWrite4,usbDev)>0) {
                iRet = PrintCmd.CheckStatus4(bRead4[0]);
            }
        }catch (Exception e){
            iRet = -1;
        }
        return iRet;
    }


    /**
     *  0 打印机正常 、1 打印机未连接或未上电、
     *  2 打印机和调用库不匹配,3 打印头打开 、
     *  4 切刀未复位 、5 打印头过热 、
     *  6 黑标错误 、7 纸尽 、8 纸将尽
     * @param iStatus
     * @return
     */
    public int checkStatus(int iStatus) {
        int iRet;
        StringBuilder sMsg = new StringBuilder();
        switch (iStatus) {
            case 0:
                sMsg.append(normal);       // 正常
                iRet = 0;
                L.d(TAG,"USB_打印机正常");
                break;
            case 8:
                sMsg.append(paperWillExh); // 纸将尽
                L.d(TAG,"USB_纸将尽");
                iRet = 8;
                break;
            case 3:
                sMsg.append(printerHeadOpen); //打印头打开
                iRet = 3;
                break;
            case 4:
                sMsg.append(cutterNotReset);
                iRet = 4;
                break;
            case 5:
                sMsg.append(printHeadOverheated);
                iRet = 5;
                break;
            case 6:
                sMsg.append(blackMarkError);
                iRet = 6;
                break;
            case 7:
                sMsg.append(paperExh);     // 纸尽==缺纸
                L.d(TAG,"USB_缺纸");
                iRet = 7;
                break;
            case 1:
                sMsg.append(notConnectedOrNotPopwer);
                iRet = 1;
                break;
            default:
                sMsg.append(abnormal);     // 异常
                iRet = -1;
                L.d(TAG,"打印机_error"+sMsg.toString());
                break;
        }
        L.d(sMsg.toString());
        return iRet;
    }


    /**
     * 常规设置
     */
    public void setClean() {
        mUsbDriver.write(PrintCmd.SetClean());// 清除缓存,初始化
    }

    /**
     *  走纸换行、切纸、清理缓存
     */
    public void setFeedCut(int iMode,UsbDevice usbDev) {
        mUsbDriver.write(PrintCmd.PrintFeedline(5),usbDev);      // 走纸换行
        mUsbDriver.write(PrintCmd.PrintCutpaper(iMode),usbDev);  // 切纸类型
    }

    /**
     * 根据系统语言获取测试文本
     */
    public void getStrDataByLanguage(Context context){
        codeStr = "welcome";
        if("".equalsIgnoreCase(codeStr))
            codeStr = Constant.WebAddress;
        if(Utils.isZh(context)){
            title = Constant.TITLE_CN;
            strData = Constant.STRDATA_CN;
        }else {
            title = Constant.TITLE_US;
            strData = Constant.STRDATA_US;
        }
        num = String.valueOf(Number) + "\n\n";
        Number++;
    }

    /**
     *  BroadcastReceiver when insert/remove the device USB plug into/from a USB port
     *  BroadcastReceiver when insert/remove the device USB plug into/from a USB
     *  创建一个广播接收器接收USB插拔信息：当插入USB插头插到一个USB端口，或从一个USB端口，移除装置的USB插头
     */
    BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                try{
                    if(mUsbDriver.usbAttached(intent)) {
                        UsbDevice device = (UsbDevice) intent
                                .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                                || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                                || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
                            if(mUsbDriver.openUsbDevice(device)) {
                                if(device.getProductId()==PID11){
                                    mUsbDev1 = device;
                                    Log.d(TAG, "mUsbDev1" + mUsbDev1);
                                }else{
                                    mUsbDev2 = device;
                                    Log.d(TAG, "mUsbDev1" + mUsbDev1);
                                }

                            }
                        }
                    }
                }catch (Exception e){
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                try{
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
                        mUsbDriver.closeUsbDevice(device);
                        if(device.getProductId()==PID11){
                            mUsbDev1 = null;
                            Log.d(TAG, "mUsbDev1" + "null");
                        }else{
                            mUsbDev2 = null;
                            Log.d(TAG, "mUsbDev1" + "null");
                        }

                    }
                }catch (Exception e){}

            } else if (ACTION_USB_PERMISSION.equals(action)) {
                try{
                    synchronized (this) {
                        UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                                    || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                                    || (device.getProductId() == PID15 && device.getVendorId() == VENDORID))
                            {
                                if(mUsbDriver.openUsbDevice(device)) {
                                    if(device.getProductId()==PID11){
                                        mUsbDev1 = device;
                                        Log.d(TAG, "mUsbDev1" + mUsbDev1);
                                    } else{
                                        mUsbDev2 = device;
                                        Log.d(TAG, "mUsbDev2" + mUsbDev2);
                                    }
                                }
                            }
                        } else {
                            //T.showShort(this, "permission denied for device");
                            Log.d(TAG, "permission denied for device " + device);
                        }
                    }
                }catch (Exception e){}

            }
        }
    };

}
