package hk.yijian.hardware.idcardTest;






import java.io.UnsupportedEncodingException;

import com.hdos.usbdevice.publicSecurityIDCardLib;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import hk.yijian.hardwarelibrary.R;


public class IDcardActivityTest extends Activity {

	private IDcardActivityTest mact;
	private TextView ViewRe ; //结果显示区域
	private TextView viewBmp ; //显示头像区域
	private Button btReadBaseMsg ;//读基本信息按钮
	private publicSecurityIDCardLib iDCardDevice;
	private Button bt_close;

	private byte[] name = new byte[32];
	private byte[] sex = new byte[6];
	private byte[] birth = new byte[18];
	private byte[] nation = new byte[12];
	private byte[] address = new byte[72];
	private byte[] Department = new byte[32];
	private byte[] IDNo = new byte[38];
	private byte[] EffectDate = new byte[18];
	private byte[] ExpireDate = new byte[18];
	private byte [] BmpFile = new byte[38556];
	private String pkName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		if (Build.VERSION.SDK_INT >= 21) {
			View decorView = getWindow().getDecorView();
			int option = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
					| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
					| View.SYSTEM_UI_FLAG_IMMERSIVE;
			decorView.setSystemUiVisibility(option);
			//getWindow().setNavigationBarColor(Color.TRANSPARENT);
			//getWindow().setStatusBarColor(Color.TRANSPARENT);
		}
		setContentView(R.layout.activity_idcard_main);
		mact = this;
		pkName=this.getPackageName();
		iDCardDevice=new publicSecurityIDCardLib(this);
		ViewRe = findViewById(R.id.result);
		viewBmp = findViewById(R.id.bmp);
		btReadBaseMsg = findViewById(R.id.SDT_ReadBaseMsg);
		bt_close = findViewById(R.id.bt_close);
		setFinishOnTouchOutside(false);
		bt_close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

		//读基本信息
		btReadBaseMsg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try{

					int ret;
					String show = "";
					ViewRe.setText("");
					viewBmp.setText("");
					ret =iDCardDevice.readBaseMsgToStr(pkName,BmpFile,name,sex,nation, birth, address, IDNo, Department,
							EffectDate, ExpireDate);

					int[] colors =iDCardDevice.convertByteToColor(BmpFile);
					Bitmap b = Bitmap.createBitmap(colors, 102, 126,Config.ARGB_8888);
					ImageSpan imgSpan = new ImageSpan(b);
					SpannableString spanString = new SpannableString("icon");
					spanString.setSpan(imgSpan, 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

					if(ret==0x90) {
						try {
							show = "姓名:"+new String(name, "Unicode")+'\n'
									+"性别:"+new String(sex, "Unicode")+'\n'
									+"民族:"+ new String(nation, "Unicode")+"族"+'\n'
									+"出生日期:"+new String(birth, "Unicode")+'\n'
									+"住址:"+new String(address, "Unicode")+'\n'
									+"身份证号码:"+new String(IDNo, "Unicode")+'\n'
									+"签发机关:"+new String(Department, "Unicode")+'\n'
									+"有效日期:"+ new String(EffectDate, "Unicode") + "至" + new String(ExpireDate, "Unicode")+'\n';
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					else
						show ="读基本信息失败:"+String.format("0x%02x", ret);
					if(ret==0x90) viewBmp.setText(spanString);  			/*显示头像*/
					ViewRe.setText(show);
				}catch (Exception e){

				}
			}//end onclick()

		});
	}

}
