package hk.yijian.linkface.util;

import com.linkface.liveness.LFLivenessSDK;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import hk.yijian.linkface.data.LFConstants;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 **/
public class LivenessUtils {
    private static final String TAG = "LivenessUtils";

    /*
    * Return whether the system is root or not.
    */
    public static boolean isRootSystem() {
        int systemRootState = 0; // not root
        File f = null;
        final String kSuSearchPaths[] = {"/system/bin/", "/system/xbin/",
                "/system/sbin/", "/sbin/", "/vendor/bin/"};
        try {
            for (int i = 0; i < kSuSearchPaths.length; i++) {
                f = new File(kSuSearchPaths[i] + "su");
                if (f != null && f.exists()) {
                    systemRootState = 1; //root
                }
            }
        } catch (Exception e) {
        }
        return systemRootState == 1;
    }

    /**
     * 根据byte数组，生成文件
     */
    public static String saveFile(byte[] bfile, String filePath, String fileName) {
        String fileAbsolutePath = null;
        if (bfile == null) {
            return fileAbsolutePath;
        }
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        try {
            File dir = new File(filePath);
            if (!dir.exists() && dir.isDirectory()) {
                dir.mkdirs();
            }
            File file = new File(filePath + File.separator + fileName);
            fileAbsolutePath = file.getAbsolutePath();
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
            fileAbsolutePath = null;
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return fileAbsolutePath;
    }

    public static LFLivenessSDK.LFLivenessMotion[] getMctionOrder(String input) {
        String[] splitStrings = input.split("\\s+");
        LFLivenessSDK.LFLivenessMotion[] detectList = new LFLivenessSDK.LFLivenessMotion[splitStrings.length];
        for (int i = 0; i < splitStrings.length; i++) {
            if (splitStrings[i].equalsIgnoreCase(LFConstants.BLINK)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.BLINK;
            } else if (splitStrings[i].equalsIgnoreCase(LFConstants.NOD)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.NOD;
            } else if (splitStrings[i].equalsIgnoreCase(LFConstants.MOUTH)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.MOUTH;
            } else if (splitStrings[i].equalsIgnoreCase(LFConstants.YAW)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.YAW;
            }
        }
        return detectList;
    }

    public static String[] getDetectActionOrder(String input) {
        String[] splitStrings = input.split("\\s+");
        return splitStrings;
    }


    public static void deleteFiles(String folderPath) {
        File dir = new File(folderPath);
        if (dir == null || !dir.exists() || !dir.isDirectory() || dir.listFiles() == null)
            return;
        for (File file : dir.listFiles()) {
            if (file.isFile())
                file.delete();
        }
    }
}
