package hk.yijian.linkface.util;

import com.linkface.liveness.LFLivenessSDK;

import java.io.Serializable;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 */

public class LFReturnResult implements Serializable {
    private LFLivenessSDK.LFLivenessImageResult[] imageResults;
    private String videoResultPath;

    public LFLivenessSDK.LFLivenessImageResult[] getImageResults() {
        return imageResults;
    }

    public void setImageResults(LFLivenessSDK.LFLivenessImageResult[] imageResults) {
        this.imageResults = imageResults;
    }

    public String getVideoResultPath() {
        return videoResultPath;
    }

    public void setVideoResultPath(String videoResultPath) {
        this.videoResultPath = videoResultPath;
    }
}
