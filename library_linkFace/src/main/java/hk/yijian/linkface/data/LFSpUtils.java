package hk.yijian.linkface.data;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 */

public class LFSpUtils {
    public static final String LF_SP_LIVENESS = "lf_sp_liveness";

    public static final String LF_MUSIC_TIP_SWITCH = "lf_music_tips_switch";
    public static final String LF_USE_BACK_CAMERA = "lf_use_back_camera";
    public static final String LF_USE_RANDOM_SEQUENCE = "lf_use_random_sequence";
    public static final String LF_OUTPUT_TYPE = "lf_output_type";
    public static final String LF_ACTION_SEQUENCE = "lf_action_sequence";
    public static final String LF_DETECT_COMPLEXITY = "lf_detect_complexity";


    public static boolean getMusicTipSwitch(Context context) {
        SharedPreferences sharedPreferences = null;
        if (context != null) {
            sharedPreferences = context.getSharedPreferences(LF_SP_LIVENESS, Activity.MODE_PRIVATE);
        }
        return getBoolean(sharedPreferences, LF_MUSIC_TIP_SWITCH, true);
    }

    public static void saveMusicTipSwitch(Context context, boolean musicTipSwitch) {
        SharedPreferences sharedPreferences = null;
        if (context != null) {
            sharedPreferences = context.getSharedPreferences(LF_SP_LIVENESS, Activity.MODE_PRIVATE);
        }
        saveBoolean(sharedPreferences, LF_MUSIC_TIP_SWITCH, musicTipSwitch);
    }

    public static boolean getUseBackCamera(Context context) {
        SharedPreferences sharedPreferences = null;
        if (context != null) {
            sharedPreferences = context.getSharedPreferences(LF_SP_LIVENESS, Activity.MODE_PRIVATE);
        }
        return getBoolean(sharedPreferences, LF_USE_BACK_CAMERA, true);
    }

    public static void saveUseBackCamera(Context context, boolean useBackCamera) {
        SharedPreferences sharedPreferences = null;
        if (context != null) {
            sharedPreferences = context.getSharedPreferences(LF_SP_LIVENESS, Activity.MODE_PRIVATE);
        }
        saveBoolean(sharedPreferences, LF_USE_BACK_CAMERA, useBackCamera);
    }

    public static void saveUseRandomSequence(Context context, boolean useRandomSequence) {
        saveBoolean(getSharedPreferences(context), LF_USE_RANDOM_SEQUENCE, useRandomSequence);
    }

    public static boolean getUseRandomSequence(Context context) {
        return getBoolean(getSharedPreferences(context), LF_USE_RANDOM_SEQUENCE, false);
    }

    /**
     * 存储输出类型
     *
     * @param context
     * @param outputType
     */
    public static void saveOutputType(Context context, String outputType) {
        saveString(getSharedPreferences(context), LF_OUTPUT_TYPE, outputType);
    }

    public static String getOutputType(Context context, String defaultType) {
        return getString(getSharedPreferences(context), LF_OUTPUT_TYPE, defaultType);
    }

    /**
     * 存储动作序列
     *
     * @param context
     * @param actionSequence
     */
    public static void saveActionSequence(Context context, String actionSequence) {
        saveString(getSharedPreferences(context), LF_ACTION_SEQUENCE, actionSequence);
    }

    public static String getActionSequence(Context context) {
        return getString(getSharedPreferences(context), LF_ACTION_SEQUENCE, "");
    }

    /**
     * 存储难易程度
     *
     * @param context
     * @param complexity
     */
    public static void saveComplexity(Context context, String complexity) {
        saveString(getSharedPreferences(context), LF_DETECT_COMPLEXITY, complexity);
    }

    public static String getComplexity(Context context, String defaultComplexity) {
        return getString(getSharedPreferences(context), LF_DETECT_COMPLEXITY, defaultComplexity);
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = null;
        if (context != null) {
            sharedPreferences = context.getSharedPreferences(LF_SP_LIVENESS, Activity.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public static void saveBoolean(SharedPreferences sharedPreferences, String key, boolean value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putBoolean(key, value)
                    .commit();
        }
    }

    public static boolean getBoolean(SharedPreferences sharedPreferences, String key, boolean defaultValue) {
        boolean result = defaultValue;
        if (sharedPreferences != null) {
            result = sharedPreferences.getBoolean(key, defaultValue);
        }
        return result;
    }

    public static void saveString(SharedPreferences sharedPreferences, String key, String value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putString(key, value)
                    .commit();
        }
    }

    public static String getString(SharedPreferences sharedPreferences, String key, String defaultValue) {
        String result = defaultValue;
        if (sharedPreferences != null) {
            result = sharedPreferences.getString(key, defaultValue);
        }
        return result;
    }

}
