package hk.yijian.linkface.data;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 */

public class LFCommonUtils {
    /**
     * EXTRA_MOTION_SEQUENCE 动作检测序列配置，
     * 支持四种检测动作， BLINK(眨眼), MOUTH（张嘴）, NOD（点头）, YAW（摇头）, 各个动作以空格隔开。 推荐第一个动作为BLINK。
     * 默认配置为"BLINK MOUTH NOD YAW"
     */

    public static String getActionSequence(List<String> actionSequence) {
        StringBuilder builder = new StringBuilder();
        if (actionSequence != null) {
            for (String action : actionSequence) {
                builder.append(action)
                        .append(" ");
            }
        }
        return builder.toString();
    }

    /**
     * 和getActionSequence相反，根据生成的动作序列转换成list
     *
     * @param actionStr
     * @return
     */
    public static List<String> getActionList(String actionStr) {
        List<String> actionList = new ArrayList<>();
        if (!TextUtils.isEmpty(actionStr)) {
            String[] actionArr = actionStr.split(" ");
            actionList.addAll(Arrays.asList(actionArr));
        }
        return actionList;
    }
}
