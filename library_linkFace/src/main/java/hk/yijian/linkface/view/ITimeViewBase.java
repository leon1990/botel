package hk.yijian.linkface.view;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 **/
public interface ITimeViewBase {
    public void setProgress(float currentTime);

    public void hide();

    public void show();

    public int getMaxTime();
}