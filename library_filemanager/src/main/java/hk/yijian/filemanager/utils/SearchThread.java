package hk.yijian.filemanager.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

	public class SearchThread implements Callable<ArrayList<String>> {
		private File directory;
		private String  filename;
		public SearchThread(File directory, String  filename) {
			this.directory = directory;
			this.filename = filename;
		}
		/**
		 * callable线程
		 */
		@Override
		public ArrayList<String> call() throws Exception {
			// TODO Auto-generated method stub
			ArrayList<String> list = new ArrayList<String>();
			File[] files = directory.listFiles();
			ArrayList<Future<ArrayList<String>>> results = new ArrayList<Future<ArrayList<String>>>();
			if(files!=null){
				for (File f : files) {
					if (filename.equals(f.getName())) {
						list.add(f.getPath());
					}
					if (f.isDirectory()){
						Callable<ArrayList<String>> callable = new SearchThread(f, filename);
						FutureTask<ArrayList<String>> task = new FutureTask<ArrayList<String>>(callable);
						results.add(task);
						new Thread(task).start();	
					}  
				}
			}
			for (Future<ArrayList<String>> result : results) {
				list.addAll(result.get());
			}
			return list;
		}
		
	}

