package hk.yijian.filemanager.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Filel extends File{
	
	public Filel(String path) {
		super(path);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 复制选择
	 * @param destDir
	 * @param delscr
	 * @return
	 */
	public boolean copyTo(String destDir,boolean delscr){
		File file = new File(super.getPath());
		if(file.isDirectory())
			return copyDirectory(super.getPath(), destDir,delscr);
		else 
			return copyFile(super.getPath(), destDir,delscr);
		
	}
	/**
	 * 复制文件
	 * @param scrPath
	 * @param destDir
	 * @param delscr
	 * @return
	 */
	private  boolean copyFile(String scrPath, String destDir,boolean delscr) {
		boolean flag = false;
		File file = new File(scrPath);
		String filename =file.getName();
		String destPath ;
		if(destDir.equals("/")){
			destPath = destDir+filename;
		}
		else{
			destPath = destDir+"/"+filename;
		}
		File destFile = new File(destPath);
		if(destFile.exists()&&destFile.isFile()){
			System.out.println("目标目录下已有同名文件");
			return false;
		}
		File newfile = new File(destPath);
		try{
			FileInputStream fis = new FileInputStream(scrPath);
			FileOutputStream fos = new FileOutputStream(newfile);
			byte[]buf =new byte[1024];
			int c;
			while((c=fis.read(buf))!=-1){
				fos.write(buf,0,c);
			}
			fis.close();
			fos.close();
			flag=true;
		}catch(IOException e){
			
		}
		if(flag){
			System.out.println("复制成功");
		}
		if(delscr == true){
			file.delete();
		}
		return flag;
	}
	/**
	 * 复制文件夹
	 * @param scrPath
	 * @param destDir
	 * @param delscr
	 * @return
	 */
	private  boolean copyDirectory(String scrPath, String destDir,boolean delscr) {
		boolean flag = false;
		File scrFile = new File(scrPath);
		String dirName = scrFile.getName();
		String destPath ;
		if(destDir.equals("/")){
			destPath = destDir+dirName;
		}
		else{
			destPath = destDir+"/"+dirName;
		}
		File[] files = scrFile.listFiles();
		File desdir = new File(destPath);
		if(desdir.exists()&&desdir.isDirectory()){
			return false;
		}
		desdir.mkdir();
		for(File f:files){
			if(f.isDirectory()){
				copyDirectory(f.getPath(),desdir.getPath(),delscr);
			}
			if(f.isFile()){
				copyFile(f.getPath(),destPath,delscr);
				flag = true;
			}
			
		}
		if(delscr == true){
			scrFile.delete();
		}
		return flag;
	}
}
