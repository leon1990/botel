package hk.yijian.filemanager.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import hk.yijian.filemanagerlib.R;


public class FileAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Bitmap directory, file,text;
    //存储文件名称
    private ArrayList<String> names = null;
    //存储文件路径
    private ArrayList<String> paths = null;

    //参数初始化
    public FileAdapter(Context context, ArrayList<String> na, ArrayList<String> pa) {
        names = na;
        paths = pa;
        directory = BitmapFactory.decodeResource(context.getResources(), R.drawable.dir);
        file = BitmapFactory.decodeResource(context.getResources(), R.drawable.file);
        text = BitmapFactory.decodeResource(context.getResources(), R.drawable.text);
        
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return names.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
   
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;
        //如果缓存convertView为空，则需要创建View
        if (null == convertView) {
        	//根据自定义的Item布局加载布局
            convertView = inflater.inflate(R.layout.file, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.listitem);
            holder.image = (ImageView) convertView.findViewById(R.id.imageView);
           //将设置好的布局保存到缓存中，并将其设置在Tag里，以便后面方便取出Tag
            convertView.setTag(holder);
        } else {
        	//取出tag
            holder = (ViewHolder) convertView.getTag();
        }
        File f = new File(paths.get(position).toString());
        if (names.get(position).equals("rot")) {
            holder.text.setText("根目录");
            holder.image.setImageBitmap(directory);
        } else if (names.get(position).equals("last")) {
            holder.text.setText("上一级目录");
            holder.image.setImageBitmap(directory);
        } else {
            holder.text.setText(f.getName());
            if (f.isDirectory()) {
                holder.image.setImageBitmap(directory);
            } else if (f.isFile()) {
            	if(getMIMEType(f).equals("text")){
            		holder.image.setImageBitmap(text);
            	}else{
            		holder.image.setImageBitmap(file);
            	}
            } else {
                System.out.println(f.getName());
            }
        }
        return convertView;
    }
 /* 使用 ViewHolder 的关键好处是缓存了显示数据的视图（View），加快了 UI 的响应速度。
  	当我们判断 convertView == null  的时候，如果为空，就会根据设计好的List的Item布局（XML）
  	来为convertView赋值，并生成一个viewHolder来绑定converView里面的各个View控件（XML布局里面的那些控件）
  	再用convertView的setTag将viewHolder设置到Tag中，以便系统第二次绘制ListView时从Tag中取出。
          如果convertView不为空的时候，就会直接用convertView的getTag()，来获得一个ViewHolder*/
    private class ViewHolder {
        private TextView text;
        private ImageView image;
    }

    private String getMIMEType(File file) {
        String type = "";
        String name = file.getName();
        //文件扩展名
        String end = name.substring(name.lastIndexOf(".") + 1, name.length()).toLowerCase();
        if (end.equals("m4a") || end.equals("mp3") || end.equals("wav")) {
            type = "audio";
        } else if (end.equals("mp4") || end.equals("3gp")) {
            type = "video";
        } else if (end.equals("jpg") || end.equals("png") || end.equals("jpeg") || end.equals("bmp") || end.equals("gif")) {
            type = "image";
        } else if(end.equals("txt") || end.equals("java") || end.equals("ini") || end.equals("bat")){
        	type = "text";
        }
        else {
            //如果无法直接打开，跳出列表由用户选择
            type = "*";
        }
//        type += "/*";
        return type;
    }
}
