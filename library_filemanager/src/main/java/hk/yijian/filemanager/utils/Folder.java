package hk.yijian.filemanager.utils;

import java.io.File;
import java.text.DecimalFormat;

public class Folder {
	private String name;
	private String path;
	private int    dircount;
	private int    filecount;
	private long   size;
	
	/**
	 * Folder类构造函数
	 * @param path
	 */
	public Folder(String path){
		File file = new File(path);
		this.path = path;
		if(file.exists()){
			this.name = file.getName();
			File[] files = file.listFiles();
				 for (File f : files)
				 {
					 if(f.isDirectory())
					 {
						 this.dircount++;
					 }else if(f.isFile())
					 {
						 this.filecount++;
					 }else{
					 
				 	}
			 	}
			 }
		this.size=(getDirSize(path));
	}
	
	/**
	 * 获取文件大小
	 * @param file
	 * @return (long)size
	 */
	private  long getFileSize(File file){
		return file.length();	
	}
	/**
	 * 获取文件夹大小
	 * @param path
	 * @return (long)size
	 */
	private  long getDirSize(String path){
		long l=0;
		File file = new File(path);
		if(file.exists()){
			if(file.isDirectory())
			{
				File[] files = file.listFiles();
				if(files!=null)
				{
					for(File f: files)
					{
						l+=getDirSize(f.getPath());
					}
				}
			}else if(file.isFile())
			{
				l+=getFileSize(file);
			}
		}
		return l;
	}
	/**
	 * 
	 * @param fileS
	 * @return
	 */
	public  static String FormetFileSize(long fileS) {//转换文件大小
		DecimalFormat df = new DecimalFormat("#.00");
		String fileSizeString = "";
		if (fileS < 1024) 
		{
			fileSizeString = df.format((double) fileS) + 'B';
		} else if (fileS < 1048576) 
		{
			fileSizeString = df.format((double) fileS / 1024) + 'K';
		} else if (fileS < 1073741824) {
			fileSizeString = df.format((double) fileS / 1048576) + 'M';
		} else 
		{
			fileSizeString = df.format((double) fileS / 1073741824) + 'G';
		}
	return fileSizeString;
	}
	/**
	 * 
	 * @return boolean
	 */
	public boolean delete(){
		File file = new File(path);
		File[] files = file.listFiles();
		if(files.length!=0){
			for(File f : files){
				if(f.isDirectory()){
					new Folder(f.getPath()).delete();
				}else{
					f.delete();
				}
				file.delete();
			}
		}else
			file.delete();
		return true;
	}
	
	public String getName() {
		return name;
	}

	public String getPath() {
		return path;
	}

	public int getDircount() {
		return dircount;
	}
	public int getFilecount() {
		return filecount;
	}
	public long getSize() {
		return size;
	}
}
