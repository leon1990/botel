package hk.yijian.filemanager.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import hk.yijian.filemanager.utils.FileAdapter;
import hk.yijian.filemanager.utils.Filel;
import hk.yijian.filemanagerlib.R;

public class Copy extends ListActivity {
	private static final String ROOT_PATH = "/";
    private static String NOW_PATH;
	private ArrayList<String> FileName = null;
	private ArrayList<String> FilePath = null;
	private Button bt_copyto ;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		if (Build.VERSION.SDK_INT >= 21) {
			View decorView = getWindow().getDecorView();
			int option = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
					| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
					| View.SYSTEM_UI_FLAG_IMMERSIVE
					;
			decorView.setSystemUiVisibility(option);
			//getWindow().setNavigationBarColor(Color.TRANSPARENT);
			//getWindow().setStatusBarColor(Color.TRANSPARENT);
		}
		setContentView(R.layout.activity_copy);
		showFileDir(ROOT_PATH);
		final String soupath = getIntent().getStringExtra("soupath");
		final boolean delscr = getIntent().getBooleanExtra("isdel", false);
		bt_copyto = (Button)findViewById(R.id.bt_copyto);
		bt_copyto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Filel f = new Filel(soupath);
				boolean status = f.copyTo(NOW_PATH,delscr);

				if(status == true){
					displayToast("复制成功");
					finish();
				}
				else
					displayToast("复制失败,请检查是否重名");

			}
		});

	}
	/**
	 * 显示文件夹
	 * @param path
	 */
	private void showFileDir(String path) {
        FileName = new ArrayList<String>();
        FilePath = new ArrayList<String>();
        File file = new File(path);
        NOW_PATH = path;

        File[] files = file.listFiles();
        //如果当前目录不是根目录
        if (!ROOT_PATH.equals(path)) {
            FileName.add("rot");
            FilePath.add(ROOT_PATH);
            FileName.add("last");
            FilePath.add(file.getParent());
        }
        //添加所有文件
        for (File f : files) {
            FileName.add(f.getName());
            FilePath.add(f.getPath());
        }
        this.setListAdapter(new FileAdapter(this, FileName, FilePath));
    }
	/**
	 * 点击事件
	 */
	protected void onListItemClick(ListView l, View v, int position, long id) {
	        String path = FilePath.get(position);
	        File file = new File(path);
	        // 文件存在并可读
	        if (file.exists() && file.canRead()) {
	            if (file.isDirectory()) {
	                //显示子目录及文件
	                showFileDir(path);
	            } else {
	            	
	            }	
	        }
	        //没有权限
	        else {
	            Resources res = getResources();
	            new AlertDialog.Builder(this).setTitle("Message")
	                    .setMessage(res.getString(R.string.no_permission))
	                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            Toast.makeText(getApplicationContext(), "没有权限", Toast.LENGTH_SHORT).show();
	                        }
	                    }).show();
	        }
	        super.onListItemClick(l, v, position, id);
	    }
	/**
	 * 显示消息
	 * @param message
	 */
	private void displayToast(String message) {
	        Toast.makeText(Copy.this, message, Toast.LENGTH_SHORT).show();
	    }

}
