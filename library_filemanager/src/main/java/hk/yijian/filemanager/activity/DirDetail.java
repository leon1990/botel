package hk.yijian.filemanager.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import hk.yijian.filemanager.utils.Folder;
import hk.yijian.filemanagerlib.R;

public class DirDetail extends Activity {
	private TextView v_dirpath;
	private TextView v_dircount;
	private TextView v_filecount;
	private TextView v_size;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		if (Build.VERSION.SDK_INT >= 21) {
			View decorView = getWindow().getDecorView();
			int option = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
					| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
					| View.SYSTEM_UI_FLAG_IMMERSIVE
					;
			decorView.setSystemUiVisibility(option);
			//getWindow().setNavigationBarColor(Color.TRANSPARENT);
			//getWindow().setStatusBarColor(Color.TRANSPARENT);
		}
		setContentView(R.layout.activity_dir_detail);
		String path = getIntent().getStringExtra("path");

		Folder d = new Folder(path);

		v_dirpath =  (TextView)findViewById(R.id.dirpath);
		v_dircount=  (TextView)findViewById(R.id.dircount);
		v_filecount= (TextView)findViewById(R.id.filecount);
		v_size =	 (TextView)findViewById(R.id.size);

		v_dirpath.setText(path);
		v_size.setText(Folder.FormetFileSize(d.getSize()));
		v_dircount.setText(String.valueOf(d.getFilecount()));
		v_filecount.setText(String.valueOf(d.getFilecount()));

	}


}
