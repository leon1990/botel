package hk.yijian.filemanager.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import hk.yijian.filemanager.utils.DisplayUtil;
import hk.yijian.filemanagerlib.R;

public class Preview extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE
                    ;
            decorView.setSystemUiVisibility(option);
            //getWindow().setNavigationBarColor(Color.TRANSPARENT);
            //getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
		setContentView(R.layout.activity_preview);
		LinearLayout v_ptext = (LinearLayout)findViewById(R.id.ptext);
		String path = getIntent().getStringExtra("path");

		try {
	        File file=new File(path);

	        if(file.isFile() && file.exists()){ //判断文件是否存在
	          InputStreamReader read = new InputStreamReader(new FileInputStream(file));
	          BufferedReader bf = new BufferedReader(read);
	          String lineTxt = null;
	          int i=0;
	          while((lineTxt = bf.readLine()) != null&&i<100){
	        	  TextView w = new TextView(this);
	        	  LinearLayout itemMain = new LinearLayout(this);
	  	          itemMain.setOrientation(LinearLayout.HORIZONTAL);
	  	          LinearLayout.LayoutParams itemMainparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtil.dip2px(this, 30));  //设置宽与高
	  	          itemMainparams.setMargins(0,DisplayUtil.dip2px(this,1),0,0);    //设置每个item与上一个控件的间隔是1dip
	  	          itemMain.setLayoutParams(itemMainparams);
	  	          itemMain.setBackgroundColor(Color.WHITE);    //设置背景色
	  	          itemMain.setVerticalGravity(Gravity.CENTER);

	  	          LinearLayout.LayoutParams wparams = new LinearLayout.LayoutParams(DisplayUtil.dip2px(this, ViewGroup.LayoutParams.MATCH_PARENT), DisplayUtil.dip2px(this, ViewGroup.LayoutParams.MATCH_PARENT));
	  	          wparams.weight = 1;
	  	          w.setLayoutParams(wparams);
	        	  w.setText(i+lineTxt);
	        	  w.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
	        	  itemMain.addView(w);
	        	  v_ptext.addView(itemMain);
	        	  System.out.println(lineTxt);
	        	  i++;
	          }
	          read.close();
	    }else{
	      System.out.println("找不到指定的文件");
	    }
	    } catch (Exception e) {
	      System.out.println("读取文件内容出错");
	      e.printStackTrace();
	    }

	}
}
