package hk.yijian.tencentface.samples;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.exception.ClientException;
import com.tencent.faceid.exception.ServerException;
import com.tencent.faceid.model.GetLipLanguageRequest;
import com.tencent.faceid.model.GetLipLanguageResult;

/**
 * 获取唇语示例
 */
public class GetLipLanguage {

    GetLipLanguageRequest getLipLanguageRequest;
    FaceIdClient faceIdClient;

    String errorMsg;

    public GetLipLanguage(FaceIdClient faceIdClient, String bucket, String seq, String sign) {

        getLipLanguageRequest = new GetLipLanguageRequest(bucket, seq);
        getLipLanguageRequest.setSign(sign);
        this.faceIdClient = faceIdClient;
        errorMsg = "";
    }



    public GetLipLanguageResult send() {

        GetLipLanguageResult getLipLanguageResult = null;

        try {
            getLipLanguageResult = faceIdClient.getLipLanguage(getLipLanguageRequest);
        } catch (ClientException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        } catch (ServerException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        }

        return getLipLanguageResult;
    }

    public boolean cancel() {

        return faceIdClient.cancel(getLipLanguageRequest.getRequestId());
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
