package hk.yijian.tencentface.samples;

import android.text.TextUtils;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.exception.ClientException;
import com.tencent.faceid.exception.ServerException;
import com.tencent.faceid.model.ImageIdCardCompareRequest;
import com.tencent.faceid.model.ImageIdCardCompareResult;

import java.io.File;

/**
 *
 */
public class ImageIdCompare {

    ImageIdCardCompareRequest imageIdCardCompareRequest;
    FaceIdClient faceIdClient;

    String errorMsg;

    public ImageIdCompare(FaceIdClient faceIdClient, String bucket, String idCardNumber, String idCardName,
                                     String imagePath, String imageUrl, String seq, String sign) {

        if (!TextUtils.isEmpty(imageUrl)) {

            imageIdCardCompareRequest = new ImageIdCardCompareRequest(bucket, idCardNumber, idCardName, imageUrl, seq);
        } else if (!TextUtils.isEmpty(imagePath)) {
            imageIdCardCompareRequest = new ImageIdCardCompareRequest(bucket, idCardNumber, idCardName, new File(imagePath), seq);
        }
        imageIdCardCompareRequest.setSign(sign);
        this.faceIdClient = faceIdClient;
        errorMsg = "";
    }



    public ImageIdCardCompareResult send() {

        ImageIdCardCompareResult imageIdCardCompareResult = null;

        try {
            imageIdCardCompareResult = faceIdClient.imageIdCardCompare(imageIdCardCompareRequest);
        } catch (ClientException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        } catch (ServerException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        }

        return imageIdCardCompareResult;
    }

    public boolean cancel() {

        return faceIdClient.cancel(imageIdCardCompareRequest.getRequestId());
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
