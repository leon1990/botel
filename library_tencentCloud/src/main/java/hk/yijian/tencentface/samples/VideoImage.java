package hk.yijian.tencentface.samples;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.exception.ClientException;
import com.tencent.faceid.exception.ServerException;
import com.tencent.faceid.model.VideoImageIdentityRequest;
import com.tencent.faceid.model.VideoImageIdentityResult;

/**
 *
 */
public class VideoImage {


    VideoImageIdentityRequest videoImageIdentityRequest;
    FaceIdClient faceIdClient;

    String errorMsg;

    public VideoImage(FaceIdClient faceIdClient, String bucket, String validateData, String videoPath,
                   String imagePath, boolean compare, String seq, String sign) {

        videoImageIdentityRequest = new VideoImageIdentityRequest(bucket, validateData, videoPath,
                imagePath, compare, seq);
        videoImageIdentityRequest.setSign(sign);
        this.faceIdClient = faceIdClient;
        errorMsg = "";
    }



    public VideoImageIdentityResult send() {

        VideoImageIdentityResult videoImageIdentityResult = null;
        try {
            videoImageIdentityResult = faceIdClient.videoImageIdentity(videoImageIdentityRequest);
        } catch (ClientException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        } catch (ServerException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        }catch (Exception e) {
            e.printStackTrace();
            errorMsg = e.toString();
        }
        return videoImageIdentityResult;
    }

    public boolean cancel() {

        return faceIdClient.cancel(videoImageIdentityRequest.getRequestId());
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
