package hk.yijian.tencentface.samples;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.exception.ClientException;
import com.tencent.faceid.exception.ServerException;
import com.tencent.faceid.model.VideoIdCardIdentityRequest;
import com.tencent.faceid.model.VideoIdCardIdentityResult;

/**
 *
 */
public class VideoId {

    VideoIdCardIdentityRequest videoIdCardIdentityRequest;
    FaceIdClient faceIdClient;

    String errorMsg;

    public VideoId(FaceIdClient faceIdClient, String bucket, String validateData, String videoPath,
                   String idCardNumber, String idCardName, String seq, String sign) {

        videoIdCardIdentityRequest = new VideoIdCardIdentityRequest(bucket, validateData, videoPath,
                idCardNumber, idCardName, seq);
        videoIdCardIdentityRequest.setSign(sign);
        this.faceIdClient = faceIdClient;
        errorMsg = "";
    }

    public VideoIdCardIdentityResult send() {

        VideoIdCardIdentityResult videoIdCardIdentityResult = null;
        try {
            videoIdCardIdentityResult = faceIdClient.videoIdCardIdentity(videoIdCardIdentityRequest);
        } catch (ClientException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        } catch (ServerException e) {
            e.printStackTrace();
            errorMsg = e.toString();
        }


        return videoIdCardIdentityResult;
    }

    public boolean cancel() {

        return faceIdClient.cancel(videoIdCardIdentityRequest.getRequestId());
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}


