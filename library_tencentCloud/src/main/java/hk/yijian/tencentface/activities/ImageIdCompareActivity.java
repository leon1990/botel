package hk.yijian.tencentface.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.log.AAILogger;
import com.tencent.faceid.model.ImageIdCardCompareResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hk.yijian.tencentface.common.FaceIdHelper;
import hk.yijian.tencentface.common.SignHelper;
import hk.yijian.tencentfacelibrary.R;
import hk.yijian.tencentface.common.Utils;
import hk.yijian.tencentface.samples.ImageIdCompare;


/**
 * 图片和身份证对比
 */
public class ImageIdCompareActivity  extends AppCompatActivity {

    Logger logger = LoggerFactory.getLogger(ImageIdCompareActivity.class);

    TextView textView;

    EditText idCardNumber;

    EditText idCardName;

    EditText url;

    EditText seq;

    Button chooseImage;

    Button send;

    Button cancel;

    FaceIdClient faceIdClient;

    Handler handler;

    String imagePath;

    final int FILE_SELECT_REQUEST_CODE = 1;

    ImageIdCompare imageIdCompare;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_id);

        handler = new Handler(getMainLooper());

        FaceIdHelper faceIdHelper = FaceIdHelper.getInstance(this);

        final String bucket = faceIdHelper.getBucket();

        // 正式业务下请在第三方服务器上获取签名
        final int duration = 3600;
        final String sign = new SignHelper().getSign(bucket, duration);

        faceIdClient = faceIdHelper.getFaceIdClient();

        textView = (TextView) findViewById(R.id.text_image_id);

        idCardNumber = (EditText) findViewById(R.id.number_image_id);
        idCardName = (EditText) findViewById(R.id.name_image_id);
        url = (EditText) findViewById(R.id.url_image_id);
        seq = (EditText) findViewById(R.id.seq_image_id);
        chooseImage = (Button) findViewById(R.id.choose_image_id);
        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(Intent.createChooser(intent,
                            ImageIdCompareActivity.this.getString(R.string.choose_image)),
                            FILE_SELECT_REQUEST_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    // Potentially direct the user to the Market with a Dialog
                    Toast.makeText(ImageIdCompareActivity.this,
                            ImageIdCompareActivity.this.getString(R.string.file_manager),
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
        send = (Button) findViewById(R.id.button_send_image_id);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String numberString = idCardNumber.getText().toString();
                final String nameString = idCardName.getText().toString();
                if (TextUtils.isEmpty(nameString) || TextUtils.isEmpty(numberString)) {
                    Toast.makeText(ImageIdCompareActivity.this,
                            ImageIdCompareActivity.this.getString(R.string.write_id_first), Toast.LENGTH_SHORT).show();
                    return ;
                }
                final String urlString = url.getText().toString();
                final String seqString = seq.getText().toString();

                if (TextUtils.isEmpty(urlString) && TextUtils.isEmpty(imagePath)) {
                    Toast.makeText(ImageIdCompareActivity.this,
                            ImageIdCompareActivity.this.getString(R.string.choose_image_url_first),
                            Toast.LENGTH_SHORT).show();
                    return ;
                }

                imageIdCompare = new ImageIdCompare(faceIdClient, bucket, numberString,
                        nameString, imagePath, urlString, seqString, sign);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final ImageIdCardCompareResult imageIdCardCompareResult = imageIdCompare.send();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (imageIdCardCompareResult!=null){
                                    textView.setText(imageIdCardCompareResult.toString());
                                } else if (!TextUtils.isEmpty(imageIdCompare.getErrorMsg())){
                                    textView.setText(imageIdCompare.getErrorMsg());
                                }
                            }
                        });
                    }
                }).start();

            }
        });

        cancel = (Button) findViewById(R.id.button_cancel_image_id);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imageIdCompare!=null && imageIdCompare.cancel()) {
                            textView.setText(R.string.cancel_success);
                        } else {
                            textView.setText(R.string.cancel_failed);
                        }
                    }
                });
            }
        });

    }

    //Logger logger = LoggerFactory.getLogger(ImageIdCompareActivity.class);

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK || data == null) {

            return;
        }

        switch (requestCode) {

            case FILE_SELECT_REQUEST_CODE:

                onFileSelectActivityResult(data);
                break;
        }
    }

    public void onFileSelectActivityResult(Intent data) {
        try {
            Uri uri = data.getData();
            imagePath = Utils.getPath(this, uri);
            AAILogger.info(logger, "image path = "+imagePath);
            textView.setText(imagePath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
