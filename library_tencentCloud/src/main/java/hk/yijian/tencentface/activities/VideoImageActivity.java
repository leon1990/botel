package hk.yijian.tencentface.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.log.AAILogger;
import com.tencent.faceid.model.VideoImageIdentityResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hk.yijian.tencentfacelibrary.R;
import hk.yijian.tencentface.common.FaceIdHelper;
import hk.yijian.tencentface.common.SignHelper;
import hk.yijian.tencentface.common.Utils;
import hk.yijian.tencentface.samples.VideoImage;

/**
 * 视频和图像身份鉴定
 */
public class VideoImageActivity extends AppCompatActivity {

    Logger logger = LoggerFactory.getLogger(ImageIdCompareActivity.class);

    TextView textView;

    EditText lip;

    EditText seq;

    Button chooseImage;

    Button chooseVideo;

    Button send;

    Button cancel;

    FaceIdClient faceIdClient;

    Handler handler;

    String imagePath;

    String videoPath;

    final int IMAGE_SELECT_REQUEST_CODE = 1;
    final int VIDEO_SELECT_REQUEST_CODE = 2;

    VideoImage videoImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_image);

        handler = new Handler(getMainLooper());

        FaceIdHelper faceIdHelper = FaceIdHelper.getInstance(this);

        final String bucket = faceIdHelper.getBucket();

        // 正式业务下请在第三方服务器上获取签名
        final int duration = 3600;
        final String sign = new SignHelper().getSign(bucket, duration);

        faceIdClient = faceIdHelper.getFaceIdClient();


        textView = (TextView) findViewById(R.id.text_video_image);

        lip = (EditText) findViewById(R.id.lip_video_image);

        seq = (EditText) findViewById(R.id.seq_video_image);

        chooseImage = (Button) findViewById(R.id.choose_image_video_image);
        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(Intent.createChooser(intent, VideoImageActivity.this.getString(R.string.choose_image)),
                            IMAGE_SELECT_REQUEST_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    // Potentially direct the user to the Market with a Dialog
                    Toast.makeText(VideoImageActivity.this,
                            VideoImageActivity.this.getString(R.string.file_manager),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        chooseVideo = (Button) findViewById(R.id.choose_video_image);
        chooseVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(Intent.createChooser(intent, VideoImageActivity.this.getString(R.string.choose_video)),
                            VIDEO_SELECT_REQUEST_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    // Potentially direct the user to the Market with a Dialog
                    Toast.makeText(VideoImageActivity.this,
                            VideoImageActivity.this.getString(R.string.file_manager),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        send = (Button) findViewById(R.id.button_send_video_image);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String lipString = lip.getText().toString();
                if (TextUtils.isEmpty(lipString)) {
                    Toast.makeText(VideoImageActivity.this, VideoImageActivity.this.getString(R.string.write_lip_first),
                            Toast.LENGTH_SHORT).show();
                    return ;
                }
                if (TextUtils.isEmpty(videoPath)) {
                    Toast.makeText(VideoImageActivity.this, VideoImageActivity.this.getString(R.string.choose_video_first),
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                String seqString = seq.getText().toString();
                if (TextUtils.isEmpty(imagePath)) {
                    videoImage = new VideoImage(faceIdClient, bucket, lipString, videoPath, imagePath, false, seqString, sign);
                } else {
                    videoImage = new VideoImage(faceIdClient, bucket, lipString, videoPath, imagePath, true, seqString, sign);
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final VideoImageIdentityResult videoImageIdentityResult = videoImage.send();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (videoImageIdentityResult != null) {
                                    textView.setText(videoImageIdentityResult.toString());
                                } else if (!TextUtils.isEmpty(videoImage.getErrorMsg())){
                                    textView.setText(videoImage.getErrorMsg());
                                }
                            }
                        });
                    }
                }).start();
            }
        });

        cancel = (Button) findViewById(R.id.button_cancel_video_image);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                    if (videoImage!=null && videoImage.cancel()) {
                        textView.setText(R.string.cancel_success);
                    } else {
                        textView.setText(R.string.cancel_failed);
                    }
                    }
                });
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK || data == null) {

            return;
        }

        switch (requestCode) {

            case IMAGE_SELECT_REQUEST_CODE:

                onImageSelectActivityResult(data);
                break;
            case VIDEO_SELECT_REQUEST_CODE:
                onVideoSelectActivityResult(data);
                break;

        }
    }

    public void onImageSelectActivityResult(Intent data) {
        try {
            Uri uri = data.getData();
            imagePath = Utils.getPath(this, uri);
            AAILogger.info(logger, "image path = "+imagePath);
            textView.setText(imagePath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onVideoSelectActivityResult(Intent data) {
        try {
            Uri uri = data.getData();
            videoPath = Utils.getPath(this, uri);
            AAILogger.info(logger, "video path = "+videoPath);
            textView.setText(videoPath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
