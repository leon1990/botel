package hk.yijian.tencentface.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tencent.faceid.FaceIdClient;
import com.tencent.faceid.model.GetLipLanguageResult;

import hk.yijian.tencentface.common.FaceIdHelper;
import hk.yijian.tencentface.common.SignHelper;
import hk.yijian.tencentfacelibrary.R;
import hk.yijian.tencentface.samples.GetLipLanguage;


/**
 * 获取唇语
 */
public class GetLipLanguageActivity extends AppCompatActivity {

    TextView textView;

    EditText seq;

    Button send;

    Button cancel;

    FaceIdClient faceIdClient;

    Handler handler;

    GetLipLanguage getLipLanguage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_lip_language);

        handler = new Handler(getMainLooper());

        FaceIdHelper faceIdHelper = FaceIdHelper.getInstance(this);

        final String bucket = faceIdHelper.getBucket();

        // 正式业务下请在第三方服务器上获取签名
        final int duration = 3600;
        final String sign = new SignHelper().getSign(bucket, duration);

        faceIdClient = faceIdHelper.getFaceIdClient();

        textView = (TextView) findViewById(R.id.text_get_lip);
        seq = (EditText) findViewById(R.id.seq_get_lip);
        send = (Button) findViewById(R.id.button_send_get_lip);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String seqString = seq.getText().toString();
                getLipLanguage = new GetLipLanguage(faceIdClient, bucket, seqString, sign);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final GetLipLanguageResult getLipLanguageResult = getLipLanguage.send();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (getLipLanguageResult!=null) {
                                    textView.setText(getLipLanguageResult.toString());
                                } else if (!TextUtils.isEmpty(getLipLanguage.getErrorMsg())){
                                    textView.setText(getLipLanguage.getErrorMsg());
                                }
                            }
                        });
                    }
                }).start();
            }
        });

        cancel = (Button) findViewById(R.id.button_cancel_get_lip);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (getLipLanguage!=null && getLipLanguage.cancel()) {
                            textView.setText(R.string.cancel_success);
                        } else {

                            textView.setText(R.string.cancel_failed);
                        }
                    }
                });
            }
        });
    }

}
