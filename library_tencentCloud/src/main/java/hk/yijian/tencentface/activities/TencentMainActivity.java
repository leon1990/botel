package hk.yijian.tencentface.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tencent.faceid.config.ClientConfiguration;
import com.tencent.faceid.log.AAILogger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hk.yijian.tencentfacelibrary.R;


public class TencentMainActivity extends AppCompatActivity {

    Logger logger = LoggerFactory.getLogger(TencentMainActivity.class);

    static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;

    Button sendGetLipLanguageRequest;
    Button sendImageCardCompareRequest;
    Button sendVideoImageIdentityRequest;
    Button sendVideoIdCardIdentityRequest;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_centant_main);

        // 用户设置
        ClientConfiguration.setMaxTaskConcurrentNumber(5); // 最大并发任务执行数

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            checkPermission();
        }

        sendGetLipLanguageRequest = (Button) findViewById(R.id.send_get_lip_language_request);
        sendImageCardCompareRequest = (Button) findViewById(R.id.send_image_card_compare_request);
        sendVideoImageIdentityRequest = (Button) findViewById(R.id.send_video_image_compare_request);
        sendVideoIdCardIdentityRequest = (Button) findViewById(R.id.send_video_id_card_compare_request);

        sendGetLipLanguageRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TencentMainActivity.this, GetLipLanguageActivity.class);
                startActivity(intent);
            }
        });

        sendImageCardCompareRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TencentMainActivity.this, ImageIdCompareActivity.class);
                startActivity(intent);
            }
        });

        sendVideoImageIdentityRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                Intent intent = new Intent(TencentMainActivity.this, VideoImageActivity.class);
                startActivity(intent);
            }
        });

        sendVideoIdCardIdentityRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TencentMainActivity.this, VideoIdActivity.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                return;
            }
        }
    }

    private void checkPermission() {

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        AAILogger.info(logger, "on create()");
        if (permissionCheck== PackageManager.PERMISSION_GRANTED) {

        } else if (permissionCheck==PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }
    }
}
