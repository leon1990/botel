package hk.yijian.tencentface.common;

/**
 * Created by Administrator on 2016/11/23.
 */
public class UserInfo {

    /*
       cos.appid = 1254386949
       cos.secretid = AKIDlwnpnmw5LQIBzbbXzJRPCnRsJcljt8Ym
       cos.secretkey = lfOExUp5DmCTXm1690ueDlCSguB6raGk
       cos.region = ap-guangzhou
       cos.bucket.name.img = imgdev
    */

    /**
     * 请替换为自己的appid
     */
    private static final String appid = "1254386949";

    /**
     * 请替换为自己的bucket
     */
    private static final String bucket = "imgdev";

    /**
     * 请替换为自己的secretId
     */
    private static final String secretId = "AKIDlwnpnmw5LQIBzbbXzJRPCnRsJcljt8Ym";

    /**
     * 请替换为自己的secretKey
     */
    private static final String secretKey = "lfOExUp5DmCTXm1690ueDlCSguB6raGk"; // 正式业务下请不要暴露secretKey



    public static String getAppid() {
        return appid;
    }

    public static String getBucket() {
        return bucket;
    }

    public static String getSecretId() {
        return secretId;
    }

    public static String getSecretKey() {
        return secretKey;
    }
}
