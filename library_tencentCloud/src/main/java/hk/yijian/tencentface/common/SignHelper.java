package hk.yijian.tencentface.common;

import com.tencent.faceid.auth.CredentialProvider;

/**
 * 正式环境下请不要在本地进行签名
 */
public class SignHelper {

    String secretKey;

    CredentialProvider credentialProvider;

    public SignHelper() {

        secretKey = UserInfo.getSecretKey();
        credentialProvider = new CredentialProvider(UserInfo.getAppid(), UserInfo.getSecretId(), secretKey);
    }

    public String getSign(String bucket, long duration) {

        return credentialProvider.getMultipleSign(bucket, duration);
    }
}
