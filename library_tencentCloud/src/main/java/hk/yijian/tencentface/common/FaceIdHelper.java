package hk.yijian.tencentface.common;

import android.content.Context;

import com.tencent.faceid.FaceIdClient;

public class FaceIdHelper {

    FaceIdClient faceIdClient;

    String appid;

    String bucket;

    String secretId;


    public static FaceIdHelper getInstance(Context context) {

        if (faceIdHelper==null) {
            synchronized (object) {
                if (faceIdHelper==null) {
                    faceIdHelper = new FaceIdHelper(context, UserInfo.getAppid(), UserInfo.getBucket(),
                            UserInfo.getSecretId());
                }
            }
        }
        return faceIdHelper;
    }

    private static Object object = new Object();

    private static FaceIdHelper faceIdHelper;

    private FaceIdHelper(Context context, String appid, String bucket, String secretId) {

        faceIdClient = new FaceIdClient(context, appid);
        this.bucket = bucket;
        this.secretId = secretId;
        this.appid = appid;
    }


    public String getSecretId() {
        return secretId;
    }

    public String getBucket() {
        return bucket;
    }

    public FaceIdClient getFaceIdClient() {
        return faceIdClient;
    }

    public String getAppid() {
        return appid;
    }
}
