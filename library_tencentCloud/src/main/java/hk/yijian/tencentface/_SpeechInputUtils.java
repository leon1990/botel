package hk.yijian.tencentface;


import android.content.Context;

import com.tencent.aai.AAIClient;
import com.tencent.aai.audio.data.AudioRecordDataSource;
import com.tencent.aai.auth.AbsCredentialProvider;
import com.tencent.aai.auth.LocalCredentialProvider;
import com.tencent.aai.exception.ClientException;
import com.tencent.aai.exception.ServerException;
import com.tencent.aai.listener.AudioRecognizeResultListener;
import com.tencent.aai.model.AudioRecognizeRequest;
import com.tencent.aai.model.AudioRecognizeResult;

/**
 * Created by lmh on 2017/11/8.
 */

public class _SpeechInputUtils {

    private int appid = 0;
    private int projectid = 0;
    private String secretId = "XXX";

    private AAIClient aaiClient;
    private AudioRecognizeRequest audioRecognizeRequest;

    private _SpeechInputUtils() {
    }

    private static class _SpeechUtilsLoader {
        private static final _SpeechInputUtils INSTANCE = new _SpeechInputUtils();
    }

    public static _SpeechInputUtils getInstance() {
        return _SpeechInputUtils._SpeechUtilsLoader.INSTANCE;
    }

    public void openSpeech(Context context) {
        // 为了方便用户测试，sdk提供了本地签名，但是为了secretKey的安全性，正式环境下请自行在第三方服务器上生成签名。
        AbsCredentialProvider credentialProvider = new LocalCredentialProvider("your secretKey");
        try {
            // 1、初始化AAIClient对象。
            aaiClient = new AAIClient(context, appid, projectid, secretId, credentialProvider);

            // 2、初始化语音识别请求。
            audioRecognizeRequest = new AudioRecognizeRequest.Builder()
                    .pcmAudioDataSource(new AudioRecordDataSource()) // 设置语音源为麦克风输入
                    .build();

            // 3、初始化语音识别结果监听器。
            final AudioRecognizeResultListener audioRecognizeResultListener = new AudioRecognizeResultListener() {
                @Override
                public void onSliceSuccess(AudioRecognizeRequest audioRecognizeRequest, AudioRecognizeResult audioRecognizeResult, int i) {
                    // 返回语音分片的识别结果
                }

                @Override
                public void onSegmentSuccess(AudioRecognizeRequest audioRecognizeRequest, AudioRecognizeResult audioRecognizeResult, int i) {
                    // 返回语音流的识别结果
                }

                @Override
                public void onSuccess(AudioRecognizeRequest audioRecognizeRequest, String s) {
                    // 返回所有的识别结果
                }

                @Override
                public void onFailure(AudioRecognizeRequest audioRecognizeRequest, ClientException e, ServerException e1) {
                    // 识别失败
                }
            };

            // 4、启动语音识别
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (aaiClient!=null) {
                        aaiClient.startAudioRecognize(audioRecognizeRequest, audioRecognizeResultListener);
                    }
                }
            }).start();

        } catch (ClientException e) {
            e.printStackTrace();
        }

    }

    public void closeSpeech() {
        // 1、获得请求的id
        final int requestId = audioRecognizeRequest.getRequestId();
        // 2、调用stop方法
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (aaiClient!=null){
                    aaiClient.stopAudioRecognize(requestId);
                }
            }
        }).start();
    }


    public void cancelSpeech() {
        // 1、获得请求的id
        final int requestId = audioRecognizeRequest.getRequestId();
        // 2、调用cancel方法
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (aaiClient!=null){
                    aaiClient.cancelAudioRecognize(requestId);
                }
            }
        }).start();
    }

}
