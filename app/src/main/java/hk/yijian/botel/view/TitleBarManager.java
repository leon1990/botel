package hk.yijian.botel.view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;
import hk.yijian.botel.R;
import hk.yijian.botel.constant._HandlerCode;

/**
 * 控制标题容器内容的切换
 *
 * @author Administrator
 */
public class TitleBarManager {

    private TextView tv_week_hour;
    private TextView tv_month_year;
    private TextView tv_title_name;
    private LinearLayout ll_time_picture;
    private FrameLayout ll_title_text;
    private LinearLayout ll_text_signal;
    private LinearLayout ll_time_table;
    private TextView iv_user_name;
    private ImageView iv_user_img;

    private SharedPreferences mShUserData;
    private int i = 0;

    //Handler
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.IS_LOGIN_USER:
                    IsLogin();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @SuppressLint("StaticFieldLeak")
    private static TitleBarManager instance = new TitleBarManager();


    private TitleBarManager() {
    }

    public static TitleBarManager getInstance() {
        return instance;
    }

    /**
     * 初始化
     */
    public void init(Activity activity) {
        mShUserData = activity.getSharedPreferences("userdata", Context.MODE_PRIVATE);
        ll_time_picture = activity.findViewById(R.id.ll_time_picture);
        ll_title_text = activity.findViewById(R.id.ll_title_text);
        ll_text_signal = activity.findViewById(R.id.ll_text_signal);
        ll_time_table = activity.findViewById(R.id.ll_time_table);
        tv_week_hour = activity.findViewById(R.id.tv_week_hour);
        tv_month_year = activity.findViewById(R.id.tv_month_year);
        tv_title_name = activity.findViewById(R.id.tv_title_name);
        iv_user_name = activity.findViewById(R.id.iv_user_name);
        iv_user_img = activity.findViewById(R.id.iv_user_img);
        IsLogin();
        setWeekHour();
        setYearMonth();
    }


    @SuppressLint("SetTextI18n")
    public void NoLogin(){
        iv_user_img.setBackgroundResource(R.mipmap.user_img);
        iv_user_name.setText("未登录");
    }

    @SuppressLint("SetTextI18n")
    public void IsLogin(){
       try{
           String mobile = mShUserData.getString("mobile", "");
           boolean login = mShUserData.getBoolean("login", false);
           if(login){
               iv_user_img.setBackgroundResource(R.mipmap.user_img);
               String mMobile = mobile.substring(0,3) + "*****" + mobile.substring(8,11);
               iv_user_name.setText(mMobile);
               i = 0;
           }else {
               iv_user_img.setBackgroundResource(R.mipmap.user_img);
               iv_user_name.setText("未登录"+mobile);
               i = i+1;
               if(i < 5){
                   Log.d("mHandler","再来一次！");
                   mHandler.removeMessages(_HandlerCode.IS_LOGIN_USER);
                   mHandler.sendEmptyMessageDelayed(_HandlerCode.IS_LOGIN_USER,300);
               }
           }

       }catch (Exception e){}
    }


    /**
     * 设置Title名
     * @param str
     */
    public void changeTitleText(String str) {
        tv_title_name.setText(str);
    }

    /**
     * 设置月日
     */
    @TargetApi(Build.VERSION_CODES.N)
    private void setYearMonth() {
        @SuppressLint("SimpleDateFormat")
        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
        SimpleDateFormat formatter = new SimpleDateFormat("MM月dd日");
        Date curDate = new Date(System.currentTimeMillis());
        String month_year_str = formatter.format(curDate);
        tv_month_year.setText(month_year_str);
    }

    /**
     * 设置周几和小时
     */
    @TargetApi(Build.VERSION_CODES.N)
    private void setWeekHour() {
        @SuppressLint("SimpleDateFormat")
        //SimpleDateFormat formatter = new SimpleDateFormat("EEEE HH:mm");
        SimpleDateFormat formatter = new SimpleDateFormat("EEE HH:mm");
        Date curDate = new Date(System.currentTimeMillis());
        String week_hour_str = formatter.format(curDate);
        tv_week_hour.setText(week_hour_str);
    }

}
