package hk.yijian.botel.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;
import hk.yijian.botel.R;


/**
 * Created by lmh on 2018/04/27
 */
@SuppressLint("AppCompatCustomView")
public class TimeView extends TextView {

    public static final int CHANGE_TIME_UI = 1001;
    private onCountDownFinishListener mOnCountDownFinishListener;
    private long diff;


    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {//Looper.getMainLooper()

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CHANGE_TIME_UI:
                    diff = diff - 1;
                    getShowTime();
                    if (diff > 0) {
                        //Message message = handler.obtainMessage(1);
                        handler.sendEmptyMessageDelayed(CHANGE_TIME_UI, 1000);
                        Log.d("TimeView", "sendEmptyMessageDelayed");
                    } else {
                        Log.d("TimeView", "onFinish");
                        if(mOnCountDownFinishListener != null){
                            mOnCountDownFinishListener.onFinish();
                        }
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    
    public TimeView(Context context) {
        this(context, null);
    }

    public TimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TimeView);
        diff = a.getInteger(R.styleable.TimeView_time, 120);
        //mStartTime();
    }

    //开始计时
    public void mStartTime() {
        Log.i("TimeView", "mStartTime------------------------------------------");
        handler.removeMessages(CHANGE_TIME_UI);
        getTime();
        //Message message = handler.obtainMessage(1);
        handler.sendEmptyMessageDelayed(CHANGE_TIME_UI, 1000);
    }

    //开始计时
    public void mStopTime() {
        try {
            Log.w("TimeView", "mStopTime------------------------------------------");
            handler.removeMessages(CHANGE_TIME_UI);
        }catch (Exception e){}
    }
    
    /**
     * 得到时间差
     */
    private void getTime() {
        try {
            setText(""+diff+"\"");
        } catch (Exception e) {
        }
    }

    /**
     * 获得要显示的时间
     */
    private void getShowTime() {
        setText(""+diff+"\"");
    }

    public void setOnCountDownFinishListener(onCountDownFinishListener onCountDownFinishListener) {
        this.mOnCountDownFinishListener = onCountDownFinishListener;
    }

    public interface onCountDownFinishListener {
        void onFinish();
    }

}
