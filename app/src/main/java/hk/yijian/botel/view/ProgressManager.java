package hk.yijian.botel.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import hk.yijian.botel.R;
import hk.yijian.botel.constant._HandlerCode;


/**
 * 控制标题容器内容的切换
 *
 * @author Administrator
 */
public class ProgressManager {

    private LinearLayout ll_progress_bar;
    private LinearLayout ll_five_four_gone;
    private TextView tv_progress_one;
    private TextView tv_progress_two;
    private TextView tv_progress_three;
    private TextView tv_progress_four;
    private TextView tv_progress_five;
    private TextView tv_progress_oneinfo;
    private TextView tv_progress_twoinfo;
    private TextView tv_progress_threeinfo;
    private TextView tv_progress_fourinfo;
    private TextView tv_progress_fiveinfo;
    private LinearLayout ll_back_delete;
    private ImageView iv_back;
    private ImageView iv_delete;
    private ImageView iv_user_img;
    private TimeView tv_address_countdown;

    private static ProgressManager instance = new ProgressManager();

    private ProgressManager() {
    }

    public static ProgressManager getInstance() {
        return instance;
    }

    public void init(Activity activity) {
        ll_back_delete = activity.findViewById(R.id.ll_back_delete);
        ll_progress_bar = activity.findViewById(R.id.ll_progress_bar);
        iv_back = activity.findViewById(R.id.iv_back);
        iv_delete = activity.findViewById(R.id.iv_delete);
        tv_progress_one = activity.findViewById(R.id.tv_progress_one);
        tv_progress_two = activity.findViewById(R.id.tv_progress_two);
        tv_progress_three = activity.findViewById(R.id.tv_progress_three);
        tv_progress_four = activity.findViewById(R.id.tv_progress_four);
        tv_progress_five = activity.findViewById(R.id.tv_progress_five);
        ll_five_four_gone = activity.findViewById(R.id.ll_set_gone);
        tv_progress_oneinfo = activity.findViewById(R.id.tv_progress_oneinfo);
        tv_progress_twoinfo = activity.findViewById(R.id.tv_progress_twoinfo);
        tv_progress_threeinfo = activity.findViewById(R.id.tv_progress_threeinfo);
        tv_progress_fourinfo = activity.findViewById(R.id.tv_progress_fourinfo);
        tv_progress_fiveinfo = activity.findViewById(R.id.tv_progress_fiveinfo);
        iv_user_img = activity.findViewById(R.id.iv_user_img);
        tv_address_countdown = activity.findViewById(R.id.tv_address_countdown);
    }

    public void setProgressInvisible() {
        ll_progress_bar.setVisibility(View.INVISIBLE);
    }

    public void setFourFiveProgressGone() {
        ll_five_four_gone.setVisibility(View.GONE);
    }

    public void showProgress(int no) {
        switch (no) {
            case 1:
                tv_progress_one.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_two.setTextColor(Color.parseColor("#2caf70"));
                tv_progress_three.setTextColor(Color.parseColor("#585858"));
                tv_progress_four.setTextColor(Color.parseColor("#585858"));
                tv_progress_five.setTextColor(Color.parseColor("#585858"));
                tv_progress_one.setBackgroundResource(R.mipmap.circular_bg_pic);
                tv_progress_two.setBackgroundResource(R.mipmap.circular_bg_pic_ing);
                break;
            case 2:
                tv_progress_one.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_one.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_two.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_three.setTextColor(Color.parseColor("#2caf70"));
                tv_progress_four.setTextColor(Color.parseColor("#585858"));
                tv_progress_five.setTextColor(Color.parseColor("#585858"));
                tv_progress_two.setBackgroundResource(R.mipmap.circular_bg_pic);
                tv_progress_three.setBackgroundResource(R.mipmap.circular_bg_pic_ing);
                break;
            case 3:
                tv_progress_one.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_two.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_one.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_two.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_three.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_four.setTextColor(Color.parseColor("#2caf70"));
                tv_progress_five.setTextColor(Color.parseColor("#585858"));
                tv_progress_three.setBackgroundResource(R.mipmap.circular_bg_pic);
                tv_progress_four.setBackgroundResource(R.mipmap.circular_bg_pic_ing);
                break;
            case 4:
                tv_progress_one.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_two.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_three.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_one.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_two.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_three.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_four.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_five.setTextColor(Color.parseColor("#2caf70"));
                tv_progress_four.setBackgroundResource(R.mipmap.circular_bg_pic);
                tv_progress_five.setBackgroundResource(R.mipmap.circular_bg_pic_ing);
                break;
            case 5:
                tv_progress_one.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_two.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_three.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_four.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
                tv_progress_one.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_two.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_three.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_four.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_five.setTextColor(Color.parseColor("#FFFFFF"));
                tv_progress_five.setBackgroundResource(R.mipmap.circular_bg_pic);
                break;
        }
    }

    public void mShowProgress() {
        tv_progress_one.setBackgroundResource(R.mipmap.circular_bg_pic_ok);
        tv_progress_one.setTextColor(Color.parseColor("#FFFFFF"));
        tv_progress_two.setBackgroundResource(R.mipmap.circular_bg_pic);
        tv_progress_three.setBackgroundResource(R.mipmap.circular_bg_pic_ing);
        tv_progress_two.setTextColor(Color.parseColor("#FFFFFF"));
        tv_progress_three.setTextColor(Color.parseColor("#2caf70"));
        tv_progress_four.setTextColor(Color.parseColor("#585858"));
        tv_progress_five.setTextColor(Color.parseColor("#585858"));
    }

    public void setProgressNetInfo() {
        tv_progress_oneinfo.setText("输入号码");
        tv_progress_twoinfo.setText("验证订单");
        tv_progress_threeinfo.setText("身份验证");
        tv_progress_fourinfo.setText("支付金额");
        tv_progress_fiveinfo.setText("取卡取票");
    }

    public void setProgressOutInfo() {
        //no login
        tv_progress_oneinfo.setText("插入房卡");
        tv_progress_twoinfo.setText("确认消费");
        tv_progress_threeinfo.setText("取卡取票");
    }

    public void setProgressInInfo() {
        tv_progress_oneinfo.setText("选择房型");
        tv_progress_twoinfo.setText("确认入住");
        tv_progress_threeinfo.setText("身份验证");
        tv_progress_fourinfo.setText("支付金额");
        tv_progress_fiveinfo.setText("取卡取票");
    }

    public void setProgressContinueInfo() {
        tv_progress_oneinfo.setText("插入房卡");
        tv_progress_twoinfo.setText("选择天数");
        tv_progress_threeinfo.setText("身份验证");
        tv_progress_fourinfo.setText("支付金额");
        tv_progress_fiveinfo.setText("取卡取票");
    }


}
