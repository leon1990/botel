/*****************************************************************************
 * _Application.java
 *****************************************************************************
 * Copyright © 2010-2013 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package hk.yijian.botel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.android.tpush.XGIOperateCallback;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;
import com.tencent.bugly.crashreport.CrashReport;

import org.greenrobot.greendao.database.Database;

import java.io.IOException;

import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.bean.greenDAO.DaoMaster;
import hk.yijian.botel.bean.greenDAO.DaoSession;
import hk.yijian.botel.util.AppUtils;
import hk.yijian.botel.util.MD5Utils;
import hk.yijian.hardware._Log;
import hk.yijian.library.CalligraphyConfig;
import hk.yijian.library.view.CustomViewWithTypefaceSupport;
import hk.yijian.library.view.TextField;


public class _Application extends Application {

    private final static String TAG = "_Application";
    private static _Application instance;
    public static _Application getInstance() {
        return instance;
    }
    //green DAO
    public static final String DB_NAME = "user.db";
    private static DaoSession sDaoSession;
    //okhttp
    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("application/json; charset=utf-8");
    public static final OkHttpClient mOkHttpClient = new OkHttpClient();
    static {
        //mOkHttpClient.connectTimeoutMillis();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        //腾讯信鸽
        XGPushConfig.enableDebug(this,true);
        XGPushManager.registerPush(this, new XGIOperateCallback() {
            @Override
            public void onSuccess(Object data, int flag) {//token在设备卸载重装的时候有可能会变
                Log.d("TPush_net_response", "注册成功，设备token为：" + data.toString());

                //TODO deviceId
                String mDeviceId =  MD5Utils.StrMd5(AppUtils.getMacAddress());
                //String mDeviceId = "220c45e4909ae2a923dad3697fcdbf2c";

                String mDeviceNo =  data.toString();
                String str = "{\"deviceId\": \""+mDeviceId+"\",\"deviceNo\": \""+mDeviceNo+"\"}";
                mPostUploadToken(str);
            }
            @Override
            public void onFail(Object data, int errCode, String msg) {
                Log.d("TPush_net_response", "注册失败，错误码：" + errCode + ",错误信息：" + msg);
            }
        });
        XGPushManager.registerPush(getApplicationContext(), "XINGE");
        XGPushManager.setTag(this,"XINGE");
        //腾讯bugly
        CrashReport.initCrashReport(getApplicationContext(), "8dadf37ea1", true);
        //green DAO
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, DB_NAME);
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                Log.v("ActivityLifecycle", "onActivityCreated");
            }
            @Override
            public void onActivityStopped(Activity activity) {
                Log.v("ActivityLifecycle", "onActivityStopped");
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Log.v("ActivityLifecycle", "onActivityStarted");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                Log.v("ActivityLifecycle","onActivitySaveInstanceState");
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.v("ActivityLifecycle","onActivityResumed");
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.v("ActivityLifecycle", "onActivityPaused");
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Log.v("ActivityLifecycle","onActivityDestroyed");
            }
        });


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SourceHanSansCN-Light.otf")
                .setFontAttrId(R.attr.fontPath)
                .addCustomViewWithSetTypeface(CustomViewWithTypefaceSupport.class)
                .addCustomStyle(TextField.class, R.attr.textFieldStyle)
                .build()
        );


    }


    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.w(TAG, "System is running low on memory");
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this) ;
    }


    /**
     *  上传 Token
     */
    private synchronized void mPostUploadToken(String str) {
        _Log.d("Token参数_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.XG_TOKEN_NO_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _Log.i("Token参数_net_response",_Constants.TOAST_SERVER_NULL);

            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("Token_net_response","result="+result);
            }
        });
    }
}
