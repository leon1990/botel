package hk.yijian.botel.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import hk.yijian.hardware._Log;

/**
 * Created by young on 2017/12/9.
 */

public class ShutdownReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SHUTDOWN)) {
            _Log.d("ShutdownReceiver", "android.intent.action.BOOT_COMPLETED");
        }
    }
}