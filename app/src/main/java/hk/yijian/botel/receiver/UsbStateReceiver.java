package hk.yijian.botel.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import java.util.HashMap;
import java.util.Iterator;

import hk.yijian.hardware._Log;

/**
 * Created by young on 2017/11/7.
 */

public class UsbStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String USBaction = intent.getAction();
        _Log.d("USBaction", "USBaction=" + USBaction);
        UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(USBaction)) {
            //Toast.makeText(context, "USB_拔出", Toast.LENGTH_SHORT).show();
            int VendorId = device.getVendorId();
            if (device != null) {
                _Log.d("StringUSB", "StringUSB_拔出=" + VendorId);
                if (VendorId == 7119) {//摄像头
                } else if (VendorId == 1024) {//身份证
                    //ret = 202;
                } else if (VendorId == 1305) {//打印小票
                } else if (VendorId == 8746) {//触摸屏线
                }
                if (VendorId == 1305 || VendorId == 8746 || VendorId == 1024 || VendorId == 7119) {
                    //Intent intent1 = new Intent(context, LoginActivity.class);
                    //intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //context.startActivity(intent1);
                }
            }
        } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(USBaction)) {
            //Toast.makeText(context, "USB_插入", Toast.LENGTH_SHORT).show();
            int VendorId = device.getVendorId();
            if (device != null) {
                _Log.d("StringUSB", "StringUSB_插入=" + VendorId);
                if (VendorId == 7119) {
                } else if (VendorId == 1024) {
                    //ret = iDCardDevice.getSAMStatus();
                    //recreate();
                    //Toast.makeText(context,"1024"+ret,Toast.LENGTH_SHORT).show();
                } else if (VendorId == 1305) {
                } else if (VendorId == 8746) {
                }
                if (VendorId == 1305 || VendorId == 8746 || VendorId == 1024 || VendorId == 7119) {
                    //Intent intent1 = new Intent(context, LoginActivity.class);
                    //intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //context.startActivity(intent1);
                }
            }
            UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
            Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
            StringBuilder sb = new StringBuilder();
            while (deviceIterator.hasNext()) {
                UsbDevice usbDevice = deviceIterator.next();
                sb.append("DeviceName=" + usbDevice.getDeviceName() + "\n");
                sb.append("DeviceId=" + usbDevice.getDeviceId() + "\n");
                sb.append("VendorId=" + usbDevice.getVendorId() + "\n");
                sb.append("ProductId=" + usbDevice.getProductId() + "\n");
                sb.append("-------------------------\n");
            }
            _Log.d("StringBuilderUSB", "" + sb + "________ret=\n");
        }
    }
}
