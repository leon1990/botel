package hk.yijian.botel.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import hk.yijian.hardware._Log;

public class BootBroadcastReceiver extends BroadcastReceiver {

    public static final String action_boot = "android.intent.action.BOOT_COMPLETED";
    public static final String action_init_idcard = "android.init.hardware";
    public LocalBroadcastManager localBroadcastManager ;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(action_boot)) {
            _Log.d("BootBroadcastReceiver", "android.intent.action.BOOT_COMPLETED");
            //Intent StartIntent = new Intent(context, LoginActivity.class);
            //StartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //context.startActivity(StartIntent);
            localBroadcastManager = LocalBroadcastManager.getInstance(context);
            localBroadcastManager.sendBroadcast(new Intent(action_init_idcard));
        }
        if (intent.getAction().equals(android.net.ConnectivityManager.CONNECTIVITY_ACTION)) {
            //RoomLog.d("BootBroadcastReceiver", "CONNECTIVITY_ACTION");
        }
    }
}
