package hk.yijian.botel.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by wangyuhang@evergrande.cn on 2017/7/12 0012.
 * <p>
 * 负责监听网络状态的变化
 * Android API为M及以上时，需要动态注册监听
 */
public class NetworkChangedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if (activeNetworkInfo.getType() == (ConnectivityManager.TYPE_WIFI)) {
                //Toast.makeText(context, "TYPE_WIFI", Toast.LENGTH_SHORT).show();
            } else if (activeNetworkInfo.getType() == (ConnectivityManager.TYPE_MOBILE)) {
                //Toast.makeText(context, "TYPE_MOBILE", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(context, "有线网络", Toast.LENGTH_SHORT).show();
            }
        } else {
            //Toast.makeText(context, "无网络", Toast.LENGTH_SHORT).show();
        }
    }
}