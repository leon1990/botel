package hk.yijian.botel.netty.entity;

/**
 * Created by young on 2017/11/18.
 */

public class RequestModel {

    private int msgType;
    private String deviceId;
    private String body;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }


}
