package hk.yijian.botel.netty;

import android.content.Context;
import android.content.Intent;

/**
 * Created by young on 2017/11/20.
 */

public class NettyUtils {

    public static boolean isDebug = false;

    public static void openNetty(Context context) {
        if (isDebug) {
            context.startService(new Intent(context, NettyService.class));//TODO 建立长连接
        }

    }

    public static void closeNetty(Context context) {
        if (isDebug) {
            context.stopService(new Intent(context, NettyService.class));
        }

    }

}
