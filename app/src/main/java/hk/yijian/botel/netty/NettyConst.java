package hk.yijian.botel.netty;

/**
 * Created by lmh on 11/18/2017.
 */

public class NettyConst {

    public static final String HOST = "10.2.201.40";
    public static final int TCP_PORT = 9999;

    public final static byte STATUS_CONNECT_SUCCESS = 1;
    public final static byte STATUS_CONNECT_CLOSED = 0;
    public final static byte STATUS_CONNECT_ERROR = 0;
}
