package hk.yijian.botel.netty;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import hk.yijian.botel.netty.entity.RequestBody;
import hk.yijian.botel.netty.entity.RequestModel;
import hk.yijian.botel.netty.utils.WriteLogUtil;
import hk.yijian.botel.util.AppUtils;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;
import hk.yijian.hardware._UsbPrintUtils;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

/**
 * Created by lmh on 11/18/2017.
 */
public class NettyService extends Service implements NettyListener {

    public Context context;
    private NetworkReceiver receiver;
    private String mac;

    private int times = 0;
    private Gson mgson;
    private RequestModel auth;
    private RequestBody request;
    private String requestBody = "";

    private ScheduledExecutorService mScheduledExecutorService;

    private void shutdown() {
        if (mScheduledExecutorService != null) {
            mScheduledExecutorService.shutdown();
            mScheduledExecutorService = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //context = OneApplication.getAppContext();
        context = this;
        //K720RoomCardUtils.getInstance().InitLink();
        _UsbPrintUtils.getInstance().InitUsbPrint(context);
        _IDCardUtils.getInstance().InitIDCardDevice(context);
        mac = AppUtils.getWifiNetInfo(context);
        mgson = new Gson();
        request = new RequestBody();
        auth = new RequestModel();

        receiver = new NetworkReceiver();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

        //TODO 每隔2秒向服务器发送心跳包
        mScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                times = times + 1;
                if (times == 6) {//60秒传一次
                    times = -1;

                    int roomCardDeviceStatus = _RoomCardUtils.getInstance().K720_Query();
                    int printDeviceStatus = _UsbPrintUtils.getInstance().getPrinterStatus();
                    int idcardDeviceStatus = _IDCardUtils.getInstance().getSAMStatus();

                    request.setMsgType(3);
                    request.setDeviceId(mac);
                    request.setBody(mac + " send data packet");
                    request.setIdcardDeviceStatus(idcardDeviceStatus);
                    request.setPrintDeviceStatus(roomCardDeviceStatus);
                    request.setRoomCardDeviceStatus(printDeviceStatus);
                    requestBody = mgson.toJson(request, RequestBody.class);

                   /* requestBody =
                            "{\"msgType\":3," + "\"body\":\""+mac+": send data packet\"," +
                                    "\"idcardDeviceStatus\":"+idcardDeviceStatus+"," +
                                    "\"roomCardDeviceStatus\":"+roomCardDeviceStatus+"," +
                                    "\"printDeviceStatus\":"+printDeviceStatus+"}";
                                    */

                } else {//10秒传一次
                    auth.setMsgType(1);
                    auth.setDeviceId(mac);
                    auth.setBody(mac + " send heartbeat msg");
                    requestBody = mgson.toJson(auth, RequestModel.class);
                    //requestBody = "{\"msgType\":1," + "\"body\":\""+mac+": send heartbeat msg\"}";
                }

                _Log.d("Nettydata_requestBody", "Write heartbeat" + requestBody);
                NettyClient.getInstance().sendMsgToServer(requestBody, new ChannelFutureListener() {    //3
                    @Override
                    public void operationComplete(ChannelFuture future) {
                        if (future.isSuccess()) {                //4
                            _Log.d("Netty", "Write heartbeat successful");
                        } else {
                            _Log.e("Netty", "Write heartbeat error");
                            WriteLogUtil.writeLogByThread("heartbeat error");
                        }
                    }

                });
            }
        }, 10, 10, TimeUnit.SECONDS);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NettyClient.getInstance().setListener(this);
        connect();
        return START_NOT_STICKY;
    }

    @Override
    public void onServiceStatusConnectChanged(int statusCode) {        //连接状态监听
        _Log.d("Netty", "connect status:" + statusCode);
        if (statusCode == NettyConst.STATUS_CONNECT_SUCCESS) {
            authenticData();
        } else {
            WriteLogUtil.writeLogByThread("tcp connect error");
        }
    }

    /**
     * 数据请求
     */
    private void authenticData() {
        auth.setMsgType(0);
        auth.setDeviceId(mac);
        auth.setBody(mac + " request login");
        String requestBody = mgson.toJson(auth, RequestModel.class);
        //String requestBody = "{\"msgType\":0," + "\"body\":\""+mac+": request login\"}";
        _Log.d("Nettydata_RequestModel", "RequestModel:" + requestBody);
        NettyClient.getInstance().sendMsgToServer(requestBody, new ChannelFutureListener() {    //3
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {                //4
                    _Log.d("Netty", "Write auth successful");
                } else {
                    _Log.d("Netty", "Write auth error");
                    WriteLogUtil.writeLogByThread("tcp auth error");
                }
            }
        });
    }

    @Override
    public void onMessageResponse(String str) {
        _Log.d("Netty_Response", "tcp receive data:" + str);
        if (str.equals("OK")) { // 接收

        } else {

        }
        handle();// 响应  TODO 实现自己的业务逻辑

    }

    private void handle() {

    }

    private void connect() {
        if (!NettyClient.getInstance().getConnectStatus()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    NettyClient.getInstance().connect();//连接服务器
                }
            }).start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        shutdown();
        NettyClient.getInstance().setReconnectNum(0);
        NettyClient.getInstance().disconnect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI
                        || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    connect();
                }
            }
        }
    }
}
