package hk.yijian.botel.netty.entity;

/**
 * Created by young on 2017/11/18.
 */

public class RequestBody {

    private int msgType;
    private String deviceId;
    private String body;
    private int idcardDeviceStatus;
    private int roomCardDeviceStatus;
    private int printDeviceStatus;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }


    public int getIdcardDeviceStatus() {
        return idcardDeviceStatus;
    }

    public void setIdcardDeviceStatus(int idcardDeviceStatus) {
        this.idcardDeviceStatus = idcardDeviceStatus;
    }

    public int getRoomCardDeviceStatus() {
        return roomCardDeviceStatus;
    }

    public void setRoomCardDeviceStatus(int roomCardDeviceStatus) {
        this.roomCardDeviceStatus = roomCardDeviceStatus;
    }

    public int getPrintDeviceStatus() {
        return printDeviceStatus;
    }

    public void setPrintDeviceStatus(int printDeviceStatus) {
        this.printDeviceStatus = printDeviceStatus;
    }


}
