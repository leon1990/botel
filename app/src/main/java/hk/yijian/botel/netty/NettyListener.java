package hk.yijian.botel.netty;


/**
 * Created by lmh on 11/18/2017.
 */
public interface NettyListener {

    /**
     * 当接收到系统消息
     */
    void onMessageResponse(String trs);

    /**
     * 当服务状态发生变化时触发
     */
    void onServiceStatusConnectChanged(int statusCode);
}
