/*
 *   Copyright (C)  2016 android@19code.com
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on++++++ an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package hk.yijian.botel.util;

import android.Manifest;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


/**
 * Create by lmh 2017/11/17
 */
public class AppUtils {

    public static final String TAG = "OneAppUtils";


    /**
     * 获取本地软件版本号
     */
    public static int getLocalVersion(Context ctx) {
        int localVersion = 0;
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }

    /**
     * 获取本地软件版本号名称
     */
    public static String getLocalVersionName(Context ctx) {
        String localVersion = "";
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }


    public static String getAppName(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        String appName = null;
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(packageName, 0);
            appName = String.valueOf(pm.getApplicationLabel(applicationInfo));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appName;
    }


    public static Drawable getAppIcon(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        Drawable appIcon = null;
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(packageName, 0);
            appIcon = applicationInfo.loadIcon(pm);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appIcon;
    }

    public static long getAppFirstInstallTime(Context context, String packageName) {
        long lastUpdateTime = 0;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            lastUpdateTime = packageInfo.firstInstallTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return lastUpdateTime;
    }

    public static long getAppLastUpdateTime(Context context, String packageName) {
        long lastUpdateTime = 0;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            lastUpdateTime = packageInfo.lastUpdateTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return lastUpdateTime;
    }


    public static long getAppSize(Context context, String packageName) {
        long appSize = 0;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
            appSize = new File(applicationInfo.sourceDir).length();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appSize;
    }

    public static String getAppApk(Context context, String packageName) {
        String sourceDir = null;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
            sourceDir = applicationInfo.sourceDir;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return sourceDir;
    }

    public static String getAppVersionName(Context context, String packageName) {
        String appVersion = null;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            appVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appVersion;
    }

    public static int getAppVersionCode(Context context, String packageName) {
        int appVersionCode = 0;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            appVersionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appVersionCode;
    }

    public static String getAppInstaller(Context context, String packageName) {
        return context.getPackageManager().getInstallerPackageName(packageName);
    }


    public static int getAppTargetSdkVersion(Context context, String packageName) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            return applicationInfo.targetSdkVersion;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int getAppUid(Context context, String packageName) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            return applicationInfo.uid;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int getNumCores() {
        try {
            File dir = new File("/sys/devices/system/cpu/");
            File[] files = dir.listFiles(new FileFilter() {

                @Override
                public boolean accept(File pathname) {
                    return Pattern.matches("cpu[0-9]", pathname.getName());
                }

            });
            return files.length;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static boolean getRootPermission(Context context) {
        String packageCodePath = context.getPackageCodePath();
        Process process = null;
        DataOutputStream os = null;
        try {
            String cmd = "chmod 777 " + packageCodePath;
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(cmd + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                process.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public static String[] getAppPermissions(Context context, String packname) {
        String[] requestedPermissions = null;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(packname, PackageManager.GET_PERMISSIONS);
            requestedPermissions = info.requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return requestedPermissions;
    }

    public static boolean hasPermission(Context context, String permission) {
        if (context != null && !TextUtils.isEmpty(permission)) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    if (PackageManager.PERMISSION_GRANTED == packageManager.checkPermission(permission, context
                            .getPackageName())) {
                        return true;
                    }
                    Log.d("OneAppUtils", "Have you  declared permission " + permission + " in AndroidManifest.xml ?");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static boolean isInstalled(Context context, String packageName) {
        boolean installed = false;
        if (TextUtils.isEmpty(packageName)) {
            return false;
        }
        List<ApplicationInfo> installedApplications = context.getPackageManager().getInstalledApplications(0);
        for (ApplicationInfo in : installedApplications) {
            if (packageName.equals(in.packageName)) {
                installed = true;
                break;
            } else {
                installed = false;
            }
        }
        return installed;
    }

    @Deprecated
    public static boolean installApk(Context context, String filePath) {
        File file = new File(filePath);
        if (!file.exists() || !file.isFile() || file.length() <= 0) {
            return false;
        }
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(Uri.parse("file://" + filePath), "application/vnd.android.package-archive");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        return true;
    }

    @Deprecated
    public static boolean uninstallApk(Context context, String packageName) {
        if (TextUtils.isEmpty(packageName)) {
            return false;
        }
        Intent i = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + packageName));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        return true;
    }


    public static boolean isSystemApp(Context context, String packageName) {
        boolean isSys = false;
        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(packageName, 0);
            if (applicationInfo != null && (applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) > 0) {
                isSys = true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            isSys = false;
        }
        return isSys;
    }

    public static boolean isServiceRunning(Context context, String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningServiceInfo> servicesList = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (RunningServiceInfo si : servicesList) {
            if (className.equals(si.service.getClassName())) {
                isRunning = true;
            }
        }
        return isRunning;
    }

    public static boolean stopRunningService(Context context, String className) {
        Intent intent_service = null;
        boolean ret = false;
        try {
            intent_service = new Intent(context, Class.forName(className));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (intent_service != null) {
            ret = context.stopService(intent_service);
        }
        return ret;
    }


    public static void killProcesses(Context context, int pid, String processName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String packageName;
        try {
            if (!processName.contains(":")) {
                packageName = processName;
            } else {
                packageName = processName.split(":")[0];
            }
            activityManager.killBackgroundProcesses(packageName);
            Method forceStopPackage = activityManager.getClass().getDeclaredMethod("forceStopPackage", String.class);
            forceStopPackage.setAccessible(true);
            forceStopPackage.invoke(activityManager, packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String runScript(String script) {
        String sRet;
        try {
            final Process m_process = Runtime.getRuntime().exec(script);
            final StringBuilder sbread = new StringBuilder();
            Thread tout = new Thread(new Runnable() {
                public void run() {
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(m_process.getInputStream()),
                            8192);
                    String ls_1;
                    try {
                        while ((ls_1 = bufferedReader.readLine()) != null) {
                            sbread.append(ls_1).append("\n");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            tout.start();

            final StringBuilder sberr = new StringBuilder();
            Thread terr = new Thread(new Runnable() {
                public void run() {
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(m_process.getErrorStream()),
                            8192);
                    String ls_1;
                    try {
                        while ((ls_1 = bufferedReader.readLine()) != null) {
                            sberr.append(ls_1).append("\n");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            terr.start();

            m_process.waitFor();
            while (tout.isAlive()) {
                Thread.sleep(50);
            }
            if (terr.isAlive())
                terr.interrupt();
            String stdout = sbread.toString();
            String stderr = sberr.toString();
            sRet = stdout + stderr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return sRet;
    }

    public static void runApp(Context context, String packagename) {
        context.startActivity(new Intent(context.getPackageManager().getLaunchIntentForPackage(packagename)));
    }


    public static String getMacAddress() {

        String macAddress = null;
        StringBuffer buf = new StringBuffer();
        NetworkInterface networkInterface = null;
        try {
            networkInterface = NetworkInterface.getByName("eth1");
            if (networkInterface == null) {
                networkInterface = NetworkInterface.getByName("wlan0");
            }
            if (networkInterface == null) {
                return "02:00:00:00:00:02";
            }
            byte[] addr = networkInterface.getHardwareAddress();
            for (byte b : addr) {
                buf.append(String.format("%02X:", b));
            }
            if (buf.length() > 0) {
                buf.deleteCharAt(buf.length() - 1);
            }
            macAddress = buf.toString();
        } catch (SocketException e) {
            e.printStackTrace();
            return "02:00:00:00:00:02";
        }
        return macAddress;
    }

    /**
     * 获取用户使用WiFi时的网络连接信息
     *
     * @param context Context
     */
    public static String getWifiNetInfo(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        // wifi状态，返回2或者3表示wifi可用
        int status = wifiManager.getWifiState();

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        // mac地址
        String macAddress = wifiInfo.getMacAddress();
        // 返回基本服务集标识符(BSSID)当前的访问点。目前如果没有网络连接，该标识符返回0
        String bissId = wifiInfo.getBSSID();
        // 获取Ip地址
        int ipAddress = wifiInfo.getIpAddress();
        // 每个配置的网络都有一个唯一的标识，用于识别网络上执行操作时的请求者。
        // 这个方法返回当前连接网络的ID
        int networkId = wifiInfo.getNetworkId();
        // 返回当前的接收信号强度
        int rssi = wifiInfo.getRssi();
        // 当前的服务集标识符
        String ssId = wifiInfo.getSSID();
        // 当前的链接速度（网速），单位Mbps
        int linkSpeed = wifiInfo.getLinkSpeed();

        Log.i(TAG, "status = " + status);
        Log.i(TAG, "macAddress = " + macAddress);
        Log.i(TAG, "bissId = " + bissId);
        Log.i(TAG, "ipAddress = " + ipAddress);
        Log.i(TAG, "networkId = " + networkId);
        Log.i(TAG, "rssi = " + rssi);
        Log.i(TAG, "ssId = " + ssId);
        Log.i(TAG, "linkSpeed = " + linkSpeed);

        return macAddress;
    }

    public static void getTelephonyInfo(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        // 手机串号：GSM手机的IMEI 和 CDMA手机的 MEID.
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String deviceId = telephonyManager.getDeviceId();
        // 返回设备的软件版本号，例如GSM手机的IMEI/SV
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String imei = telephonyManager.getDeviceSoftwareVersion();

        /* 手机类型：
         * @see #PHONE_TYPE_NONE // 无信号
         * @see #PHONE_TYPE_GSM  // GSM信号
         * @see #PHONE_TYPE_CDMA // CDMA信号
         * @see #PHONE_TYPE_SIP  // SIP信号
         */
        int phoneType = telephonyManager.getPhoneType();
        String imsi = telephonyManager.getSubscriberId();

        // 运营商名称（仅当用户已在网络注册时有效，在CDMA网络中结果也许不可靠）
        String networkOperatorName = telephonyManager.getNetworkOperatorName();
        // 注册网络的MCC+MNC
        String networkOperator = telephonyManager.getNetworkOperator();
        // 网络国家代码，MCC
        String networkCountryIso = telephonyManager.getNetworkCountryIso();

        /*
         * 当前使用的网络类型：
         * NETWORK_TYPE_UNKNOWN  网络类型未知  0
           NETWORK_TYPE_GPRS     GPRS网络  1
           NETWORK_TYPE_EDGE     EDGE网络  2
           NETWORK_TYPE_UMTS     UMTS网络  3
           NETWORK_TYPE_HSDPA    HSDPA网络  8
           NETWORK_TYPE_HSUPA    HSUPA网络  9
           NETWORK_TYPE_HSPA     HSPA网络  10
           NETWORK_TYPE_CDMA     CDMA网络,IS95A 或 IS95B.  4
           NETWORK_TYPE_EVDO_0   EVDO网络, revision 0.  5
           NETWORK_TYPE_EVDO_A   EVDO网络, revision A.  6
           NETWORK_TYPE_1xRTT    1xRTT网络  7
         */
        int networkType = telephonyManager.getNetworkType();

        // 当前网络是否处于漫游
        boolean isNetworkRoaming = telephonyManager.isNetworkRoaming();

        // 获取手机SIM卡的序列号
        String simSerialNumber = telephonyManager.getSimSerialNumber();

        Log.i(TAG, "simSerialNumber = " + simSerialNumber);
        Log.i(TAG, "isNetworkRoaming = " + isNetworkRoaming);
        Log.i(TAG, "networkType = " + networkType);
        Log.i(TAG, "networkCountryIso = " + networkCountryIso);
        Log.i(TAG, "networkOperator = " + networkOperator);
        Log.i(TAG, "networkOperatorName = " + networkOperatorName);
        Log.i(TAG, "imsi = " + imsi);
        Log.i(TAG, "phoneType = " + phoneType);
        Log.i(TAG, "deviceId = " + deviceId);
        Log.i(TAG, "phoneType = " + imei);

        // 是否有ICC卡（sim卡）
        boolean isSim = telephonyManager.hasIccCard();

        /*
         * 获取Sim卡状态，SIM的状态信息：
         SIM_STATE_UNKNOWN          未知状态 0
         SIM_STATE_ABSENT           没插卡 1
         SIM_STATE_PIN_REQUIRED     锁定状态，需要用户的PIN码解锁 2
         SIM_STATE_PUK_REQUIRED     锁定状态，需要用户的PUK码解锁 3
         SIM_STATE_NETWORK_LOCKED   锁定状态，需要网络的PIN码解锁 4
         SIM_STATE_READY            就绪状态 5
         */
        int simState = telephonyManager.getSimState();

        // 获取ISO国家码，相当于提供SIM卡的国家码。
        String simCountryIso = telephonyManager.getSimCountryIso();

        // 通话状态：无活动、响铃、摘机（接通）
        int callState = telephonyManager.getCallState();

        // 获取本机的手机号（有些手机号无法获取，是因为运营商在SIM中没有写入手机号）
        String phoneNumber = telephonyManager.getLine1Number();
        //获取语音邮件号码：
        String voiceMailNumber = telephonyManager.getVoiceMailNumber();

        /*
         * 获取数据连接的活动类型
         *
         * @see #DATA_ACTIVITY_NONE
         * @see #DATA_ACTIVITY_IN // 下行流量
         * @see #DATA_ACTIVITY_OUT // 上行流量
         * @see #DATA_ACTIVITY_INOUT
         * @see #DATA_ACTIVITY_DORMANT // 休眠
         */
        int dataActivty = telephonyManager.getDataActivity();


        Log.i(TAG, "dataActivty = " + dataActivty);
        Log.i(TAG, "voiceMailNumber = " + voiceMailNumber);
        Log.i(TAG, "phoneNumber = " + phoneNumber);
        Log.i(TAG, "callState = " + callState);
        Log.i(TAG, "simCountryIso = " + simCountryIso);
        Log.i(TAG, "simState = " + simState);
        Log.i(TAG, "isSim = " + isSim);


    }

    public static void getDeviceInfo(Context context) {
        // 硬件制造商
        String manufacturer = Build.MANUFACTURER;
        // 系统定制商
        String brand = Build.BRAND;
        // 主板
        String board = Build.BOARD;
        // 机型信息
        String model = Build.MODEL;
        // 显示屏参数
        String display = Build.DISPLAY;
        // Android版本
        String release = Build.VERSION.RELEASE;
        // Android SDK版本
        int sdkInt = Build.VERSION.SDK_INT;

        // 获取用户的语言环境
        Locale locale = context.getResources().getConfiguration().locale;
        String country = locale.getCountry();
        String displayLanguage = locale.getDisplayLanguage();
        String displayName = locale.getDisplayName();
        String displayVariant = locale.getDisplayVariant();

        int mnc = context.getResources().getConfiguration().mnc;
        int mcc = context.getResources().getConfiguration().mcc;
        // 横竖屏
        int orientation = context.getResources().getConfiguration().orientation;
        int densityDpi = context.getResources().getConfiguration().densityDpi;
        float fontScale = context.getResources().getConfiguration().fontScale;

        Log.i(TAG, "manufacturer = " + manufacturer);
        Log.i(TAG, "brand = " + brand);
        Log.i(TAG, "board = " + board);
        Log.i(TAG, "model = " + model);
        Log.i(TAG, "display = " + display);
        Log.i(TAG, "release = " + release);
        Log.i(TAG, "sdkInt = " + sdkInt);
        Log.i(TAG, "country = " + country);
        Log.i(TAG, "displayLanguage = " + displayLanguage);
        Log.i(TAG, "displayName = " + displayName);
        Log.i(TAG, "displayVariant = " + displayVariant);
        Log.i(TAG, "displayName = " + displayName);
        Log.i(TAG, "mnc = " + mnc);
        Log.i(TAG, "mcc = " + mcc);
        Log.i(TAG, "orientation = " + orientation);
        Log.i(TAG, "densityDpi = " + densityDpi);
        Log.i(TAG, "fontScale = " + fontScale);
    }

    private static void LoactionInfo(Location location) {
        Log.i(TAG, "时间：" + location.getTime());
        Log.i(TAG, "经度：" + location.getLongitude());
        Log.i(TAG, "纬度：" + location.getLatitude());
        Log.i(TAG, "海拔：" + location.getAltitude());
    }

    /**
     * 获取用户的当前位置信息
     *
     * @param context Context
     */
    public static void getLocationInfo(final Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        final LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 设置查询条件（我想要你当前位置的那些信息）
        String bestProvider = lm.getBestProvider(getCriteria(), true);

        // 获取位置信息
        // 如果不设置查询要求，getLastKnownLocation方法传人的参数为LocationManager.GPS_PROVIDER
        Location location = lm.getLastKnownLocation(bestProvider);
        LoactionInfo(location);

        // 监听状态
        lm.addGpsStatusListener(new GpsStatus.Listener() {
            public void onGpsStatusChanged(int event) {
                switch (event) {

                    // 第一次定位
                    case GpsStatus.GPS_EVENT_FIRST_FIX:
                        Log.i(TAG, "第一次定位");
                        break;
                    // 卫星状态改变
                    case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                        Log.i(TAG, "卫星状态改变");

                        // 获取当前状态
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        GpsStatus gpsStatus = lm.getGpsStatus(null);
                        // 获取卫星颗数的默认最大值
                        int maxSatellites = gpsStatus.getMaxSatellites();
                        Iterator<GpsSatellite> iters = gpsStatus.getSatellites().iterator();
                        int count = 0;
                        while (iters.hasNext() && count <= maxSatellites) {
                            count++;
                        }
                        Log.i(TAG, "搜索到：" + count + "颗卫星");
                        break;
                    // 定位启动
                    case GpsStatus.GPS_EVENT_STARTED:
                        Log.i(TAG, "定位启动");
                        break;
                    // 定位结束
                    case GpsStatus.GPS_EVENT_STOPPED:
                        Log.i(TAG, "定位结束");
                        break;
                }
            }

            ;
        });

        // 默认采用网络连接定位，若GPS可用则使用GPS定位
        String provider = LocationManager.NETWORK_PROVIDER;
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            provider = LocationManager.GPS_PROVIDER;
        }

        /**
         * 参数1：设备：有GPS_PROVIDER和NETWORK_PROVIDER两种
         * 参数2：位置信息更新周期，单位毫秒
         * 参数3：位置变化最小距离：当位置距离变化超过此值时，将更新位置信息
         * 参数4：位置变化监听器
         *
         * 注：如果参数3不为0，则以参数3为准；参数3为0，则通过时间（参数2）来定时更新；两者为0，则随时刷新
         * 3分钟更新一次或者最小位移变化超过10米更新一次
         */
        lm.requestLocationUpdates(provider, 3 * 60 * 1000, 10, new LocationListener() {

            /**
             * 位置信息变化时触发
             */
            public void onLocationChanged(Location location) {
                LoactionInfo(location);
            }

            /**
             * GPS状态变化时触发
             */
            public void onStatusChanged(String provider, int status, Bundle extras) {
                switch (status) {
                    // GPS状态为可见时
                    case LocationProvider.AVAILABLE:
                        Log.i(TAG, "当前GPS状态为可见状态");
                        break;
                    // GPS状态为服务区外时
                    case LocationProvider.OUT_OF_SERVICE:
                        Log.i(TAG, "当前GPS状态为服务区外状态");
                        break;
                    // GPS状态为暂停服务时
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        Log.i(TAG, "当前GPS状态为暂停服务状态");
                        break;
                }
            }

            /**
             * GPS开启时触发
             */
            public void onProviderEnabled(String provider) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                Location location = lm.getLastKnownLocation(provider);
                LoactionInfo(location);
            }

            /**
             * GPS禁用时触发
             */
            public void onProviderDisabled(String provider) {

            }
        });
    }

    /**
     * 返回查询条件
     *
     * @return
     */
    private static Criteria getCriteria() {
        Criteria criteria = new Criteria();
        // 设置定位精确度 Criteria.ACCURACY_COARSE比较粗略，Criteria.ACCURACY_FINE则比较精细
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        // 设置是否要求速度
        criteria.setSpeedRequired(true);
        // 设置是否允许运营商收费
        criteria.setCostAllowed(false);
        // 设置是否需要方位信息
        criteria.setBearingRequired(true);
        // 设置是否需要海拔信息
        criteria.setAltitudeRequired(true);
        // 设置对电源的需求
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        return criteria;
    }


    /**
     * 获取ip地址
     * @return
     */
    public static String getHostIP() {

        String hostIp = null;
        try {
            Enumeration nis = NetworkInterface.getNetworkInterfaces();
            InetAddress ia = null;
            while (nis.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) nis.nextElement();
                Enumeration<InetAddress> ias = ni.getInetAddresses();
                while (ias.hasMoreElements()) {
                    ia = ias.nextElement();
                    if (ia instanceof Inet6Address) {
                        continue;// skip ipv6
                    }
                    String ip = ia.getHostAddress();
                    if (!"127.0.0.1".equals(ip)) {
                        hostIp = ia.getHostAddress();
                        break;
                    }
                }
            }
        } catch (SocketException e) {
            Log.i("yao", "SocketException");
            e.printStackTrace();
        }
        return hostIp;

    }
}
