package hk.yijian.botel.faceUI.linkface;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import hk.yijian.botel.activity.RecLinkFaceActivity;
import hk.yijian.botel.constant._Constants;
import hk.yijian.linkface.data.LFCommonUtils;
import hk.yijian.linkface.data.LFConstants;
import hk.yijian.linkface.data.LFSpUtils;

/**
 * Created by young on 2017/11/27.
 */

public class LFLivenessUtil {

    //---------------link face------------------------
    public static void startLFLiveness(Context context,String activityStr) {

        Bundle bundle = new Bundle();
        /**
         * OUTPUT_TYPE 配置, 传入的outputType类型为singleImg （单图），multiImg （多图），video（低质量视频），fullvideo（高质量视频）
         */
        bundle.putString(RecLinkFaceActivity.OUTTYPE, getOutputType(context));
        /**
         * EXTRA_MOTION_SEQUENCE 动作检测序列配置，支持四种检测动作， BLINK(眨眼), MOUTH（张嘴）, NOD（点头）, YAW（摇头）, 各个动作以空格隔开。 推荐第一个动作为BLINK。
         * 默认配置为"BLINK MOUTH NOD YAW"
         */
        bundle.putString(RecLinkFaceActivity.EXTRA_MOTION_SEQUENCE, getActionSequence(context));
        /**
         * SOUND_NOTICE 配置, 传入的soundNotice为boolean值，true为打开, false为关闭。
         */
        bundle.putBoolean(RecLinkFaceActivity.SOUND_NOTICE, LFSpUtils.getMusicTipSwitch(context));
        /**
         * COMPLEXITY 配置, 传入的complexity类型为normal,支持四种难度，easy, normal, hard, hell.
         */
        bundle.putString(RecLinkFaceActivity.COMPLEXITY, getComplexity(context));


        Intent intent = new Intent();
        intent.setClass(context, RecLinkFaceActivity.class);
        intent.putExtras(bundle);
        //
        intent.putExtra(_Constants.BUNDLE_REY, activityStr);

        //设置返回图片结果
        intent.putExtra(RecLinkFaceActivity.KEY_DETECT_IMAGE_RESULT, true);
        //设置返回video结果，video模式才会返回
        intent.putExtra(RecLinkFaceActivity.KEY_DETECT_VIDEO_RESULT, false);
        //startActivityForResult(intent, KEY_TO_DETECT_REQUEST_CODE);
        context.startActivity(intent);
    }



    /**
     * 获取设置中设置好的outputType,
     * 若没有设置，默认传入Constants.SINGLEIMG，Constants.MULTIIMG，Constants.VIDEO三个中任意一个
     *
     * @return
     */
    private static String getOutputType(Context context) {
        String outputType = LFConstants.FULLVIDEO;
        outputType = LFSpUtils.getOutputType(context, LFConstants.FULLVIDEO);
        return outputType;
    }


    /**
     * 获取检测序列
     *
     * @return
     */
    private static String getActionSequence(Context context) {
        String actionSequence = "";
        //是否使用随机序列
        boolean isRandomSequence = LFSpUtils.getUseRandomSequence(context);
        if (isRandomSequence) {
            actionSequence = getRandomSequence();
        } else {
            actionSequence = LFSpUtils.getActionSequence(context);
            //如果没有设置动作序列，则使用默认动作序列
            if (TextUtils.isEmpty(actionSequence)) {
                actionSequence = LFCommonUtils.getActionSequence(getDefaultSequenceList());
            }
        }
        return actionSequence;
    }

    /**
     * 获取难易程度，在Constants.EASY，Constants.NORMAL，Constants.HARD，Constants.HELL中选取一个,
     * 默认Constants.NORMAL
     *
     * @return
     */
    private static String getComplexity(Context context) {
        String complexity = "";
        complexity = LFSpUtils.getComplexity(context, LFConstants.NORMAL);
        return complexity;
    }


    private static String getRandomSequence() {
        Random random = new Random();
        List<String> actionList = new ArrayList<>();
        for (int i = 0; ; i++) {
            int randomAction = random.nextInt(4);
            String actionByPosition = getActionByPosition(randomAction);
            if (!actionList.contains(actionByPosition)) {
                actionList.add(actionByPosition);
                if (actionList.size() >= 4) {
                    break;
                }
            }
        }
        return LFCommonUtils.getActionSequence(actionList);
    }

    private static List<String> getDefaultSequenceList() {
        List<String> sequenceDataList = new ArrayList<>();

        sequenceDataList.add(LFConstants.BLINK);
        /*sequenceDataList.add(LFConstants.MOUTH);
        sequenceDataList.add(LFConstants.YAW);
        sequenceDataList.add(LFConstants.NOD);*/
        return sequenceDataList;
    }

    private static String getActionByPosition(int position) {
        String action = "";
        switch (position) {
            case 0:
                action = LFConstants.BLINK;
                break;
            case 1:
                action = LFConstants.MOUTH;
                break;
            case 2:
                action = LFConstants.NOD;
                break;
            case 3:
                action = LFConstants.YAW;
                break;
        }
        return action;
    }
}
