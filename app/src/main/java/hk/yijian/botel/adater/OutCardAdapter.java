package hk.yijian.botel.adater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hk.yijian.botel.R;

/**
 * Created by young on 2017/12/22.
 */

public class OutCardAdapter extends BaseAdapter {

    private Context context;
    private String[] list;
    private boolean state = false;//初始化
    public int roomsCard;
    /*
     roomCardStateChange(false);//按钮初始样式
     public void roomCardStateChange(boolean state) {
        this.state = state;
    }
     */

    public OutCardAdapter(Context context,String[] list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_room_card_item, null);
            holder = new ViewHolder();
            holder.switchButton = view.findViewById(R.id.mCheckSwithcButton);
            holder.ll_room_no = view.findViewById(R.id.ll_room_no);
            holder.tv_room_no = view.findViewById(R.id.tv_room_no);
            holder.ll_room_no.setText(list[i]);
            holder.switchButton.setBackgroundResource(R.mipmap.button_close);//默认关闭
            holder.switchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (state) {
                        //roomCardStateChange(false);
                        holder.switchButton.setBackgroundResource(R.mipmap.button_close);
                        roomsCard = 1;
                    } else if (!state) {
                        //roomCardStateChange(true);
                        holder.switchButton.setBackgroundResource(R.mipmap.button_open);
                        roomsCard = 2;

                    }
                }
            });
            view.setTag(holder);
        }
        return view;
    }

    static class ViewHolder {
        ImageView switchButton;
        TextView ll_room_no;
        TextView tv_room_no;
    }

    public void roomCardStateChange(boolean state) {
        this.state = state;
    }
}
