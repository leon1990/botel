package hk.yijian.botel.adater;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;

import hk.yijian.botel.R;
import hk.yijian.botel.activity.CreateOrderActivity;
import hk.yijian.botel.bean.test.GuestPriceBean;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.util.ClickUtil;
import hk.yijian.hardware._Log;
import hk.yijian.view.toast._Toast;

/**
 * Created by young on 2017/9/11.
 */

public class PriceAdapter extends BaseAdapter {

    public SharedPreferences mSharedPreferences;
    public SharedPreferences.Editor editor;
    private Context context;
    private GuestPriceBean guestBean;
    private static final int[] list_form_item = {
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_006, R.mipmap.img_room_006, R.mipmap.img_room_004, R.mipmap.img_room_003
    };

    public PriceAdapter(Context context, String str) {
        this.context = context;
        mSharedPreferences = context.getSharedPreferences("cachedata", Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
        parseUserData(str);

    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_form_item, null);
            holder = new ViewHolder();
            holder.iv_icon = convertView.findViewById(R.id.iv_room_img);
            holder.tv_price = convertView.findViewById(R.id.tv_room_price);
            holder.tv_type = convertView.findViewById(R.id.tv_room_type);
            holder.iv_icon.setBackgroundResource(list_form_item[i]);
            holder.tv_type.setText("" + guestBean.getResult().get(i).typename);
            holder.tv_price.setText("¥" + guestBean.getResult().get(i).getRate() + "    ");

            holder.iv_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ClickUtil.isFastClick()) {
                        if (guestBean.getResult().get(i).getIscheckin().equals("1")) {
                            editor.putString("type", guestBean.getResult().get(i).getType());
                            editor.putString("roomno", guestBean.getResult().get(i).getRoomno());
                            editor.putInt("deposit", guestBean.getResult().get(i).getDeposit());
                            editor.putInt("rate", guestBean.getResult().get(i).getRate());
                            editor.putInt("howcard", guestBean.getResult().get(i).howcard);
                            editor.putString("roomtypename", guestBean.getResult().get(i).typename);
                            editor.commit();//editor.apply();

                            Bundle mBundle = new Bundle();
                            mBundle.putInt("hotelid",2);
                            mBundle.putInt("page",0);
                            mBundle.putInt("roomtypeid",2);
                            mBundle.putInt("size",0);
                            Intent intent = new Intent(context, CreateOrderActivity.class);
                            intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_IN_VALUE);
                            intent.putExtras(mBundle);
                            context.startActivity(intent);
                        } else {
                            _Toast.makeText( context , "该房间已经被预定" , 0, _Toast.SUCCESS,_Toast.SCALE);
                        }
                    }

                }
            });
            //holder.tv_type.setText(dataList.get(i).getType());
            //holder.tv_price.setText(dataList.get(i).getRate());
            convertView.setTag(holder);
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return guestBean.getResult().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    static class ViewHolder {
        ImageView iv_icon;
        TextView tv_type;
        TextView tv_price;
    }

    //load data
    private void parseUserData(String str) {
        String strContent = getJson("fragmentprice.json");
        if (!TextUtils.isEmpty(strContent)) {
            try {
                Gson mgson = new Gson();
                guestBean = mgson.fromJson(strContent, GuestPriceBean.class);
                _Log.d("guestBean", "msg =" + guestBean.getMsg());
                _Log.d("guestBean", "code =" + guestBean.getCode());
                _Log.d("guestBean", "results =" + guestBean.getResult().size());
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (str.equals("small")) {
            Collections.sort(guestBean.getResult(), new Comparator<GuestPriceBean.Result>() {
                @Override
                public int compare(GuestPriceBean.Result result, GuestPriceBean.Result t1) {
                    int i = t1.getRate() - result.getRate();
                    return i;
                }
            });
        } else if (str.equals("big")) {
            Collections.sort(guestBean.getResult(), new Comparator<GuestPriceBean.Result>() {
                @Override
                public int compare(GuestPriceBean.Result result, GuestPriceBean.Result t1) {
                    int i = result.getRate() - t1.getRate();
                    return i;
                }
            });
        }
    }

    public String getJson(String filename) {
        InputStream mInputStream = null;
        String resultString = "";
        try {
            mInputStream = context.getAssets().open(filename);
            byte[] buffer = new byte[mInputStream.available()];
            mInputStream.read(buffer);
            resultString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                mInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString.toString();
    }

}
