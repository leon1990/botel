package hk.yijian.botel.adater;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import hk.yijian.botel.R;
import hk.yijian.botel.activity.CreateOrderActivity;
import hk.yijian.botel.bean.response.RoomListAllData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.util.ClickUtil;

/**
 * Created by young on 2017/9/11.
 */

public class HomeAdapter extends BaseAdapter {

    public SharedPreferences mSharedPreferences;
    public SharedPreferences.Editor editor;
    private Context context;
    private List<RoomListAllData> roomDataList;

    /*
    private HomeGuestBean guestBean;
    private static final int[] list_form_item = {
            R.mipmap.img_room_001, R.mipmap.img_room_002,
            R.mipmap.img_room_003, R.mipmap.img_room_004,
            R.mipmap.img_room_005, R.mipmap.img_room_001,
            R.mipmap.img_room_002, R.mipmap.img_room_003,
            R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002,
            R.mipmap.img_room_003, R.mipmap.img_room_004,
            R.mipmap.img_room_005, R.mipmap.img_room_001,
            R.mipmap.img_room_002, R.mipmap.img_room_003,
            R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_006, R.mipmap.img_room_006,
            R.mipmap.img_room_004, R.mipmap.img_room_003
    };*/


    public HomeAdapter(Context context,List<RoomListAllData> mList) {
        this.context = context;
        this.roomDataList = mList;
        mSharedPreferences = context.getSharedPreferences("cachedata", Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
        //parseUserData();
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_form_item, null);
            holder = new ViewHolder();
            holder.iv_icon = convertView.findViewById(R.id.iv_room_img);
            holder.tv_price = convertView.findViewById(R.id.tv_room_price);
            holder.tv_type = convertView.findViewById(R.id.tv_room_type);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        //Glide.with(context).load(roomDataList.get(i).data.get(i).deviceMainPhoto).into(holder.iv_icon);
        holder.tv_type.setText("" + roomDataList.get(i).data.get(i).roomTypeName);
        holder.tv_price.setText("¥" + roomDataList.get(i).data.get(i).standardFee + "    ");


        holder.iv_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ClickUtil.isFastClick()) {
                    editor.putString(_Constants.BUNDLE_REY, _Constants.BUNDLE_IN_VALUE);
                    editor.putString("type", roomDataList.get(i).data.get(i).roomTypeName);
                    editor.putString("roomno","110" );
                    editor.putInt("deposit", roomDataList.get(i).data.get(i).depositFee);
                    editor.putInt("rate", roomDataList.get(i).data.get(i).standardFee);
                    editor.putInt("howcard", 1);
                    editor.putString("roomtypename", roomDataList.get(i).data.get(i).roomTypeName);
                    editor.commit();//editor.apply();
                    Bundle mBundle = new Bundle();
                    mBundle.putInt("hotelid",2);
                    mBundle.putInt("page",0);
                    mBundle.putInt("roomtypeid",2);
                    mBundle.putInt("size",0);
                    Intent intent = new Intent(context, CreateOrderActivity.class);
                    intent.putExtras(mBundle);
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }


    @Override
    public int getCount() {
        return roomDataList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    static class ViewHolder {
        ImageView iv_icon;
        TextView tv_type;
        TextView tv_price;
    }

    //load data
    /*
    private void parseUserData() {
        String strContent = getJson("fragmenall.json");
        if (!TextUtils.isEmpty(strContent)) {
            try {
                Gson mgson = new Gson();
                guestBean = mgson.fromJson(strContent, HomeGuestBean.class);
                _Log.d("guestBean", "msg =" + guestBean.msg);
                _Log.d("guestBean", "code =" + guestBean.code);
                _Log.d("guestBean", "results =" + guestBean.result.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getJson(String filename) {
        InputStream mInputStream = null;
        String resultString = "";
        try {
            mInputStream = context.getAssets().open(filename);
            byte[] buffer = new byte[mInputStream.available()];
            mInputStream.read(buffer);
            resultString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                mInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString.toString();
    }*/


}
