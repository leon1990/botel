package hk.yijian.botel.adater;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import hk.yijian.botel.R;
import hk.yijian.botel.activity.CreateOrderActivity;
import hk.yijian.botel.bean.test.TimeGuestBean;
import hk.yijian.botel.bean.response.RoomListAllData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.util.ClickUtil;
import hk.yijian.hardware._Log;
import hk.yijian.view.toast._Toast;

/**
 * Created by young on 2017/9/11.
 */

public class TimeAdapter extends BaseAdapter {

    public SharedPreferences mSharedPreferences;
    public SharedPreferences.Editor editor;
    private TimeGuestBean guestBean;
    private Context context;
    private List<RoomListAllData> roomDataList;


    private static final int[] list_form_item = {
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_001, R.mipmap.img_room_002, R.mipmap.img_room_003, R.mipmap.img_room_004, R.mipmap.img_room_005,
            R.mipmap.img_room_006, R.mipmap.img_room_006, R.mipmap.img_room_004, R.mipmap.img_room_003
    };

    public TimeAdapter(Context context,List roomDataList) {
        this.context = context;
        this.roomDataList = roomDataList;
        //parseUserData();
        //mSharedPreferences = context.getSharedPreferences("cachedata", Context.MODE_PRIVATE);
        //editor = mSharedPreferences.edit();
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_form_item, null);
            holder = new ViewHolder();
            holder.iv_icon = convertView.findViewById(R.id.iv_room_img);
            holder.tv_price = convertView.findViewById(R.id.tv_room_price);
            holder.tv_type = convertView.findViewById(R.id.tv_room_type);
            holder.iv_icon.setBackgroundResource(list_form_item[i]);
            holder.tv_type.setText("" + guestBean.result.get(i).typename);
            holder.tv_price.setText("¥" + guestBean.result.get(i).rate + "    ");
            holder.iv_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ClickUtil.isFastClick()) {
                        if (guestBean.result.get(i).ischeckin.equals("1")) {
                            editor.putString("type", guestBean.result.get(i).typename);
                            editor.putString("roomno", guestBean.result.get(i).roomno);
                            editor.putInt("deposit", guestBean.result.get(i).deposit);
                            editor.putInt("rate", guestBean.result.get(i).rate);
                            editor.putInt("howcard", guestBean.result.get(i).howcard);
                            editor.putString("roomtypename", guestBean.result.get(i).typename);
                            editor.commit();//editor.apply();
                            Bundle mBundle = new Bundle();
                            mBundle.putInt("hotelid",2);
                            mBundle.putInt("page",0);
                            mBundle.putInt("roomtypeid",2);
                            mBundle.putInt("size",0);
                            Intent intent = new Intent(context, CreateOrderActivity.class);
                            intent.putExtra(_Constants.BUNDLE_REY,  _Constants.BUNDLE_TIME_VALUE);
                            intent.putExtras(mBundle);
                            //intent.putExtra("type", guestBean.result.get(i).type);
                            //intent.putExtra("roomno", guestBean.result.get(i).roomno);
                            //intent.putExtra("deposit", guestBean.result.get(i).deposit);
                            //intent.putExtra("rate", guestBean.result.get(i).rate);
                            context.startActivity(intent);
                        } else {
                            _Toast.makeText( context , "该房间已经被预定" , 0, _Toast.SUCCESS,_Toast.SCALE);
                        }
                    }

                }
            });
            convertView.setTag(holder);
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return guestBean.result.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    static class ViewHolder {
        ImageView iv_icon;
        TextView tv_type;
        TextView tv_price;
    }

    //load data
    private void parseUserData() {
        String strContent = getJson("fragmenall.json");
        if (!TextUtils.isEmpty(strContent)) {
            try {
                Gson mgson = new Gson();
                guestBean = mgson.fromJson(strContent, TimeGuestBean.class);
                _Log.d("guestBean", "msg =" + guestBean.msg);
                _Log.d("guestBean", "code =" + guestBean.code);
                _Log.d("guestBean", "results =" + guestBean.result.size());
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    public String getJson(String filename) {
        InputStream mInputStream = null;
        String resultString = "";
        try {
            mInputStream = context.getAssets().open(filename);
            byte[] buffer = new byte[mInputStream.available()];
            mInputStream.read(buffer);
            resultString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                mInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString.toString();
    }

}
