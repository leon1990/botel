package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.timessquare.CalendarPickerView;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.OrderData;
import hk.yijian.botel.bean.response.PayGood;
import hk.yijian.botel.bean.response.RoomNoListData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.util.ClickUtil;
import hk.yijian.botel.view.RoundImageView;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;



public class CreateOrderActivity extends BaseActivity {

    private TextView tv_end_days;
    private TextView tv_start_days;
    private TextView tv_live_days;
    private ImageView iv_add_day;
    private ImageView iv_minus_day;

    private Button iv_checkin_yes;
    private LinearLayout lv_add_hostel;
    private ImageView iv_cancel_img;
    private CheckBox cb_choice_button;
    private ImageView iv_back;
    private ImageView iv_detele;
    private GridView gv_room_no;
    private LinearLayout ll_day_time;
    private TextView tv_room_rate;
    private TextView tv_room_deposit;
    private TextView tv_hostelroom_number;
    private ImageView iv_guest_reduce;
    private TextView tv_guest_no;
    private ImageView iv_guest_pluse;
    private ImageView iv_more_room_no;
    private TextView tv_room_no;
    private TextView tv_how_long;
    //
    private Date mStartDate;
    private Date mEndDate;
    private String mStartDateStr;
    private String mEndDateStr;

    private Calendar nextYear;
    private String datetime;
    private SimpleDateFormat sdf;
    //
    private DecimalFormat df;


    private ImageView iv_cancel_time;
    private HourAdapter mTimeadapter;
    private GridView gv_hour_no;
    private int currentItem = 0;

    private int hotelid;
    private int roomtypeid;
    //
    private PayGood payGoodBean;

    private int guestNO = 1;
    //
    private RoomNoListData roomNoData;
    private List<RoomNoListData.Data.RoomList>  roomNoList =
            new ArrayList<RoomNoListData.Data.RoomList>();
    //create order data
    private int orderSourceType;//1：OTA订房，2：自助机订房，3：微信小程序订房，4：微信公众号订房
    private int roomNum;
    private String guestName;
    private String startDate;
    private String endDate;
    private int hotelId ;
    private String hotelName;
    private String roomTypeName;
    private int breakfastNum;
    private int roomType;
    private int guestNum;
    //
    private int mDepositStr;//押金
    private int mRoomFeeStr;//房费
    private int _mRoomFee;
    private int mTotalFee; //总金额
    private String _RoomFeeStr;
    private String _DepositStr;

    private int mHowlongdays = 1;
    private int mContinueDays = 1;
    private int howRooms;

    private String mRoomNoStr;
    private String mobile;
    private String arriveTime ;
    private int isInvoice;
    private int invoiceType;
    private String invoiceTitle ;
    //
    private RoundImageView iv_room_photo;
    private TextView tv_end_day;
    private TextView tv_start_day;
    private LinearLayout ll_start_end_day;

    private  ArrayList<String> mPhonoList;
    //
    private PopupWindow RoomNoPopWindow;
    private PopupWindow DatePopWindow;
    private PopupWindow mTimePopWindow;

    private RoomListAdapter mRoomListadapter;

    private int mfloor;
    private int stayType;
    private String activityTag;

    private TimeView tv_address_countdown;

    /**
     * Handler
     */
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case _HandlerCode.CHANGE_VIEW_ROOMNO:
                    mRoomNoStr = roomNoList.get(0).roomNo;//显示一个默认房间号
                    mfloor = Integer.parseInt(mRoomNoStr.substring(0,1));
                    tv_room_no.setText(mRoomNoStr);
                    break;
                case _HandlerCode.CHANGE_SINGLE_SELETE_ROOMNO:
                    mRoomNoStr = roomNoList.get(currentItem).roomNo;
                    mfloor = Integer.parseInt(mRoomNoStr.substring(0,1));
                    tv_room_no.setText(roomNoList.get(currentItem).roomNo);
                    break;
                case _HandlerCode.CHANGE_POPWIN_VIEW_ROOMNO:
                    RoomNOPopupWindow(lv_add_hostel);
                    break;
                case _HandlerCode.CHANGE_ROOM_RATE:
                    mRoomFeeStr = _mRoomFee * mHowlongdays;//房费
                    _RoomFeeStr = df.format((double)(mRoomFeeStr)/100);
                    tv_room_rate.setText(_RoomFeeStr);
                    mTotalFee = mDepositStr + (mRoomFeeStr);
                    break;

                case _HandlerCode.CHANGE_UI_CONTINUE_ROOM:
                    mUserEditor.putString("mobile",payGoodBean.data.mobile);
                    mUserEditor.putBoolean("login",true);
                    mUserEditor.commit();
                    TitleBarManager.getInstance().init(CreateOrderActivity.this);

                    switch (activityTag) {
                        case _Constants.BUNDLE_CONTINUE_VALUE:
                            mRoomNoStr = mShareOrderData.getString("ticket_roomNo", "");//ticket_roomFee
                            //stayType = mShareOrderData.getInt("ticket_stayType", 0); // stayType 1:全天房 2:钟点房

                            if (payGoodBean.data.stayType == 2) {
                                mStartDateStr = getDayStr(payGoodBean.data.zendTime);
                                mEndDateStr = getDayAfter(mStartDateStr, 1);
                            } else {
                                mStartDateStr = payGoodBean.data.endDate;
                                mEndDateStr = getDayAfter(mStartDateStr, 1);
                            }
                            stayType = 1; //如果是钟点房 转 全天房
                            iv_more_room_no.setVisibility(View.INVISIBLE);
                            tv_how_long.setText(R.string.hotel_days);
                            tv_start_days.setVisibility(View.GONE);
                            tv_end_days.setVisibility(View.GONE);
                            //
                            iv_minus_day.setVisibility(View.VISIBLE);
                            iv_add_day.setVisibility(View.VISIBLE);
                            tv_live_days.setText(mContinueDays + "晚");
                            tv_room_no.setText(mRoomNoStr);
                            mTotalFee = mRoomFeeStr;

                            ll_start_end_day.setVisibility(View.VISIBLE);
                            tv_start_day.setText("入住："+mStartDateStr);
                            tv_end_day.setText("离店："+mEndDateStr);

                            tv_room_rate.setText("¥"+_RoomFeeStr);
                            tv_room_deposit.setText("¥"+_DepositStr + "(已付)");
                            break;
                    }

            }
            super.handleMessage(msg);
        }
    };


    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_order_detials);

        iv_checkin_yes = (Button) findViewById(R.id.iv_checkin_yes);
        ll_day_time = (LinearLayout) findViewById(R.id.ll_day_time);
        lv_add_hostel = (LinearLayout) findViewById(R.id.lv_add_hostel);
        tv_room_rate = (TextView) findViewById(R.id.tv_room_rate);
        tv_room_deposit = (TextView) findViewById(R.id.tv_room_deposit);
        tv_hostelroom_number = (TextView) findViewById(R.id.tv_hostelroom_number);
        iv_guest_reduce = (ImageView) findViewById(R.id.iv_guest_reduce);
        tv_guest_no = (TextView) findViewById(R.id.tv_guest_no);
        iv_guest_pluse = (ImageView) findViewById(R.id.iv_guest_pluse);
        tv_room_no = (TextView) findViewById(R.id.tv_room_no);
        tv_start_days = (TextView) findViewById(R.id.tv_start_days);
        tv_how_long = (TextView) findViewById(R.id.tv_how_long);
        tv_end_days = (TextView) findViewById(R.id.tv_end_days);
        tv_live_days = (TextView) findViewById(R.id.tv_live_days);
        iv_more_room_no = (ImageView) findViewById(R.id.iv_more_room_no);
        iv_add_day = (ImageView) findViewById(R.id.iv_add_day);
        iv_minus_day = (ImageView) findViewById(R.id.iv_minus_day);
        //
        tv_end_day = (TextView) findViewById(R.id.tv_end_day);
        tv_start_day = (TextView) findViewById(R.id.tv_start_day);
        ll_start_end_day =  (LinearLayout) findViewById(R.id.ll_start_end_day);
        //
        iv_room_photo = (RoundImageView) findViewById(R.id.iv_room_photo);
        //
        activityTag = getIntent().getStringExtra(_Constants.BUNDLE_REY);
        mPhonoList = (ArrayList<String>)getIntent().getStringArrayListExtra("roomPhotos");
        try{
            Glide.with(CreateOrderActivity.this).load(mPhonoList.get(0)).into(iv_room_photo);
        }catch (Exception e){}
        //
        df = new DecimalFormat("0.00");
        nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        mStartDate = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(mStartDate);
        calendar.add(calendar.DATE, 1);
        mEndDate = calendar.getTime();
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        initOrderView();
    }

    @Override
    protected void initTitleView() {
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请确认入住信息");
        TitleBarManager.getInstance().init(this);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_TWO);
        ProgressManager.getInstance().setProgressInInfo();
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_detele = (ImageView) findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CreateOrderActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish",Toast.LENGTH_LONG).show();
            }
        });
        iv_back.setOnClickListener(mBackListener);
        iv_detele.setOnClickListener(mBackHomeListener);

        switch (activityTag) {
            case _Constants.BUNDLE_TIME_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.TIME_ROOM);
                break;
            case _Constants.BUNDLE_CONTINUE_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CONTINUE_ROOM);
                break;
            case _Constants.BUNDLE_IN_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_IN);
                break;
            case _Constants.BUNDLE_NET_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.NET_CARD);
                break;
        }
    }

    @Override
    protected void initClickListener() {
        iv_checkin_yes.setOnClickListener(mCheckinYesListener);
        lv_add_hostel.setOnClickListener(mAddHostelListener);

        ll_day_time.setOnClickListener(mTimeDayListener);
        iv_guest_reduce.setOnClickListener(mGuestReduceListener);
        iv_guest_pluse.setOnClickListener(mGuestPluseListener);
        iv_add_day.setOnClickListener(mAddDayListener);
        iv_minus_day.setOnClickListener(mMinusDayListener);
    }

    @Override
    protected void initTouchListener() {

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
    }


    /**
     * 续房 加天数
     */
    private final View.OnClickListener mMinusDayListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if(mContinueDays <= 1){
            }else {
                mContinueDays = mContinueDays - 1;
                mRoomFeeStr = mRoomFeeStr - _mRoomFee;
                _RoomFeeStr = df.format((double)(mRoomFeeStr)/100);

                tv_live_days.setText(mContinueDays+"晚");
                tv_room_rate.setText(_RoomFeeStr);
                mEndDateStr = getDayAfter(mStartDateStr,mContinueDays);
                //tv_start_day.setText("入住："+mStartDateStr);
                tv_end_day.setText("离店："+mEndDateStr);
                mTotalFee =  mRoomFeeStr;
            }
        }
    };

    /**
     * 续房 /减天数
     */
    private final View.OnClickListener mAddDayListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (mContinueDays >= 15 ) {//TODO  十五天
            } else {
                mContinueDays = mContinueDays + 1;
                mRoomFeeStr = mRoomFeeStr + _mRoomFee;
                _RoomFeeStr = df.format((double)(mRoomFeeStr)/100);

                tv_live_days.setText(mContinueDays+"晚");
                tv_room_rate.setText(_RoomFeeStr);
                mEndDateStr = getDayAfter(mStartDateStr,mContinueDays);
                //tv_start_day.setText("入住："+mStartDateStr);
                tv_end_day.setText("离店："+mEndDateStr);
                mTotalFee = mRoomFeeStr;
            }
        }
    };

    /**
     *
     */
    private String getDayStr(String dayAfter) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(dayAfter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day);

        String mDayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return mDayAfter;
    }

    /**
     * 获取日期以后的时间
     */
    private String getDayAfter(String dayAfter,int days) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(dayAfter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day + days);

        String mDayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return mDayAfter;
    }

    private final View.OnClickListener mCancleTimeListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            mTimePopWindow.dismiss();
        }
    };


    private final View.OnClickListener mGuestPluseListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (guestNO >= 1 * 2) {
            } else {
                guestNO = guestNO + 1;
                tv_guest_no.setText("" + guestNO);
            }
        }
    };

    private final View.OnClickListener mGuestReduceListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (guestNO <= 1) {
            } else {
                guestNO = guestNO - 1;
                tv_guest_no.setText("" + guestNO);
            }
        }
    };

    private final View.OnClickListener mCancleListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            //gv_room_no.setAdapter(adapter);
            RoomNoPopWindow.dismiss();
        }
    };

    private final View.OnClickListener mBackListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            Intent intenT;
            switch (activityTag) {
                case _Constants.BUNDLE_TIME_VALUE:
                    intenT = new Intent(CreateOrderActivity.this, CheckInTimeRoomActivity.class);
                    startActivity(intenT);
                    finish();
                    break;
                case _Constants.BUNDLE_CONTINUE_VALUE:
                    intenT = new Intent(CreateOrderActivity.this, LoginActivity.class);
                    startActivity(intenT);
                    finish();
                    break;
                case _Constants.BUNDLE_IN_VALUE:
                    intenT = new Intent(CreateOrderActivity.this, CheckInAllRoomDataActivity.class);
                    startActivity(intenT);
                    finish();
                    break;
                case _Constants.BUNDLE_NET_VALUE:
                    intenT = new Intent(CreateOrderActivity.this, CheckInNetOrderActivity.class);
                    startActivity(intenT);
                    finish();
                    break;
            }
        }
    };

    private final View.OnClickListener mBackHomeListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            backLoginPage(CreateOrderActivity.this);
        }
    };

    private final View.OnClickListener mTimeDayListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(activityTag.equals(_Constants.BUNDLE_TIME_VALUE)){//钟点房
                mTimePopWindow(view);
            }else if(activityTag.equals(_Constants.BUNDLE_CONTINUE_VALUE)){
            }else {
                mDatePopWindow(view);
            }
        }
    };

    /**
     * 创建订单
     */
    private final View.OnClickListener mCheckinYesListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            if (ClickUtil.isFastClick()) {

                getOrderData();
                saveOrderData();//
                String Order = "";
                if(activityTag.equals(_Constants.BUNDLE_CONTINUE_VALUE)){
                    //payGoodBean.data    续房加 orderNo
                    Order = "{\"deviceId\":\"" + mDeviceId + "\"," +
                            "\"orderNo\":\"" + payGoodBean.data.orderNo + "\"," +
                            "\"orderSourceType\":" + orderSourceType + "," +
                            "\"hotelId\":" + hotelId + "," +
                            "\"hotelName\":\"" + hotelName + "\"," +
                            "\"roomType\":" + roomType + "," +
                            "\"roomTypeName\":\"" + roomTypeName + "\"," +
                            "\"startDate\":\"" + startDate + "\"," +
                            "\"endDate\":\"" + endDate + "\"," +
                            "\"roomNum\":" + roomNum + "," +
                            "\"guestNum\":" + guestNum + "," +
                            "\"orderFee\":" + mTotalFee + "," +
                            "\"roomFee\":" + mRoomFeeStr + "," +
                            "\"depositFee\":" + mDepositStr + "," +
                            "\"stayType\":" + 1 + "," +
                            "\"mobile\":\"" + mobile + "\"," +
                            "\"arriveTime\":\"" + arriveTime + "\"," +
                            "\"isInvoice\":" + isInvoice + "," +
                            "\"invoiceType\":" + invoiceType + "," +
                            "\"invoiceTitle\":\"" + invoiceTitle + "\"," +
                            "\"rooms\":[{\"floor\":" + mfloor + "," +
                            "\"roomNo\":\"" + mRoomNoStr + "\"," +
                            "\"guestName\":\"" + guestName + "\"," +
                            "\"breakfastNum\":" + breakfastNum + "}]}";
                }else {
                    if(null == roomNoData.data || roomNoData.data.size() ==0 ){
                        _LToast("房间号为空！");
                        return;
                    }
                    // do okhttp
                    Order = "{\"deviceId\":\"" + mDeviceId + "\"," +
                            "\"orderSourceType\":" + orderSourceType + "," +
                            "\"hotelId\":" + hotelId + "," +
                            "\"hotelName\":\"" + hotelName + "\"," +
                            "\"roomType\":" + roomType + "," +
                            "\"roomTypeName\":\"" + roomTypeName + "\"," +
                            "\"startDate\":\"" + startDate + "\"," +
                            "\"endDate\":\"" + endDate + "\"," +
                            "\"roomNum\":" + roomNum + "," +
                            "\"guestNum\":" + guestNum + "," +
                            "\"orderFee\":" + mTotalFee + "," +
                            "\"roomFee\":" + mRoomFeeStr + "," +
                            "\"depositFee\":" + mDepositStr + "," +
                            "\"stayType\":" + stayType + "," +
                            "\"mobile\":\"" + mobile + "\"," +
                            "\"arriveTime\":\"" + arriveTime + "\"," +
                            "\"isInvoice\":" + isInvoice + "," +
                            "\"invoiceType\":" + invoiceType + "," +
                            "\"invoiceTitle\":\"" + invoiceTitle + "\"," +
                            "\"rooms\":[{\"floor\":" + mfloor + "," +
                            "\"roomNo\":\"" + mRoomNoStr + "\"," +
                            "\"guestName\":\"" + guestName + "\"," +
                            "\"breakfastNum\":" + breakfastNum + "}]}";
                }
                mPostCreateOrder(Order);
            }
        }
    };

    private final View.OnClickListener mChioseListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            gv_room_no.setAdapter(mRoomListadapter);

        }
    };

    private final View.OnClickListener mAddHostelListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (ClickUtil.isFastClick()) {
                if(activityTag.equals(_Constants.BUNDLE_CONTINUE_VALUE)){
                }else {
                    String postStr = "{\"hotelId\": " + hotelid + ",\"roomTypeId\": " + roomtypeid + "}";
                    mPostRoomNoList(postStr,2);//code = 2 选定一个房间号
                }
            }
        }
    };

    /**
     * 初始化订单详情UI
     */
    private void initOrderView(){
        if(activityTag.equals(_Constants.BUNDLE_CONTINUE_VALUE)){//续房
            //续房不给加人数
            iv_guest_reduce.setVisibility(View.INVISIBLE);
            iv_guest_pluse.setVisibility(View.INVISIBLE);
            try{
                _RoomCardUtils.getInstance().GetPermissions();
                String mReadCardData = _RoomCardUtils.getInstance().ReadCardData();
                String mOutCardNo = mReadCardData.substring(0,20);
                _Log.d("创建订单card_net_response","mCardNo="+ mOutCardNo);
                mGetOrderStatus(mOutCardNo);// 查询订单状态
            }catch (Exception e){}
        }else {
            try{//Room NO
                hotelid = mSharePublicData.getInt("public_hotelid",0);
                roomtypeid = mShareOrderData.getInt("ticket_roomType",0);
                String postStr = "{\"hotelId\": " + hotelid + ",\"roomTypeId\": " + roomtypeid + "}";
                mPostRoomNoList(postStr,1);//code = 1 默认一个房间号
            }catch (Exception e){}
        }
        //_EndDate = mShareOrderData.getString("ticket_endDate", "");
        mDepositStr = mShareOrderData.getInt("ticket_deposit", 0);
        mRoomFeeStr = mShareOrderData.getInt("ticket_roomFee", 0);//ticket_roomFee
        _mRoomFee = mRoomFeeStr ;//中间固定值
        _Log.i("_mRoomFee_net_response","_mRoomFee = "+_mRoomFee+"mRoomFeeStr = "+mRoomFeeStr);
        mRoomFeeStr = _mRoomFee * mHowlongdays;
        _RoomFeeStr = df.format((double)(mRoomFeeStr)/100);
        _DepositStr = df.format((double)(mDepositStr)/100);
        howRooms = 1;   //多少个房间
        tv_hostelroom_number.setText(howRooms +"  间");
        mTotalFee = mRoomFeeStr + mDepositStr;

        switch (activityTag) {
            case _Constants.BUNDLE_IN_VALUE:
            case _Constants.BUNDLE_NET_VALUE:
                mStartDateStr = sdf.format(mStartDate.getTime());
                mEndDateStr = sdf.format(mEndDate);
                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd");
                String mStartDay = formatter.format(mStartDate.getTime());
                String mEndDay = formatter.format(mEndDate);
                tv_how_long.setText(R.string.hotel_days);
                tv_start_days.setText(mStartDay);
                tv_end_days.setText(mEndDay);
                tv_live_days.setText(1+"晚");
                tv_room_rate.setText("¥"+_RoomFeeStr);
                tv_room_deposit.setText("¥"+_DepositStr);
                stayType = 1;
                break;
            case _Constants.BUNDLE_TIME_VALUE:
                mStartDateStr =  sdf.format(mStartDate.getTime())+" 12:00";
                mEndDateStr =  sdf.format(mStartDate.getTime())+" 14:00";
                tv_how_long.setText("小  时:");
                tv_start_days.setText("12:00 - ");
                tv_end_days.setText("14:00");
                tv_live_days.setText("");
                tv_room_rate.setText("¥"+_RoomFeeStr);
                tv_room_deposit.setText("¥"+_DepositStr);
                stayType = 2;
                break;
        }
    }

    /**
     * 获取订单数据
     */
    private void getOrderData(){
        mobile = "0";
        arriveTime = "";
        isInvoice = 0;
        invoiceType = 0 ;
        invoiceTitle = "0";
        hotelName = mSharePublicData.getString("public_hotelname", "");
        hotelId = mSharePublicData.getInt("public_hotelid", 0);
        roomType =  mShareOrderData.getInt("ticket_roomType", 0);
        roomTypeName = mShareOrderData.getString("ticket_roomtypename", "");
        breakfastNum = mShareOrderData.getInt("ticket_breakfastNum", 0);
        //stayType = mShareOrderData.getInt("ticket_stayType", 0); // stayType 1:全天房     2:钟点房
        roomNum = 1;
        startDate =  mStartDateStr;
        endDate = mEndDateStr;

        if(activityTag.equals(_Constants.BUNDLE_CONTINUE_VALUE)){
            orderSourceType = 2;//1：OTA订房，2：自助机订房，3：微信小程序订房，4：微信公众号订房
            //startDate = mStartDateStr+" 00:00";
            //endDate = mEndDateStr+" 00:00";
            guestName = "";
            guestNum = mShareOrderData.getInt("ticket_guestNum", 0);
        }else {
            orderSourceType = mShareOrderData.getInt("ticket_orderSourceType", 0);
            guestName = mShareOrderData.getString("ticket_guestName", "");
            guestNum = guestNO;
        }
    }

    /**
     * 缓存订单数据
     */
    private void saveOrderData(){
        mOrderEditor.putString("ticket_roomTypeName",roomTypeName);
        mOrderEditor.putInt("ticket_howcard", guestNO);
        mOrderEditor.putInt("guesttotal", guestNO);//判断出多少张放卡
        mOrderEditor.putString("ticket_roomno",mRoomNoStr );
        mOrderEditor.putInt("ticket_guestsize", guestNO);
        mOrderEditor.putInt("ticket_roomsize", 1);
        mOrderEditor.putString("ticket_startDate", startDate);
        mOrderEditor.putString("ticket_endDate", endDate);
        mOrderEditor.putInt("ticket_roomFee", mRoomFeeStr);
        mOrderEditor.putInt("ticket_depositFee", mDepositStr);
        mOrderEditor.putInt("ticket_total",mTotalFee);
        mOrderEditor.commit();
        mPublicEditor.putInt("testTimes", guestNO);     //判断多少人需要人脸识别
        mPublicEditor.commit();
        _Log.i("mTotalFee_net_response","mTotalFee = "+mTotalFee);
    }

    /**
     * Hour list
     */
    private class HourAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup container) {

            final HourViewHolder holder;
            if (convertView == null) {
                final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_detial_hour_item, null);
                holder = new HourViewHolder();
                holder.tv_start_hour = convertView.findViewById(R.id.tv_start_hour);
                holder.tv_end_hour = convertView.findViewById(R.id.tv_end_hour);
                convertView.setTag(holder);
            }else{
                holder = (HourViewHolder) convertView.getTag();
            }
            holder.tv_start_hour.setText("12:00");
            holder.tv_end_hour.setText("14:00");

            return convertView;
        }
    }
    static class HourViewHolder {
        TextView tv_start_hour;
        TextView tv_end_hour;
    }

    /**
     *  RoomNo List
     */
    private class RoomListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return roomNoList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup container) {

            final ViewHolder holder;
            if (convertView == null) {
                final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_detial_roomno_item, null);
                holder = new ViewHolder();
                holder.fl_roomno_area = convertView.findViewById(R.id.fl_roomno_area);
                holder.ll_roomno_area = convertView.findViewById(R.id.ll_roomno_area);
                holder.tv_room_no = convertView.findViewById(R.id.tv_room_no);
                holder.tv_direction_text = convertView.findViewById(R.id.tv_direction_text);
                holder.tv_price_text = convertView.findViewById(R.id.tv_price_text);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tv_room_no.setText("" + roomNoList.get(position).roomNo);
            holder.tv_price_text.setText("" + roomNoList.get(position).roomFee);
            holder.tv_direction_text.setText("有窗");//朝向


            if (position == currentItem) {
                holder.fl_roomno_area.setBackgroundResource(R.mipmap.room_item_click_bg);
                holder.ll_roomno_area.setBackgroundResource(R.mipmap.room_item_click_bg);
                mHandler.removeMessages(_HandlerCode.CHANGE_SINGLE_SELETE_ROOMNO);
                mHandler.sendEmptyMessage(_HandlerCode.CHANGE_SINGLE_SELETE_ROOMNO);
            } else {
                holder.fl_roomno_area.setBackgroundResource(R.color.roomno_item_bg);
                holder.ll_roomno_area.setBackgroundResource(R.color.roomno_item_bg);

            }
            return convertView;
        }
    }
    static class ViewHolder {
        FrameLayout fl_roomno_area;
        FrameLayout ll_roomno_area;
        TextView tv_room_no;
        TextView tv_direction_text;
        TextView tv_price_text;
    }


    /**
     * pop日历
     * @param view
     */
    private synchronized void mDatePopWindow(View view){
        LinearLayout frameLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.calendar_dialog, null, false);
        DatePopWindow = new PopupWindow(frameLayout, 435, WindowManager.LayoutParams.MATCH_PARENT, true);
        DatePopWindow.setTouchable(true);
        DatePopWindow.setOutsideTouchable(true);
        DatePopWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        DatePopWindow.showAsDropDown(view);
        //TODO  CalendarPickerView
        final CalendarPickerView dialogView = frameLayout.findViewById(R.id.calendar_view);
        ImageView imageView = frameLayout.findViewById(R.id.iv_back_calendar);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePopWindow.dismiss();
            }
        });
        datetime = mSharePublicData.getString("datetime", "");
        try {
            tv_end_days.setText("" + sdf.parse(datetime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (datetime.equals("")) {
            dialogView.init(mStartDate, nextYear.getTime()).withSelectedDate(mEndDate);
        } else {
            //Tue Oct 24 00:00:00 GMT+00:00 2017
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
            try {
                mEndDate = sdf.parse(datetime);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialogView.init(mStartDate, nextYear.getTime()).withSelectedDate(mEndDate);
        }
        //dialogView.init(today, nextYear.getTime()).inMode(RANGE);//选定间隔日期
        dialogView.setMclickListener(new CalendarPickerView.mClickListener() {
            @Override
            public void mClick(Date clickedDate) {
                DateFormat dateFormat = new SimpleDateFormat("MM-dd");
                tv_end_days.setText(dateFormat.format(clickedDate));
                mPublicEditor.putString("datetime", "" + clickedDate);
                mPublicEditor.commit();

                DateFormat mdf = new SimpleDateFormat("yyyy-MM-dd");
                mStartDateStr = mdf.format(mStartDate);
                mEndDateStr = mdf.format(clickedDate);
                try {
                    Date d1 = mdf.parse(mdf.format(mStartDate));
                    Date d2 = mdf.parse(mdf.format(clickedDate));
                    long diff = d2.getTime() - d1.getTime();
                    long days = diff / (1000 * 60 * 60 * 24);
                    mHowlongdays = (int)days;

                    if(mHowlongdays == 0){
                        mHowlongdays = 1;
                        _LToast("离店日期不能为当天");
                        tv_live_days.setText(mHowlongdays + "晚");
                        mHandler.sendEmptyMessage(_HandlerCode.CHANGE_ROOM_RATE);  //  刷新UI
                    }else {
                        tv_live_days.setText(mHowlongdays + "晚");
                        mHandler.sendEmptyMessage(_HandlerCode.CHANGE_ROOM_RATE);  //  刷新UI
                    }

                } catch (Exception e) {
                }
                DatePopWindow.dismiss();
            }
            @Override
            public void mInvalidClick() {
            }
        });
    }

    /**
     * pop 选择时间
     * @param view
     */
    private synchronized void mTimePopWindow(View view){
        View popupView = getLayoutInflater().inflate(R.layout.time_hour_popupwindow, null);
        mTimePopWindow = new PopupWindow(popupView, 435, WindowManager.LayoutParams.WRAP_CONTENT, true);
        mTimePopWindow.setTouchable(true);
        mTimePopWindow.setOutsideTouchable(true);
        mTimePopWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        mTimePopWindow.showAsDropDown(view);
        iv_cancel_time = popupView.findViewById(R.id.iv_cancel_time);
        iv_cancel_time.setOnClickListener(mCancleTimeListener);
        gv_hour_no = popupView.findViewById(R.id.gv_time_hour);
        mTimeadapter = new HourAdapter();
        gv_hour_no.setAdapter(mTimeadapter);
        gv_hour_no.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mTimeadapter.notifyDataSetChanged();
                //dismiss
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mTimePopWindow.dismiss();
                    }
                }, 150);
            }
        });
    }

    /**
     * pop选择房间号
     * @param view
     */
    private synchronized void RoomNOPopupWindow(View view){
        View popupView = getLayoutInflater().inflate(R.layout.hostel_no_popupwindow, null);
        RoomNoPopWindow = new PopupWindow(popupView,  435, WindowManager.LayoutParams.WRAP_CONTENT, true);
        RoomNoPopWindow.setTouchable(true);
        RoomNoPopWindow.setOutsideTouchable(true);
        RoomNoPopWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        RoomNoPopWindow.showAsDropDown(view);
        iv_cancel_img = popupView.findViewById(R.id.iv_cancel_img);
        cb_choice_button = popupView.findViewById(R.id.cb_choice_button);
        iv_cancel_img.setOnClickListener(mCancleListener);
        cb_choice_button.setOnClickListener(mChioseListener);
        gv_room_no = popupView.findViewById(R.id.gv_room_no);
        mRoomListadapter = new RoomListAdapter();
        gv_room_no.setAdapter(mRoomListadapter);
        gv_room_no.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                currentItem = position;
                mRoomListadapter.notifyDataSetChanged();
                //dismiss
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RoomNoPopWindow.dismiss();
                    }
                }, 150);
            }
        });
    }

    /**
     * 查看订单状态
     */
    private synchronized void mGetOrderStatus(String str) {
        _Log.d("OrderStatus_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.QUERY_ORDER_STATUS_URL+str)
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast("OrderStatus"+_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("OrderStatus_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson();
                    payGoodBean = mgson.fromJson(result, PayGood.class);
                    if (payGoodBean.code == 0) {

                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_CONTINUE_ROOM,200);
                        if(payGoodBean.data != null){
                        }else {
                            _LToast(_Constants.TOAST_DATA_NULL);
                        }
                    }else if(payGoodBean.code == -1){
                        _LToast(""+payGoodBean.msg);
                    }
                    try {
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }

    /**
     * RoomNOList
     * @param str
     */
    private synchronized void mPostRoomNoList(String str,final int code) {
        _Log.d("getRoomNoList参数_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.DETIALS_ROOM_NO_LIST_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast("RoomNoList"+_Constants.TOAST_SERVER_NULL);
            }
            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("getRoomNoList_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson();
                    roomNoData = mgson.fromJson(result, RoomNoListData.class);
                    if(roomNoData.code == 0){
                        if(null == roomNoData.data || roomNoData.data.size() ==0 ){
                            _LToast("获取Room NO List为空");
                        }else {
                            roomNoList.clear();
                            for (int i = 0; i < roomNoData.data.size(); i++) {
                                for (int j = 0; j < roomNoData.data.get(i).roomList.size(); j++) {
                                    roomNoList.add(roomNoData.data.get(i).roomList.get(j));
                                }
                            }
                            if(code == 1){
                                mHandler.sendEmptyMessage(_HandlerCode.CHANGE_VIEW_ROOMNO);
                            }else {
                                mHandler.sendEmptyMessage(_HandlerCode.CHANGE_POPWIN_VIEW_ROOMNO);
                            }
                        }
                    }else {
                        _LToast(""+roomNoData.msg);
                    }

                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }
    /**
     * OrderPost
     * @param str
     */
    private synchronized void mPostCreateOrder(String str) {

        _Log.d("createOrder参数_net_response", "str=" + str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.CREATE_ORDER_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast("CreateOrder"+_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("createOrder_net_response", "str=" + result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson();
                    OrderData orderBean = mgson.fromJson(result, OrderData.class);
                    if (orderBean.code == 0) {
                        mOrderEditor.putString("ticket_orderno", ""+orderBean.data.orderNo);
                        mOrderEditor.putString("ticket_orderStatus", ""+2);//未支付
                        mOrderEditor.commit();
                        // 身份验证 /人脸识别
                        Intent intent = new Intent(CreateOrderActivity.this, ReadIdCardActivity.class);
                        intent.putExtra(_Constants.BUNDLE_REY, activityTag);
                        intent.putStringArrayListExtra("roomPhotos", mPhonoList);
                        startActivity(intent);
                        finish();
                    }else if(orderBean.code == -1){
                        _LToast(""+orderBean.msg);

                        backLoginPage(CreateOrderActivity.this);
                    }
                    try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }

}

