package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DecimalFormat;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.PayData;
import hk.yijian.botel.bean.response.PayGood;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;

public class PaymentRateActivity extends BaseActivity {

    private ImageView iv_back;
    private ImageView iv_delete;
    private LinearLayout ll_scancode_layout;
    private LinearLayout ll_paygood_layout;
    private ImageView iv_rate_bg_weixin;
    private ImageView iv_rate_bg_ali;
    private TextView tv_pay_total;

    private int totalNO = 0;
    private PayData payBean;
    private PayGood payGoodBean;
    private String orderNoStr;
    private String activityTag;
    private TimeView tv_address_countdown;

    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.PAY_ORDER_API:
                    _Log.d("orderBeanResponse", "msg =" + payBean.data.aliQrCode);
                    try{
                        Glide.with(PaymentRateActivity.this)
                            .load(payBean.data.wxQrCode)
                            .into(iv_rate_bg_ali);
                        Glide.with(PaymentRateActivity.this)
                                .load(payBean.data.aliQrCode)
                                .into(iv_rate_bg_weixin);
                    }catch (Exception e){}
                    mHandler.sendEmptyMessageDelayed(_HandlerCode.PAY_GOOD_API, 6000);
                    break;
                case _HandlerCode.PAY_GOOD_API:
                    mGetPayStatus(orderNoStr);
                    break;
                case _HandlerCode.READ_ROOM_CARD_DATA:
                    mHandler.removeMessages(_HandlerCode.DO_PAYMENT_OK);
                    mHandler.sendEmptyMessageDelayed(_HandlerCode.DO_PAYMENT_OK, 2 * 1000);
                    break;
                case _HandlerCode.DO_PAYMENT_OK:
                    Intent intent = new Intent(PaymentRateActivity.this, OutCardOutTicketActivity.class);
                    intent.putExtra(_Constants.BUNDLE_REY,activityTag);
                    startActivity(intent);
                    finish();
                    break;
                case _HandlerCode.CHANGE_PAY_UI:
                    ll_scancode_layout.setVisibility(View.INVISIBLE);
                    ll_paygood_layout.setVisibility(View.VISIBLE);
                    break;
            }
            super.handleMessage(msg);
        }
    };



    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_public_pay);
        ll_scancode_layout = findViewById(R.id.ll_scancode_layout);
        ll_paygood_layout = findViewById(R.id.ll_paygood_layout);
        tv_pay_total = findViewById(R.id.tv_pay_total);
        iv_rate_bg_weixin = findViewById(R.id.iv_rate_bg_weixin);
        iv_rate_bg_ali = findViewById(R.id.iv_rate_bg_ali);
        //
        totalNO = mShareOrderData.getInt("ticket_total", 0);
        orderNoStr = mShareOrderData.getString("ticket_orderno", "");
        DecimalFormat dfRate = new DecimalFormat("0.00");
        String totalNORate = dfRate.format((double)(totalNO)/100);
        tv_pay_total.setText("总费用：" + totalNORate + "元");
        try {
            String mMac = mSharePublicData.getString("public_mac", "");
            int mHotelId = mSharePublicData.getInt("public_hotelid", 0);
            String mOrderNO = mShareOrderData.getString("ticket_orderno", "");
            String mPay = "{\"orderNo\":\"" + mOrderNO + "\",\"hotelId\":" + mHotelId + ",\"deviceId\":\"" + mMac + "\"}";
            _Log.d("mPay",""+mPay);
            mPostPayCode(mPay);
        }catch (Exception e){}
        //_BaiduTTSUtils.getInstance()._PauseBaiduTTS();
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请选择支付宝或微信支付");
    }

    @Override
    protected void initTitleView() {
        activityTag = getIntent().getStringExtra(_Constants.BUNDLE_REY);
        TitleBarManager.getInstance().init(this);
        ProgressManager.getInstance().init(this);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(PaymentRateActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish", Toast.LENGTH_LONG).show();
            }
        });
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backLoginPage(PaymentRateActivity.this);
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PaymentRateActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
                //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
            }
        });
        switch (activityTag) {
            case _Constants.BUNDLE_TIME_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FOUR);
                TitleBarManager.getInstance().changeTitleText(_Constants.TIME_ROOM);
                ProgressManager.getInstance().setProgressContinueInfo();
                break;
            case _Constants.BUNDLE_CONTINUE_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FOUR);
                TitleBarManager.getInstance().changeTitleText(_Constants.CONTINUE_ROOM);
                ProgressManager.getInstance().setProgressContinueInfo();
                break;
            case _Constants.BUNDLE_IN_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FOUR);
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_IN);
                ProgressManager.getInstance().setProgressInInfo();
                break;
            case _Constants.BUNDLE_OUT_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_TWO);
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_OUT);
                ProgressManager.getInstance().setFourFiveProgressGone();
                ProgressManager.getInstance().setProgressOutInfo();
                break;
            case _Constants.BUNDLE_NET_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FOUR);
                TitleBarManager.getInstance().changeTitleText(_Constants.NET_CARD);
                ProgressManager.getInstance().setProgressNetInfo();
                break;
        }

    }

    @Override
    protected void initClickListener() {


    }


    @Override
    protected void initTouchListener() {

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        mHandler.removeMessages(_HandlerCode.PAY_GOOD_API);
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
    }


    /**
     * 获取支付二维码
     * @param str
     */
    private synchronized void mPostPayCode(String str) {
        _Log.d("payment参数_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.CREATE_PAY_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("payment_net_response","result="+result);

                if (!TextUtils.isEmpty(result)) {
                    try {
                        Gson mgson = new Gson();
                        payBean = mgson.fromJson(result, PayData.class);
                        if (payBean.code == 0) {
                            _Log.d("orderBeanResponse", "msg =" + payBean.msg);
                            _Log.d("orderBeanResponse", "code =" + payBean.code);
                            _Log.d("orderBeanResponse", "results =" + payBean.data.aliQrCode);
                            _Log.d("orderBeanResponse", "results =" + payBean.data.wxQrCode);
                            mHandler.sendEmptyMessage(_HandlerCode.PAY_ORDER_API);
                        }else if(payBean.code == -1){
                            _LToast(""+payBean.msg);
                        }
                    } catch (Exception e) {
                        _LToast(_Constants.TOAST_PARSE_NULL);
                        //e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }


    /**
     * 轮询订单支付状态
     */
    private synchronized void mGetPayStatus(String str) {
        _Log.d("paying_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.QUERY_ORDER_STATUS_URL+str)
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("paying_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    try {
                        Gson mgson = new Gson();
                        payGoodBean = mgson.fromJson(result, PayGood.class);
                        if (payGoodBean.code == 0) {
                            if(payGoodBean.data != null){
                                if (payGoodBean.data.orderStatus == 1) {//TODO 待支付
                                    mHandler.sendEmptyMessageDelayed(_HandlerCode.PAY_GOOD_API, 3000);
                                    _Log.d("orderBeanget_net_response", "msg =待支付");
                                }else if (payGoodBean.data.orderStatus == 3 ) {//TODO 支付完成
                                    /*
                                    for(int i = 0 ; i < payGoodBean.data.guests.size(); i++) {
                                        mOrderEditor.putString("ticket_guestName"+i,
                                        payGoodBean.data.guests.get(i).guestName);
                                        mOrderEditor.commit();
                                    }
                                    */
                                    mHandler.sendEmptyMessage(_HandlerCode.CHANGE_PAY_UI);
                                    mHandler.sendEmptyMessageDelayed(_HandlerCode.DO_PAYMENT_OK, 1500);
                                }
                            }else {
                            }
                        }else if(payGoodBean.code == -1){
                            _LToast(""+payBean.msg);
                        }
                    } catch (Exception e) {
                        _LToast(_Constants.TOAST_PARSE_NULL);
                        //e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }

}
