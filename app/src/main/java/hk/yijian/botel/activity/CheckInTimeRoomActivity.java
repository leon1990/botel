package hk.yijian.botel.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.fragment.TimeFragment;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;

public class CheckInTimeRoomActivity extends BaseActivity {

    private ImageView iv_back;
    private ImageView iv_delete;
    private TimeView tv_address_countdown;

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_time_room);


        if (null == savedInstanceState) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.time_room_container, TimeFragment.newInstance()).commit();
        }
    }

    @Override
    protected void initTitleView() {
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请选择入住房型");

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CheckInTimeRoomActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish", Toast.LENGTH_LONG).show();
            }
        });
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backLoginPage(CheckInTimeRoomActivity.this);

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CheckInTimeRoomActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        TitleBarManager.getInstance().init(this);
        TitleBarManager.getInstance().changeTitleText(_Constants.TIME_ROOM);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_ONE);
        ProgressManager.getInstance().setProgressInInfo();
    }



    @Override
    protected void initClickListener() {

    }

    @Override
    protected void initTouchListener() {

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
    }

}
