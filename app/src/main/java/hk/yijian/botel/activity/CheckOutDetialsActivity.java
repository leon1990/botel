package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.OkCheckOutData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;


public class CheckOutDetialsActivity extends BaseActivity {

    private Button bt_confirm_bill;
    private TextView tv_room_no;
    private TextView tv_room_rate;
    private TextView tv_room_deposit;
    private ImageView iv_back;
    private ImageView iv_delete;
    private OkCheckOutData okCheckOutData;
    private String mRoomNo;
    private String mRoomFee;
    private String mDepositFee;
    private TimeView tv_address_countdown;

    // Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.CHANGE_OUT_CARD_OK:
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String _todayTime = df.format(System.currentTimeMillis());
                    String _checkInTime ;
                    String _checkOutTime ;
                    if(2 == okCheckOutData.data.stayType){
                        _checkInTime = getDayStr(okCheckOutData.data.zstartTime);
                        _checkOutTime = getDayStr(okCheckOutData.data.zendTime);
                    }else {
                        _checkInTime = getDayStr(okCheckOutData.data.startDate);
                        _checkOutTime = getDayStr(okCheckOutData.data.endDate);
                    }
                    _Log.i("time_net_response",
                            "_checkInTime =" +_checkInTime +
                                    "_todayTime =" +_todayTime +
                                    "_checkOutTime"+_checkOutTime);
                    int _compareDate = compare_date(_todayTime,_checkOutTime);
                    if(_compareDate == 1){
                        _LToast("该订单已经过期！该房卡无法退卡.");
                        return;
                    }
                    Intent intent = new Intent(CheckOutDetialsActivity.this, OutCardOutTicketActivity.class);
                    intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_OUT_VALUE);
                    startActivity(intent);
                    finish();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * 比较两个时间大小
     * @param DATE1
     * @param DATE2
     * @return
     */
    private int compare_date(String DATE1, String DATE2) {
        int i = 0;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                _Log.i("dt1在dt2前");
                i = 1;
                return i;
            } else if (dt1.getTime() < dt2.getTime()) {
                _Log.i("dt1在dt2后");
                i = -1;
                return i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }
    /**
     * 将时间转为 yyyy-MM-dd 格式
     */
    private String getDayStr(String dayAfter) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(dayAfter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day);

        String mDayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return mDayAfter;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_check_out_detial);
        bt_confirm_bill = (Button) findViewById(R.id.bt_confirm_bill);
        tv_room_no = (TextView) findViewById(R.id.tv_room_no);
        tv_room_rate = (TextView) findViewById(R.id.tv_room_rate);
        tv_room_deposit = (TextView) findViewById(R.id.tv_room_deposit);

        Bundle bundle = this.getIntent().getExtras();
        mRoomNo = bundle.getString("ticket_roomNo");
        mRoomFee = bundle.getString("ticket_roomFee");
        mDepositFee = bundle.getString("ticket_depositFee");
        tv_room_no.setText("" + mShareOrderData.getString("ticket_roomno",""));
        //tv_room_rate.setText("" + mRoomFee);
        //tv_room_deposit.setText("" + mDepositFee);
    }

    @Override
    protected void initTitleView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CheckOutDetialsActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish",Toast.LENGTH_LONG).show();
            }
        });
        iv_back.setOnClickListener(mBackClickListener);
        iv_delete.setOnClickListener(mDeleteClickListener);
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请确认退房信息");
        TitleBarManager.getInstance().init(this);
        TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_OUT);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_TWO);
        ProgressManager.getInstance().setFourFiveProgressGone();
        ProgressManager.getInstance().setProgressOutInfo();
    }

    @Override
    protected void initClickListener() {
        bt_confirm_bill.setOnClickListener(mConfirmBillListener);

    }


    @Override
    protected void initTouchListener() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
    }

    private final View.OnClickListener mConfirmBillListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try{
                long userId = mShUserData.getLong("userId",0);
                // 读取放卡数据
                _RoomCardUtils.getInstance().GetPermissions();
                String mReadCardData = _RoomCardUtils.getInstance().ReadCardData();
                String mOutCardNo = mReadCardData.substring(0,20);//订单号
                int mRoomNo = Integer.parseInt(mReadCardData.substring(20,26));// 房间号
                String mOutCardId = mReadCardData.substring(26,32);// 房卡id
                _Log.d("退房详情card_net_response","mCardNo="+ mOutCardNo);
                _Log.d("退房详情card_net_response","mRoomNo="+ mRoomNo);
                _Log.d("退房详情card_net_response","mCardId="+ mOutCardId);
                //
                String outCardStr = "{\"orderNo\":\"" + mOutCardNo + "\"," +
                        "\"roomNo\":\"" + mRoomNo + "\"," +
                        "\"userId\":" + userId + "," +
                        "\"cardIds\":[\"" + mReadCardData + "\"]}";

                mPostOutCardInfo(outCardStr);
            }catch (Exception e){}
        }
    };

    private final View.OnClickListener mBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            backLoginPage(CheckOutDetialsActivity.this);
        }
    };
    private final View.OnClickListener mDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            backLoginPage(CheckOutDetialsActivity.this);

        }
    };

    //
    /**
     * @param str
     */
    private synchronized void mPostOutCardInfo(String str) {
        _Log.d("CheckOutdetial参数_net_response", "str=" + str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.ORDER_OUT_CONFIRM_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }
            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("CheckOutdetial_net_response", "result=" + result);
                if (!TextUtils.isEmpty(result)) {

                    Gson mgson = new Gson();
                    okCheckOutData = mgson.fromJson(result, OkCheckOutData.class);
                    if (okCheckOutData.code == 0) {

                        mOrderEditor.putString("ticket_roomno",""+okCheckOutData.data.roomNo);
                        mOrderEditor.putString("ticket_orderno",""+okCheckOutData.data.orderNo);
                        mOrderEditor.putString("ticket_roomcardno",""+okCheckOutData.data.roomNo);
                        //mOrderEditor.putString("ticket_roomcardno",
                        // ""+okCheckOutData.data.guests.get(0).roomCardId);
                        //mOrderEditor.putString("ticket_guestName",
                        // ""+okCheckOutData.data.guests.get(0).guestName);
                        mOrderEditor.putString("ticket_hotelName",""+okCheckOutData.data.hotelName);
                        mOrderEditor.putString("ticket_createtime",""+okCheckOutData.data.startDate);
                        mOrderEditor.putString("ticket_orderNo",""+okCheckOutData.data.orderNo);
                        mOrderEditor.putString("ticket_roomTypeName",""+okCheckOutData.data.roomTypeName);
                        mOrderEditor.putInt("ticket_depositFee", okCheckOutData.data.depositFee);
                        mOrderEditor.commit();
                        // stayType = 1   stayType = 2
                        if(2 == okCheckOutData.data.stayType){
                            mOrderEditor.putString("ticket_startDate", ""+okCheckOutData.data.zstartTime);
                            mOrderEditor.putString("ticket_endDate", ""+okCheckOutData.data.zendTime);
                            mOrderEditor.commit();
                        }else {
                            mOrderEditor.putString("ticket_startDate", ""+okCheckOutData.data.startDate);
                            mOrderEditor.putString("ticket_endDate", ""+okCheckOutData.data.endDate);
                            mOrderEditor.commit();
                        }

                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_OUT_CARD_OK,500);
                        try {
                        } catch (Exception e) {
                            _LToast("Exception");
                        }

                    }else {
                        _LToast(""+okCheckOutData.msg);
                        backLoginPage(CheckOutDetialsActivity.this);
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                    backLoginPage(CheckOutDetialsActivity.this);
                }
            }
        });
    }

    /*
    // 解析数据
    private void parseUserData() {
        String strContent = getJson("outcard.json");
        if (!TextUtils.isEmpty(strContent)) {
            try {
                Gson mgson = new Gson();
                outCardBean = mgson.fromJson(strContent, OutCardBean.class);
                _Log.d("outcardguestBean", "msg =" + outCardBean.msg);
                _Log.d("outcardguestBean", "code =" + outCardBean.code);
                _Log.d("outcardguestBean", "results =" + outCardBean.netdata.size());
                _Log.d("outcardguestBean", "results =" + outCardBean.netdata.get(0).howmany);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getJson(String filename) {
        InputStream mInputStream = null;
        String resultString = "";
        try {
            mInputStream = this.getAssets().open(filename);
            byte[] buffer = new byte[mInputStream.available()];
            mInputStream.read(buffer);
            resultString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                mInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString.toString();
    }
    */

}
