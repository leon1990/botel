package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.linkface.liveness.LFLivenessSDK;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.LFLiveBeanData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.faceUI.linkface.FaceOverlapFragment;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._Log;
import hk.yijian.linkface.data.LFConstants;
import hk.yijian.linkface.util.LFMediaPlayer;
import hk.yijian.linkface.util.LFReturnResult;
import hk.yijian.linkface.util.LFSensorManager;
import hk.yijian.linkface.util.LivenessUtils;
import hk.yijian.linkface.view.CircleTimeView;
import hk.yijian.linkface.view.TimeViewContoller;

public class RecLinkFaceActivity extends BaseActivity {

    //---------------link face------------------------
    public final static String LF_TAG = "_net_response_face";
    public static final int RESULT_CREATE_HANDLE_ERROR = 1001;//加载库文件出现错误
    //无法访问摄像头，没有权限或摄像头被占用
    public static final int RESULT_CAMERA_ERROR_NOPRERMISSION_OR_USED = 2;
    public static final int RESULT_INTERNAL_ERROR = 3;//内部错误
    public static final int RESULT_SDK_INIT_FAIL_APPLICATION_ID_ERROR = 4;//包名绑定错误
    public static final int RESULT_SDK_INIT_FAIL_OUT_OF_DATE = 5;//License过期
    //List<String> list = new ArrayList<String>();
    //
    /**
     * 传入活体检测的动作序列
     */
    public static final String EXTRA_MOTION_SEQUENCE = "motionSequence";
    /**
     * 获取info信息
     */
    public static final String EXTRA_INFO = "info";
    /**
     * 是否打开语音提示
     */
    public static final String SOUND_NOTICE = "soundNotice";
    /**
     *  输出类型
     */
    public static final String OUTTYPE = "outType";
    /**
     * 设置复杂度
     */
    public static final String COMPLEXITY = "complexity";
    /**
     * 设置是否返回图片结果
     */
    public static final String KEY_DETECT_IMAGE_RESULT = "key_detect_image_result";
    /**
     * 设置是否返回video结果,只有video模式才会返回
     */
    public static final String KEY_DETECT_VIDEO_RESULT = "key_detect_video_result";
    public static final String LIVENESS_FILE_NAME = "livenessResult";
    public static final String LIVENESS_VIDEO_NAME = "livenessVideoResult.mp4";
    /**
     * 传入保存结果的文件路径
     */
    public static String EXTRA_RESULT_PATH = "resultPath";
    public static String SEQUENCE_JSON = "sequence_json";
    public static String OUTPUT_TYPE = "";

    private ViewGroup mVGBottomDots;
    private CircleTimeView mTimeView;
    private LinearLayout mLlytTitle;

    private AlertDialog  mDialog;

    private boolean mIsStart = false, mSoundNoticeOrNot = true;
    private FaceOverlapFragment mFragment;
    //
    public SparseArray<String> IDCardInfo;
    public int retNo;
    public Bitmap bimg;

    private TimeViewContoller mTimeViewContoller;
    private LFMediaPlayer mMediaPlayer = new LFMediaPlayer();
    private String[] mDetectList = null;
    private LFLivenessSDK.LFLivenessMotion[] mMotionList = null;
    private LFSensorManager mSensorManger;
    private int mCurrentDetectStep = 0;
    //---------------link face------------------------
    //init okhttp
    public static final OkHttpClient mOkHttpClient = new OkHttpClient();
    private static final int CURRENT_ANIMATION = -1;

    private Context mContext;
    private Drawable drawable;
    private String IDNoStr;
    private String nameStr;
    private String sexStr;
    private String nationStr;
    private String birthDateStr;
    private String addressStr;
    private String licenseStr;
    private String startStr;
    private String endStr;

    private String mRoomNO;
    private String mOrderNO;
    private int doNetTime = 0;
    private int sex = 0;
    private File fileIdcard;
    private File fileProto;
    private File fileLive;
    private SparseArray<String> LFmap;
    private String activityTag;
    //
    private ImageView iv_back;
    private ImageView iv_delete;
    private ImageView iv_linkface_line;
    private FrameLayout ll_scan_layout;
    private  Animation verticalAnimation;
    //
    private LinearLayout ll_success_layout;
    private LinearLayout ll_error_layout;
    //
    private ImageView iv_id_card_img;
    private TextView iv_id_card_name;
    private TextView iv_id_card_no;
    private TextView tv_text_info;
    //
    private FrameLayout fl_linkface_layout;
    private TimeView tv_address_countdown;

    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                //Success
                case _HandlerCode.CHANGE_UI_SUCCESS_MESSAGE:
                    String payStatus = mShareOrderData.getString("ticket_orderStatus", "").trim();
                    if(payStatus.equals("3".trim())){
                        Intent intent = new Intent(RecLinkFaceActivity.this, OutCardOutTicketActivity.class);
                        intent.putExtra(_Constants.BUNDLE_REY, activityTag);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(RecLinkFaceActivity.this, PaymentRateActivity.class);
                        intent.putExtra(_Constants.BUNDLE_REY, activityTag);
                        startActivity(intent);
                        finish();
                    }
                    break;
                case _HandlerCode.CHANGEUI_SUCECESS_MESSAGE:
                    mSuccessFaceUI();
                    break;
                //error
                case _HandlerCode.CHANGE_UI_ERROR_MESSAGE:
                    showDialog();
                    break;
                case _HandlerCode.CHANGEUI_ERROR_MESSAGE:
                    mFailFaceUI();
                    break;

            }
            super.handleMessage(msg);
        }
    };


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        int left = ll_scan_layout.getLeft();
        int right = ll_scan_layout.getRight();
        int top = ll_scan_layout.getTop();
        int bottom = ll_scan_layout.getBottom();
        int height = ll_scan_layout.getHeight()/2;
        verticalAnimation = new TranslateAnimation(left, left,top-height, bottom-height);
        verticalAnimation.setDuration(3500);
        verticalAnimation.setRepeatCount(Animation.INFINITE);
        verticalAnimation.setRepeatMode(Animation.REVERSE);
        iv_linkface_line.setAnimation(verticalAnimation);
        verticalAnimation.startNow();
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_read_face);
        mContext = this;
        ll_scan_layout  = (FrameLayout) findViewById(R.id.ll_scan_layout);
        iv_linkface_line  = (ImageView) findViewById(R.id.iv_linkface_line);
        ll_success_layout = (LinearLayout) findViewById(R.id.ll_success_layout);
        ll_error_layout = (LinearLayout) findViewById(R.id.ll_error_layout);
        //
        iv_id_card_img = findViewById(R.id.iv_id_card_img);
        iv_id_card_name = findViewById(R.id.iv_id_card_name);
        iv_id_card_no = findViewById(R.id.iv_id_card_no);
        tv_text_info = findViewById(R.id.tv_text_info);
        //
        fl_linkface_layout = findViewById(R.id.fl_linkface_layout);

        activityTag = getIntent().getStringExtra(_Constants.BUNDLE_REY);
        mOrderNO = mShareOrderData.getString("ticket_orderno","");
        mRoomNO = mShareOrderData.getString("ticket_roomno", "");
        LFmap = new SparseArray<String>();
        // 获取身份证信息
        IDCardInfo = _IDCardUtils.getInstance().readIDCardInfo(pkName);
        retNo = Integer.parseInt(IDCardInfo.get(0));
        if(retNo == 0x90){
            bimg = _IDCardUtils.getInstance().readIDCardImg();
            drawable = new BitmapDrawable(bimg);
            nameStr = IDCardInfo.get(1);
            sexStr = IDCardInfo.get(2);
            nationStr = IDCardInfo.get(3);
            birthDateStr = IDCardInfo.get(4);
            addressStr = IDCardInfo.get(5);
            IDNoStr = IDCardInfo.get(6);
            licenseStr = IDCardInfo.get(7);
            startStr = IDCardInfo.get(8);
            endStr = IDCardInfo.get(9);

            if(IDNoStr.trim().equals("")){
                Intent intent = new Intent(RecLinkFaceActivity.this,ReadIdCardActivity.class);
                intent.putExtra(_Constants.BUNDLE_REY,activityTag);
                startActivity(intent);
                finish();
            }else {
                //TODO
                _Log.d("_net_response_face_idcard", "IDNoStr= " + IDNoStr);

                File appDir = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-idcard.png");
                saveIdCardImage(bimg,appDir);//TODO 提交图片bimg
                inilinkFaceView();

            }

        }else {
            _LToast("获取身份证信息失败！");
            Intent intent = new Intent(RecLinkFaceActivity.this,ReadIdCardActivity.class);
            intent.putExtra(_Constants.BUNDLE_REY,activityTag);
            startActivity(intent);
            finish();
            //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
        }
    }

    @Override
    protected void initTitleView() {
        TitleBarManager.getInstance().init(this);
        ProgressManager.getInstance().init(this);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(RecLinkFaceActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish", Toast.LENGTH_LONG).show();
            }
        });

        switch (activityTag) {
            case _Constants.BUNDLE_TIME_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                TitleBarManager.getInstance().changeTitleText(_Constants.TIME_ROOM);
                ProgressManager.getInstance().setProgressContinueInfo();
                break;
            case _Constants.BUNDLE_CONTINUE_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                TitleBarManager.getInstance().changeTitleText(_Constants.CONTINUE_ROOM);
                ProgressManager.getInstance().setProgressContinueInfo();
                break;
            case _Constants.BUNDLE_IN_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_IN);
                ProgressManager.getInstance().setProgressInInfo();
                break;
            case _Constants.BUNDLE_NET_VALUE:
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                TitleBarManager.getInstance().changeTitleText(_Constants.NET_CARD);
                ProgressManager.getInstance().setProgressNetInfo();
                break;
        }
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backLoginPage(RecLinkFaceActivity.this);

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecLinkFaceActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        try{
            if (isDialogShowing()) {
                mMediaPlayer.stop();
            }
            mSensorManger.registerListener(mSensorEventListener);
            if (mIsStart) {
                //showDialog();
                showIndicateView();
                mFragment.registerLivenessDetectCallback(mLivenessListener);
                restartAnimationAndLiveness();
            }

        }catch (Exception e){}
    }

    @Override
    protected void initClickListener() {

    }

    @Override
    protected void initTouchListener() {
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            mSensorManger.unregisterListener(mSensorEventListener);
            mMediaPlayer.stop();
        }catch (Exception e){}

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        verticalAnimation.cancel();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();

        if (mFragment != null) {
            mFragment.registerLivenessDetectCallback(null);
            mFragment = null;
        }
        if (mTimeViewContoller != null) {
            mTimeViewContoller.setCallBack(null);
            mTimeViewContoller = null;
        }
        try {
            mMediaPlayer.release();
            mMediaPlayer = null;
        } catch (Exception e) {}
    }



    /**
     *
     */
    private void inilinkFaceView() {
        //-------------linkface------------
        mSensorManger = new LFSensorManager(this);
        Bundle bundle = getIntent().getExtras();
        mDetectList = LivenessUtils.getDetectActionOrder(bundle.getString(RecLinkFaceActivity.EXTRA_MOTION_SEQUENCE));
        mMotionList = LivenessUtils.getMctionOrder(bundle.getString(RecLinkFaceActivity.EXTRA_MOTION_SEQUENCE));
        EXTRA_RESULT_PATH = bundle.getString(RecLinkFaceActivity.EXTRA_RESULT_PATH);

        if (EXTRA_RESULT_PATH == null) {
            EXTRA_RESULT_PATH = Environment
                    .getExternalStorageDirectory().getAbsolutePath()
                    + File.separator
                    + "liveness" + File.separator;
        }
        File livenessFolder = new File(EXTRA_RESULT_PATH);
        if (!livenessFolder.exists()) {
            livenessFolder.mkdirs();
        }
        OUTPUT_TYPE = bundle.getString(LFConstants.OUTTYPE);
        mSoundNoticeOrNot = bundle.getBoolean(RecLinkFaceActivity.SOUND_NOTICE);

        mVGBottomDots = (ViewGroup) findViewById(R.id.viewGroup);
        if (mDetectList.length >= 1) {
            for (int i = 0; i < mDetectList.length; i++) {
                TextView tvBottomCircle = new TextView(this);
                tvBottomCircle.setBackgroundResource(R.drawable.drawable_liveness_detect_bottom_cicle_bg_selector);
                tvBottomCircle.setEnabled(true);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dp2px(8),
                        dp2px(8));
                layoutParams.leftMargin = dp2px(8);
                mVGBottomDots.addView(tvBottomCircle, layoutParams);
            }
        }
        initLfView();

        mLlytTitle = (LinearLayout) findViewById(R.id.noticeLinearLayout);
        mLlytTitle.setVisibility(View.INVISIBLE);
    }


    private void initLfView() {
        mTimeView = (CircleTimeView) findViewById(R.id.time_view);
        mTimeViewContoller = new TimeViewContoller(mTimeView);
        mFragment = (FaceOverlapFragment) getFragmentManager()
                .findFragmentById(R.id.overlapFragment);
        mFragment.registerLivenessDetectCallback(mLivenessListener);
    }

    private void onLivenessDetectCallBack(final int value, final int status, final byte[] livenessEncryptResult,
                                          final byte[] videoResult, final LFLivenessSDK.LFLivenessImageResult[] imageResult) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCurrentDetectStep = status + 1;
                if (value == LFLivenessSDK.LFLivenessMotion.BLINK.getValue()) {
                    updateUi(R.string.note_blink, R.raw.raw_liveness_detect_blink, status + 1);
                    Log.i(LF_TAG, "onLivenessDetect" + "***value***" + value);
                } else if (value == LFLivenessSDK.LFLivenessMotion.MOUTH.getValue()) {
                    updateUi(R.string.note_mouth, R.raw.raw_liveness_detect_mouth, status + 1);
                    Log.i(LF_TAG, "onLivenessDetect" + "***value***" + value);
                } else if (value == LFLivenessSDK.LFLivenessMotion.NOD.getValue()) {
                    updateUi(R.string.note_nod, R.raw.raw_liveness_detect_nod, status + 1);
                    Log.i(LF_TAG, "onLivenessDetect" + "***value***" + value);
                } else if (value == LFLivenessSDK.LFLivenessMotion.YAW.getValue()) {
                    updateUi(R.string.note_yaw, R.raw.raw_liveness_detect_yaw, status + 1);
                    Log.i(LF_TAG, "onLivenessDetect" + "***value***" + value);
                } else if (value == LFConstants.LIVENESS_SUCCESS) {
                    updateTheLastStepUI(mVGBottomDots);
                    saveFinalEncrytFile(livenessEncryptResult, videoResult, imageResult);
                    Log.i(LF_TAG, "onLivenessDetect" + "***value***" + value);
                } else if (value == LFConstants.DETECT_BEGIN_WAIT) {
                    showDetectWaitUI();
                } else if (value == LFConstants.DETECT_END_WAIT) {
                    removeDetectWaitUI();
                } else if (value == LFConstants.LIVENESS_TIME_OUT) {
                    //showDialog();
                    mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                    mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                    mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_ERROR_MESSAGE);
                    mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_ERROR_MESSAGE,2000);

                    LivenessUtils.saveFile(livenessEncryptResult, EXTRA_RESULT_PATH, LIVENESS_FILE_NAME);
                }
                /*
                else if (value == LFConstants.LIVENESS_TRACKING_MISSED) {
                    showDialog();
                    _LToast("LIVENESS_TRACKING_MISSED");
                    LivenessUtils.saveFile(livenessEncryptResult, EXTRA_RESULT_PATH, LIVENESS_FILE_NAME);
                }
                */
            }
        });
    }

    private FaceOverlapFragment.OnLivenessCallBack mLivenessListener = new FaceOverlapFragment.OnLivenessCallBack() {
        @Override
        public void onLivenessDetect(final int value, final int status, byte[] livenessEncryptResult,
                                     byte[] videoResult, LFLivenessSDK.LFLivenessImageResult[] imageResult) {
            onLivenessDetectCallBack(value, status, livenessEncryptResult, videoResult, imageResult);
            Log.i(LF_TAG, "onLivenessDetect" + "***value***" + value);
        }
    };

    SensorEventListener mSensorEventListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            mFragment.addSequentialInfo(event.sensor.getType(), event.values);
        }
    };

    private void removeDetectWaitUI() {
        setLivenessState(false);
        mLlytTitle.setVisibility(View.VISIBLE);
        onLivenessDetectCallBack(mMotionList[0].getValue(), 0, null, null, null);
    }

    private void setLivenessState(boolean pause) {
        if (null == mFragment) {
            return;
        }
        if (pause) {
            mFragment.stopLiveness();
        } else {
            mFragment.startLiveness();
        }
    }

    private void showDetectWaitUI() {
        mIsStart = true;
        if (mTimeViewContoller != null) {
            mTimeViewContoller.setCallBack(null);
        }
        mLlytTitle.setVisibility(View.INVISIBLE);
    }

    private void hideIndicateView() {
        if (mVGBottomDots != null) {
            mVGBottomDots.setVisibility(View.GONE);
        }

    }

    private void showIndicateView() {
        if (mVGBottomDots != null) {
            mVGBottomDots.setVisibility(View.GONE);//VISIBLE
        }
    }

    private void hideTimeContoller() {
        if (mTimeViewContoller != null) {
            mTimeViewContoller.hide();
        }
    }

    private void restartAnimationAndLiveness() {
        setLivenessState(false);
        LivenessUtils.deleteFiles(EXTRA_RESULT_PATH);
        if (mDetectList.length >= 1) {
            View childAt = mVGBottomDots.getChildAt(0);
            childAt.setEnabled(false);
        }
        startAnimation(CURRENT_ANIMATION);
        mMediaPlayer.release();
        playSoundNotice(mCurrentDetectStep);
    }

    private void mCancelRecording() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideTimeContoller();
                hideIndicateView();
            }
        });


        mMediaPlayer.release();

        try{
            mSensorManger.unregisterListener(mSensorEventListener);
            mMediaPlayer.stop();
        }catch (Exception e){}



        tv_address_countdown.mStopTime();
        verticalAnimation.cancel();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();

        if (mFragment != null) {
            mFragment.registerLivenessDetectCallback(null);
            mFragment = null;
        }
        if (mTimeViewContoller != null) {
            mTimeViewContoller.setCallBack(null);
            mTimeViewContoller = null;
        }
        try {
            mMediaPlayer.release();
            mMediaPlayer = null;
        } catch (Exception e) {}
    }
    /**
     * play Sound
     * @param step
     */
    private void playSoundNotice(int step) {

        if (step > 0) {
            if (mDetectList[step - 1].equalsIgnoreCase(getString(R.string.blink))) {
                if (mSoundNoticeOrNot) {
                    //_BaiduTTSUtils.getInstance()._OpenBaiduTTS("请眨眨眼");
                    //mMediaPlayer.setMediaSource(mContext, "linkface_notice_blink.mp3", true);
                }
            } else if (mDetectList[step - 1].equalsIgnoreCase(getString(R.string.nod))) {
                if (mSoundNoticeOrNot) {
                    //_BaiduTTSUtils.getInstance()._OpenBaiduTTS("请点点头");
                    //mMediaPlayer.setMediaSource(mContext, "linkface_notice_nod.mp3", true);
                }
            } else if (mDetectList[step - 1].equalsIgnoreCase(getString(R.string.mouth))) {
                if (mSoundNoticeOrNot) {
                    //_BaiduTTSUtils.getInstance()._OpenBaiduTTS("请张张嘴");
                    //mMediaPlayer.setMediaSource(mContext, "linkface_notice_mouth.mp3", true);
                }
            } else if (mDetectList[step - 1].equalsIgnoreCase(getString(R.string.yaw))) {
                if (mSoundNoticeOrNot) {
                    //_BaiduTTSUtils.getInstance()._OpenBaiduTTS("请摇摇头");
                    //mMediaPlayer.setMediaSource(mContext, "linkface_notice_yaw.mp3", true);
                }
            }
        }
    }

    private void startAnimation(int animation) {
        if (animation != CURRENT_ANIMATION) {
            if (isDialogShowing()) {
                return;
            }
        }
        mTimeViewContoller.start();
        mTimeViewContoller.setCallBack(new TimeViewContoller.CallBack() {
            @Override
            public void onTimeEnd() {
                mFragment.onTimeEnd();
            }
        });
    }

    private boolean isDialogShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void onErrorHappen(int resultCode) {
        setResult(resultCode);
        _Log.d("_net_response_face_onErrorHappen",""+resultCode);

        Toast.makeText(getApplicationContext(),"Sorry...人脸识别过期了...",Toast.LENGTH_LONG).show();
        finish();
    }

    /**
     * 人脸识别失败弹窗
     */
    private void showDialog() {

        if (isDialogShowing()) {
            return;
        }
        if (mDetectList.length >= 1) {
            for (int i = 0; i < mDetectList.length; i++) {
                View childAt = mVGBottomDots.getChildAt(i);
                if (childAt != null) {
                    childAt.setEnabled(true);
                }
            }
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideTimeContoller();
                hideIndicateView();
            }
        });

        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            final View view_custom = this.getLayoutInflater().inflate(R.layout.linkface_liveness_dialog, null,false);
            RelativeLayout iv_dialog_yes = view_custom.findViewById(R.id.iv_dialog_yes);
            ImageView iv_dialog_no = view_custom.findViewById(R.id.iv_dialog_no);
            builder.setView(view_custom);
            builder.setCancelable(false);
            mDialog = builder.create();
            mDialog.getWindow().setDimAmount((float) 0.8);//设置昏暗度为0
            mDialog.show();

            Window window = mDialog.getWindow();
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = 335;
            params.height = 360 ;
            window.setAttributes(params);
            iv_dialog_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                    mAgainFaceUI();

                    showIndicateView();
                    mFragment.registerLivenessDetectCallback(mLivenessListener);
                    restartAnimationAndLiveness();

                    _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请眨眨眼");

                }
            });
            iv_dialog_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                    onErrorHappen(RESULT_CANCELED);
                    backLoginPage(RecLinkFaceActivity.this);
                }
            });
        }catch (Exception e){}

        if (((Activity) mContext).isFinishing()) {
            return;
        }
        mMediaPlayer.release();
    }

    private int dp2px(float dpValue) {
        int densityDpi = this.getResources().getDisplayMetrics().densityDpi;
        return (int) (dpValue * (densityDpi / 160));
    }


    /**
     *  TODO 上传身份证信息
     * @param livenessEncryptResult
     * @param videoResult
     * @param imageResult
     */
    public void saveFinalEncrytFile(byte[] livenessEncryptResult, byte[] videoResult,
                                    LFLivenessSDK.LFLivenessImageResult[] imageResult) {

        //Intent intent = new Intent();
        LFReturnResult returnResult = new LFReturnResult();
        boolean isReturnImage = getIntent().getBooleanExtra(KEY_DETECT_IMAGE_RESULT, false);
        if (isReturnImage) {
            returnResult.setImageResults(imageResult);
        }


        _Log.d("_net_response_face_LipLanguage_", "videoResult=" + videoResult);
        if (videoResult != null) {
            //videopath = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-data.mp4");
            //saveVideo(videoResult, videopath);
            //

            //String videopath = "/data/data/hk.yijian.botel/cache/";
            //fileAbsolutePath = LivenessUtils.saveFile(videoResult,videopath,"data-video.mp4");
            //boolean isReturnVideo = getIntent().getBooleanExtra(KEY_DETECT_VIDEO_RESULT, false);
            //if (isReturnVideo) {
            //   returnResult.setVideoResultPath(fileAbsolutePath);
            // }
        }
        //intent.putExtra(KEY_DETECT_IMAGE_RESULT, returnResult);
        if (livenessEncryptResult != null) {
            //LivenessUtils.saveFile(livenessEncryptResult, EXTRA_RESULT_PATH, LIVENESS_FILE_NAME);
            //setResult(RESULT_OK, intent);
            File fileByte = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-data.proto");
            createFileWithByte(livenessEncryptResult, fileByte);
            if (imageResult != null) {
                int size = imageResult.length;
                if (size > 0) {
                    LFLivenessSDK.LFLivenessImageResult imageResultImg = imageResult[0];
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(imageResultImg.image,
                            0, imageResultImg.image.length);

                    File appDir = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-live.png");
                    saveLiveImage(imageBitmap,appDir);
                }
            }
        }
        //TODO net post
        String mNetorderNO = mShareOrderData.getString("netorderno", "");
        if (sexStr.equals("男")) {
            sex = 1;
        } else if (sexStr.equals("女")) {
            sex = 2;
        }
        fileIdcard = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-idcard.png");
        fileProto = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-data.proto");
        fileLive = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-live.png");
        LFmap.put(0, mDeviceId.trim());
        LFmap.put(1, nameStr.trim());
        LFmap.put(2, nationStr.trim());
        LFmap.put(3, "" + sex);
        LFmap.put(4, IDNoStr.trim());
        LFmap.put(5, birthDateStr.trim());
        LFmap.put(6, addressStr.trim());
        LFmap.put(7, licenseStr.trim());
        LFmap.put(8, startStr.trim());
        LFmap.put(9, endStr.trim());
        LFmap.put(10, mRoomNO.trim());
        LFmap.put(11, mOrderNO.trim());
        LFmap.put(12, "LINKFACE");
        LFmap.put(13, "0");

        /**
         *
         * 解决方案：人脸识别这块，直接上传人脸数据到linkface ，获取比对率.
         */
        mOrderEditor.putString("ticket_guestName",nameStr.trim());
        mOrderEditor.commit();
        mUpLoadCardInfoLinkFace(fileIdcard,fileProto);

    }

    public static final String POST_URL = "https://cloudapi.linkface.cn/identity/liveness_watermark_verification";


    /**
     * save idCard img
     * @param bmp
     * @param file
     */
    public void saveIdCardImage(Bitmap bmp,File file) {
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 0, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveLiveImage(Bitmap bmp,File file) {
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 30, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFileWithByte(byte[] bytes, File file) {
        FileOutputStream outputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            bufferedOutputStream.write(bytes);
            bufferedOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    private String getStringWithID(int id) {
        return getResources().getString(id);
    }

    private void updateTheLastStepUI(ViewGroup viewGroup) {
        mMediaPlayer.release();
    }

    private void updateUi(int stringId, int animationId, int number) {
        if (animationId != 0) {
            startAnimation(animationId);
        }
        if (number - 2 >= 0) {
            View childAt = mVGBottomDots.getChildAt(number - 2);
            childAt.setEnabled(false);
        }
        playSoundNotice(number);
    }

    //以最省内存的方式读取本地资源的图片
    public static Bitmap readBitMap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        //获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    private void saveVideo(byte[] bytes, File file) {
        FileOutputStream outputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            bufferedOutputStream.write(bytes);
            bufferedOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    private Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * 认证成功的UI
     */
    private void mSuccessFaceUI() {
        _BaiduTTSUtils.getInstance()._PauseBaiduTTS();
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("验证成功");

        if(isDialogShowing()){
            mDialog.dismiss();
        }
        fl_linkface_layout.setVisibility(View.INVISIBLE);
        tv_text_info.setVisibility(View.INVISIBLE);
        //
        ll_success_layout.setVisibility(View.VISIBLE);
        iv_id_card_img.setBackground(drawable);
        String mName = nameStr;
        mName = mName.substring(0,1) + "***";
        String idNo = IDNoStr;
        idNo = idNo.substring(0,4) + "**********" + idNo.substring(14,18);
        iv_id_card_name.setText(mName);
        iv_id_card_no.setText(idNo);
    }
    /**
     * 认证失败的UI
     */
    private void mFailFaceUI() {
        _BaiduTTSUtils.getInstance()._PauseBaiduTTS();
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("验证失败，请重新验证");
        fl_linkface_layout.setVisibility(View.INVISIBLE);
        tv_text_info.setVisibility(View.INVISIBLE);
        ll_error_layout.setVisibility(View.VISIBLE);
    }
    /**
     * 重新认证的UI
     */
    private void mAgainFaceUI() {
        fl_linkface_layout.setVisibility(View.VISIBLE);
        tv_text_info.setVisibility(View.VISIBLE);
        ll_error_layout.setVisibility(View.INVISIBLE);
    }


    /**
     * okhttp
     * 上传住客身份信息
     */
    private synchronized void mUpLoadCardInfoLinkFace(File idcard, File proto) {

        RequestBody _fileIdcard = RequestBody.create(MediaType.parse("application/octet-stream"), idcard);
        RequestBody _fileProto = RequestBody.create(MediaType.parse("application/octet-stream"), proto);

        //RequestBody formBody = new FormEncodingBuilder()
        RequestBody multipartBuilder = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                //.setType(MultipartBody.FORM)
                .addFormDataPart("api_id", "eae5237eaae34ddebb3a21b08853d0c5")
                .addFormDataPart("api_secret", "65c4f953007a4de2850e5490f3912a05")
                .addFormDataPart("watermark_picture_file", IDNoStr.trim() + "-idcard.png", _fileIdcard)
                .addFormDataPart("liveness_data_file", IDNoStr.trim() + "-data.proto", _fileProto)
                .build();

        Request request = new Request.Builder()
                //.header("Authorization", "Client-ID " + "...")
                .header("User-Agent", "OkHttp Headers.java")
                .addHeader("Accept", "application/json; q=0.5")
                .addHeader("Accept", "application/vnd.OneRoom.v3+json")
                .url(POST_URL)
                .post(multipartBuilder)
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("_net_response_face_net_response", "result=" + result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson();
                    LFLiveBeanData lfLiveBean = mgson.fromJson(result, LFLiveBeanData.class);
                    if (lfLiveBean.status.equals("OK")&&lfLiveBean.verify_score>=(0.6)) {
                        mUpLoadCardInfo(LFmap, fileIdcard, fileProto, fileLive);
                    } else if(lfLiveBean.verify_score < (0.6)){
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                        mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_ERROR_MESSAGE);
                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_ERROR_MESSAGE,2000);
                    }
                }
            }
        });
    }

    /**
     * okhttp
     *
     * 上传住客身份信息
     */
    private synchronized void mUpLoadCardInfo(SparseArray<String> LFmap, File file01, File file02, File file03) {
        for(int i = 0, nsize = LFmap.size(); i < nsize; i++) {
            Object obj = LFmap.valueAt(i);
            _Log.d("_net_response_face_LinkFace_upload", "str=" + obj);
        }
        RequestBody fileIdcard = RequestBody.create(MediaType.parse("application/octet-stream"), file01);
        RequestBody fileProto = RequestBody.create(MediaType.parse("application/octet-stream"), file02);
        RequestBody fileLive = RequestBody.create(MediaType.parse("application/octet-stream"), file03);
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("deviceId", "" + LFmap.get(0))
                .addFormDataPart("name", "" + LFmap.get(1))
                .addFormDataPart("nation", "" + LFmap.get(2))
                .addFormDataPart("gender", "" + LFmap.get(3))
                .addFormDataPart("idCardNo", "" + LFmap.get(4))
                .addFormDataPart("dayOfBirth", "" + LFmap.get(5))
                .addFormDataPart("address", "" + LFmap.get(6))
                .addFormDataPart("signOrg", "" + LFmap.get(7))
                .addFormDataPart("startDate", "" + LFmap.get(8))
                .addFormDataPart("endDate", "" + LFmap.get(9))
                .addFormDataPart("roomNo", "" + LFmap.get(10))
                .addFormDataPart("orderNo", "" + LFmap.get(11))
                .addFormDataPart("faceType", "" + LFmap.get(12))
                .addFormDataPart("dataSim", "" + LFmap.get(13))
                .addFormDataPart("headImg", LFmap.get(4) + "-idcard.png", fileIdcard)
                .addFormDataPart("liveImg", LFmap.get(4) + "-live.png", fileLive)
                .addFormDataPart("liveProtoBuf", LFmap.get(4) + "-data.proto", fileProto)
                .build();

        Request request = new Request.Builder()
                //.header("Authorization", "Client-ID " + "...")
                .header("User-Agent", "OkHttp Headers.java")
                .addHeader("Accept", "application/json; q=0.5")
                .addHeader("Accept", "application/vnd.OneRoom.v3+json")
                .url(_ConstantsAPI.FACE_TEST_URL)
                .post(requestBody)
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("_net_response_face_upload", "result=" + result);
                if (!TextUtils.isEmpty(result)) {
                    // 还剩几个人需要身份验证
                    int TestTimes = mSharePublicData.getInt("testTimes", 0);
                    if(TestTimes>1){
                        TestTimes = TestTimes -1;
                        mPublicEditor.putInt("testTimes", TestTimes);
                        mPublicEditor.putInt("backfacecode", 200);
                        mPublicEditor.commit();//editor.apply();
                        _LToast("还剩"+TestTimes+"个人需要身份验证！！");
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(RecLinkFaceActivity.this,ReadIdCardActivity.class);
                                intent.putExtra(_Constants.BUNDLE_REY,activityTag);
                                startActivity(intent);
                                finish();
                                //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                            }
                        },500);
                    }else {
                        mPublicEditor.putInt("backfacecode", 0);
                        mPublicEditor.commit();
                        mUserEditor.putString("idcardNO", "");// 清除保存的身份证姓名
                        mUserEditor.commit();

                        mCancelRecording();
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                        mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_SUCECESS_MESSAGE);
                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE,2000);
                    }
                }
            }
        });
    }

}


