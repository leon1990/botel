package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.OutCardData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;
import hk.yijian.hardware._UsbPrintUtils;
import hk.yijian.ykprinter._YKPrintUtils;

public class OutCardOutTicketActivity extends BaseActivity {

    private ImageView iv_out_card;
    private TextView tv_outcard_get_info;
    private ImageView iv_back;
    private ImageView iv_detele;

    private AnimationDrawable outCardAD;
    private String roomCardInfo;
    private OutCardData outCardBean;
    private SparseArray<String> mTicket;
    private String ticket_roomTypeName ;
    private String ticket_roomNo;
    private String ticket_createtime;
    private String ticket_orderNo;
    private int ticket_roomFee;
    private int ticket_depositFee;
    private String ticket_startDate;
    private String ticket_endDate;
    private String ticket_guestName;
    private String ticket_mHotelName ;
    private String ticket_mRoomFee;
    private String ticket_mDepositFee;
    private int _typeCode;
    private int _cardNo;
    private Context context;
    private String activityTag;

    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @SuppressLint("SetTextI18n")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.OUT_CARD_VIEW: //动画 UI
                    outCardAnimation();
                    if(activityTag.equals(_Constants.BUNDLE_OUT_VALUE)){
                        tv_outcard_get_info.setText("请领取小票,欢迎下次光临");
                    }else if(activityTag.equals(_Constants.BUNDLE_NET_VALUE)){
                        tv_outcard_get_info.setText("请领取小票和房卡");
                    }else {
                        tv_outcard_get_info.setText("请领取小票和房卡");
                    }

                    break;
                case _HandlerCode.PRINT_TICKET_CARD://
                    //      订单号 + 房间号+ 卡id
                    //【00000000000000000000 000000 000000】
                    //  20180208101084076503 005089 747915
                    // orderNO[20个数字]+ roomNo[房间号不够6位补零]+ CardIdNo[随机生成的6位数]
                    /**
                     *  _typeCode = 1 退房    typeCode = 2 入住  typeCode = 3  续房
                     *  _cardNo = 1 退一张卡   _cardNo = 2 退第二张房卡卡
                     */
                    long userId = mShUserData.getLong("userId",0);
                    String ticket_orderNo = mShareOrderData.getString("ticket_orderno", "");
                    String ticket_roomNo = mShareOrderData.getString("ticket_roomno", "");
                    _Log.d("打印小票Ocard_net_response","ticket_orderNo = "+ticket_orderNo+
                            "\n ticket_roomNo ="+ticket_roomNo);

                    if(activityTag.equals(_Constants.BUNDLE_OUT_VALUE)){
                        // [退房]读取房卡数据
                        roomCardInfo = mShareOrderData.getString("ticket_roomcardno", "");
                        _Log.d("打印小票Ocard_net_response","_orderNo = "+roomCardInfo+
                                "\n activityTag ="+activityTag);

                    }else {// [续房/入住]生成房卡数据
                       try {
                           //orderNo
                           BigDecimal _orderNoBD = new BigDecimal(ticket_orderNo);
                           DecimalFormat _orderNoDf = new DecimalFormat("00000000000000000000");
                           String _orderNo = _orderNoDf.format(_orderNoBD);
                           _Log.d("打印小票Ocard_net_response","_orderNo = "+_orderNo);
                           //roomNo
                           BigDecimal _roomNoBD = new BigDecimal(ticket_roomNo);
                           DecimalFormat _roomNoDf = new DecimalFormat("000000");
                           String _roomNo = _roomNoDf.format(_roomNoBD);
                           _Log.d("打印小票Ocard_net_response","_roomNo="+_roomNo);
                           //CardId
                           String ticket_cardId = ""+(int) ((Math.random() * 9 + 1) * 100000);
                           BigDecimal _cardIdBD = new BigDecimal(ticket_cardId);
                           DecimalFormat  _cardIdDf = new DecimalFormat("000000");
                           String _cardId = _cardIdDf.format(_cardIdBD);
                           _Log.d("打印小票Ocard_net_response","_cardId="+_cardId);
                           roomCardInfo = _orderNo + _roomNo + _cardId;// 订单号+房间号+卡id
                           _Log.d("打印小票Ocard_net_response","roomCardInfo="+roomCardInfo);
                       }catch (Exception e){}
                    }
                    String str = "{\"orderNo\":\"" + ticket_orderNo + "\"," +
                            "\"userId\":" + userId + "," +
                            "\"rooms\":[{\"roomNo\":\"" + ticket_roomNo + "\"," +
                            "\"cards\":[{\"roomCardId\":\"" + roomCardInfo +
                            "\"}]}]}";
                    mUpdateOrderData(str,_typeCode,_cardNo);

                    break;
                case _HandlerCode.TICKET_CARD_CHECKIN://入住-吐卡
                    // 到读卡位置需要时间/延迟执行
                    boolean OutCard = _RoomCardUtils.getInstance().OutCardToReadRegion();
                    if (OutCard) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _RoomCardUtils.getInstance().GetPermissions();
                                int WriteCard =_RoomCardUtils.getInstance().WriteCardData(roomCardInfo);//32
                                _Log.d("写入小票数据Ocard_net_response",
                                        "roomCardInfo="+roomCardInfo+ "WriteCard ="+WriteCard);
                                _Log.d("Ocard_net_response","1 = 写卡数据长度有误," +
                                        "2 = 写入小票数据成功，3 = 写数据命令执行失败");
                                switch(WriteCard) {
                                    case 2:
                                        //吐完第一张房卡
                                        _RoomCardUtils.getInstance().OutRoomCardExit();
                                        //发送消息查询发卡机状态,提示卡口有房卡
                                        mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,3900);
                                        break;
                                    case 1://TODO 写卡数据长度有误
                                    case 3://TODO 写数据命令执行失败
                                        _WriteCardAgain();
                                        break;
                                }
                            }
                        },3900);
                    }

                    break;
                case _HandlerCode.QUERY_CARD_MACHINE_STATE://查询一下,发卡机状态
                    backLoginPage(OutCardOutTicketActivity.this);
                    break;
                case _HandlerCode.TICKET_CARD_CONTINUE://续房-吐卡
                    /*
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            _RoomCardUtils.getInstance().GetPermissions();
                            int WriteCard =_RoomCardUtils.getInstance().WriteCardData(roomCardInfo);//32
                            _Log.d("续房/写入小票数据Ocard_net_response","roomCardInfo="+roomCardInfo+
                                    "\n WriteCard ="+WriteCard);

                            switch(WriteCard) {
                                case 2:
                                    _RoomCardUtils.getInstance().OutRoomCard();//吐房卡
                                    mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            backLoginPage(OutCardOutTicketActivity.this);
                                        }
                                    },2500);

                                    break;
                                case 1://TODO 写卡数据长度有误
                                case 3://TODO 写卡数据长度有误
                                    break;
                            }
                        }
                    },3900);
                    */
                    _RoomCardUtils.getInstance().GetPermissions();
                    int WriteCard =_RoomCardUtils.getInstance().WriteCardData(roomCardInfo);//32
                    _Log.d("续房/写入小票数据Ocard_net_response","roomCardInfo="+roomCardInfo+
                            "\n WriteCard ="+WriteCard);
                    switch(WriteCard) {
                        case 2:
                            _RoomCardUtils.getInstance().OutRoomCardExit();//吐房卡
                            mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,2000);
                            break;
                        case 1://TODO 写卡数据长度有误
                        case 3://TODO 写卡数据长度有误
                            break;
                    }
                    break;
                case _HandlerCode.TICKET_CARD_CHECKOUT://退房-收卡
                    _RoomCardUtils.getInstance().ToCardBox();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            backLoginPage(OutCardOutTicketActivity.this);
                        }
                    },2500);
                    break;
                case _HandlerCode.TICKET_CARD_CHECKIN_SPEAKER: //入住的语音提示
                    _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请领取小票和房卡");
                    break;
                case _HandlerCode.TICKET_CARD_CONTINUE_SPEAKER: //续房的语音提示
                    _BaiduTTSUtils.getInstance()._OpenBaiduTTS( "续房成功，请领取小票和房卡");
                    break;
                case _HandlerCode.TICKET_CARD_CHECKOUT_SPEAKER: //退房的语音提示
                    _BaiduTTSUtils.getInstance()._OpenBaiduTTS("退房成功，请领取小票");
                    break;
            }
            super.handleMessage(msg);
        }
    };


    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_public_outcad);
        context = this;
        iv_out_card = findViewById(R.id.iv_out_card);
        tv_outcard_get_info = findViewById(R.id.tv_outcard_get_info);

        activityTag = getIntent().getStringExtra(_Constants.BUNDLE_REY);
        if (activityTag.equals(_Constants.BUNDLE_OUT_VALUE)) {//退房卡,直接打印小票
            _typeCode = 1;
            _cardNo = 1;
            _PrintTicket(_typeCode,_cardNo);
            mHandler.sendEmptyMessage(_HandlerCode.OUT_CARD_VIEW);/** 出卡/收卡动画 */
            mHandler.sendEmptyMessageDelayed(_HandlerCode.TICKET_CARD_CHECKOUT,1000);//收卡
            mHandler.sendEmptyMessage(_HandlerCode.TICKET_CARD_CHECKOUT_SPEAKER);//语音提示
        } else if(activityTag.equals(_Constants.BUNDLE_CONTINUE_VALUE)) {
            _typeCode = 3;
            _cardNo = 1;
            mHandler.sendEmptyMessage(_HandlerCode.PRINT_TICKET_CARD);
        }else {
            _typeCode = 2;
            _cardNo = 1;
            mHandler.sendEmptyMessage(_HandlerCode.PRINT_TICKET_CARD);
        }
    }

    @Override
    protected void initTitleView() {
        iv_detele = (ImageView) findViewById(R.id.iv_delete);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setVisibility(View.INVISIBLE);
        iv_detele.setOnClickListener(mHomeListener);
        TitleBarManager.getInstance().init(this);
        ProgressManager.getInstance().init(this);
        switch (activityTag) {
            case _Constants.BUNDLE_TIME_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.TIME_ROOM);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FIVE);
                ProgressManager.getInstance().setProgressContinueInfo();
                tv_outcard_get_info.setText("正在出小票和房卡");
                break;
            case _Constants.BUNDLE_CONTINUE_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CONTINUE_ROOM);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FIVE);
                ProgressManager.getInstance().setProgressContinueInfo();
                tv_outcard_get_info.setText("正在出小票和房卡");
                break;
            case _Constants.BUNDLE_IN_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_IN);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FIVE);
                ProgressManager.getInstance().setProgressInInfo();
                tv_outcard_get_info.setText("正在出小票和房卡");
                break;
            case _Constants.BUNDLE_NET_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.NET_CARD);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_FIVE);
                ProgressManager.getInstance().setProgressNetInfo();
                tv_outcard_get_info.setText("正在出小票和房卡");
                break;
            case _Constants.BUNDLE_OUT_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_OUT);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                ProgressManager.getInstance().setFourFiveProgressGone();
                ProgressManager.getInstance().setProgressOutInfo();
                tv_outcard_get_info.setText("正在出小票");
                break;

        }
    }

    @Override
    protected void initClickListener() {

    }

    @Override
    protected void initTouchListener() {
    }

    private final View.OnClickListener mHomeListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            backLoginPage(OutCardOutTicketActivity.this);

        }
    };

    private final View.OnClickListener mConfirmCardListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //TODO 执行出房卡的code
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(_HandlerCode.OUT_CARD_VIEW);
                }
            }, 1000);

        }
    };

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
        _BaiduTTSUtils.getInstance()._ReleaseBaiduTTS();
        closeAnimation();
    }


    /**
     * Animation
     */
    private void outCardAnimation() {
        iv_out_card.setImageResource(R.drawable.out_roomcard_frame);
        outCardAD = (AnimationDrawable) iv_out_card.getDrawable();
        outCardAD.isOneShot();
        outCardAD.start();
    }

    private void closeAnimation() {
        outCardAD.stop();
    }

    /**
     * Ticket Data
     */
    private void _TicketData(int code,int cardNo){

        DecimalFormat df = new DecimalFormat("0.00");
        if(code == 1){
            ticket_createtime = mShareOrderData.getString("ticket_createtime","");
            ticket_orderNo = mShareOrderData.getString("ticket_orderNo","");
            ticket_roomTypeName = mShareOrderData.getString("ticket_roomTypeName","");
            ticket_startDate = mShareOrderData.getString("ticket_startDate", "");
            ticket_endDate = mShareOrderData.getString("ticket_endDate", "");
            ticket_guestName = mShareOrderData.getString("ticket_guestName","");
            ticket_mHotelName =  mShareOrderData.getString("ticket_hotelName","");
            ticket_roomNo =  mShareOrderData.getString("ticket_roomno","");
            int _mDepositFee = mShareOrderData.getInt("ticket_depositFee", 0);
            ticket_mDepositFee = df.format((double)(_mDepositFee)/100);
        }else {
            ticket_mHotelName = outCardBean.data.hotelName;
            ticket_roomTypeName = outCardBean.data.roomTypeName;
            ticket_roomNo= outCardBean.data.roomNo;
            ticket_orderNo = outCardBean.data.orderNo;
            ticket_roomFee = outCardBean.data.roomFee;
            ticket_depositFee = outCardBean.data.depositFee;
            ticket_mRoomFee = df.format((double)(ticket_roomFee)/100);
            ticket_mDepositFee = df.format((double)(ticket_depositFee)/100);
            if(2 == outCardBean.data.stayType){  // 1:全天房     2:钟点房
                ticket_createtime = outCardBean.data.createTime;
                ticket_startDate = outCardBean.data.zstartTime;
                ticket_endDate = outCardBean.data.zendTime;
            }else {
                ticket_createtime = outCardBean.data.createTime;
                ticket_startDate = outCardBean.data.startDate;
                ticket_endDate = outCardBean.data.endDate;
            }
            ticket_guestName = mShareOrderData.getString("ticket_guestName","");

            //roomCardInfo
            /*
            if(cardNo == 1){
                ticket_guestName = outCardBean.data.guests.get(0).guestName;
            }else{// 出第二张房卡的小票
                ticket_guestName = outCardBean.data.guests.get(1).guestName;
            }
             */
        }

    }
    /**
     * 打印小票
     * @param code
     */
    private void _PrintTicket(int code,int cardNo){
        //-------------------小票格式---------------------
        _TicketData(code,cardNo);
        String title = ticket_mHotelName+"\n";
        String _DepositFeeStr = "";
        String _RoomFeeStr = "";
        String _ReceiptStr = "";
        if(code == 1){//退房
            _ReceiptStr = "    [退房回执]     \n";
            //_DepositFeeStr = "已退押金：" + ticket_mDepositFee + "\n" ;
            _DepositFeeStr = "已退房                      \n" ;
            _RoomFeeStr = "          \n";

            String roomCardTicket =
                    "=========================================\n" +
                            "订单日期：" + ticket_createtime + "\n" +
                            "订单号 ：  " + ticket_orderNo + "\n" +
                            "房间号 ：" + ticket_roomNo + "\n" +
                            "入住房型：" + ticket_roomTypeName + "\n" +
                            "入住时间 ：" + ticket_startDate + "\n" +
                            "离店时间 ：" + ticket_endDate + "\n" +
                            "          \n" +
                            _DepositFeeStr +
                            _RoomFeeStr +
                            "=========================================\n" +
                            "          \n" +
                            "温馨提示：请带好随行物品,欢迎再次光临!\n";
            //-------------------小票格式---------------------
            //打印小票
            mTicket = new SparseArray<String>();
            mTicket.put(1, title);
            mTicket.put(2, _ReceiptStr);
            mTicket.put(3, roomCardTicket);
            //YKprinter
            String ticketData = title+"\n"+_ReceiptStr+"\n"+roomCardTicket;
            if(_YKPrintUtils.getInstance().initYKPrinter(OutCardOutTicketActivity.this)){
                Log.d("_YKPrintUtils","初始化成功");
                _YKPrintUtils.getInstance().printTicketData(ticketData);
            }else {
                Log.d("_YKPrintUtils","初始化失败");
            }
            //usbprinter
            //_UsbPrintUtils.getInstance().getPrintTicketData(OutCardOutTicketActivity.this, mTicket);

        }else if(code == 2){
            _ReceiptStr = "    [入住回执]    \n";
            _DepositFeeStr = "押金：" + ticket_mDepositFee + "\n" ;
            _RoomFeeStr = "房费：" + ticket_mRoomFee + "\n";

            String roomCardTicket =
                    "=========================================\n" +
                            "订单日期：" + ticket_createtime + "\n" +
                            "订单号：  " + ticket_orderNo + "\n" +
                            "房间号：" + ticket_roomNo + "\n" +
                            "顾客姓名：" + ticket_guestName + "\n" +
                            "入住房型：" + ticket_roomTypeName + "\n" +
                            "入住时间：" + ticket_startDate + "\n" +
                            "离店时间：" + ticket_endDate + "\n" +
                            "          \n" +
                            _DepositFeeStr +
                            _RoomFeeStr +
                            "=========================================\n" +
                            "          \n" +
                            "温馨提示：请带好随行物品,欢迎再次光临!\n";
            //-------------------小票格式---------------------
            //打印小票
            mTicket = new SparseArray<String>();
            mTicket.put(1, title);
            mTicket.put(2, _ReceiptStr);
            mTicket.put(3, roomCardTicket);
            //YKprinter
            String ticketData = title+"\n"+_ReceiptStr+"\n"+roomCardTicket;
            if(_YKPrintUtils.getInstance().initYKPrinter(OutCardOutTicketActivity.this)){
                Log.d("_YKPrintUtils","初始化成功");
                _YKPrintUtils.getInstance().printTicketData(ticketData);
            }else {
                Log.d("_YKPrintUtils","初始化失败");
            }
            //usbprinter
            //_UsbPrintUtils.getInstance().getPrintTicketData(OutCardOutTicketActivity.this, mTicket);

        }else if(code == 3){
            ticket_createtime = mShareOrderData.getString("ticket_c_startDate","");
            _ReceiptStr = "   [续房回执]    \n";
            _DepositFeeStr = "押金：" + ticket_mDepositFee + "\n" ;
            _RoomFeeStr = "房费：" + ticket_mRoomFee + "\n";
            String roomCardTicket =
                    "=========================================\n" +
                            "订单日期：" + ticket_createtime + "\n" +
                            "订单号：  " + ticket_orderNo + "\n" +
                            "房间号：" + ticket_roomNo + "\n" +
                            "顾客姓名：" + ticket_guestName + "\n" +
                            "入住房型：" + ticket_roomTypeName + "\n" +
                            "入住时间：" + ticket_startDate + "\n" +
                            "离店时间：" + ticket_endDate + "\n" +
                            "          \n" +
                            _DepositFeeStr +
                            _RoomFeeStr +
                            "=========================================\n" +
                            "          \n" +
                            "温馨提示：请带好随行物品,欢迎再次光临!\n";
            //-------------------小票格式---------------------
            //打印小票
            mTicket = new SparseArray<String>();
            mTicket.put(1, title);
            mTicket.put(2, _ReceiptStr);
            mTicket.put(3, roomCardTicket);
            //YKprinter
            String ticketData = title+"\n"+_ReceiptStr+"\n"+roomCardTicket;
            if(_YKPrintUtils.getInstance().initYKPrinter(OutCardOutTicketActivity.this)){
                Log.d("_YKPrintUtils","初始化成功");
                _YKPrintUtils.getInstance().printTicketData(ticketData);
            }else {
                Log.d("_YKPrintUtils","初始化失败");
            }
            //usbprinter
            //_UsbPrintUtils.getInstance().getPrintTicketData(OutCardOutTicketActivity.this, mTicket);
        }
    }


    /**
     * 吐卡-再次将数据写入房卡
     */
    int i = 0;
    private void _WriteCardAgain(){
        i = i + 1;
        _RoomCardUtils.getInstance().GetPermissions();
        int WriteCard =_RoomCardUtils.getInstance().WriteCardData(roomCardInfo);//32
        switch(WriteCard) {
            case 2:
                _RoomCardUtils.getInstance().OutRoomCardExit();//吐完第一张房卡
                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,2000);
                break;
            case 1://写卡数据长度有误
            case 3://写数据命令执行失败
                _WriteCardAgain();
                break;
        }
        if(i == 6){
            _RoomCardUtils.getInstance().OutRoomCardExit();//吐卡
            mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,2000);
            return;
        }
    }

    /**
     *  Okhttp
     *  入住——小票数据
     */
    private synchronized void mUpdateOrderData(String str,final int code,final int cardNo) { //入住/退房
        _Log.d("参数OutCardTicket0card_net_response__"+code,"__str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.CHECKIN_CARD_INFO_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("OutCardTicket0card_net_response__"+code,"__str="+result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson();
                    outCardBean = mgson.fromJson(result, OutCardData.class);
                    if (outCardBean.code == 0) {
                        if(outCardBean.data != null){
                            if(code == 1){      //退房  code
                                //_PrintTicket(code,cardNo);    //打印小票
                                //mHandler.sendEmptyMessage(_HandlerCode.TICKET_CARD_CHECKOUT_SPEAKER);//语音提示
                                //mHandler.sendEmptyMessageDelayed(_HandlerCode.TICKET_CARD_CHECKOUT,3000);//收卡
                            }else if(code == 2){//入住  code
                                mHandler.sendEmptyMessage(_HandlerCode.OUT_CARD_VIEW);/** 出卡/收卡动画 */
                                _PrintTicket(code,cardNo);//打印小票
                                mHandler.sendEmptyMessage(_HandlerCode.TICKET_CARD_CHECKIN_SPEAKER);//语音提示
                                mHandler.sendEmptyMessage(_HandlerCode.TICKET_CARD_CHECKIN);//吐卡
                            }else if(code == 3){//续房  code
                                mHandler.sendEmptyMessage(_HandlerCode.OUT_CARD_VIEW);/** 出卡/收卡动画 */
                                _PrintTicket(code,cardNo);//打印小票
                                mHandler.sendEmptyMessage(_HandlerCode.TICKET_CARD_CONTINUE_SPEAKER);//语音提示
                                mHandler.sendEmptyMessage(_HandlerCode.TICKET_CARD_CONTINUE);//吐卡
                            }
                        }
                    }else if(outCardBean.code == -1){
                        mHandler.sendEmptyMessage(_HandlerCode.OUT_CARD_VIEW);/** 出卡/收卡动画 */
                        _LToast(""+outCardBean.msg);
                    }else {
                        mHandler.sendEmptyMessage(_HandlerCode.OUT_CARD_VIEW);/** 出卡/收卡动画 */
                    }
                }
            }
        });
    }

}