package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DecimalFormat;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.CheckOutData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;


public class CheckOutActivity extends BaseActivity {

    private ImageView out_room_card;
    private ImageView iv_back;
    private ImageView iv_delete;
    private CheckOutData checkOutData;
    private TimeView tv_address_countdown;

    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case _HandlerCode.READ_ROOM_CARD_DATA:

                    try{
                        //  读取卡数据
                        _RoomCardUtils.getInstance().GetPermissions();
                        String mRoomCardNo = _RoomCardUtils.getInstance().ReadCardData();
                        if(mRoomCardNo == null ||"".equals(mRoomCardNo)){
                            _SToast("Sorry...该卡不能识别...");
                            backLoginPage(CheckOutActivity.this);
                            return;
                        }
                        String mCardNo = mRoomCardNo.substring(0,20);// 订单号
                        int mRoomNo = Integer.parseInt(mRoomCardNo.substring(20,26));// 房间号
                        String mCardId = mRoomCardNo.substring(26,32);// 房卡id

                        _Log.d("退房card_net_response","mCardNo="+ mCardNo);
                        _Log.d("退房card_net_response","mRoomNo="+ mRoomNo);
                        _Log.d("退房card_net_response","mCardId="+ mCardId);

                        //  房卡
                        String outCardStr01 = "{\"orderNo\":\"" + mCardNo +
                                "\",\"roomNo\":\"" + mRoomNo +
                                "\",\"cardIds\":[\"" + mRoomCardNo + "\"]}";
                        mOrderEditor.putString("ticket_roomno",""+ mRoomNo);
                        mOrderEditor.commit();

                        mPostOutCardInfo(outCardStr01);
                    }catch (Exception e){
                        _SToast("读卡异常！");
                    }

                    break;
                case _HandlerCode.CHANGE_OUT_CARD_INFO:
                    //
                    DecimalFormat df = new DecimalFormat("0.00");
                    String roomFee = df.format((double)(checkOutData.data.roomFee)/100);
                    String depositFee = df.format((double)(checkOutData.data.depositFee)/100);

                    Intent intent = new Intent(CheckOutActivity.this, CheckOutDetialsActivity.class);
                    intent.putExtra(_Constants.BUNDLE_REY,_Constants.BUNDLE_OUT_VALUE);
                    Bundle bundle = new Bundle();
                    bundle.putString("ticket_roomNo", "" + checkOutData.data.roomNo);
                    bundle.putString("ticket_roomno", "" + checkOutData.data.roomNo);
                    bundle.putString("ticket_roomFee", "" + roomFee);
                    bundle.putString("ticket_depositFee", "" + depositFee);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    _LToast("房卡识别成功");
                    break;
                case _HandlerCode.QUERY_CARD_MACHINE_STATE:
                    //查询发卡机状态
                    SparseArray<Integer> mQueryState = _RoomCardUtils.getInstance().K720_SensorQuery();
                    mQueryState.get(0);
                    mQueryState.get(1);
                    mQueryState.get(2);
                    mQueryState.get(3);
                    mQueryState.get(4);
                    if (mQueryState.get(0) == 0) {
                        switch (mQueryState.get(4)) {
                            case 0x3E://卡片状态：只有一张卡在传感器2-3位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x3B://只有一张卡在传感器1-2位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x39://只有一张卡在传感器1位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x37://卡在传感器1-2-3的位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x36://卡在传感器2-3的位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x34://卡在传感器3位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x33://卡在传感器1-2位置(读卡位置)
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x32://卡在传感器2位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x30://卡片状态：空闲
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                            case 0x38://卡片状态：卡箱已空
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                            case 0x31://卡片状态：卡在传感器1位置(取卡位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                            case 0x35://卡片状态：卡在传感器取卡位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                        }
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };



    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_check_out);

        out_room_card = (ImageView) findViewById(R.id.iv_out_room_card);
        out_room_card.setImageResource(R.drawable.in_roomcard_frame);
        AnimationDrawable ad = (AnimationDrawable) out_room_card.getDrawable();
        ad.setOneShot(false);
        ad.start();


    }

    @Override
    protected void initTitleView() {
        TitleBarManager.getInstance().init(this);
        TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_OUT);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_ONE);
        ProgressManager.getInstance().setFourFiveProgressGone();
        ProgressManager.getInstance().setProgressOutInfo();
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CheckOutActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish",Toast.LENGTH_LONG).show();
            }
        });
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backLoginPage(CheckOutActivity.this);
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CheckOutActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请插入房卡");
        mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
    }


    @Override
    protected void initClickListener() {

    }


    @Override
    protected void initTouchListener() {

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
        mHandler.removeMessages(_HandlerCode.QUERY_CARD_MACHINE_STATE);
    }


    /**
     * 退房
     */
    private synchronized void mPostOutCardInfo(String str) {
        _Log.d("参数-退房_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.ORDER_OUT_CARD_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("退房_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    try {
                        Gson mgson = new Gson();
                        checkOutData = mgson.fromJson(result, CheckOutData.class);
                        if (checkOutData.code == 0) {
                            String orderNO = checkOutData.data.orderNo;
                            mHandler.sendEmptyMessage(_HandlerCode.CHANGE_OUT_CARD_INFO);
                            /*
                           if(orderNO == null || orderNO.equals("")){
                               _LToast("订单号为空...");
                               backLoginPage(CheckOutActivity.this);
                           }else {
                               mHandler.sendEmptyMessage(_HandlerCode.CHANGE_OUT_CARD_INFO);
                           }*/
                        }else {
                            _LToast("该开不识别！"+checkOutData.msg);
                            Intent intent = new Intent(CheckOutActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        _LToast(_Constants.TOAST_PARSE_NULL);
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }

}