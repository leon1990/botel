package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.ContinueRoomData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;

public class CheckInKeepRoomActivity extends BaseActivity {

    private ImageView iv_back;
    private ImageView iv_delete;
    //
    private ImageView iv_continue_card_bg;
    private ContinueRoomData continueRoom;
    private String parameterStr;
    private TimeView tv_address_countdown;

    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case _HandlerCode.READ_ROOM_CARD_DATA:
                    try{
                        //  读取房卡数据
                        _RoomCardUtils.getInstance().GetPermissions();
                        String mRoomCardNoCode = _RoomCardUtils.getInstance().ReadCardData();
                        if(mRoomCardNoCode == null ||"".equals(mRoomCardNoCode)){
                            _SToast("Sorry...该卡不能识别...");
                            backLoginPage(CheckInKeepRoomActivity.this);
                            return;
                        }
                        String mCardNoCode = mRoomCardNoCode.substring(0,20);
                        int mRoomNoCode = Integer.parseInt(mRoomCardNoCode.substring(20,26));
                        String mCardId = mRoomCardNoCode.substring(26,32);// 房卡id
                        _Log.d("续房card_net_response","mCardNo="+ mCardNoCode);
                        _Log.d("续房card_net_response","mRoomNo="+ mRoomNoCode);
                        _Log.d("续房card_net_response","mCardId="+ mCardId);
                        mOrderEditor.putString("ticket_orderno",""+ mCardNoCode);
                        mOrderEditor.putString("ticket_roomno",mRoomCardNoCode);
                        mOrderEditor.putString("ticket_roomcardno",mRoomCardNoCode);
                        mOrderEditor.commit();

                        parameterStr = "{\"orderNo\": \""+mCardNoCode+
                                "\",\"roomCardId\": \""+mRoomCardNoCode+
                                "\",\"roomNo\": \""+mRoomNoCode+"\"}";
                        mPostContinueOrder(parameterStr);
                    }catch (Exception e){
                        _SToast("null object reference");
                    }
                    break;
                case _HandlerCode.QUERY_CARD_MACHINE_STATE:
                    //查询发卡机状态
                    SparseArray<Integer> mQueryState = _RoomCardUtils.getInstance().K720_SensorQuery();
                    mQueryState.get(0);
                    mQueryState.get(1);
                    mQueryState.get(2);
                    mQueryState.get(3);
                    mQueryState.get(4);
                    if (mQueryState.get(0) == 0) {
                        switch (mQueryState.get(4)) {
                            case 0x3E://卡片状态：只有一张卡在传感器2-3位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x3B://只有一张卡在传感器1-2位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x39://只有一张卡在传感器1位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x37://卡在传感器1-2-3的位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x36://卡在传感器2-3的位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x34://卡在传感器3位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x33://卡在传感器1-2位置(读卡位置)
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x32://卡在传感器2位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.READ_ROOM_CARD_DATA,500);
                                break;
                            case 0x30://卡片状态：空闲
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                            case 0x38://卡片状态：卡箱已空
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                            case 0x31://卡片状态：卡在传感器1位置(取卡位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;
                            case 0x35://卡片状态：卡在传感器取卡位置
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
                                break;

                        }
                    }

                    break;
            }
        }
    };


    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_continue_room);

        iv_continue_card_bg = findViewById(R.id.iv_continue_card_bg);
        iv_continue_card_bg.setImageResource(R.drawable.in_roomcard_frame);
        AnimationDrawable ad = (AnimationDrawable) iv_continue_card_bg.getDrawable();
        ad.setOneShot(false);//循环播放
        ad.start();

    }

    @Override
    protected void initTitleView() {
        iv_back = (ImageView)findViewById(R.id.iv_back);
        iv_delete = (ImageView)findViewById(R.id.iv_delete);
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backLoginPage(CheckInKeepRoomActivity.this);

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backLoginPage(CheckInKeepRoomActivity.this);

            }
        });
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CheckInKeepRoomActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish", Toast.LENGTH_LONG).show();
            }
        });
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请插入房卡");
        TitleBarManager.getInstance().init(this);
        TitleBarManager.getInstance().changeTitleText(_Constants.CONTINUE_ROOM);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_ONE);
        ProgressManager.getInstance().setProgressContinueInfo();
        mHandler.sendEmptyMessageDelayed(_HandlerCode.QUERY_CARD_MACHINE_STATE,500);
    }


    @Override
    protected void initClickListener() {
    }

    @Override
    protected void initTouchListener() {

    }


    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeMessages(_HandlerCode.QUERY_CARD_MACHINE_STATE);
        mHandler.removeMessages(_HandlerCode.READ_ROOM_CARD_DATA);
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
    }

    /**
     *  Okhttp
     *  续房
     */
    private synchronized void mPostContinueOrder(String str) { //入住/退房
        _Log.d("Continue参数_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.CONTINUE_ROOM_DATA_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast("ContinueOrder"+_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("Continue_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                        Gson mgson = new Gson();
                        continueRoom = mgson.fromJson(result, ContinueRoomData.class);
                        if (continueRoom.code == 0) {
                            if (continueRoom.data == null){
                                _LToast("请求数据为空");
                            }else {

                                mOrderEditor.putInt("ticket_roomFee", continueRoom.data.roomFee);
                                mOrderEditor.putInt("ticket_deposit", continueRoom.data.depositFee);
                                mOrderEditor.putString("ticket_orderNo",""+continueRoom.data.orderNo);
                                mOrderEditor.putString("ticket_roomNo",""+continueRoom.data.roomNo);
                                mOrderEditor.putString("ticket_orderno",""+continueRoom.data.orderNo);
                                mOrderEditor.putString("ticket_roomno",""+continueRoom.data.roomNo);
                                mOrderEditor.putString("ticket_roomCardId",""+continueRoom.data.roomCardId);
                                //mOrderEditor.putString("ticket_photo",continueRoom.data.photo);
                                mOrderEditor.putInt("public_hotelid",continueRoom.data.hotelId);
                                mOrderEditor.putInt("ticket_roomType",continueRoom.data.roomType);
                                mOrderEditor.putString("ticket_roomtypename",""+continueRoom.data.roomTypeName);
                                mOrderEditor.putInt("ticket_guestNum",continueRoom.data.guestNum);
                                mOrderEditor.putString("ticket_liveDays",""+continueRoom.data.liveDays);
                                mOrderEditor.putInt("ticket_howcard", continueRoom.data.guestNum);
                                mOrderEditor.putInt("guesttotal", continueRoom.data.guestNum);      //判断出多少张放卡
                                mOrderEditor.putInt("ticket_guestsize", continueRoom.data.guestNum);
                                mOrderEditor.putInt("ticket_breakfastNum", continueRoom.data.breakfastNum);
                                mOrderEditor.putString("ticket_userId",""+continueRoom.data.userId);
                                mOrderEditor.putInt("ticket_stayType",continueRoom.data.stayType);
                                mOrderEditor.putString("ticket_c_startDate", ""+continueRoom.data.startDate);
                                mOrderEditor.putString("ticket_endDate", ""+continueRoom.data.endDate);
                                mOrderEditor.putString("ticket_endDate", ""+continueRoom.data.endDate);
                                mOrderEditor.commit();
                                /*
                                if(continueRoom.data.guests.size() == 2){
                                    mUserEditor.putString("idcardNOC01", ""+continueRoom.data.guests.get(0).idCardNo);
                                    mUserEditor.putString("idcardNOC02", ""+continueRoom.data.guests.get(1).idCardNo);
                                }else {
                                    mUserEditor.putString("idcardNOC01", ""+continueRoom.data.guests.get(0).idCardNo);
                                }*/

                                mPublicEditor.putInt("testTimes", continueRoom.data.guestNum);     //判断多少人需要人脸识别
                                mPublicEditor.commit();

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //_SToast("识别成功");
                                        ArrayList<String> roomPhotos = new ArrayList<String>();
                                        roomPhotos.add(""+continueRoom.data.photo.get(0));
                                        roomPhotos.add(""+continueRoom.data.photo.get(1));
                                        roomPhotos.add(""+continueRoom.data.photo.get(2));
                                        roomPhotos.add(""+continueRoom.data.photo.get(3));
                                        Intent intent = new Intent(CheckInKeepRoomActivity.this, CreateOrderActivity.class);
                                        intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_CONTINUE_VALUE);
                                        intent.putStringArrayListExtra("roomPhotos", roomPhotos);
                                        startActivity(intent);
                                    }
                                },500);
                            }
                        }else{
                            _LToast(""+continueRoom.msg);
                            backLoginPage(CheckInKeepRoomActivity.this);
                        }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                    backLoginPage(CheckInKeepRoomActivity.this);

                }
            }
        });
    }

}