/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.qcloud.image.ImageClient;
import com.qcloud.image.request.FaceCompareRequest;
import com.qcloud.image.request.FaceLiveDetectPictureRequest;
import com.qcloud.image.request.FaceMultiIdentifyRequest;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.List;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.ImgCompare;
import hk.yijian.botel.bean.response.ImgResult;
import hk.yijian.botel.bean.test.LFLiveBean;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.faceUI.youtu.CameraHelper;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._Log;


/**
 *  This activity uses the camera/camcorder as the A/V source for the {@link MediaRecorder} API.
 *  A {@link TextureView} is used as the camera preview which limits the code to API 14+. This
 *  can be easily replaced with a {@link android.view.SurfaceView} to run on older devices.
 */
public class RecYouTuFaceActivity extends BaseActivity {

    //
    private Context context;
    private Drawable drawable;
    private String IDNoStr;
    private String nameStr;
    private String sexStr;
    private String nationStr;
    private String birthDateStr;
    private String addressStr;
    private String licenseStr;
    private String startStr;
    private String endStr;
    //
    private Camera mCamera;
    private TextureView mPreview;
    private ImageView iv_linkface_line;
    private FrameLayout ll_scan_layout;
    private Animation verticalAnimation;
    //
    private LinearLayout ll_success_layout;
    private LinearLayout ll_error_layout;
    //
    private ImageView iv_id_card_img;
    private TextView iv_id_card_name;
    private TextView iv_id_card_no;
    private TextView tv_text_info;
    //
    private FrameLayout fl_linkface_layout;
    //
    private MediaRecorder mMediaRecorder;
    private boolean isRecording = false;
    private static final String TAG = "Recorder";
    private TextView tv_cap_LipLanguage;
    //
    private SparseArray<String> LFmap;
    private int sex = 0;
    //
    private File mOutputVideoFile;
    private File IdCardImagePath;
    private File VideoImagePath;
    //
    public SparseArray<String> IDCardInfo;
    public int retNo;
    public Bitmap bimg;
    //
    private ImageView iv_back;
    private ImageView iv_delete;
    private String activityTag;
    //
    private AlertDialog  mDialog;

    //腾讯静态活体检测
    private String appId = "1254386949";
    private String secretId = "AKIDlwnpnmw5LQIBzbbXzJRPCnRsJcljt8Ym";
    private String secretKey = "lfOExUp5DmCTXm1690ueDlCSguB6raGk";
    private String bucketName = "imgdev";
    private ImageClient imageClient = new ImageClient(appId, secretId, secretKey);
    private String result;
    private FaceLiveDetectPictureRequest request;
    private boolean useNewDomain = false;
    private FaceMultiIdentifyRequest requestvs;
    private String resultvs;
    private FaceCompareRequest faceCompareReqvs;
    private String retvs;

    //handler
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                //Success
                case _HandlerCode.CHANGE_UI_SUCCESS_MESSAGE:
                    String payStatus = mShareOrderData.getString("ticket_orderStatus", "").trim();
                    if(payStatus.equals("3".trim())){
                        // TODO 直接出房卡
                        Intent intent = new Intent(RecYouTuFaceActivity.this, OutCardOutTicketActivity.class);
                        intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_TIME_VALUE);
                        startActivity(intent);

                    }else {
                        //TODO 去支付
                        Intent intent = new Intent(RecYouTuFaceActivity.this, PaymentRateActivity.class);
                        intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_TIME_VALUE);
                        startActivity(intent);
                    }
                    break;

                case _HandlerCode.CHANGEUI_SUCECESS_MESSAGE:
                    mSuccessFaceUI();
                    break;
                //error
                case _HandlerCode.CHANGE_UI_ERROR_MESSAGE:
                    showDialog();
                    break;
                case _HandlerCode.CHANGEUI_ERROR_MESSAGE:
                    mFailFaceUI();
                    break;


            }
            super.handleMessage(msg);
        }
    };



    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        int left = ll_scan_layout.getLeft();
        int right = ll_scan_layout.getRight();
        int top = ll_scan_layout.getTop();
        int bottom = ll_scan_layout.getBottom();
        int height = ll_scan_layout.getHeight()/2;
        verticalAnimation = new TranslateAnimation(left, left,top-height, bottom-height);
        verticalAnimation.setDuration(3500);
        verticalAnimation.setRepeatCount(Animation.INFINITE);
        verticalAnimation.setRepeatMode(Animation.REVERSE);
        iv_linkface_line.setAnimation(verticalAnimation);
        verticalAnimation.startNow();
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_you_tu_media);
        context = this;
        mPreview = (TextureView) findViewById(R.id.cap_surface_view);
        tv_cap_LipLanguage = (TextView) findViewById(R.id.cap_tv_LipLanguage);
        ll_scan_layout  = (FrameLayout) findViewById(R.id.ll_scan_layout);
        iv_linkface_line  = (ImageView) findViewById(R.id.iv_linkface_line);
        ll_success_layout = (LinearLayout) findViewById(R.id.ll_success_layout);
        ll_error_layout = (LinearLayout) findViewById(R.id.ll_error_layout);
        //
        iv_id_card_img = findViewById(R.id.iv_id_card_img);
        iv_id_card_name = findViewById(R.id.iv_id_card_name);
        iv_id_card_no = findViewById(R.id.iv_id_card_no);
        tv_text_info = findViewById(R.id.tv_text_info);
        //
        fl_linkface_layout = findViewById(R.id.fl_linkface_layout);
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请将人脸对准识别框");


        activityTag = getIntent().getStringExtra(_Constants.BUNDLE_REY);
        LFmap = new SparseArray<String>();
        IDCardInfo = _IDCardUtils.getInstance().readIDCardInfo(pkName);
        retNo = Integer.parseInt(IDCardInfo.get(0).trim());
        if(retNo == 0x90){
            nameStr = IDCardInfo.get(1).trim();
            sexStr = IDCardInfo.get(2).trim();
            nationStr = IDCardInfo.get(3).trim();
            birthDateStr = IDCardInfo.get(4).trim();
            addressStr = IDCardInfo.get(5).trim();
            IDNoStr = IDCardInfo.get(6).trim();
            licenseStr = IDCardInfo.get(7).trim();
            startStr = IDCardInfo.get(8).trim();
            endStr = IDCardInfo.get(9).trim();
            bimg = _IDCardUtils.getInstance().readIDCardImg();
            drawable = new BitmapDrawable(bimg);
            if(IDNoStr.trim().equals("")){
                Intent intent = new Intent(RecYouTuFaceActivity.this,ReadIdCardActivity.class);
                startActivity(intent);
                finish();
            }else {
                IdCardImagePath = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-youtu_img.jpg");
                saveIdCardImage(bimg,IdCardImagePath);// 提交图片bimg
                _Log.d("idCardInfo", "drawable= " + drawable);
                initfaceView();
            }

        }else{
            _LToast("获取身份证信息失败！");
            Intent intent = new Intent(RecYouTuFaceActivity.this,ReadIdCardActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void initTitleView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backLoginPage(RecYouTuFaceActivity.this);
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecYouTuFaceActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        TitleBarManager.getInstance().init(this);
        ProgressManager.getInstance().init(this);
        switch (activityTag) {
            case _Constants.BUNDLE_TIME_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.TIME_ROOM);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                ProgressManager.getInstance().setProgressContinueInfo();
                break;
            case _Constants.BUNDLE_CONTINUE_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CONTINUE_ROOM);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                ProgressManager.getInstance().setProgressContinueInfo();
                break;
            case _Constants.BUNDLE_IN_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_IN);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                ProgressManager.getInstance().setProgressInInfo();
                break;
            case _Constants.BUNDLE_NET_VALUE:
                TitleBarManager.getInstance().changeTitleText(_Constants.NET_CARD);
                ProgressManager.getInstance().showProgress(_Constants.PROGRESS_THREE);
                ProgressManager.getInstance().setProgressNetInfo();
                break;
        }
    }

    private void initfaceView(){
        /**
         * YouTu 人脸识别
         */
        mHandler.removeCallbacks(mPostVideoImgRunnable);
        mHandler.removeCallbacks(mStopRunnable);
        mHandler.removeCallbacks(mStartRunnable);
        //
        mHandler.postDelayed(mStartRunnable,10);
        mHandler.postDelayed(mStopRunnable, 5000);
        mHandler.postDelayed(mPostVideoImgRunnable, 5500);
    }

    @Override
    protected void initClickListener() {

    }

    @Override
    protected void initTouchListener() {

    }


    @Override
    protected void onPause() {
        super.onPause();
        // if we are using MediaRecorder, release it first
        releaseMediaRecorder();
        // release the camera immediately on pause event
        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    private final Runnable mStartRunnable = new Runnable() {
        @Override
        public void run() {
            startRecordingVideo();//TODO 录制视频
        }
    };

    private final Runnable mStopRunnable = new Runnable() {
        @Override
        public void run() {
            stopRecordingVideo();//结束录制
        }
    };


    private final Runnable mPostVideoImgRunnable = new Runnable() {
        @Override
        public void run() {

            try{
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(mOutputVideoFile.getPath());
                Bitmap bitmap = retriever.getFrameAtTime(2500, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                VideoImagePath = new File(getCacheDir().getAbsolutePath(), IDNoStr + "-youtu_video_img.jpg");
                saveLiveImage(bitmap,VideoImagePath);

                new Thread(new Runnable(){
                    @Override
                    public void run() {
                        TenCentFace(VideoImagePath);
                    }
                }).start();

            }catch (Exception e){
                _Log.d("SignStr_net_response","Exception");
            }
        }
    };

    private static byte[] getFileBytes(File file) {
        byte[] imgBytes = null;
        try {
            RandomAccessFile f = new RandomAccessFile(file, "r");
            imgBytes = new byte[(int) f.length()];
            f.readFully(imgBytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return imgBytes;
    }

    /**
     * 保存live图片
     * @param bmp
     * @param file
     * @return
     */
    public File  saveLiveImage(Bitmap bmp,File file) {
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 0, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }


    /**
     * 认证失败的UI
     */
    private void mFailFaceUI() {
        _BaiduTTSUtils.getInstance()._PauseBaiduTTS();
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("验证失败，请重新验证");
        fl_linkface_layout.setVisibility(View.INVISIBLE);
        tv_text_info.setVisibility(View.INVISIBLE);
        ll_error_layout.setVisibility(View.VISIBLE);
    }

    /**
     * 重新认证的UI
     */
    private void mAgainFaceUI() {
        fl_linkface_layout.setVisibility(View.VISIBLE);
        tv_text_info.setVisibility(View.VISIBLE);
        ll_error_layout.setVisibility(View.INVISIBLE);
    }


    /**
     * 人脸识别失败弹窗
     */
    private void showDialog() {

        if (isDialogShowing()) {
            return;
        }
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            final View view_custom = this.getLayoutInflater().inflate(R.layout.linkface_liveness_dialog, null,false);
            RelativeLayout iv_dialog_yes = view_custom.findViewById(R.id.iv_dialog_yes);
            ImageView iv_dialog_no = view_custom.findViewById(R.id.iv_dialog_no);
            builder.setView(view_custom);
            builder.setCancelable(false);
            mDialog = builder.create();
            mDialog.getWindow().setDimAmount((float) 0.8);//设置昏暗度为0
            mDialog.show();

            Window window = mDialog.getWindow();
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = 335;
            params.height = 360 ;
            window.setAttributes(params);
            iv_dialog_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                    mAgainFaceUI();
                    initfaceView();
                    _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请将人脸对准识别框");

                }
            });
            iv_dialog_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();

                    backLoginPage(RecYouTuFaceActivity.this);
                }
            });
        }catch (Exception e){}

        if (((Activity) context).isFinishing()) {
            return;
        }
    }
    private boolean isDialogShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    /**
     * 认证成功的UI
     */
    private void mSuccessFaceUI() {
        _BaiduTTSUtils.getInstance()._PauseBaiduTTS();
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("验证成功");

        if(isDialogShowing()){
            mDialog.dismiss();
        }
        fl_linkface_layout.setVisibility(View.INVISIBLE);
        tv_text_info.setVisibility(View.INVISIBLE);
        //
        ll_success_layout.setVisibility(View.VISIBLE);
        iv_id_card_img.setBackground(drawable);
        String mName = nameStr;
        mName = mName.substring(0,1) + "***";
        String idNo = IDNoStr;
        idNo = idNo.substring(0,4) + "**********" + idNo.substring(14,18);
        iv_id_card_name.setText(mName);
        iv_id_card_no.setText(idNo);
    }




    /**
     * 腾讯静态活体检测 /人脸比对
     */
    private void TenCentFace(File VideoImagePath){

        //TODO 活体检测
        request = new FaceLiveDetectPictureRequest(bucketName, VideoImagePath);
        result = imageClient.faceLiveDetectPicture(request, useNewDomain);
        Gson mgson = new Gson();
        final ImgResult imgResult = mgson.fromJson(result, ImgResult.class);
        if(imgResult.code == 0){

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_cap_LipLanguage.setText("code："+imgResult.code+
                            "/ message："+imgResult.message+
                            "/ score："+imgResult.data.score);
                }
            });

            if(imgResult.data.score >=87){
                //TODO 人脸比对
                faceCompareReqvs = new FaceCompareRequest(bucketName, VideoImagePath, IdCardImagePath);
                retvs = imageClient.faceCompare(faceCompareReqvs);
                final ImgCompare imgCompare = mgson.fromJson(retvs, ImgCompare.class);
                Log.d("SignStr_net_response","retvs："+retvs);
                if(imgCompare.code == 0){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_cap_LipLanguage.setText("code："+imgResult.code+
                                    "/ message："+imgResult.message+
                                    "/ score："+imgResult.data.score+"\n"+
                                    "code："+imgCompare.code+
                                    "/ message："+imgCompare.message+
                                    "/ score："+imgCompare.data.similarity);
                        }
                    });
                    if(imgCompare.data.similarity >= 65){
                        Log.d("SignStr_net_response","比对成功");
                        mUpLoadCardInfo(LFmap,VideoImagePath, IdCardImagePath);
                        //跳转页面
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                        mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_SUCECESS_MESSAGE);
                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE,1000);
                    }else {
                        //人脸比对失败
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                        mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                        mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_ERROR_MESSAGE);
                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_ERROR_MESSAGE,1000);
                    }
                }else {
                    //人脸比对失败
                    mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                    mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                    mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_ERROR_MESSAGE);
                    mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_ERROR_MESSAGE,1000);
                    Log.d("SignStr_net_response",imgCompare.message);
                }
            }else {
                //人脸比对失败
                //TODO 截图多张/一张不行/换另外一张
                Log.d("SignStr_net_response","不是活体");
                mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
                mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
                mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_ERROR_MESSAGE);
                mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_ERROR_MESSAGE,1000);
            }
        }else {
            //人脸比对失败
            mHandler.removeMessages(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE);
            mHandler.removeMessages(_HandlerCode.CHANGE_UI_ERROR_MESSAGE);
            mHandler.sendEmptyMessage(_HandlerCode.CHANGEUI_ERROR_MESSAGE);
            mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_ERROR_MESSAGE,1000);
            Log.d("SignStr_net_response",imgResult.message);
        }

    }


    public static Bitmap readBitMap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        //获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            // clear recorder configuration
            mMediaRecorder.reset();
            // release the recorder object
            mMediaRecorder.release();
            mMediaRecorder = null;
            // Lock camera for later use i.e taking it back from MediaRecorder.
            // MediaRecorder doesn't need it anymore and we will release it if the activity pauses.
            mCamera.lock();
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            // release the camera for other applications
            mCamera.release();
            mCamera = null;
        }
    }



    private boolean prepareVideoRecorder(){
        try{
            // BEGIN_INCLUDE (configure_preview)
            mCamera = CameraHelper.getDefaultCameraInstance();

            // We need to make sure that our preview and recording video size are supported by the
            // camera. Query camera to find all the sizes and choose the optimal size given the
            // dimensions of our preview surface.
            Camera.Parameters parameters = mCamera.getParameters();
            List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
            List<Camera.Size> mSupportedVideoSizes = parameters.getSupportedVideoSizes();
            Camera.Size optimalSize = CameraHelper.getOptimalVideoSize(mSupportedVideoSizes,
                    mSupportedPreviewSizes, mPreview.getWidth(), mPreview.getHeight());

            // Use the same size for recording profile.
            CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
            profile.videoFrameWidth = optimalSize.width;
            profile.videoFrameHeight = optimalSize.height;

            // likewise for the camera object itself.
            parameters.setPreviewSize(profile.videoFrameWidth, profile.videoFrameHeight);
            mCamera.setParameters(parameters);

            try {
                // Requires API level 11+, For backward compatibility use {@link setPreviewDisplay}
                // with {@link SurfaceView}
                mCamera.setPreviewTexture(mPreview.getSurfaceTexture());
            } catch (IOException e) {
                Log.e(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
                return false;
            }
            // END_INCLUDE (configure_preview)


            // BEGIN_INCLUDE (configure_media_recorder)
            mMediaRecorder = new MediaRecorder();
            mCamera.startPreview();
            // Step 1: Unlock and set camera to MediaRecorder
            mCamera.unlock();
            mMediaRecorder.setCamera(mCamera);

            // Step 2: Set sources
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

            // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
            mMediaRecorder.setProfile(profile);
            // Step 4: Set output file
            mOutputVideoFile = CameraHelper.getOutputMediaFile(context,IDNoStr);
            Log.d(TAG, "mOutputFile : " + mOutputVideoFile);

            if (mOutputVideoFile == null) {
                return false;
            }
            mMediaRecorder.setOutputFile(mOutputVideoFile.getPath());

            // Step 5: Prepare configured MediaRecorder
            try {
                mMediaRecorder.prepare();
            } catch (IllegalStateException e) {
                Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
                releaseMediaRecorder();
                return false;
            } catch (IOException e) {
                Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
                releaseMediaRecorder();
                return false;
            }
        }catch (Exception e){
            _LToast("相机准备失败！请检查摄像头接线！");
        }
        return true;
    }

    /**
     * Asynchronous task for preparing the {@link MediaRecorder} since it's a long blocking
     * operation.
     */
    @SuppressLint("StaticFieldLeak")
    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            try{
                // initialize video camera
                if (prepareVideoRecorder()) {
                    // Camera is available and unlocked, MediaRecorder is prepared,
                    // now you can start recording
                    mMediaRecorder.start();
                    isRecording = true;
                } else {
                    // prepare didn't work, release the camera
                    releaseMediaRecorder();
                    return false;
                }

            }catch (Exception e){}
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
           try{
               if (!result) {
                   RecYouTuFaceActivity.this.finish();
               }
               // inform the user that recording has started
           }catch (Exception ignored){}
        }
    }

    private void startRecordingVideo() {
        new MediaPrepareTask().execute(null, null, null);
    }

    private void stopRecordingVideo() { // stop recording and release camera
        try {
            mMediaRecorder.stop();  // stop the recording
            //mOutputVideoFile.delete();
        } catch (RuntimeException e) {
            Log.d(TAG, "RuntimeException: stop() is called immediately after start()");
        }
        releaseMediaRecorder(); // release the MediaRecorder object
        if (mCamera != null) {
            mCamera.stopPreview();
        }
        // inform the user that recording has stopped
        isRecording = false;
        releaseCamera();
    }

    @SuppressLint("SdCardPath")
    public String  saveIdCardImage(Bitmap bmp,File file) {
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 0, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getPath() ;
    }


    /**
     * Okhttp
     * 上传住客身份信息
     */
    private synchronized void mUpLoadCardInfo(SparseArray<String> LFmap, File file01, File file02) {

        for(int i = 0, nsize = LFmap.size(); i < nsize; i++) {
            Object obj = LFmap.valueAt(i);
            _Log.d("参数YOUTU_net_response", "str=" + obj);
        }
        _Log.d("参数YOUTU_net_response", "str=" + file02.getPath());
        _Log.d("参数YOUTU_net_response", "str=" + file01.getPath());


        RequestBody fileIdcard = RequestBody.create(MediaType.parse("application/octet-stream"), file01);
        RequestBody fileLive = RequestBody.create(MediaType.parse("application/octet-stream"), file02);
        //TODO okhttp
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
        //TODO okhttp3
        //RequestBody requestBody = new MultipartBody.Builder()
        //        .setType(MultipartBody.FORM)
                .addFormDataPart("deviceId", "" + LFmap.get(0))
                .addFormDataPart("name", "" + LFmap.get(1))
                .addFormDataPart("nation", "" + LFmap.get(2))
                .addFormDataPart("gender", "" + LFmap.get(3))
                .addFormDataPart("idCardNo", "" + LFmap.get(4))
                .addFormDataPart("dayOfBirth", "" + LFmap.get(5))
                .addFormDataPart("address", "" + LFmap.get(6))
                .addFormDataPart("signOrg", "" + LFmap.get(7))
                .addFormDataPart("startDate", "" + LFmap.get(8))
                .addFormDataPart("endDate", "" + LFmap.get(9))
                .addFormDataPart("orderNo", "" + LFmap.get(11))
                .addFormDataPart("roomNo", "" + LFmap.get(10))
                .addFormDataPart("faceType", "" + LFmap.get(12))
                .addFormDataPart("dataSim", "" + LFmap.get(13))
                .addFormDataPart("headImg", LFmap.get(4) + "-idcard.png", fileIdcard)
                .addFormDataPart("liveImg", LFmap.get(4) + "-live.png", fileLive)
                .addFormDataPart("liveProtoBuf","liveProtoBuf")
                .build();

        Request request = new Request.Builder()
                //.header("Authorization", "Client-ID " + "...")
                .header("User-Agent", "OkHttp Headers.java")
                .addHeader("Accept", "application/json; q=0.5")
                .addHeader("Accept", "application/vnd.OneRoom.v3+json")
                .url(_ConstantsAPI.FACE_TEST_URL)
                .post(requestBody)
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast("RecYouTuFaceActivity"+_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("YOUTU_net_response", "result=" + result);
                if (!TextUtils.isEmpty(result)) {
                    try {
                        //LFLiveBean lfLiveBean = new LinkFaceEngine().parseJSON(result);
                        Gson mgson = new Gson();
                        LFLiveBean lfLiveBean = mgson.fromJson(result, LFLiveBean.class);

                        if (lfLiveBean.code == 0) {
                            //mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_DO_INTENT_MESSAGE, 2 * 1000);
                            int TestTimes = mSharePublicData.getInt("testTimes", 0);
                            if(TestTimes>1){// 还剩几个人需要身份验证
                                TestTimes = TestTimes -1;
                                mPublicEditor.putInt("testTimes", TestTimes);
                                mPublicEditor.putInt("backfacecode", 200);// 是否需要弹 公安提示对话框
                                mPublicEditor.commit();
                                //_LToast("还剩"+TestTimes+"个人需要身份验证！！");
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(RecYouTuFaceActivity.this,ReadIdCardActivity.class);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                                    }
                                },500);
                            }else {
                                mPublicEditor.putInt("backfacecode", 0);
                                mPublicEditor.commit();
                                // 清除保存的身份证姓名
                                mUserEditor.putString("idcardNO", "");
                                mUserEditor.commit();

                                mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_SUCCESS_MESSAGE,500);
                            }
                        }else if(lfLiveBean.code == -1){
                            //_LToast(""+lfLiveBean.msg);
                        } else {
                            //_LToast("人证不一致！");
                        }
                    } catch (Exception e) {
                        //_LToast(_Constants.TOAST_PARSE_NULL);
                        e.printStackTrace();
                    }
                }else {
                    //_LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }

}
