package hk.yijian.botel.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.bean.response.NetOrderData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;


public class CheckInNetOrderActivity extends BaseActivity {

    private Button bt_confirm_order;
    private Button number_1;
    private Button number_2;
    private Button number_3;
    private Button number_4;
    private Button number_5;
    private Button number_6;
    private Button number_7;
    private Button number_8;
    private Button number_9;
    private Button number_0;
    private Button number_clear_all;
    private LinearLayout ll_order_pay_card;
    private ImageView iv_hide_keyboard;
    private TextView tv_search_order;
    private TextView number_enter;
    private ImageView number_clear_last;
    private EditText et_phone_no;
    private EditText et_phone_code;

    private TextView net_order_deposit;
    private TextView net_order_roomfee;
    private TextView tv_roomfee_roomno;
    private TextView net_deposit_roomno;
    private LinearLayout ll_no_keyboard;
    private LinearLayout ll_input_phone;
    private ImageView iv_guest_reduce;
    private ImageView iv_guest_pluse;
    private TextView tv_guest_no;
    private ExpandableListView lv_order_list;
    private LinearLayout ll_order_list;
    private ImageView iv_back;
    private ImageView iv_delete;
    //
    private NetOrderData netOrder;
    private int guestTotalNO = 0;
    private int mHotelId;
    private Context context;
    private int selectPosition = 0;//记录用户选择的变量
    private int selectGroupPosition = 0;//记录用户选择的变量
    private TimeView tv_address_countdown;
    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.NET_ORDER_CHANGE_UI:
                    _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请核实订单信息");
                    ll_order_list.setVisibility(View.VISIBLE);
                    ll_no_keyboard.setVisibility(View.INVISIBLE);
                    loadOrderList();
                    break;
            }
            super.handleMessage(msg);
        }
    };



    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_net_card);
        context = this;


        tv_search_order = (TextView) findViewById(R.id.tv_search_order);
        bt_confirm_order = (Button) findViewById(R.id.bt_confirm_order);
        ll_no_keyboard = (LinearLayout) findViewById(R.id.ll_no_keyboard);
        ll_input_phone  = (LinearLayout) findViewById(R.id.ll_input_phone);
        et_phone_no = (EditText) findViewById(R.id.et_phone_no);
        et_phone_code = (EditText) findViewById(R.id.et_phone_code);

        lv_order_list = (ExpandableListView) findViewById(R.id.lv_order_list);
        ll_order_list = (LinearLayout) findViewById(R.id.ll_order_list);
        iv_guest_reduce = (ImageView) findViewById(R.id.iv_guest_reduce);
        iv_guest_pluse = (ImageView) findViewById(R.id.iv_guest_pluse);
        tv_guest_no = (TextView) findViewById(R.id.tv_guest_no);
        net_order_deposit = (TextView) findViewById(R.id.net_order_deposit);
        net_order_roomfee = (TextView) findViewById(R.id.net_order_roomfee);
        tv_roomfee_roomno = (TextView) findViewById(R.id.tv_roomfee_roomno);
        net_deposit_roomno = (TextView) findViewById(R.id.net_deposit_roomno);
        ll_order_pay_card  = (LinearLayout) findViewById(R.id.ll_order_pay_card);
        //
        number_1 = (Button) findViewById(R.id.btn_1);
        number_2 = (Button) findViewById(R.id.btn_2);
        number_3 = (Button) findViewById(R.id.btn_3);
        number_4 = (Button) findViewById(R.id.btn_4);
        number_5 = (Button) findViewById(R.id.btn_5);
        number_6 = (Button) findViewById(R.id.btn_6);
        number_7 = (Button) findViewById(R.id.btn_7);
        number_8 = (Button) findViewById(R.id.btn_8);
        number_9 = (Button) findViewById(R.id.btn_9);
        number_0 = (Button) findViewById(R.id.btn_0);
        number_clear_all = (Button) findViewById(R.id.number_clear_all);
        iv_hide_keyboard = (ImageView) findViewById(R.id.iv_hide_keyboard);
        number_enter = (TextView) findViewById(R.id.number_enter);
        number_clear_last = (ImageView) findViewById(R.id.number_clear_last);

        ll_order_pay_card.setVisibility(View.INVISIBLE);
        hiddenInput();
        //
        mHotelId = mSharePublicData.getInt("public_hotelid", 0);
        String mMobile = mShUserData.getString("mobile", "");
        int intentCode = mShUserData.getInt("intentcode", 0);
        _Log.d("_net_response","intentCode="+intentCode);
        int status = 3;// status = 4不可选 已入住  status = 3可选 未入住
        switch(intentCode) {
            case 200://推送消息
                ll_no_keyboard.setVisibility(View.INVISIBLE);
                ll_input_phone.setVisibility(View.INVISIBLE);
                ll_order_list.setVisibility(View.VISIBLE);
                et_phone_no.setText(""+mMobile);
                String net_str ="{\"hotelId\": "+mHotelId+",\"mobile\": \""+mMobile+"\",\"status\": "+status+"}";
                mSearchNetOrder(net_str);
                break;
            case 201://刷身份证
                ll_no_keyboard.setVisibility(View.INVISIBLE);
                ll_input_phone.setVisibility(View.INVISIBLE);
                ll_order_list.setVisibility(View.VISIBLE);
                String userName = mShUserData.getString("userName", "");
                String netStr ="{\"hotelId\": "+mHotelId+",\"userName\": \""+userName+"\",\"status\": "+status+"}";
                mSearchNetOrder(netStr);
                break;
            case 203://直接点击进入

                _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请输入手机号码");
                ll_no_keyboard.setVisibility(View.VISIBLE);
                ll_input_phone.setVisibility(View.VISIBLE);
                ll_order_list.setVisibility(View.INVISIBLE);
                et_phone_no.setText("");
                break;
        }
    }

    @Override
    protected void initTitleView() {
        TitleBarManager.getInstance().init(this);
        TitleBarManager.getInstance().changeTitleText(_Constants.NET_CARD);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_ONE);
        ProgressManager.getInstance().setProgressNetInfo();
        iv_back = (ImageView)findViewById(R.id.iv_back);
        iv_delete = (ImageView)findViewById(R.id.iv_delete);
        iv_delete.setOnClickListener(mDeleteListener);
        iv_back.setOnClickListener(mBackListener);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CheckInNetOrderActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish",Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tv_address_countdown.mStopTime();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();
    }

    @Override
    protected void initClickListener() {
        iv_guest_reduce.setOnClickListener(mGuestReduceListener);
        iv_guest_pluse.setOnClickListener(mGuestPluseListener);
        bt_confirm_order.setOnClickListener(mConfirmListener);
        number_clear_last.setOnClickListener(mClearPhoneListener);
        number_clear_last.setOnLongClickListener(mClearPhoneLongListener);
        number_clear_all.setOnClickListener(mClearPhoneNoListener);
        iv_hide_keyboard.setOnClickListener(mHideBoardListener);
        et_phone_no.setOnClickListener(mHideBoardClickListener);
        //et_phone_no.setOnTouchListener(mVisibleBoardListener);
        //et_phone_no.setFocusable(true);
        tv_search_order.setOnClickListener(mEnterPhoneListener);
        number_enter.setOnClickListener(mEnterPhoneListener);

        number_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_1);
            }
        });
        number_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_2);
            }
        });
        number_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_3);
            }
        });
        number_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_4);
            }
        });
        number_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_5);
            }
        });
        number_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_6);
            }
        });
        number_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_7);
            }
        });
        number_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_8);
            }
        });
        number_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_9);
            }
        });
        number_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPhoneNo(number_0);

            }
        });
    }

    @Override
    protected void initTouchListener() {

    }

    private final View.OnClickListener mBackListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(CheckInNetOrderActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    };

    private final View.OnClickListener mDeleteListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            backLoginPage(CheckInNetOrderActivity.this);
        }
    };

    private final View.OnClickListener mEnterPhoneListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            final String mPhone = et_phone_no.getText().toString();

            if (TextUtils.isEmpty(mPhone)) {
                _LToast("请输入手机号码");
            } else {//matches():正则表达式匹配
                if (mPhone != null && mPhone.length() == 11) {
                    Pattern pattern = Pattern.compile("^1[3|4|5|6|7|8][0-9]\\d{8}$");
                    Matcher matcher = pattern.matcher(mPhone);
                    if (matcher.find()) {
                        int status = 3;//
                        String net_str ="{\"hotelId\": "+mHotelId+",\"mobile\": \""+mPhone+"\",\"status\": "+status+"}";
                        _Log.d("mEnterPhoneListener","mEnterPhoneListener="+net_str);
                        mSearchNetOrder(net_str);
                    } else {
                        _SToast("输入号码有误！");
                        ll_no_keyboard.setVisibility(View.VISIBLE);
                    }
                } else {
                    _SToast("输入手机号码不正确");
                    ll_no_keyboard.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    private final View.OnClickListener mConfirmListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String _todayTime = df.format(System.currentTimeMillis());
            String _checkInTime = getDayStr(netOrder.data.get(selectGroupPosition).startDate);
            String _checkOutTime = getDayStr(netOrder.data.get(selectGroupPosition).endDate);
            _Log.i("time_net_response",
                    "_checkInTime =" +_checkInTime +
                    "_todayTime =" +_todayTime +
                    "_checkOutTime"+_checkOutTime);

            int _compareDate = compare_date(_todayTime,_checkOutTime);
            if(_compareDate == 1){
                _LToast("该订单已经过期！");
                return;
            }
            if(_todayTime.equals(_checkInTime)){//检验入住时间是否是合法
                if(guestTotalNO == 0){
                    _LToast("请选定入住人！");
                    return;
                }
                mOrderEditor.putInt("guesttotal", guestTotalNO);     //判断出多少张房卡
                mOrderEditor.putString("ticket_orderno", netOrder.data.get(selectGroupPosition).orderNo);
                mOrderEditor.putString("ticket_orderStatus", "" + netOrder.data.get(selectGroupPosition).orderStatus);
                mOrderEditor.putString("ticket_netguestName", ""+netOrder.data.get(selectGroupPosition).rooms.get(selectPosition).guestName);
                mOrderEditor.putString("ticket_roomno", netOrder.data.get(selectGroupPosition).rooms.get(selectPosition).roomNo);
                mOrderEditor.putInt("ticket_depositFee", netOrder.data.get(selectGroupPosition).depositFee);
                mOrderEditor.commit();
                mPublicEditor.putInt("testTimes", guestTotalNO);     //判断多少人需要人脸识别
                mPublicEditor.commit();
                /**
                 *  身份验证 / 人脸识别
                 */
                Intent intent = new Intent(CheckInNetOrderActivity.this, ReadIdCardActivity.class);
                intent.putExtra(_Constants.BUNDLE_REY,_Constants.BUNDLE_NET_VALUE);
                startActivity(intent);
                finish();
                //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
            }else {
                _LToast("该订单的入住时间不是今天！");

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        backLoginPage(CheckInNetOrderActivity.this);

                    }
                },1500);
                return;
            }

        }
    };

    private final View.OnClickListener mGuestReduceListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            if (guestTotalNO <= 1) {
            } else if(guestTotalNO == 0){
                _LToast("请选定入住人");
            }else {
                guestTotalNO = guestTotalNO - 1;
                tv_guest_no.setText("" + guestTotalNO);
            }
        }
    };

    private final View.OnClickListener mGuestPluseListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            if (guestTotalNO >= 2) {
            } else if(guestTotalNO == 0){
                _LToast("请选定入住人");
            }else {
                guestTotalNO = guestTotalNO + 1;
                tv_guest_no.setText("" + guestTotalNO);
            }
        }
    };

    private final View.OnClickListener mHideBoardListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            ll_no_keyboard.setVisibility(View.INVISIBLE);
        }
    };

    private final View.OnClickListener mHideBoardClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            if (ll_order_list.getVisibility() == View.INVISIBLE) {
                ll_no_keyboard.setVisibility(View.VISIBLE);
            }
        }
    };

    private final View.OnTouchListener mVisibleBoardListener = new View.OnTouchListener() {


        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (ll_order_list.getVisibility() == View.INVISIBLE) {
                ll_no_keyboard.setVisibility(View.VISIBLE);
            }
            return false;
        }
    };

    private final View.OnLongClickListener mClearPhoneLongListener = new View.OnLongClickListener() {

        @Override
        public boolean onLongClick(View view) {
            et_phone_no.setText("");
            return false;
        }
    };

    private final View.OnClickListener mClearPhoneListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            String roomIdInput = et_phone_no.getText().toString();
            if (roomIdInput.length() > 0) {
                et_phone_no.setText(roomIdInput.substring(0, roomIdInput.length() - 1));
                et_phone_no.setSelection(roomIdInput.length() - 1);
            }
        }
    };

    private final View.OnClickListener mClearPhoneNoListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            et_phone_no.setText("");
        }
    };

    /**
     * 输入号码长度限制
     * @param tv
     */
    private void inputPhoneNo(TextView tv) {
        String roomInput = et_phone_no.getText().toString();
        if (null != roomInput && !"".equals(roomInput)) {
            et_phone_no.setText(roomInput + tv.getText().toString());
            et_phone_no.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
            et_phone_no.setText(roomInput + tv.getText().toString());
        } else {
            et_phone_no.setText(roomInput + tv.getText().toString());
            //et_phone_no.setSelection(roomInput.length() + 1);
        }
    }
    /**
     * 隐藏输入法
     */
    private void hiddenInput(){
        if (android.os.Build.VERSION.SDK_INT <= 10) {
            et_phone_no.setInputType(InputType.TYPE_NULL);
            et_phone_code.setInputType(InputType.TYPE_NULL);
        } else {
            CheckInNetOrderActivity.this.getWindow().setSoftInputMode(WindowManager.
                    LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            try {
                Class<EditText> cls = EditText.class;
                Method setSoftInputShownOnFocus;
                setSoftInputShownOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
                setSoftInputShownOnFocus.setAccessible(true);
                setSoftInputShownOnFocus.invoke(et_phone_no, false);
                setSoftInputShownOnFocus.invoke(et_phone_code, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 比较两个时间大小
     * @param DATE1
     * @param DATE2
     * @return
     */
    private int compare_date(String DATE1, String DATE2) {
        int i = 0;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                _Log.i("dt1在dt2前");
                i = 1;
                return i;
            } else if (dt1.getTime() < dt2.getTime()) {
                _Log.i("dt1在dt2后");
                i = -1;
                return i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }
    /**
     * 将时间转为 yyyy-MM-dd 格式
     */
    private String getDayStr(String dayAfter) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(dayAfter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day);

        String mDayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return mDayAfter;
    }


    /**
     *  显示订单数据
     */
    private void loadOrderList(){
        /*
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd");
        Date curDate = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(curDate);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        formatter.format(calendar.getTime());
        */
        ProgressManager.getInstance().mShowProgress();
        ll_order_list.setVisibility(View.VISIBLE);
        ll_no_keyboard.setVisibility(View.INVISIBLE);
        int roomSize = 1;
        guestTotalNO = 1;
        DecimalFormat df = new DecimalFormat("0.00");
        String depositFee = df.format((double)(netOrder.data.get(selectPosition).depositFee)/100);
        String roomFee = df.format((double)(netOrder.data.get(selectPosition).roomFee)/100);
        net_order_deposit.setText("房费:   ￥"+ depositFee +"元");
        net_order_roomfee.setText("押金:   ￥"+ roomFee +"元");
        //tv_roomfee_roomno.setText("（"+roomSize+"间）");
        //net_deposit_roomno.setText("（"+roomSize+"间）");
        tv_guest_no.setText(""+guestTotalNO);

        final mNetListAdapter mNetListAdapter = new mNetListAdapter(this);
        lv_order_list.setAdapter(mNetListAdapter);
        lv_order_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                if(netOrder.data.get(groupPosition).rooms.get(childPosition).status == 4){
                    _SToast("已入住不可选");
                    selectGroupPosition = groupPosition;
                    selectPosition = childPosition;
                }else {
                    selectGroupPosition = groupPosition;
                    selectPosition = childPosition;
                    mNetListAdapter.notifyDataSetChanged();
                }
                _Log.d("groupPosition_net_response","groupPosition="+groupPosition+"——childPosition="+childPosition);
                return true;
            }
        });
        int groupCount = lv_order_list.getCount();
        for (int i=0; i<groupCount; i++) {
            lv_order_list.expandGroup(i);
        }
        lv_order_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;//不可点击
            }
        });
    }

    /**
     * Order List 订单列表
     */
    public class mNetListAdapter extends BaseExpandableListAdapter {
        Context context;

        public mNetListAdapter(Context context){
            this.context = context;
        }

        @Override
        public int getGroupCount() {
            return netOrder.data.size();
        }

        @Override
        public int getChildrenCount(int i) {
            return netOrder.data.get(i).rooms.size();
        }

        @Override
        public Object getGroup(int i) {
            return netOrder.data.get(i);
        }

        @Override
        public Object getChild(int i, int i1) {
            return netOrder.data.get(i).rooms.get(i1);
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int i1) {
            return i1;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
            final GroupHolder holder;
            if(convertView==null){
                convertView= LayoutInflater.from(context).inflate(R.layout.list_orderdetial_title,
                        viewGroup,false);
                holder = new GroupHolder();
                holder.tv_order_no = convertView.findViewById(R.id.tv_order_no);
                holder.tv_time_checkin = convertView.findViewById(R.id.tv_time_checkin);
                holder.tv_time_checkout = convertView.findViewById(R.id.tv_time_checkout);
                convertView.setTag(holder);
            }else{
                holder = (GroupHolder)convertView.getTag();
            }
            String mPay;
            if ((netOrder.data.get(i).orderStatus) == 3) {
                mPay = "已付";
            } else {
                mPay = "到付";
            }
            try {
                holder.tv_order_no.setText(Html.fromHtml("<font size=\"3\" color=\"white\">订单编号："+
                        netOrder.data.get(i).orderNo+"("+"</font><font size=\"3\" color=\"red\">" +
                        mPay + "</font><font size=\"3\" color=\"white\">)</font>"));
                holder.tv_time_checkin.setText("入住时间：" + netOrder.data.get(i).startDate);
                holder.tv_time_checkout.setText("离店时间：" + netOrder.data.get(i).endDate);

            }catch (Exception ignored){}
            return convertView;
        }

        @Override
        public View getChildView(final int i,final int i1, boolean b, View convertView, ViewGroup viewGroup) {
            final ChildHolder holder;
            if(convertView==null){
                convertView=LayoutInflater.from(context).inflate(R.layout.list_orderdetial_item,
                        viewGroup,false);
                holder = new ChildHolder();
                holder.lv_serial_no = convertView.findViewById(R.id.lv_serial_no);
                holder.tv_room_type = convertView.findViewById(R.id.tv_room_type);
                holder.tv_room_no = convertView.findViewById(R.id.tv_room_no);
                holder.tv_order_name = convertView.findViewById(R.id.tv_order_name);
                holder.cb_order_checkbox = convertView.findViewById(R.id.cb_order_checkbox);
                convertView.setTag(holder);
            }else{
                holder = (ChildHolder) convertView.getTag();
            }

            holder.lv_serial_no.setText(""+(i1+1));
            holder.tv_room_no.setText(netOrder.data.get(i).rooms.get(i1).roomNo);
            holder.tv_room_type.setText(netOrder.data.get(i).rooms.get(i1).roomTypeName);
            holder.tv_order_name.setText("" +netOrder.data.get(i).rooms.get(i1).guestName);
            if(netOrder.data.get(i).rooms.get(i1).status == 4
                    || netOrder.data.get(i).rooms.get(i1).status == 6){
                // status = 4 或6 不可选 已入住  status = 3可选 未入住

                holder.lv_serial_no.setTextColor(0xFFCCCCCC);
                holder.tv_room_no.setTextColor(0xFFCCCCCC);
                holder.tv_room_type.setTextColor(0xFFCCCCCC);
                holder.tv_order_name.setTextColor(0xFFCCCCCC);
                //holder.cb_order_checkbox.setVisibility(View.INVISIBLE);
            }else {
                holder.lv_serial_no.setTextColor(0xFF333333);
                holder.tv_room_no.setTextColor(0xFF333333);
                holder.tv_room_type.setTextColor(0xFF333333);
                holder.tv_order_name.setTextColor(0xFF333333);
            }

            if(selectGroupPosition == i && selectPosition == i1){
                if(netOrder.data.get(i).rooms.get(i1).status == 4
                        || netOrder.data.get(i).rooms.get(i1).status == 6){

                    holder.cb_order_checkbox.setChecked(false);
                    //ll_order_pay_card.setVisibility(View.VISIBLE);
                    guestTotalNO = 0;
                }else {
                    holder.cb_order_checkbox.setChecked(true);
                    ll_order_pay_card.setVisibility(View.VISIBLE);
                    /*
                    BigDecimal orderFee = new BigDecimal(netOrder.data.get(i).orderFee/100);
                    DecimalFormat df = new DecimalFormat("00");
                    String mOrderFee = df.format(orderFee);
                    BigDecimal orderFeeN = new BigDecimal(netOrder.data.get(i).depositFee/100);
                    DecimalFormat dfN = new DecimalFormat("00");
                    String mOrderFeeN = dfN.format(orderFeeN);*/
                    DecimalFormat df = new DecimalFormat("0.00");
                    String roomFee = df.format((double)(netOrder.data.get(i).roomFee)/100);
                    String depositFee = df.format((double)(netOrder.data.get(i).depositFee)/100);
                    net_order_deposit.setText("房费:   ￥"+ roomFee +"元");
                    net_order_roomfee.setText("押金:   ￥"+ depositFee +"元");

                    int roomSize = 1;
                    guestTotalNO = 1;
                    //tv_roomfee_roomno.setText("（"+roomSize+"间）");
                    //net_deposit_roomno.setText("（"+roomSize+"间）");
                    tv_guest_no.setText(""+guestTotalNO);
                }
            }else{
                holder.cb_order_checkbox.setChecked(false);
            }

            return convertView;
        }

        /**
         * ExpandableListView 子条目需要响应click事件,返回true
         * */
        @Override
        public boolean isChildSelectable(int i, int i1) {
            return true;
        }
    }

    public static class GroupHolder{
        TextView tv_order_no;
        TextView tv_time_checkin;
        TextView tv_time_checkout;
    }
    public static class ChildHolder{
        TextView lv_serial_no;
        TextView tv_room_type;
        TextView tv_room_no;
        TextView tv_order_name;
        RadioButton cb_order_checkbox;
    }

    /**
     * 查询网络订单
     * @param str
     */
    private synchronized void mSearchNetOrder(String str) {
        _Log.d("网订取卡参数_net_response","str="+str);

        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.NET_ORDER_LIST_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("网订取卡_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    try {
                        Gson mgson = new Gson();
                        netOrder = mgson.fromJson(result, NetOrderData.class);
                        if (netOrder.code == 0) {
                            if(netOrder.data!=null && !netOrder.data.isEmpty()){
                                mHandler.sendEmptyMessage(_HandlerCode.NET_ORDER_CHANGE_UI);
                            }else {
                                //_LToast(_Constants.TOAST_DATA_NULL);
                                _LToast("该号码无网络订单");
                            }
                        }else if(netOrder.code == -1){
                            _LToast(""+netOrder.msg);
                        }
                    } catch (Exception e) {
                        _LToast(_Constants.TOAST_PARSE_NULL);
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }
}


