package hk.yijian.botel.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.HomeActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.fragment.HomeFragment;
import hk.yijian.botel.fragment.HotFragment;
import hk.yijian.botel.fragment.PriceFragment;
import hk.yijian.botel.fragment.ThemeFragment;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._Log;

public class CheckInAllRoomDataActivity extends BaseActivity {

    private LinearLayout bt_menu_all;
    private LinearLayout bt_menu_price;
    private LinearLayout bt_menu_hot;
    private LinearLayout bt_menu_theme;
    private HomeFragment allRoomFragment;
    private HotFragment hotFragment;
    private PriceFragment priceFragment;
    private ThemeFragment themeFragment;
    private ImageView iv_all_icon;
    private ImageView iv_price_icon;
    private ImageView iv_hot_icon;
    private ImageView iv_theme_icon;
    private ImageView menu_icon_click01;
    private ImageView menu_icon_click02;
    private ImageView menu_icon_click03;
    private ImageView menu_icon_click04;
    private TextView tv_all_text;
    private TextView tv_price_text;
    private TextView tv_hot_text;
    private TextView tv_theme_text;
    private ImageView iv_back;
    private ImageView iv_detele;
    private int isCheck = 0;
    private TimeView tv_address_countdown;

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_check_in);

        bt_menu_all = (LinearLayout) findViewById(R.id.bt_menu_all);
        bt_menu_price = (LinearLayout) findViewById(R.id.bt_menu_price);
        bt_menu_hot = (LinearLayout) findViewById(R.id.bt_menu_hot);
        bt_menu_theme = (LinearLayout) findViewById(R.id.bt_menu_theme);
        iv_all_icon = (ImageView) findViewById(R.id.iv_all_icon);
        iv_price_icon = (ImageView) findViewById(R.id.iv_price_icon);
        iv_hot_icon = (ImageView) findViewById(R.id.iv_hot_icon);
        iv_theme_icon = (ImageView) findViewById(R.id.iv_theme_icon);
        tv_all_text = (TextView) findViewById(R.id.tv_all_text);
        tv_price_text = (TextView) findViewById(R.id.tv_price_text);
        tv_hot_text = (TextView) findViewById(R.id.tv_hot_text);
        tv_theme_text = (TextView) findViewById(R.id.tv_theme_text);
        tv_hot_text = (TextView) findViewById(R.id.tv_hot_text);
        tv_theme_text = (TextView) findViewById(R.id.tv_theme_text);
        //
        menu_icon_click01 = (ImageView) findViewById(R.id.menu_icon_click01);
        menu_icon_click02 = (ImageView) findViewById(R.id.menu_icon_click02);
        menu_icon_click03 = (ImageView) findViewById(R.id.menu_icon_click03);
        menu_icon_click04 = (ImageView) findViewById(R.id.menu_icon_click04);

        iv_all_icon.setBackgroundResource(R.mipmap.img_menu_icon_click_001);
        iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_003);
        iv_hot_icon.setBackgroundResource(R.mipmap.img_menu_icon_002);
        iv_theme_icon.setBackgroundResource(R.mipmap.img_menu_icon_004);
        tv_all_text.setTextColor(Color.parseColor("#FFc300"));
        tv_price_text.setTextColor(Color.parseColor("#000000"));
        tv_hot_text.setTextColor(Color.parseColor("#000000"));
        tv_theme_text.setTextColor(Color.parseColor("#000000"));
        menu_icon_click01.setVisibility(View.VISIBLE);
        menu_icon_click02.setVisibility(View.INVISIBLE);
        menu_icon_click03.setVisibility(View.INVISIBLE);
        menu_icon_click04.setVisibility(View.INVISIBLE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        allRoomFragment = HomeFragment.newInstance();
        transaction.replace(R.id.frame_content, allRoomFragment).commit();

    }

    @Override
    protected void initTitleView() {
        iv_back = (ImageView)findViewById(R.id.iv_back);
        iv_detele = (ImageView)findViewById(R.id.iv_delete);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(CheckInAllRoomDataActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),"CountDownFinish",Toast.LENGTH_LONG).show();
            }
        });
        iv_back.setOnClickListener(mBackListener);
        iv_detele.setOnClickListener(mBackHomeListener);
        _Log.d("HomeFragment","onResume=");
        _BaiduTTSUtils.getInstance()._OpenBaiduTTS("请选择入住房型");
        TitleBarManager.getInstance().init(this);
        TitleBarManager.getInstance().changeTitleText(_Constants.CHECK_IN);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().showProgress(_Constants.PROGRESS_ONE);
        ProgressManager.getInstance().setProgressInInfo();

    }


    @Override
    protected void initClickListener() {

        bt_menu_all.setOnClickListener(mAllListener);
        bt_menu_price.setOnClickListener(mPriceListener);
        bt_menu_hot.setOnClickListener(mHotListener);
        bt_menu_theme.setOnClickListener(mThemeListener);
    }

    @Override
    protected void initTouchListener() {
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        _Log.d("HomeFragment","onDestroy=");
        tv_address_countdown.mStopTime();
        _BaiduTTSUtils.getInstance()._StopBaiduTTS();

    }


    private final View.OnClickListener mBackListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(CheckInAllRoomDataActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    };
    private final View.OnClickListener mBackHomeListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            backLoginPage(CheckInAllRoomDataActivity.this);
        }
    };

    /**
     * 所有房间
     */
    private final View.OnClickListener mAllListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            menu_icon_click01.setVisibility(View.VISIBLE);
            menu_icon_click02.setVisibility(View.INVISIBLE);
            menu_icon_click03.setVisibility(View.INVISIBLE);
            menu_icon_click04.setVisibility(View.INVISIBLE);
            iv_all_icon.setBackgroundResource(R.mipmap.img_menu_icon_click_001);
            iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_003);
            iv_hot_icon.setBackgroundResource(R.mipmap.img_menu_icon_002);
            iv_theme_icon.setBackgroundResource(R.mipmap.img_menu_icon_004);
            tv_all_text.setTextColor(Color.parseColor("#FFc300"));
            tv_price_text.setTextColor(Color.parseColor("#000000"));
            tv_hot_text.setTextColor(Color.parseColor("#000000"));
            tv_theme_text.setTextColor(Color.parseColor("#000000"));

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (allRoomFragment == null) {
                allRoomFragment = HomeFragment.newInstance();
            }
            transaction.replace(R.id.frame_content, allRoomFragment);
            transaction.addToBackStack(null);
            transaction.commit();



        }
    };
    /**
     * 价格排序
     */
    private final View.OnClickListener mPriceListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            menu_icon_click01.setVisibility(View.INVISIBLE);
            menu_icon_click02.setVisibility(View.VISIBLE);
            menu_icon_click03.setVisibility(View.INVISIBLE);
            menu_icon_click04.setVisibility(View.INVISIBLE);
            iv_all_icon.setBackgroundResource(R.mipmap.img_menu_icon_001);
            //iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_click_003);
            iv_hot_icon.setBackgroundResource(R.mipmap.img_menu_icon_002);
            iv_theme_icon.setBackgroundResource(R.mipmap.img_menu_icon_004);

            tv_all_text.setTextColor(Color.parseColor("#000000"));
            tv_price_text.setTextColor(Color.parseColor("#FFc300"));
            tv_hot_text.setTextColor(Color.parseColor("#000000"));
            tv_theme_text.setTextColor(Color.parseColor("#000000"));

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (isCheck == 0) {
                isCheck = isCheck + 1;
                priceFragment = PriceFragment.newInstance("small");
                iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_003);
            } else if (isCheck == 1) {
                isCheck = isCheck - 1;
                priceFragment = PriceFragment.newInstance("big");
                iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_click_003);
            }
            transaction.replace(R.id.frame_content, priceFragment);
            transaction.addToBackStack(null);
            transaction.commit();


        }
    };
    /**
     * 热门房
     */
    private final View.OnClickListener mHotListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            menu_icon_click01.setVisibility(View.INVISIBLE);
            menu_icon_click02.setVisibility(View.INVISIBLE);
            menu_icon_click03.setVisibility(View.VISIBLE);
            menu_icon_click04.setVisibility(View.INVISIBLE);
            iv_all_icon.setBackgroundResource(R.mipmap.img_menu_icon_001);
            iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_003);
            iv_hot_icon.setBackgroundResource(R.mipmap.img_menu_icon_click_002);
            iv_theme_icon.setBackgroundResource(R.mipmap.img_menu_icon_004);
            tv_all_text.setTextColor(Color.parseColor("#000000"));
            tv_price_text.setTextColor(Color.parseColor("#000000"));
            tv_hot_text.setTextColor(Color.parseColor("#FFc300"));
            tv_theme_text.setTextColor(Color.parseColor("#000000"));

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (hotFragment == null) {
                hotFragment = HotFragment.newInstance();
            }
            transaction.replace(R.id.frame_content, hotFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }
    };
    /**
     * 主题房
     */
    private final View.OnClickListener mThemeListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            menu_icon_click01.setVisibility(View.INVISIBLE);
            menu_icon_click02.setVisibility(View.INVISIBLE);
            menu_icon_click03.setVisibility(View.INVISIBLE);
            menu_icon_click04.setVisibility(View.VISIBLE);
            iv_all_icon.setBackgroundResource(R.mipmap.img_menu_icon_001);
            iv_price_icon.setBackgroundResource(R.mipmap.img_menu_icon_003);
            iv_hot_icon.setBackgroundResource(R.mipmap.img_menu_icon_002);
            iv_theme_icon.setBackgroundResource(R.mipmap.img_menu_icon_click_004);

            tv_all_text.setTextColor(Color.parseColor("#000000"));
            tv_price_text.setTextColor(Color.parseColor("#000000"));
            tv_hot_text.setTextColor(Color.parseColor("#000000"));
            tv_theme_text.setTextColor(Color.parseColor("#FFc300"));

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (themeFragment == null) {
                themeFragment = ThemeFragment.newInstance();
            }
            transaction.replace(R.id.frame_content, themeFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    };

}
