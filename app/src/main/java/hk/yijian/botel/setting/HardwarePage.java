package hk.yijian.botel.setting;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.activity.OutCardOutTicketActivity;
import hk.yijian.botel.util.NetUtils;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._RoomCardUtils;
import hk.yijian.hardware.cameraTest.CameraUtils;
import hk.yijian.hardware.idcardTest.IDcardActivityTest;
import hk.yijian.ykprinter._YKPrintUtils;

public class HardwarePage extends BaseActivity {

    //
    public TextView content;
    public TextView tv_refresh;
    //
    public TextView tv_is_connected;
    public TextView tv_network_type;
    public Button bt_settings_wifi;
    //
    public TextView tv_printer_status;
    public TextView tv_roomcard_status;
    public Button bt_printer_test;
    public Button bt_roomcard_test;
    public TextView tv_idcard_status;
    public Button bt_idcard_test;
    public TextView tv_camera_hardware;
    public Button bt_camera_test;
    private TextView tv_title_name;
    private LinearLayout ll_progress_bar;
    private LinearLayout ll_time_table;
    private LinearLayout ll_text_signal;
    private ImageView iv_back;
    private ImageView iv_delete;


    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_hardware);
        tv_title_name = (TextView) findViewById(R.id.tv_title_name);
        ll_progress_bar = (LinearLayout) findViewById(R.id.ll_progress_bar);
        ll_time_table = (LinearLayout) findViewById(R.id.ll_time_table);
        ll_text_signal = (LinearLayout) findViewById(R.id.ll_text_signal);
        content = (TextView) findViewById(R.id.content);
        tv_refresh = (TextView) findViewById(R.id.tv_refresh);

        //
        tv_is_connected = (TextView) findViewById(R.id.tv_is_connected);
        tv_network_type = (TextView) findViewById(R.id.tv_network_type);
        bt_settings_wifi = (Button) findViewById(R.id.bt_settings_wifi);
        //
        tv_camera_hardware = (TextView) findViewById(R.id.tv_camera_hardware);
        bt_camera_test = (Button) findViewById(R.id.bt_camera_test);
        //
        tv_printer_status = (TextView) findViewById(R.id.tv_printer_status);
        bt_printer_test = (Button) findViewById(R.id.bt_printer_test);
        tv_roomcard_status = (TextView) findViewById(R.id.tv_roomcard_status);
        bt_roomcard_test = (Button) findViewById(R.id.bt_roomcard_test);
        tv_idcard_status = (TextView) findViewById(R.id.tv_idcard_status);
        bt_idcard_test = (Button) findViewById(R.id.bt_idcard_test);

        refresh();

        ll_time_table.setVisibility(View.GONE);
        ll_progress_bar.setVisibility(View.GONE);
        ll_text_signal.setVisibility(View.GONE);
        //iv_back.setVisibility(View.INVISIBLE);
        //tv_title_name.setText(Html.fromHtml("<font size=\"3\" color=\"red\">" +
        //        "硬件连接</font><font size=\"3\" color=\"green\">状态</font>"));
        tv_title_name.setText("硬件连接状态");

        boolean  isConnected = NetUtils.isConnected(HardwarePage.this);
        String networkTypeName = NetUtils.getNetworkTypeName(HardwarePage.this);
        if (isConnected) {
            tv_is_connected.setText("有网络");
        } else {
            tv_is_connected.setText("无网络");
        }
        if (networkTypeName.equals("wifi")) {
            tv_network_type.setText("网络类型：wifi" );
        }else if(networkTypeName.equals("3g")){
            tv_network_type.setText("网络类型：3g");
        }else if(networkTypeName.equals("2g")){
            tv_network_type.setText("网络类型：2g");
        }else if(networkTypeName.equals("wap")){
            tv_network_type.setText("网络类型：wap");
        }else if(networkTypeName.equals("有线网络")){
            tv_network_type.setText("网络类型：有线网络");
        }else if(networkTypeName.equals("disconnect")){
            tv_network_type.setText("网络：disconnect");
        }else {
            tv_network_type.setText("网络类型：有线");
        }

    }

    @Override
    protected void initTitleView() {
        iv_back = findViewById(R.id.iv_back);
        iv_delete = findViewById(R.id.iv_delete);
        ProgressManager.getInstance().init(this);
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backLoginPage(HardwarePage.this);

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HardwarePage.this, FunctionActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void initClickListener() {

        bt_roomcard_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    int initRoomCardState = _RoomCardUtils.getInstance().InitLink();
                    int roomCard = _RoomCardUtils.getInstance().K720_Query();
                    if (roomCard == 0) {
                        tv_roomcard_status.setText("连接良好");
                    } else {
                        tv_roomcard_status.setText("连接异常");
                    }
                    //_RoomCardUtils.getInstance().OutRoomCard();
                }catch (Exception e){}

            }
        });

        bt_printer_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    String ticketData = "test";
                    //YKprinter
                    if(_YKPrintUtils.getInstance().initYKPrinter(HardwarePage.this)){
                        Log.d("_YKPrintUtils","初始化成功");
                        _YKPrintUtils.getInstance().printTicketData(ticketData);
                    }else {
                        Log.d("_YKPrintUtils","初始化失败");
                    }
                    /*
                    _UsbPrintUtils.getInstance().InitUsbPrint(HardwarePage.this);
                    int usbPrinter = _UsbPrintUtils.getInstance().getPrinterStatus();
                    if (usbPrinter == (-1)) {
                        tv_printer_status.setText("连接异常");
                    }else {
                        tv_printer_status.setText("连接良好");
                    }
                    SparseArray<String> mTicket = new SparseArray<String>();
                    mTicket.put(1, "测试_printStr");
                    mTicket.put(2, "测试_printStr");
                    mTicket.put(3, "测试_printStr");
                    _UsbPrintUtils.getInstance().getPrintTicketData(HardwarePage.this, mTicket);
                    */
                }catch (Exception e){}
                //_UsbPrintUtils.getInstance().CloseUsbPrint();
            }
        });

        bt_idcard_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    _IDCardUtils.getInstance().InitIDCardDevice(HardwarePage.this);
                    int IDCardDevice = _IDCardUtils.getInstance().getSAMStatus();
                    if (IDCardDevice == 144) {
                        tv_idcard_status.setText("连接良好");
                    } else {
                        tv_idcard_status.setText("连接异常");
                    }
                }catch (Exception e){}

                Intent intent = new Intent(HardwarePage.this, IDcardActivityTest.class);
                startActivity(intent);
            }
        });

        bt_camera_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                boolean isConnect = CameraUtils.hasCameraDevice(HardwarePage.this);
                if (isConnect) {
                    tv_camera_hardware.setText("支持摄像");
                } else {
                    tv_camera_hardware.setText("不支持摄像");
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);
                //Intent intent = new Intent(HardwarePage.this, CameraActivityTest.class);
                //startActivity(intent);
            }
        });

        tv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
            }
        });

        bt_settings_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetUtils.openWifiNetSetting(HardwarePage.this);
                openWifi();
                mWindowBack(HardwarePage.this, FunctionActivity.class);
            }
        });
    }

    @Override
    protected void initTouchListener() {

    }

    @SuppressLint("SetTextI18n")
    private void refresh() {
        List<String> deviceNames = getConnectedDevicesNames();
        if (deviceNames.isEmpty())
            content.setText("no devices");
        else {
            content.setText("");
            for (String name : deviceNames)
                content.append(name);
        }
    }

    @SuppressLint("NewApi")
    public List<String> getConnectedDevicesNames() {
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        List<String> deviceNames = new ArrayList<>();
        while (deviceIterator.hasNext()) {
            UsbDevice device = deviceIterator.next();
            deviceNames.add(device.getDeviceName() + "_");
            deviceNames.add("DeviceId=" + device.getDeviceId() + "_");
            deviceNames.add("VendorId=" + device.getVendorId() + "_");
            //deviceNames.add("ProductName=" + device.getProductName() + "\r\n");
        }
        return deviceNames;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
            } else{
            }
        }
    }

}
