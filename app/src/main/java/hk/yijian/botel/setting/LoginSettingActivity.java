package hk.yijian.botel.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.util.AppUtils;
import hk.yijian.botel.util.HidekeyBoard;
import hk.yijian.botel.view.ProgressManager;

public class LoginSettingActivity extends BaseActivity {

    private LinearLayout ll_progress_bar;
    private TextView tv_title_name;
    private ImageView tv_visible_input;
    private TextView myCourse_roomId_input;
    private TextView set_tv_search_order;
    private LinearLayout ll_time_table;
    private LinearLayout ll_text_signal;
    private TextView tv_version_info;
    private TextView tv_model_info;
    private TextView tv_device_info;
    private TextView tv_mac_info;
    private ImageView iv_back;
    private ImageView iv_delete;

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_wifi_setting);

        tv_visible_input = (ImageView) findViewById(R.id.tv_visible_input);
        ll_progress_bar = (LinearLayout) findViewById(R.id.ll_progress_bar);
        myCourse_roomId_input = (TextView) findViewById(R.id.myCourse_roomId_input);
        myCourse_roomId_input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        set_tv_search_order = (TextView) findViewById(R.id.set_tv_search_order);
        tv_title_name = (TextView) findViewById(R.id.tv_title_name);
        ll_time_table = (LinearLayout) findViewById(R.id.ll_time_table);
        ll_text_signal = (LinearLayout) findViewById(R.id.ll_text_signal);
        tv_version_info = (TextView) findViewById(R.id.tv_version_info);
        tv_model_info = (TextView) findViewById(R.id.tv_model_info);
        tv_device_info = (TextView) findViewById(R.id.tv_device_info);
        tv_mac_info = (TextView) findViewById(R.id.tv_mac_info);


        HidekeyBoard.init(LoginSettingActivity.this);
        //
        //iv_back.setVisibility(View.GONE);
        ll_time_table.setVisibility(View.GONE);
        ll_progress_bar.setVisibility(View.GONE);
        ll_text_signal.setVisibility(View.GONE);
        //iv_back.setVisibility(View.INVISIBLE);
        //tv_title_name.setText(Html.fromHtml("<font size=\"3\" color=\"red\">" +
        //        "一间科技</font><font size=\"3\" color=\"green\">设置</font>"));
        tv_title_name.setText("一间科技设置");
        tv_model_info.setText(android.os.Build.MODEL);
        tv_device_info.setText("一间科技自助入住机");
        tv_version_info.setText(AppUtils.getLocalVersionName(LoginSettingActivity.this));
        tv_mac_info.setText(mDeviceId);
    }

    @Override
    protected void initTitleView() {
        iv_back = findViewById(R.id.iv_back);
        iv_delete = findViewById(R.id.iv_delete);
        ProgressManager.getInstance().init(this);
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        iv_delete.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                backLoginPage(LoginSettingActivity.this);
                return true;
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        iv_back.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                backLoginPage(LoginSettingActivity.this);
                return true;
            }
        });
    }


    @Override
    protected void initClickListener() {




        myCourse_roomId_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        set_tv_search_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        set_tv_search_order.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                /*
                if (myCourse_roomId_input.getText().toString().trim().equals("123456")) {
                    Intent wifi = new Intent();
                    ComponentName wificn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                    wifi.setComponent(wificn);
                    startActivity(wifi);
                    openWifi();
                    mWindowBack(LoginSettingActivity.this, LoginSettingActivity.class);
                } else if (myCourse_roomId_input.getText().toString().trim().equals("456789")) {

                    Intent intent = new Intent(LoginSettingActivity.this, FunctionActivity.class);
                    startActivity(intent);
                }*/
                if (myCourse_roomId_input.getText().toString().trim().equals("456789")) {
                    Intent intent = new Intent(LoginSettingActivity.this, FunctionActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });
    }


    @Override
    protected void initTouchListener() {

    }

}
