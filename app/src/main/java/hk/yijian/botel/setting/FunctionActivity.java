package hk.yijian.botel.setting;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.view.ProgressManager;

public class FunctionActivity extends BaseActivity {

    private TextView tv_title_name;
    private LinearLayout ll_progress_bar;
    private LinearLayout ll_os_settings;
    private LinearLayout ll_file_manager;
    private LinearLayout ll_hardware_info;

    private LinearLayout ll_time_table;
    private LinearLayout ll_text_signal;
    private LinearLayout ll_clear_master;
    private LinearLayout ll_open_browser;
    private LinearLayout ll_activation_settings;
    private Context mContext;
    private ImageView iv_back;
    private ImageView iv_delete;

    private LinearLayout ll_restart_app;
    private LinearLayout ll_restart_os;
    private LinearLayout ll_factory_app;
    private LinearLayout ll_close_os;


    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_manager);
        mContext = this;
        tv_title_name = (TextView) findViewById(R.id.tv_title_name);
        ll_progress_bar = (LinearLayout) findViewById(R.id.ll_progress_bar);
        ll_os_settings = (LinearLayout) findViewById(R.id.ll_os_settings);
        ll_file_manager = (LinearLayout) findViewById(R.id.ll_file_manager);
        ll_hardware_info = (LinearLayout) findViewById(R.id.ll_hardware_info);
        ll_restart_app = (LinearLayout) findViewById(R.id.ll_restart_app);
        ll_time_table = (LinearLayout) findViewById(R.id.ll_time_table);
        ll_text_signal = (LinearLayout) findViewById(R.id.ll_text_signal);
        ll_clear_master = (LinearLayout) findViewById(R.id.ll_clear_master);
        ll_open_browser = (LinearLayout) findViewById(R.id.ll_open_browser);
        ll_activation_settings  = (LinearLayout) findViewById(R.id.ll_activation_settings);
        ll_restart_os = (LinearLayout) findViewById(R.id.ll_restart_os);
        ll_factory_app = (LinearLayout) findViewById(R.id.ll_factory_app);
        ll_close_os = (LinearLayout) findViewById(R.id.ll_close_os);


        ll_time_table.setVisibility(View.GONE);
        ll_progress_bar.setVisibility(View.GONE);
        ll_text_signal.setVisibility(View.GONE);
        tv_title_name.setText("一间科技设置");
    }

    @Override
    protected void initTitleView() {
        iv_back = findViewById(R.id.iv_back);
        iv_delete = findViewById(R.id.iv_delete);
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backLoginPage(FunctionActivity.this);

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FunctionActivity.this, LoginSettingActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ProgressManager.getInstance().init(this);

    }


    @Override
    protected void initClickListener() {

        ll_factory_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder mDialog = new AlertDialog.Builder(mContext);
                mDialog.setTitle("提示！");
                mDialog.setMessage("恢复出厂设置");
                mDialog.setCancelable(false);
                mDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction("android.intent.action.sendkey");
                                intent.putExtra("keycode", 1241);
                                mContext.sendBroadcast(intent);
                            }
                        });
                mDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mDialog.create().dismiss();
                            }
                        });
                mDialog.create().show();

            }
        });

        ll_close_os.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final AlertDialog.Builder mDialog = new AlertDialog.Builder(mContext);
                mDialog.setTitle("提示！");
                mDialog.setMessage("重启设备");
                mDialog.setCancelable(false);
                mDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         Intent intent = new Intent();
                        intent.setAction("android.intent.action.sendkey");
                        intent.putExtra("keycode", 1235);
                        mContext.sendBroadcast(intent);
                         */
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.sendkey");
                        intent.putExtra("keycode", 1234);
                        mContext.sendBroadcast(intent);

                    }
                });
                mDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDialog.create().dismiss();
                    }
                });
                mDialog.create().show();

            }
        });


        ll_restart_os.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder mDialog = new AlertDialog.Builder(mContext);
                mDialog.setTitle("提示！");
                mDialog.setMessage("重启设备");
                mDialog.setCancelable(false);
                mDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.sendkey");
                        intent.putExtra("keycode", 1234);
                        mContext.sendBroadcast(intent);
                    }
                });
                mDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDialog.create().dismiss();
                    }
                });
                mDialog.create().show();


            }
        });

        ll_os_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                mWindowBack(mContext, FunctionActivity.class);
            }
        });
        ll_file_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FunctionActivity.this, FileManagerPage.class);
                startActivity(intent);
            }
        });


        ll_restart_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        final AlertDialog.Builder mDialog = new AlertDialog.Builder(mContext);
                        mDialog.setTitle("提示！");
                        mDialog.setMessage("重启自助机项目");
                        mDialog.setCancelable(false);
                        mDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                recreate();
                                /*
                                Intent intent = getBaseContext().getPackageManager()
                                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                System.exit(0);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                */
                            }
                        });
                        mDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mDialog.create().dismiss();
                            }
                        });
                        mDialog.create().show();


                    }
                }, 500);

            }
        });

        ll_hardware_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FunctionActivity.this, HardwarePage.class);
                startActivity(intent);
            }
        });

        ll_clear_master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FunctionActivity.this, AppManagerActivity.class);
                startActivity(intent);
            }
        });

        ll_open_browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse("http://www.baidu.com");
                intent.setData(content_url);
                startActivity(intent);
            }
        });

        ll_activation_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _LToast("激活自助机");
            }
        });
    }

    @Override
    protected void initTouchListener() {

    }


}

