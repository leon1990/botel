package hk.yijian.botel.setting.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hk.yijian.botel.R;
import hk.yijian.botel.fragment.BaseFragment;
import hk.yijian.mobilesafe.domain.AppInfo;
import hk.yijian.mobilesafe.engine.APPInfoProvider;
import hk.yijian.mobilesafe.util.DipTranslateUtil;

public class APPManagerFragment extends BaseFragment {

    private Context mContext;
    private TextView tv_rom;// 现实内存
    private TextView tv_sd;// 现实sd
    private ListView lv_app;// 显示应用信息的Listview
    private List<AppInfo> infos;// 所有的应用信息
    private RelativeLayout rl_app_loading;// 显示加载信息的view
    private List<AppInfo> userInfos;// 用户应用
    private List<AppInfo> systemInfos;// 系统应用
    private TextView tv_app_topinfo;// 显示到顶部的View

    private PopupWindow window;
    private LinearLayout ll_popu_unstall;
    private LinearLayout ll_popu_open;
    private LinearLayout ll_popu_setting;

    
    
    public APPManagerFragment() {
    }

    public static APPManagerFragment newInstance() {
        APPManagerFragment fragment = new APPManagerFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_appmanage;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mContext = getActivity();
        tv_rom = view.findViewById(R.id.tv_app_rom);
        tv_sd = view.findViewById(R.id.tv_app_sd);
        tv_rom.setText("内存可用:"
                + Formatter.formatFileSize(mContext, Environment.getDataDirectory()
                .getFreeSpace()));
        tv_sd.setText("SD卡可用:"
                + Formatter.formatFileSize(mContext, Environment
                .getExternalStorageDirectory().getFreeSpace()));
        rl_app_loading = (RelativeLayout) view.findViewById(R.id.rl_app_loading);
        lv_app = (ListView) view.findViewById(R.id.lv_app);
        tv_app_topinfo = (TextView) view.findViewById(R.id.tv_app_topinfo);

        fillData();

        lv_app.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                dismisspopup();
                // 为空跳出，因为填充数据时异步的所以调用时数据可能为空
                if (userInfos == null || systemInfos == null) {
                    return;
                }
                if (firstVisibleItem < userInfos.size() + 1) {
                    tv_app_topinfo.setText("用户程序(" + userInfos.size() + "个)");
                } else {
                    tv_app_topinfo.setText("系统程序(" + systemInfos.size() + "个)");
                }
            }
        });

        // 设置Item监听
        lv_app.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            AppInfo info = null;
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                dismisspopup();
                if (position == 0 || position == userInfos.size() + 1) {
                    return;
                }
                Object object = lv_app.getItemAtPosition(position);
                if (object != null) {
                    info = (AppInfo) object;
                }
                View contentView = View.inflate(mContext, R.layout.item_popu, null);
                window = new PopupWindow(contentView, -2, -2);
                int[] location = new int[2];
                view.getLocationInWindow(location);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.showAtLocation(parent, Gravity.LEFT + Gravity.TOP, DipTranslateUtil.dip2px(mContext, 67*2),
                        location[1]);
                AlphaAnimation aa = new AlphaAnimation(0, 1);
                aa.setDuration(500);
                // 开启动画
                contentView.startAnimation(aa);
                // 拿到popup的自定义View组件
                ll_popu_unstall = (LinearLayout) contentView.findViewById(R.id.ll_popu_unstall);
                ll_popu_open = (LinearLayout) contentView.findViewById(R.id.ll_popu_open);
                ll_popu_setting = (LinearLayout) contentView.findViewById(R.id.ll_popu_setting);
                /**
                 * 给卸载设置监听
                 */
                ll_popu_unstall.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        // 发送意图给包安装器
                        Intent intent = new Intent(Intent.ACTION_DELETE);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + info.getPackageName()));
                        startActivity(intent);
                    }
                });

                /**
                 * 给打开设置监听
                 */
                ll_popu_open.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(info.getPackageName());
                        if (intent!=null) {
                            startActivity(intent);
                        }else {
                            Toast.makeText(mContext, "该应用没有启动界面", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                /**
                 * 给设置设置监听
                 */
                ll_popu_setting.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:"+info.getPackageName()));
                        startActivity(intent);
                    }
                });
            }
        });
       
    }
    /**
     * 处理子线程要刷新的ui操作
     */
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // 加载完成后隐藏提示
            rl_app_loading.setVisibility(View.INVISIBLE);
            // 设置适配器显示
            lv_app.setAdapter(new AppAdapter());
            super.handleMessage(msg);
        }
    };

    /**
     * 填充数据
     */
    public void fillData() {
        // 加载数据时提示用户
        rl_app_loading.setVisibility(View.VISIBLE);
        // 填充数据时耗时操作所以用子线程
        new Thread() {

            public void run() {
                infos = APPInfoProvider.getAppInfo(mContext);
                // 用户应用
                userInfos = new ArrayList<AppInfo>();
                // 系统应用
                systemInfos = new ArrayList<AppInfo>();

                for (AppInfo info : infos) {
                    // 遍历所有应用信息
                    // 分类
                    if (info.isUser()) {
                        userInfos.add(info);
                    } else {
                        systemInfos.add(info);
                    }

                }
                // 请求完数据发送消息显示数据
                handler.sendEmptyMessage(0);
            };
        }.start();
    }

    /**
     * listview的适配器
     *
     * @author Administrator
     *
     */
    class AppAdapter extends BaseAdapter {

        public int getCount() {
            // 要显示的Item总数量
            return systemInfos.size() + userInfos.size() + 1 + 1;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            AppInfo info = null;
            if (position == 0) {// 0坐标显示用户程序个数
                TextView textView = new TextView(mContext);
                textView.setText("用户程序(" + userInfos.size() + "个)");
                textView.setTextColor(0xBBFFFFFF);
                textView.setBackgroundColor(0x660044FF);
                return textView;
            } else if (position <= userInfos.size()) {
                // 显示用户信息
                info = userInfos.get(position - 1);
            } else if (position == userInfos.size() + 1) {
                // 显示系统程序的个数
                TextView textView = new TextView(mContext);
                textView.setText("系统程序(" + systemInfos.size() + "个)");
                textView.setTextColor(0xBBFFFFFF);
                textView.setBackgroundColor(0x660044FF);
                return textView;
            } else {
                // 显示系统程序
                info = systemInfos.get(position - userInfos.size() - 2);
            }
            // 每个Item的View
            View view = null;
            // 优化数据下次不用再查找组件
            ViewHolder holder = null;
            if (convertView != null && convertView instanceof RelativeLayout) {
                // 有缓存则使用缓存
                view = convertView;
                holder = (ViewHolder) view.getTag();
            } else {
                // 如果没有缓存则从新填充View
                view = View.inflate(mContext, R.layout.item_app,
                        null);
                holder = new ViewHolder();
                // 查找组件冰储存至Holder中，下次拿出来使用就不用查找了
                holder.iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
                holder.tv_app_name = (TextView) view.findViewById(R.id.tv_app_name);
                holder.tv_app_location = (TextView) view.findViewById(R.id.tv_app_location);
                holder.tv_app_package = (TextView) view.findViewById(R.id.tv_app_package);
                // 储存进view中
                view.setTag(holder);
            }
            holder.iv_icon.setImageDrawable(info.getIcon()); // 设置图标
            holder.tv_app_name.setText(info.getName());// 设置应用名
            holder.tv_app_package.setText(info.getPackageName());

            if (info.isRom()) {
                holder.tv_app_location.setText("内部储存");
            } else {
                holder.tv_app_location.setText("sd卡储存");
            }

            return view;
        }

        public Object getItem(int position) {
            AppInfo info = null;
            if (position == 0) {
                return null;
            } else if (position <= userInfos.size()) {
                info = userInfos.get(position - 1);
            } else if (position == userInfos.size() + 1) {
                return null;
            } else {
                info = systemInfos.get(position - userInfos.size() - 2);
            }
            // 每个Item的信息
            return info;
        }

        public long getItemId(int position) {
            // 每个item的Id
            return 0;
        }

    }

    /**
     *
     */
    public void dismisspopup() {
        // 消除popup
        if (window != null && window.isShowing()) {
            window.dismiss();
            window = null;
        }
    }

    /**
     * 用来储存找到的组件，下次就不用再查找直接使用
     *
     * @author Administrator
     *
     */

    static class ViewHolder {
        TextView tv_app_name;
        TextView tv_app_location;
        TextView tv_app_package;
        ImageView iv_icon;
    }


}
