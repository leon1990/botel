package hk.yijian.botel.setting;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import hk.yijian.botel.BaseActivity;
import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.setting.fragment.APPManagerFragment;
import hk.yijian.botel.setting.fragment.AppCacheFragment;
import hk.yijian.botel.setting.fragment.AppProcessFragment;

public class AppManagerActivity extends BaseActivity {

    private Button bt_app_manage;
    private Button bt_app_process;
    private Button bt_app_clear;
    private LinearLayout ll_progress_bar;
    private ImageView iv_back;
    private ImageView iv_delete;
    private LinearLayout ll_text_signal;
    private LinearLayout ll_time_picture;


    private APPManagerFragment appManageFragment;
    private AppCacheFragment appClearFragment;
    private AppProcessFragment appProcessFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_app_manager);
        bt_app_manage = findViewById(R.id.bt_app_manage);
        bt_app_process = findViewById(R.id.bt_app_process);
        bt_app_clear = findViewById(R.id.bt_app_clear);
        ll_progress_bar = findViewById(R.id.ll_progress_bar);
        ll_progress_bar.setVisibility(View.INVISIBLE);
        ll_text_signal = findViewById(R.id.ll_text_signal);
        ll_text_signal.setVisibility(View.INVISIBLE);
        ll_time_picture = findViewById(R.id.ll_time_picture);
        ll_time_picture.setVisibility(View.INVISIBLE);



        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        appManageFragment = APPManagerFragment.newInstance();
        transaction.replace(R.id.app_frame_content, appManageFragment).commit();

    }

    @Override
    protected void initTitleView() {
        iv_back = findViewById(R.id.iv_back);
        iv_delete = findViewById(R.id.iv_delete);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppManagerActivity.this, FunctionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppManagerActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void initClickListener() {

        bt_app_manage.setOnClickListener(mAppManageListener);
        bt_app_process.setOnClickListener(mAppProcessListener);
        bt_app_clear.setOnClickListener(mAppClearListener);

    }

    @Override
    protected void initTouchListener() {

    }

    private final View.OnClickListener mAppManageListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (appManageFragment == null) {
                appManageFragment = APPManagerFragment.newInstance();
            }
            transaction.replace(R.id.app_frame_content, appManageFragment);
            transaction.addToBackStack(null);
            transaction.commit();



        }
    };
    private final View.OnClickListener mAppProcessListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (appProcessFragment == null) {
                appProcessFragment = AppProcessFragment.newInstance();
            }
            transaction.replace(R.id.app_frame_content, appProcessFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    };

    private final View.OnClickListener mAppClearListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (appClearFragment == null) {
                appClearFragment = AppCacheFragment.newInstance();
            }
            transaction.replace(R.id.app_frame_content, appClearFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }
    };
}
