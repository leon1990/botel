package hk.yijian.botel.setting.fragment;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hk.yijian.botel.R;
import hk.yijian.botel.fragment.BaseFragment;
import hk.yijian.mobilesafe.domain.ProcessInfo;
import hk.yijian.mobilesafe.engine.ProcessInfoProvider;
import hk.yijian.mobilesafe.util.ProcessInfoUtil;

import static android.content.Context.ACTIVITY_SERVICE;


public class AppProcessFragment extends BaseFragment {


    private Context mContext;
    private TextView tv_process_count;// 显示进程个数的View
    private TextView tv_process_memory;// 显示总内存和剩余内存的view
    private RelativeLayout rl_process;// 显示loading的view
    private List<ProcessInfo> userInfos;// 储存用户进程的list
    private List<ProcessInfo> systemInfos;// 储存系统进程的list
    private ListView lv_process;// 现实进程信息的ListView
    private ProcessAdapter adapter;// 适配器
    private long avaimemory;// 可用内存
    int proCount;// 运行中的进程个数
    private String totalm;// 总内存
    private SharedPreferences spf;// 用于储存配置信息
    private Button bt_clear_cache;
    private Button bt_reverse_select;
    private Button bt_all_select;


    

    public AppProcessFragment() {
    }

    public static AppProcessFragment newInstance() {
        AppProcessFragment fragment = new AppProcessFragment();
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_processmanage;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mContext = getActivity();
        // 拿到显示进程个数的View
        tv_process_count = (TextView) view.findViewById(R.id.tv_process_count);
        // 拿到显示总内存和剩余内存的view
        tv_process_memory = (TextView) view.findViewById(R.id.tv_process_memory);
        // 设置显示进程个数
        proCount = ProcessInfoUtil.getProcessCount(mContext);
        tv_process_count.setText("运行中的进程:" + proCount + "个");
        avaimemory = ProcessInfoUtil.getAvaiMemory(mContext);
        String avaim = Formatter.formatFileSize(mContext,
                ProcessInfoUtil.getAvaiMemory(mContext));
        totalm = Formatter.formatFileSize(mContext,
                ProcessInfoUtil.getTotalMemory(mContext));
        tv_process_memory.setText("剩余/总内存:" + avaim + "/" + totalm);
        // 拿到显示loading的view
        rl_process = (RelativeLayout) view.findViewById(R.id.rl_process_loading);
        // 拿到现实进程信息的ListView
        lv_process = (ListView) view.findViewById(R.id.lv_process);
        // 拿到SharedPreferences进行相应的配置
        spf = mContext.getSharedPreferences("config", Context.MODE_PRIVATE);


        // 填充数据
        fillData();
        // 设置Item监听
        lv_process.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // 如果是显示显示进程个数的Item则跳出不能点击
                if (position == 0 || position == userInfos.size() + 1) {
                    return;
                }

                // 获得当前位置Item的进程信息
                Object object = lv_process.getItemAtPosition(position);
                ProcessInfo info = null;
                if (object != null) {
                    // 强转为进程信息
                    info = (ProcessInfo) object;
                }
                if (mContext.getPackageName().equals(info.getPackageName())) {
                    return;
                }
                // 拿到当前位置的View
                CheckBox cb = (CheckBox) view.findViewById(R.id.cb_process);
                // 根据当前进程信息的记录来设置是否勾选
                if (info != null) {

                    if (info.isCheck()) {
                        info.setCheck(false);
                        cb.setChecked(false);
                    } else {
                        info.setCheck(true);
                        cb.setChecked(true);
                    }
                }
            }
        });



        bt_clear_cache = view.findViewById(R.id.bt_clear_cache);
        bt_clear_cache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clean();
            }
        });

        bt_all_select = view.findViewById(R.id.bt_all_select);
        bt_all_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                allSelect();
            }
        });

        bt_reverse_select = view.findViewById(R.id.bt_reverse_select);
        bt_reverse_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reverse();
            }
        });


    }

    /**
     * 处理子线程刷新Ui
     */
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // 数据填充完毕隐藏加载中提示
            rl_process.setVisibility(View.INVISIBLE);
            // 给List设置adapter显示内容
            adapter = new ProcessAdapter();
            lv_process.setAdapter(adapter);
            super.handleMessage(msg);
        }
    };

    /**
     * 填充数据
     */
    public void fillData() {
        // 填充数据提示用户正在加载中
        rl_process.setVisibility(View.VISIBLE);
        new Thread() {
            public void run() {
                // 拿到储存所有进程信息的List
                List<ProcessInfo> infos = ProcessInfoProvider
                        .getProcessInfo(mContext);
                // 储存用户进程的List
                userInfos = new ArrayList<ProcessInfo>();
                // 储存系统进程的List
                systemInfos = new ArrayList<ProcessInfo>();
                // 对进程信息分类
                for (ProcessInfo Info : infos) {
                    if (Info.isUser()) {
                        userInfos.add(Info);
                    } else {
                        systemInfos.add(Info);
                    }
                }

                handler.sendEmptyMessage(0);

            };
        }.start();
    }

    /**
     * listview的适配器，用于显示每个Item
     *
     * @author Administrator
     *
     */
    class ProcessAdapter extends BaseAdapter {
        // 当前的进程信息
        ProcessInfo info = null;
        // 储存组件的Holder
        ViewHolder holder = null;

        public int getCount() {
            // 显示的item长度
            boolean systemprocess = spf.getBoolean("systemprocess", true);
            if (systemprocess) {
                return userInfos.size() + systemInfos.size() + 1 + 1;
            } else {
                return userInfos.size() + 1;
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // 0坐标显示用户进程个数
            if (position == 0) {
                TextView textView = new TextView(mContext);
                textView.setText("用户进程(" + userInfos.size() + "个)");
                textView.setTextColor(0xBBFFFFFF);
                textView.setBackgroundColor(0x660044FF);
                return textView;
            } else if (position <= userInfos.size()) {
                // 显示用户进程信息
                info = userInfos.get(position - 1);
            } else if (position == userInfos.size() + 1) {
                // 显示系统进程的个数
                TextView textView = new TextView(mContext);
                textView.setText("系统程序(" + systemInfos.size() + "个)");
                textView.setTextColor(0xBBFFFFFF);
                textView.setBackgroundColor(0x660044FF);
                return textView;
            } else {
                // 显示系统进程
                info = systemInfos.get(position - userInfos.size() - 2);
            }
            // 显示每个item
            View view = null;
            // 如果有缓存则使用缓存,缓存必须为制定的类型
            if (convertView != null && convertView instanceof RelativeLayout) {
                view = convertView;
                holder = (ViewHolder) view.getTag();
            } else {
                // 如果无缓存则填充新的View,
                view = View.inflate(mContext,
                        R.layout.item_process, null);
                // 创建新的holder，将组件寻找好储存进去
                holder = new ViewHolder();
                holder.tv_process_name = (TextView) view
                        .findViewById(R.id.tv_process_name);
                holder.iv_process_icon = (ImageView) view
                        .findViewById(R.id.iv_process_icon);
                holder.tv_process_memory = (TextView) view
                        .findViewById(R.id.tv_process_memory);
                holder.cb_process = (CheckBox) view
                        .findViewById(R.id.cb_process);
                // 将holder储存进View,以便下次使用
                view.setTag(holder);
            }
            // 设置每个item要显示的信息
            holder.iv_process_icon.setImageDrawable(info.getIcon());
            holder.tv_process_name.setText(info.getName());
            holder.tv_process_memory.setText("内存占用:"
                    + Formatter.formatFileSize(mContext,
                    info.getMemory()));
            // 设置是否勾选
            if (info.isCheck()) {
                holder.cb_process.setChecked(true);
            } else {
                holder.cb_process.setChecked(false);
            }

            if (mContext.getPackageName().equals(info.getPackageName())) {
                holder.cb_process.setVisibility(View.GONE);
            } else {
                holder.cb_process.setVisibility(View.VISIBLE);
            }
            // 返回view
            return view;
        }

        /**
         * 获取每个Item的信息
         */
        public Object getItem(int position) {
            ProcessInfo info = null;
            if (position == 0) {
                // 0位置不返回值
                return null;
            } else if (position <= userInfos.size()) {
                // 用户进程的信息
                info = userInfos.get(position - 1);
            } else if (position == userInfos.size() + 1) {
                // 不返回值
                return null;
            } else {
                // 系统进程的信息
                info = systemInfos.get(position - userInfos.size() - 2);
            }
            // 没找到返回空
            return info;
        }

        public long getItemId(int position) {
            return 0;
        }

    }

    /**
     * 全选
     */
    public void allSelect() {
        // 遍历用户进程，全部设置为勾选
        for (ProcessInfo info : userInfos) {
            if (mContext.getPackageName().equals(info.getPackageName())) {
                continue;
            }
            info.setCheck(true);
        }
        // 遍历系统进程全部设置为勾选
        for (ProcessInfo info : systemInfos) {
            info.setCheck(true);
        }

        // 更新页面
        adapter.notifyDataSetChanged();
    }

    /**
     * 反选
     */
    public void reverse() {
        // 遍历用户进程，全部设置为当前相反的勾选状态
        for (ProcessInfo info : userInfos) {
            if (mContext.getPackageName().equals(info.getPackageName())) {
                continue;
            }
            info.setCheck(!info.isCheck());
        }
        // 遍历系统进程全部设置为当前相反的勾选状态
        for (ProcessInfo info : systemInfos) {
            info.setCheck(!info.isCheck());
        }

        // 更新页面
        adapter.notifyDataSetChanged();
    }

    /**
     * 一键清理

     */
    public void clean() {

        // 拿到activity管理器
        ActivityManager am = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
        // 存储所有要杀死的进程
        List<ProcessInfo> kills = new ArrayList<ProcessInfo>();
        int killCount = 0;
        long killmem = 0;
        for (ProcessInfo info : userInfos) {
            // 遍历所有用户进程如果为勾选状态则杀死该进程
            if (info.isCheck()) {
                am.killBackgroundProcesses(info.getPackageName());
                kills.add(info);
                killCount++;
                killmem += info.getMemory();
            }
        }
        // 遍历系统进程如果为勾选则杀死该进程
        for (ProcessInfo info : systemInfos) {
            if (info.isCheck()) {
                am.killBackgroundProcesses(info.getPackageName());
                kills.add(info);
                killCount++;
                killmem += info.getMemory();
            }
        }
        // 避免并发，上面两个集合遍历完后，在吧杀死的进程移除List
        for (ProcessInfo info : kills) {
            if (info.isUser()) {
                userInfos.remove(info);
            } else {
                systemInfos.remove(info);
            }
        }
        // 每次清理完，调整进程数，和剩余内存
        proCount -= killCount;
        avaimemory += killmem;
        // 重新设置进程数和剩余空间
        tv_process_count.setText("运行中的进程:" + proCount + "个");
        tv_process_memory.setText("剩余/总内存:"
                + Formatter.formatFileSize(mContext, avaimemory) + "/" + totalm);
        // 提示用户清理进程的情况
        Toast.makeText(
                mContext,
                "杀死了" + killCount + "个进程," + "释放了"
                        + Formatter.formatFileSize(mContext, killmem), Toast.LENGTH_SHORT).show();
        // 更新页面
        adapter.notifyDataSetChanged();

    }

    static class ViewHolder {
        ImageView iv_process_icon;
        TextView tv_process_name;
        TextView tv_process_memory;
        CheckBox cb_process;
    }
}
