package hk.yijian.botel.setting;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;

import hk.yijian.botel.LoginActivity;
import hk.yijian.botel.R;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.filemanager.activity.Copy;
import hk.yijian.filemanager.activity.DirDetail;
import hk.yijian.filemanager.activity.Preview;
import hk.yijian.filemanager.activity.Search;
import hk.yijian.filemanager.utils.FileAdapter;
import hk.yijian.filemanager.utils.Folder;

public class FileManagerPage extends ListActivity {

    private static final String ROOT_PATH = "/";
    private static String NOW_PATH;
    private ImageButton bt_search;
    //存储文件名称
    private ArrayList<String> FileName = null;
    //存储文件路径
    private ArrayList<String> FilePath = null;
    //重命名布局xml文件显示dialog
    private View view;
    private EditText v_filename;
    private EditText editText;
    private ImageView iv_back;
    private ImageView iv_delete;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE
                    ;
            decorView.setSystemUiVisibility(option);
            //getWindow().setNavigationBarColor(Color.TRANSPARENT);
            //getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_file_main);
        context = this;
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().setProgressInvisible();
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_delete = (ImageView)findViewById(R.id.iv_delete);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FileManagerPage.this,FunctionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });


        showFileDir(ROOT_PATH);//显示文件列表
        bt_search = (ImageButton)findViewById(R.id.bt_search);
        bt_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				v_filename = (EditText)findViewById(R.id.filename);
				Intent intent = new Intent(FileManagerPage.this,Search.class);
				intent.putExtra("path", NOW_PATH);
				intent.putExtra("filename", v_filename.getText().toString());
				startActivity(intent);

			}
		});
        //长按事件
        getListView().setOnItemLongClickListener(new OnItemLongClickListener(){
        	public boolean onItemLongClick(AdapterView parent, View view, int position,long id) {
        		String path = FilePath.get(position);
        		File file = new File(path);
        		// 文件存在并可读
        		if (file.exists() && file.canRead()) {
        			if (file.isDirectory()) {
        				//处理目录
        				dirHandle(file);
        			} else {
        				//处理文件
        				fileHandle(file);
        			}
        		}
        		//没有权限
        		else {
        			Resources res = getResources();
        			new AlertDialog.Builder(FileManagerPage.this).setTitle("提示")
        				.setMessage(res.getString(R.string.no_permission))
        				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        					@Override
        					public void onClick(DialogInterface dialog, int which) {
        						Toast.makeText(getApplicationContext(), "没有权限", Toast.LENGTH_SHORT).show();
        					}
        				}).show();
        		}

    		return true;
        	}
        });
        showFileDir(NOW_PATH);

    }


    /**
     * 点击事件
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String path = FilePath.get(position);
        File file = new File(path);
        // 文件存在并可读
        if (file.exists() && file.canRead()) {
            if (file.isDirectory()) {
                //显示子目录及文件
                showFileDir(path);
            } else {
                //处理文件
                fileHandle(file);
            }
        }
        //没有权限
        else {
            Resources res = getResources();
            new AlertDialog.Builder(this).setTitle("提示")
                    .setMessage(res.getString(R.string.no_permission))
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplicationContext(), "没有权限", Toast.LENGTH_SHORT).show();
                        }
                    }).show();
        }
        super.onListItemClick(l, v, position, id);
    }

    
    /**
     * 文件处理
     * @param file
     */
    private void fileHandle(final File file) {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @SuppressWarnings("deprecation")
			@Override
            public void onClick(DialogInterface dialog, int which) {
                // 打开文件
                if (which == 0) {
                    openFile(file);
                }
                //重命名
                else if (which == 1) {
                    LayoutInflater factory = LayoutInflater.from(FileManagerPage.this);
                    view = factory.inflate(R.layout.rename_dialog, null);
                    editText = (EditText) view.findViewById(R.id.editText);
                    editText.setText(file.getName());

                    DialogInterface.OnClickListener listener2 = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            String modifyName = editText.getText().toString();
                            final String fpath = file.getParentFile().getPath();
                            final File newFile = new File(fpath + "/" + modifyName);
                            if (newFile.exists()) {
                                //排除没有修改情况
                                if (!modifyName.equals(file.getName())) {
                                    new AlertDialog.Builder(FileManagerPage.this)
                                            .setTitle("注意!")
                                            .setMessage("文件名已存在，是否覆盖？")
                                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (file.renameTo(newFile)) {
                                                        showFileDir(fpath);
                                                        displayToast("重命名成功！");
                                                    } else {
                                                        displayToast("重命名失败！");
                                                    }
                                                }
                                            })
                                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            })
                                            .show();
                                }
                            } else {
                                if (file.renameTo(newFile)) {
                                    showFileDir(fpath);
                                    displayToast("重命名成功！");
                                } else {
                                    displayToast("重命名失败！");
                                }
                            }
                        }
                    };
                    AlertDialog renameDialog = new AlertDialog.Builder(FileManagerPage.this).create();
                    renameDialog.setView(view);
                    renameDialog.setButton("确定", listener2);
                    renameDialog.setButton2("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                        }
                    });
                    renameDialog.show();
                }
                //拷贝文件
                else if (which == 2){
                	Intent intent = new Intent(FileManagerPage.this,Copy.class);
                	intent.putExtra("soupath", file.getPath());
                	intent.putExtra("isdel", false);
                	startActivity(intent);
                }
                //删除文件
                else if (which == 3 ){
                	Intent intent = new Intent(FileManagerPage.this,Copy.class);
                	intent.putExtra("soupath", file.getPath());
	            	intent.putExtra("isdel", true);
                	startActivity(intent);
                	showFileDir(ROOT_PATH);
                }
                else {
                    new AlertDialog.Builder(FileManagerPage.this)
                            .setTitle("注意!")
                            .setMessage("确定要删除此文件吗？")
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (file.delete()) {
                                        //更新文件列表
                                        showFileDir(file.getParent());
                                        displayToast("删除成功！");
                                    } else {
                                        displayToast("删除失败！");
                                    }
                                }
                            })
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        };
        //选择文件时，弹出操作选项对话框
        String type = getMIMEType(file);
        String[] menu = {"打开文件", "重命名", "拷贝","剪切","删除文件",};
        if(type=="text"){
        	menu[0] = "预览";
        }
        new AlertDialog.Builder(FileManagerPage.this)
                .setTitle("请选择要进行的操作!")
                .setItems(menu, listener)
                .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }
    /**
     * 扫描显示文件列表
     * @param path
     */
    private void showFileDir(String path) {
        FileName = new ArrayList<String>();
        FilePath = new ArrayList<String>();
        File file = new File(path);
        NOW_PATH = path;

        File[] files = file.listFiles();
        //如果当前目录不是根目录
        if (!ROOT_PATH.equals(path)) {
            FileName.add("rot");
            FilePath.add(ROOT_PATH);
            FileName.add("last");
            FilePath.add(file.getParent());
        }
        //添加所有文件
        for (File f : files) {
            FileName.add(f.getName());
            FilePath.add(f.getPath());
        }
        this.setListAdapter(new FileAdapter(this, FileName, FilePath));
    }

    /**
     * 文件夹处理
     * @param file
     */
    public void dirHandle(final File file){
    	DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if(which == 0){
					Intent intent = new Intent(FileManagerPage.this,Copy.class);
                	intent.putExtra("soupath", file.getPath());
                	intent.putExtra("isdel", false);
                	startActivity(intent);
				}
				else if (which == 1){
					Intent intent = new Intent(FileManagerPage.this,Copy.class);
                	intent.putExtra("soupath", file.getPath());
	            	intent.putExtra("isdel", true);
                	startActivity(intent);
                	showFileDir(ROOT_PATH);
                	
				}
				else if (which == 2){
					Intent intent = new Intent(FileManagerPage.this,DirDetail.class);
	            	intent.putExtra("path",file.getPath());
	                startActivity(intent);
				}else {
					final Folder folder = new Folder(file.getPath());
					 new AlertDialog.Builder(FileManagerPage.this)
                     .setTitle("注意!")
                     .setMessage("确定删除文件夹及内部所有文件吗？")
                     .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             if (folder.delete()) {
                                 //更新文件列表
                                 showFileDir(file.getParent());
                                 displayToast("删除成功！");
                             } else {
                                 displayToast("删除失败！");
                             }
                         }
                     })
                     .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                         }
                     }).show();
					
				}
			}
    		
    	};
    		String[] menu = {"拷贝","剪切","详情","删除"};
    		new AlertDialog.Builder(FileManagerPage.this)
    			.setTitle("请选择要进行的操作!")
    			.setItems(menu, listener)
    			.setPositiveButton("取消", listener = new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				}).show();
    }
    //打开文件
    /**
     * 打开文件
     * @param file
     */
    private void openFile(File file) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);
        String type = getMIMEType(file);
        if(file.isDirectory()){
        	
        }
        else{
        		if(type.equals("text")){
        		
        			intent = new Intent(FileManagerPage.this,Preview.class);
        			intent.putExtra("path", file.getPath());
        	}
        	else{
        			type += "/*";
        			intent.setDataAndType(Uri.fromFile(file), type);
        	}
        }
        startActivity(intent);    
    }

    /**
     * 获取文件mimetype
     * @param file
     * @return
     */
	private String getMIMEType(File file) {
        String type = "";
        String name = file.getName();
        //文件扩展名
        String end = name.substring(name.lastIndexOf(".") + 1, name.length()).toLowerCase();
        if (end.equals("m4a") || end.equals("mp3") || end.equals("wav")) {
            type = "audio";
        } else if (end.equals("mp4") || end.equals("3gp")||end.equals("avi")) {
            type = "video";
        } else if (end.equals("jpg") || end.equals("png") || end.equals("jpeg") || end.equals("bmp") || end.equals("gif")) {
            type = "image";
        } else if(end.equals("txt") || end.equals("java") || end.equals("ini") || end.equals("bat")){
        	type = "text";
        }
        else {
            //如果无法直接打开，跳出列表由用户选择
            type = "*";
        }
//        type += "/*";
        return type;
    }
	/**
	 * 显示提示消息
	 * @param message
	 */
    private void displayToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
    /**
     * back键监听
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            	android.os.Process.killProcess(android.os.Process.myPid());
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
        }
    }
}