package hk.yijian.botel.bean.test;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by young on 2017/9/27.
 */
public class GuestPriceBean {

    private int code;
    private String msg;
    private List<Result> result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "GuestPriceBean{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                '}';
    }

    public class Result implements Comparable<Result> {
        public int howcard;
        public String typename;
        public int roomtype;
        private String guestid;
        private String type;
        private String roomno;
        private int deposit;
        private String roombigimg;
        private String roomsmallimg;
        private int rate;
        private String ispay;
        private String ischeckin;

        public Result(String guestid, String type,
                      String roomno, int deposit,
                      String roombigimg, String roomsmallimg,
                      int rate, String ispay,
                      String ischeckin) {
            this.guestid = guestid;
            this.type = type;
            this.roomno = roomno;
            this.deposit = deposit;
            this.roombigimg = roombigimg;
            this.roomsmallimg = roomsmallimg;
            this.rate = rate;
            this.ispay = ispay;
            this.ischeckin = ischeckin;

        }


        public String getGuestid() {
            return guestid;
        }

        public void setGuestid(String guestid) {
            this.guestid = guestid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getRoomno() {
            return roomno;
        }

        public void setRoomno(String roomno) {
            this.roomno = roomno;
        }

        public int getDeposit() {
            return deposit;
        }

        public void setDeposit(int deposit) {
            this.deposit = deposit;
        }

        public String getRoombigimg() {
            return roombigimg;
        }

        public void setRoombigimg(String roombigimg) {
            this.roombigimg = roombigimg;
        }

        public String getRoomsmallimg() {
            return roomsmallimg;
        }

        public void setRoomsmallimg(String roomsmallimg) {
            this.roomsmallimg = roomsmallimg;
        }

        public int getRate() {
            return rate;
        }

        public void setRate(int rate) {
            this.rate = rate;
        }

        public String getIspay() {
            return ispay;
        }

        public void setIspay(String ispay) {
            this.ispay = ispay;
        }

        public String getIscheckin() {
            return ischeckin;
        }

        public void setIscheckin(String ischeckin) {
            this.ischeckin = ischeckin;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "guestid='" + guestid + '\'' +
                    ", type='" + type + '\'' +
                    ", roomno='" + roomno + '\'' +
                    ", deposit='" + deposit + '\'' +
                    ", roombigimg='" + roombigimg + '\'' +
                    ", roomsmallimg='" + roomsmallimg + '\'' +
                    ", rate=" + rate +
                    ", ispay='" + ispay + '\'' +
                    ", ischeckin='" + ischeckin + '\'' +
                    '}';
        }

        @Override
        public int compareTo(@NonNull Result result) {
            int i = this.getRate() - result.getRate();
            return i;
        }
    }


}
