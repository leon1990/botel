package hk.yijian.botel.bean.test;

import java.util.List;

/**
 * Created by young on 2017/9/30.
 */

public class OutCardBean {


    public int code;
    public String msg;
    public List<Result> netdata;

    public class Result {
        public String typename;
        public int roomtype;
        public int howcard;
        public String guestid;
        public String roomno;
        public int deposit;
        public String roombigimg;
        public String roomsmallimg;
        public int rate;
        public String ispay;
        public String ischeckin;
        public int howmany;
    }
}
