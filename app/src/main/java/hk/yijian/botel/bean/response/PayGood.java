package hk.yijian.botel.bean.response;

import java.util.List;

/**
 * Created by young on 2017/11/30.
 */

public class PayGood {

    public int code;
    public String msg;
    public Data data;

    public class Data {

        public String deviceId;
        public int depositFee;
        public String startDate;
        public String endDate;
        public String zendTime;
        public String zstartTime;

        public String guestName;
        public int guestNum;
        public int hotelId;
        public int invoiceType;
        public int isInvoice;
        public String mobile;
        public String orderDesc;
        public String orderNo;
        public int orderFee;
        public int orderSourceType;
        public int orderStatus;
        public int roomFee;
        public int roomNum;
        public int payType;
        public int roomType;
        public String roomTypeName;
        public int stayType;

        public List<Rooms> rooms;
        public List<Guests> guests;

        public class Rooms {
            public int floor;
            public String orderNo;
            public String roomNo;
            public int roomType;
            public String roomTypeName;
            public int breakfastNum;
            public String guestName;
            public String cards;
            public int status;

        }

        public class Guests {
            public String guestName;
            public String idCardNo;
            public String orderNo;
            public String roomNo;
        }
    }

}
