package hk.yijian.botel.bean.response;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by young on 2017/12/28.
 */

public class NetOrderData {

    public int code;
    public String msg;
    public List<Data> data;

    /**
     * 依据订单号去重
     */

    public class Data implements Comparable<Data>{

        public String createTime;
        public int depositFee;
        public String endDate;
        public String guestName;

        public int hotelId;
        public String hotelName;
        public String mobile;
        public int orderFee;
        public int roomFee;



        public String orderNo;
        public int orderSourceType;
        public int orderStatus;
        public String roomNo;

        public String prepayId;
        public int roomType;
        public String startDate;
        public int userId;

        public List<Rooms> rooms;
        public class Rooms{
            public String roomNo;
            public String guestName;
            public String roomTypeName;
            public int status;
        }



        /**
         * 依据订单号排序
         */
        public Data() {
            super();
        }
        public Data(String createTime, String endDate, String mobile,
                    Integer hotelId, String hotelName, Integer orderFee,
                    String orderNo, Integer orderSourceType, Integer orderStatus,
                    String prepayId, Integer roomType,String startDate, Integer userId) {
            super();
            this.createTime = createTime;
            this.endDate = endDate;
            this.mobile = mobile;
            this.hotelId = hotelId;
            this.hotelName = hotelName;
            this.orderFee = orderFee;

            this.orderNo = orderNo;
            this.orderSourceType = orderSourceType;
            this.orderStatus = orderStatus;
            this.prepayId = prepayId;
            this.roomType = roomType;
            this.createTime = createTime;

            this.startDate = startDate;
            this.userId = userId;
        }

        @Override
        public int compareTo(@NonNull Data data) {
            return this.orderNo.compareTo(data.orderNo);
        }


    }

}
