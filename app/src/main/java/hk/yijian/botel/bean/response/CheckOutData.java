package hk.yijian.botel.bean.response;

/**
 * Created by young on 2017/12/1.
 */

public class CheckOutData {

    public int code;
    public String msg;
    public Data data;


    public class Data {
        public String orderNo;
        public String roomNo;
        public int depositFee;
        public int consumeFee;
        public int roomFee;
        public String details;
    }
}
