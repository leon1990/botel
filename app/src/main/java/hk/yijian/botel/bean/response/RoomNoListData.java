package hk.yijian.botel.bean.response;

import java.util.List;

/**
 * Created by young on 2017/9/27.
 */


public class RoomNoListData {

    public int code;
    public String msg;
    public List<Data> data;

    public class Data {

        public int floor;
        public List<RoomList> roomList;

        public class RoomList {
            public String roomNo;
            public int status;
            public int roomFee;
            public String feature;

        }

    }

}
