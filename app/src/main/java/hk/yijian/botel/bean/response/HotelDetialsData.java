package hk.yijian.botel.bean.response;

import java.util.List;

/**
 * Created by young on 2017/12/1.
 */

public class HotelDetialsData {

    public int code;
    public String msg;
    public Data data;


    public class Data {
        public int hotelId;
        public String hotelName;
        public int type;
        public String openDate;
        public String address;
        public String phone;
        public String contact;

        public int lowestFee;
        public String cityName;
        public String cityCode;
        public String lat;
        public String lon;
        public String introduce;
        public List<HotelServiceList> hotelServiceList;

        public class HotelServiceList{
            public int id;
            public String name;
            public String photo;
        }

        public String logo;
        public String slogan;
        public int star;
        public String provinceCode;
        public String provinceName;
        public String districtCode;
        public String districtName;
        public int pmsSupplier;
        public int pmsType;

        public List devicePhotos;
        public String sappMainPhoto;
        public List sappPhotos;
        public List sappPhotoList;
        public List introducePhotos;
    }
}
