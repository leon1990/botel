package hk.yijian.botel.bean.response;

public class ImgResult {

    public int code;
    public String message;
    public Data data;

    public class Data {
        public int score;
        public String session_id;
    }
}
