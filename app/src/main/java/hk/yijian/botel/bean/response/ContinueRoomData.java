package hk.yijian.botel.bean.response;

import java.util.List;

/**
 * Created by young on 2017/11/30.
 */

public class ContinueRoomData {

    public int code;
    public String msg;
    public Data data;

    public class Data {

        public String orderNo;
        public String roomNo;
        public String roomCardId;
        public int roomFee;
        public int depositFee;
        //public int orderFee;

        public List photo;

        public int hotelId;
        public int roomType;
        public String roomTypeName;
        public int guestNum;
        public String liveDays;

        public int breakfastNum;
        public String userId;
        public int stayType;

        public String startDate;
        public String endDate;

        public List<Guests> guests;
        public class Guests {
            public String guestName;
            public String idCardNo;
        }
    }
}
