package hk.yijian.botel.bean.response;

/**
 * Created by young on 2017/12/14.
 */

public class ErrorData {

    public int code;
    public String msg;
    public Data data;

    public class Data {
    }
}
