package hk.yijian.botel.bean.test;

import java.util.List;

/**
 * Created by young on 2017/9/27.
 */
public class ThemeGuestBean {

    public int code;
    public String msg;
    public List<Result> result;

    public class Result {
        public String typename;
        public int roomtype;
        public int howcard;
        public String guestid;
        public String roomno;
        public int deposit;
        public String roombigimg;
        public String roomsmallimg;
        public int rate;
        public String ispay;
        public String ischeckin;
    }

}
