package hk.yijian.botel.bean.response;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by young on 2017/9/27.
 */


public class TimeRoomData {

    private int code;
    private String msg;
    private List<Data> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "TimeRoomData{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public class Data implements Comparable<Data>{

        private int hotelId;
        private int roomTypeId;
        private String roomTypeName;
        private int standardFee;
        private int memberFee;
        private int depositFee;
        private int breakfastNum;
        private String feature;
        /*
        public List<Feature> feature;
        public class Feature {
            public String name;
            public String value;
        }
        */
        private String deviceMainPhoto;
        private List devicePhotos;
        private String sappMainPhoto;
        private List sappPhotos;

        private List<FavorList> favorList;
        private class FavorList {
            public int id;
            public String name;
            public String value;
        }

        public int getHotelId() {
            return hotelId;
        }

        public void setHotelId(int hotelId) {
            this.hotelId = hotelId;
        }

        public int getRoomTypeId() {
            return roomTypeId;
        }

        public void setRoomTypeId(int roomTypeId) {
            this.roomTypeId = roomTypeId;
        }

        public String getRoomTypeName() {
            return roomTypeName;
        }

        public void setRoomTypeName(String roomTypeName) {
            this.roomTypeName = roomTypeName;
        }

        public int getStandardFee() {
            return standardFee;
        }

        public void setStandardFee(int standardFee) {
            this.standardFee = standardFee;
        }

        public int getMemberFee() {
            return memberFee;
        }

        public void setMemberFee(int memberFee) {
            this.memberFee = memberFee;
        }

        public int getDepositFee() {
            return depositFee;
        }

        public void setDepositFee(int depositFee) {
            this.depositFee = depositFee;
        }

        public int getBreakfastNum() {
            return breakfastNum;
        }

        public void setBreakfastNum(int breakfastNum) {
            this.breakfastNum = breakfastNum;
        }

        public String getDeviceMainPhoto() {
            return deviceMainPhoto;
        }

        public void setDeviceMainPhoto(String deviceMainPhoto) {
            this.deviceMainPhoto = deviceMainPhoto;
        }

        public List getDevicePhotos() {
            return devicePhotos;
        }

        public void setDevicePhotos(List devicePhotos) {
            this.devicePhotos = devicePhotos;
        }

        public String getSappMainPhoto() {
            return sappMainPhoto;
        }

        public void setSappMainPhoto(String sappMainPhoto) {
            this.sappMainPhoto = sappMainPhoto;
        }

        public List getSappPhotos() {
            return sappPhotos;
        }

        public void setSappPhotos(List sappPhotos) {
            this.sappPhotos = sappPhotos;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public List<FavorList> getFavorList() {
            return favorList;
        }

        public void setFavorList(List<FavorList> favorList) {
            this.favorList = favorList;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "hotelId=" + hotelId +
                    ", roomTypeId=" + roomTypeId +
                    ", roomTypeName='" + roomTypeName + '\'' +
                    ", standardFee=" + standardFee +
                    ", memberFee=" + memberFee +
                    ", depositFee=" + depositFee +
                    ", breakfastNum=" + breakfastNum +
                    ", deviceMainPhoto='" + deviceMainPhoto + '\'' +
                    ", devicePhotos=" + devicePhotos +
                    ", sappMainPhoto='" + sappMainPhoto + '\'' +
                    ", sappPhotos=" + sappPhotos +
                    ", feature='" + feature + '\'' +
                    ", favorList=" + favorList +
                    '}';
        }

        public Data(int hotelId, int roomTypeId,
                    String roomTypeName, int standardFee,
                    int memberFee, int depositFee,
                    int breakfastNum, String feature,
                    String deviceMainPhoto,List devicePhotos,
                    String sappMainPhoto,List sappPhotos,List<FavorList> favorList) {
            this.hotelId = hotelId;
            this.roomTypeId = roomTypeId;
            this.roomTypeName = roomTypeName;
            this.standardFee = standardFee;
            this.memberFee = memberFee;
            this.depositFee = depositFee;
            this.breakfastNum = breakfastNum;
            this.feature = feature;
            this.deviceMainPhoto = deviceMainPhoto;
            this.devicePhotos = devicePhotos;
            this.sappMainPhoto = sappMainPhoto;
            this.sappPhotos = sappPhotos;
            this.favorList = favorList;

        }

        @Override
        public int compareTo(@NonNull Data data) {
            int i = this.getMemberFee() - data.getMemberFee();
            return i;
        }

    }

}
