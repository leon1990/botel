package hk.yijian.botel.bean.test;

import java.util.List;

/**
 * Created by young on 2017/9/29.
 */

public class NetOrder {

    private int code;
    private String msg;
    private List<Netdata> netdata;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Netdata> getNetdata() {
        return netdata;
    }

    public void setNetdata(List<Netdata> netdata) {
        this.netdata = netdata;
    }

    @Override
    public String toString() {
        return "NetOrder{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", netdata=" + netdata +
                '}';
    }

    public class Netdata {
        public String typename;
        public int roomtype;
        public int howcard;
        private String guestid;
        private String roomno;
        private int deposit;
        private String roombigimg;
        private String roomsmallimg;
        private int rate;
        private String name;
        private String ispay;
        private String ischeckin;

        public String getGuestid() {
            return guestid;
        }

        public void setGuestid(String guestid) {
            this.guestid = guestid;
        }


        public String getRoomno() {
            return roomno;
        }

        public void setRoomno(String roomno) {
            this.roomno = roomno;
        }

        public int getDeposit() {
            return deposit;
        }

        public void setDeposit(int deposit) {
            this.deposit = deposit;
        }

        public String getRoombigimg() {
            return roombigimg;
        }

        public void setRoombigimg(String roombigimg) {
            this.roombigimg = roombigimg;
        }

        public String getRoomsmallimg() {
            return roomsmallimg;
        }

        public void setRoomsmallimg(String roomsmallimg) {
            this.roomsmallimg = roomsmallimg;
        }

        public int getRate() {
            return rate;
        }

        public void setRate(int rate) {
            this.rate = rate;
        }

        public String getIspay() {
            return ispay;
        }

        public void setIspay(String ispay) {
            this.ispay = ispay;
        }

        public String getIscheckin() {
            return ischeckin;
        }

        public void setIscheckin(String ischeckin) {
            this.ischeckin = ischeckin;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Netdata{" +
                    "typename='" + typename + '\'' +
                    ", roomtype=" + roomtype +
                    ", howcard=" + howcard +
                    ", guestid='" + guestid + '\'' +
                    ", roomno='" + roomno + '\'' +
                    ", deposit=" + deposit +
                    ", roombigimg='" + roombigimg + '\'' +
                    ", roomsmallimg='" + roomsmallimg + '\'' +
                    ", rate=" + rate +
                    ", name='" + name + '\'' +
                    ", ispay='" + ispay + '\'' +
                    ", ischeckin='" + ischeckin + '\'' +
                    '}';
        }
    }
}
