package hk.yijian.botel.bean.test;

import java.util.List;

/**
 * Created by young on 2017/9/30.
 */

public class ContinueBean {

    public int code;
    public String msg;
    public List<Result> netdata;

    public class Result {
        public int howcard;
        public String guestid;
        public String typename;
        public int roomtype;
        public String roomno;
        public int deposit;
        public String roombigimg;
        public String roomsmallimg;
        public int rate;
        public String name;
        public String ispay;
        public String ischeckin;
        public int howmany;
    }
}
