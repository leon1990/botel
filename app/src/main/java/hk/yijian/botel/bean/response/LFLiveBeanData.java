package hk.yijian.botel.bean.response;



/**
 * Created by young on 2017/11/11.
 */

public class LFLiveBeanData {
    /*
    "request_id": "TID63704ab680374fb5943c2d2486be015f",
	"status": "OK",
	"hack_score": 0.8296374082565308,
	"verify_score": 0.646880030632019,
	"liveness_data_id": "fc9a06d812014c5db2b752d88f5d32a8",
	"image_id": "edf4a9d613d345e1942f3c9d62bdf6d6"
     */


    public String request_id;
    public String status;
    public double  hack_score;
    public double  verify_score;
    public String liveness_data_id;
    public String image_id;

}
