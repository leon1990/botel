package hk.yijian.botel.bean.response;

/**
 * Created by young on 2018/1/3.
 */

public class HotelId {

    public int code;
    public String msg;
    public Data data;

    public class Data {
        public String deviceId;
        public int hotelId;
        public String hotelName;
        public String type;
    }

}
