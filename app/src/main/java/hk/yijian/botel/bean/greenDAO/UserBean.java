package hk.yijian.botel.bean.greenDAO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;


@Entity(nameInDb = "t_user")
public class UserBean {

    // 主键注解
    @Id(autoincrement = true)
    private long _id;
    private String account;
    private String password;

    @Transient
    private int idno;

    @Generated(hash = 784948562)
    public UserBean(long _id, String account, String password) {
        this._id = _id;
        this.account = account;
        this.password = password;
    }

    @Generated(hash = 1203313951)
    public UserBean() {
    }

    public long get_id() {
        return this._id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
