package hk.yijian.botel.bean.response;

/**
 * Created by young on 2017/11/29.
 */

public class PayData {

    public int code;
    public String msg;
    public Data data;

    public class Data {
        public String wxQrCode;
        public String aliQrCode;
    }
}
