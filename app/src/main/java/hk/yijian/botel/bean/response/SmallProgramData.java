package hk.yijian.botel.bean.response;

/**
 * Created by young on 2017/12/1.
 */

public class SmallProgramData {

    public int code;
    public String msg;
    public Data data;

    public class Data {
        public String qrCodeUrl;
    }
}
