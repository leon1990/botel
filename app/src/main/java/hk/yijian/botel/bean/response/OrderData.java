package hk.yijian.botel.bean.response;


/**
 * Created by young on 2017/11/28.
 */
public class OrderData {

    public int code;
    public String msg;
    public Data data;

    public class Data {
        public String orderNo;
    }
}
