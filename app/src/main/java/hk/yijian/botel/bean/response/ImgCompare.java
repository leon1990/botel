package hk.yijian.botel.bean.response;

public class ImgCompare {

    public int code;
    public String message;
    public Data data;

    public class Data {
        public int similarity;
        public String session_id;
    }
}
