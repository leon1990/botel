package hk.yijian.botel.bean.response;

/**
 * Created by young on 2017/12/21.
 */

public class XgMessageUser {

    public int hasOrder;
    public int isMe;
    public String mobile;
    public int orderSourceType;
    public String type;
    public long userId;
}
