package hk.yijian.botel.bean.response;

import java.util.List;

/**
 * Created by young on 2017/9/27.
 */


public class RoomListAllData {

    public int code;
    public String msg;
    public List<Data> data;

    public class Data {
        public int hotelId;
        public int roomTypeId;
        public String roomTypeName;

        public int standardFee;
        public int memberFee;
        public int depositFee;

        public int breakfastNum;

        public String feature;
        /*
        public List<Feature> feature;
        public class Feature {
            public String name;
            public String value;
        }
        */

        public List<FavorList> favorList;
        public class FavorList {
            public int id;
            public String name;
            public String value;
        }

        public String deviceMainPhoto;
        public List devicePhotos;

        public String sappMainPhoto;
        private List sappPhotos;

    }

}
