package hk.yijian.botel.bean.response;

import java.util.List;

/**
 * Created by young on 2017/11/30.
 */

public class OutCardData {

    public int code;
    public String msg;
    public Data data;

    public class Data {
        public String orderNo;
        public int orderStatus;
        public int orderSourceType;
        public String userId;
        public int hotelId;
        public int roomType;
        public String roomTypeName;

        public String startDate;
        public String endDate;
        public String zstartTime;
        public String zendTime;
        public String createTime;

        public int orderFee;
        public int roomFee;
        public int depositFee;

        public int stayType;
        public String mobile;
        public String deviceId;
        public String orderDesc;

        public String roomNo;
        public String hotelName;

        public List<Guest> guests;

        public class Guest {
            public String orderNo;
            public String roomNo;
            public String guestName;
            public String roomCardId;

        }

    }
}
