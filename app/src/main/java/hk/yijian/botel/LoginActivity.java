package hk.yijian.botel;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import hk.yijian.botel.activity.CheckOutActivity;
import hk.yijian.botel.activity.CheckInKeepRoomActivity;
import hk.yijian.botel.activity.CheckInNetOrderActivity;
import hk.yijian.botel.setting.LoginSettingActivity;
import hk.yijian.botel.bean.response.HotelDetialsData;
import hk.yijian.botel.bean.response.HotelId;
import hk.yijian.botel.bean.response.SmallProgramData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.netty.NettyUtils;
import hk.yijian.botel.util.AppUtils;
import hk.yijian.botel.util.ClickUtil;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._Log;
import hk.yijian.hardware._RoomCardUtils;
import hk.yijian.view.banner2.RecyclerViewBanner;


public class LoginActivity extends BaseActivity {

    public final static String TAG = "LoginActivity";
    public static final String action_init_idcard = "android.init.hardware";
    public static final String action_send_message = "android.send.message";

    public static final int action_incard = 1;
    public static final int action_message = 2;
    public static final int action_outcard = 3;

    private ImageView iv_program_code;
    private ImageView iv_main_intent;
    private LinearLayout ll_checkout_room;
    private LinearLayout ll_continue_room;
    private TextView tv_bottom_text;
    private TextView tv_devices_ip;

    private SmallProgramData SmallData;

    private HotelId hotelId;
    private HotelDetialsData hotelDetialsData;

    //TODO--------------------------------轮播图 版本02--------------------------

    private class Banner {
        String url;

        public Banner(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }
    //------------------------------------------------------------------------



    //Handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.SAMLL_PROGRAM_URL:
                    try{
                        Glide.with(LoginActivity.this)
                                .load(SmallData.data.qrCodeUrl)
                                .into(iv_program_code);
                        String mHotelId = ""+hotelId.data.hotelId;//+"&type="+3
                        mGetHotelDetial(mHotelId);
                    }catch (Exception e){}
                    break;
                case _HandlerCode.CHANGE_UI_TAKE_TURN:
                    try{
                        //TODO -------------------------轮播图 版本02-----------------------
                        RecyclerViewBanner recyclerViewBanner = findViewById(R.id.rv_banner_turn);
                        final List<Banner> banners = new ArrayList<>();
                        banners.add(new Banner(""+hotelDetialsData.data.devicePhotos.get(0)));
                        banners.add(new Banner(""+hotelDetialsData.data.devicePhotos.get(1)));
                        banners.add(new Banner(""+hotelDetialsData.data.devicePhotos.get(2)));
                        //recyclerViewBanner.setRvAutoPlaying(false);
                        recyclerViewBanner.setIndicatorInterval(5000);
                        recyclerViewBanner.setRvBannerData(banners);
                        recyclerViewBanner.setOnSwitchRvBannerListener(new RecyclerViewBanner.OnSwitchRvBannerListener() {
                            @Override
                            public void switchBanner(int position, AppCompatImageView bannerView) {
                                Glide.with(bannerView.getContext())
                                        .load(banners.get(position).getUrl())
                                        .into(bannerView);
                            }
                        });

                    }catch (Exception e){}

                    break;
                case _HandlerCode.GET_PROGRAM_CODE:
                    try{
                        //
                        String str = ""+hotelId.data.hotelId+"?deviceId="+mDeviceId;
                        mGetProgramCode(str);
                    }catch (Exception e){}
                    break;
            }
            super.handleMessage(msg);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        iv_program_code = findViewById(R.id.iv_program_code);
        iv_main_intent = findViewById(R.id.ll_main_intent);
        ll_checkout_room = findViewById(R.id.ll_checkout_room);
        ll_continue_room = findViewById(R.id.ll_continue_room);
        tv_bottom_text = findViewById(R.id.tv_bottom_text);
        tv_devices_ip = findViewById(R.id.tv_devices_ip);
        tv_devices_ip.setText("006测试："+getHostIP());
        //
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter() ;
        intentFilter.addAction(action_init_idcard);
        intentFilter.addAction(action_send_message);
        localBroadcastManager.registerReceiver(mBroadcastReceiver,intentFilter );
        //分辨率
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mWidth = dm.widthPixels;
        int mHeight = dm.heightPixels;
        float dpi = dm.densityDpi;//屏幕密度
        float density = dm.density;
        _Log.i("dpi_net_response","dpi ="+dpi+"___density="+density+"--分辨率 mWidth ="+mWidth+"--mHeight="+mHeight);
        //屏幕宽高
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        int screenWidth = display.getWidth();
        int screenHeight = display.getHeight();
        _Log.d("dpi_net_response", "screenWidth=" + screenWidth + "__screenHeight=" + screenHeight);
        _Log.d("dpi_net_response","getHostIP=" + getHostIP()+"getMacAddress"+ AppUtils.getMacAddress());
        //
        _DeviceHardware();
        //
        NettyUtils.openNetty(this);
        //NettyUtils.closeNetty(this);

        //init hardware
        _InitHardware();
        _Log.d("InitHardware_net_response","_InitHardware");
        //显示、隐藏导航栏
        Intent intent = new Intent();
        intent.setAction("android.intent.action.sendkey");
        intent.putExtra("keycode", 1239);
        intent.putExtra("state", 1);//0显示 1隐藏
        this.sendBroadcast(intent);
        //
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(mTimeReceiver, filter);



    }

    @Override
    protected void initTitleView() {
        TitleBarManager.getInstance().init(LoginActivity.this);
        TitleBarManager.getInstance().NoLogin();
        // 清理数据
        mOrderEditor.clear();
        mOrderEditor.commit();
        mUserEditor.clear();
        mUserEditor.commit();
        mPublicEditor.clear();
        mPublicEditor.commit();
        //Network
        doNetwork();

        if(_IdCardCode  && _RoomCardCode == 200) { //&& _PrintCode
            SparseArray<Integer> mQueryState = _RoomCardUtils.getInstance().K720_SensorQuery();
            mQueryState.get(0);
            mQueryState.get(1);
            mQueryState.get(2);
            mQueryState.get(3);
            mQueryState.get(4);
            if (mQueryState.get(0) == 0) {
                switch (mQueryState.get(4)) {
                    case 0x3E://卡片状态：只有一张卡在传感器2-3位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x3B://只有一张卡在传感器1-2位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x39://只有一张卡在传感器1位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x37://卡在传感器1-2-3的位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x36://卡在传感器2-3的位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x34://卡在传感器3位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x33://卡在传感器1-2位置(读卡位置)
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x32://卡在传感器2位置
                        _RoomCardUtils.getInstance().Reset();
                        break;
                    case 0x30://卡片状态：空闲
                        break;
                    case 0x38://卡片状态：卡箱已空
                        break;
                    case 0x31://卡片状态：卡在传感器1位置(取卡位置
                        break;
                    case 0x35://卡片状态：卡在传感器取卡位置
                        break;
                }
            }
        }else {
            //recreate();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initClickListener() {

    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initTouchListener() {
        final int mHotelId = mSharePublicData.getInt("public_hotelid", 0);

        ll_checkout_room.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                _Log.i("urls_net_response","退房...");
                if(mHotelId == 0){
                    recreate();
                }else {
                    HardwareTest(action_outcard,CheckOutActivity.class, _Constants.BUNDLE_OUT_VALUE);
                }
                return false;
            }
        });
        ll_continue_room.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                _Log.i("urls_net_response","续房");

                HardwareTest(action_outcard,CheckInKeepRoomActivity.class,_Constants.BUNDLE_CONTINUE_VALUE);
                return false;
            }
        });

        iv_main_intent.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_IN_VALUE);
                startActivity(intent);

                /*
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP://抬起
                        if (mHotelId == 0) {
                            recreate();
                        } else {
                            mUserEditor.putString("type", "");
                            mUserEditor.putString("mobile", "");
                            //mUserEditor.putLong("userId",0);
                            mUserEditor.putInt("orderSourceType", 0);
                            mUserEditor.putInt("hasOrder", 0);
                            mUserEditor.putBoolean("login", false);
                            mUserEditor.commit();
                            try {
                                HardwareTest(action_incard, HomeActivity.class, "");
                            } catch (Exception e) {
                            }
                        }
                        break;

                }*/
                return true;

            }
        });


        tv_bottom_text.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                long[] mHints = new long[1];//设置点击次数
                System.arraycopy(mHints, 1, mHints, 0, mHints.length - 1);
                mHints[mHints.length - 1] = SystemClock.uptimeMillis();
                if (SystemClock.uptimeMillis() - mHints[0] <= 1000) {//点击次数在多少秒之内有效
                    Intent intent = new Intent(LoginActivity.this, LoginSettingActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       try {
           unregisterReceiver(mBroadcastReceiver);
           //unregisterReceiver(mTimeReceiver);
       }catch (Exception e){}

    }
    /**
     * Time Receiver
     */
    private BroadcastReceiver mTimeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_TIME_TICK.equals(intent.getAction())) {//每一分钟更新时间
                _Log.d(TAG, "TIME_TICK");
            }
        }
    };
    /**
     * Time Receiver
     */
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();
            if (action_init_idcard.equals(action)){// 获取开机广播
            }else if(action_send_message.equals(action)){
                try{
                    String messageType = intent.getStringExtra("type");
                    String mobile = intent.getStringExtra("mobile");
                    long userId = intent.getLongExtra("userId",0);
                    boolean login = intent.getBooleanExtra("login",false);
                    int pushCode = intent.getIntExtra("intentcode",0);
                    int orderSourceType = intent.getIntExtra("orderSourceType",0);
                    final int hasOrder = intent.getIntExtra("hasOrder",0);
                    mUserEditor.putString("type",messageType);
                    mUserEditor.putString("mobile",mobile);
                    mUserEditor.putLong("userId",userId);
                    mUserEditor.putInt("orderSourceType",orderSourceType);
                    mUserEditor.putInt("hasOrder",hasOrder);
                    mUserEditor.putBoolean("login",login);
                    mUserEditor.putInt("intentcode",pushCode);
                    mUserEditor.commit();
                    if(hasOrder == 1){
                        HardwareTest(action_message,CheckInNetOrderActivity.class,_Constants.BUNDLE_NET_VALUE);
                    }else {
                        HardwareTest(action_message,HomeActivity.class,"");
                    }
                }catch (Exception ignored){}
            }
        }
    };

    private void doNetwork(){
        try {
            String mMac = "?deviceId="+ mDeviceId;//+"&type="+3
            mGetHotelId(mMac);
        }catch (Exception e){}
    }

    /**
     * get host ip
     * @return
     */
    private String getHostIP() {
        String hostIp = null;
        try {
            Enumeration nis = NetworkInterface.getNetworkInterfaces();
            InetAddress ia = null;
            while (nis.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) nis.nextElement();
                Enumeration<InetAddress> ias = ni.getInetAddresses();
                while (ias.hasMoreElements()) {
                    ia = ias.nextElement();
                    if (ia instanceof Inet6Address) {
                        continue;// skip ipv6
                    }
                    String ip = ia.getHostAddress();
                    if (!"127.0.0.1".equals(ip)) {
                        hostIp = ia.getHostAddress();
                        break;
                    }
                }
            }
        } catch (SocketException e) {
            Log.i("yj", "SocketException");
            e.printStackTrace();
        }
        return hostIp;
    }

    public void _DeviceHardware(){
        Log.d("hardware_net_response", "---------------Build------------");
        Log.d("hardware_net_response", "Build.BOARD: "+ Build.BOARD);
        Log.d("hardware_net_response", "Build.BOOTLOADER: "+Build.BOOTLOADER);
        Log.d("hardware_net_response", "Build.BRAND: "+Build.BRAND);//设备牌子
        Log.d("hardware_net_response", "Build.DEVICE: "+Build.DEVICE);//设备名
        Log.d("hardware_net_response", "Build.DISPLAY: "+Build.DISPLAY);//显示设备号
        Log.d("hardware_net_response", "Build.FINGERPRINT: "+Build.FINGERPRINT);//设备指纹
        Log.d("hardware_net_response", "Build.HARDWARE: "+Build.HARDWARE);
        Log.d("hardware_net_response", "Build.HOST: "+Build.HOST);
        Log.d("hardware_net_response", "Build.ID: "+Build.ID);//设备硬件id
        Log.d("hardware_net_response", "Build.MANUFACTURER: "+Build.MANUFACTURER);//厂商
        Log.d("hardware_net_response", "Build.MODEL: "+Build.MODEL);//设备型号
        Log.d("hardware_net_response", "Build.PRODUCT: "+Build.PRODUCT);//产品名，和DEVICE一样
        Log.d("hardware_net_response", "Build.SERIAL: "+Build.SERIAL);//设备序列号
        Log.d("hardware_net_response", "Build.TAGS: "+Build.TAGS);
        Log.d("hardware_net_response", "Build.TYPE: "+Build.TYPE);
        Log.d("hardware_net_response", "Build.UNKNOWN: "+Build.UNKNOWN);
        Log.d("hardware_net_response", "Build.USER: "+Build.USER);
        Log.d("hardware_net_response", "Build.CPU_ABI: "+Build.CPU_ABI);
        Log.d("hardware_net_response", "Build.CPU_ABI2: "+Build.CPU_ABI2);
        Log.d("hardware_net_response", "Build.RADIO: "+Build.RADIO);
        Log.d("hardware_net_response", "Build.TIME: "+Build.TIME);//出厂时间
        Log.d("hardware_net_response", "Build.VERSION.CODENAME: "+Build.VERSION.CODENAME);
        Log.d("hardware_net_response", "Build.VERSION.INCREMENTAL: "+Build.VERSION.INCREMENTAL);//不详，重要
        Log.d("hardware_net_response", "Build.VERSION.RELEASE: "+Build.VERSION.RELEASE);//系统版本号
        Log.d("hardware_net_response", "Build.VERSION.SDK: "+Build.VERSION.SDK);//api级数
        Log.d("hardware_net_response", "Build.VERSION.SDK_INT: "+Build.VERSION.SDK_INT);//api级数，int型返回
        Log.d("hardware_net_response", "---------------Build------------");
        Log.d("hardware_net_response", "getVersionCode: "+ getVersionCode(this));
        Log.d("hardware_net_response", "getVersionName: "+ getVersionName(this));
        Log.d("hardware_net_response", "appID: "+ getApplicationInfo().processName);
        Log.d("hardware_net_response", "appID: "+ getApplicationInfo().packageName);
        Log.d("hardware_net_response", "appID: "+ getApplication().getPackageName());
        Log.d("hardware_net_response", "getPackageName: "+ getPackageName());


        @SuppressLint("WifiManagerLeak")
        WifiManager wifi = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);
        String macAddress = wifi.getConnectionInfo().getMacAddress();
        String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = Settings.System.getString(getContentResolver(), Settings.System.ANDROID_ID);
        android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission")
        String simSerialNum = tm.getSimSerialNumber();
        String serialNum = Build.SERIAL;
        String deviceManufacturer = Build.MANUFACTURER;
        String deviceModel = Build.MODEL;
        String deviceBrand = Build.BRAND;
        String device = Build.DEVICE;


        Log.e("hardware_net_response", "Identifier_Device_ID: "+deviceId);
        Log.e("hardware_net_response", "Identifier_Mac_Address: "+macAddress);
        Log.e("hardware_net_response", "Identifier_Android_ID: "+androidId);
        Log.e("hardware_net_response", "Identifier_Serial_Num: "+serialNum);
        Log.e("hardware_net_response", "Identifier_Sim_SN: "+simSerialNum);
        Log.e("hardware_net_response", "Identifier_Manufacturer: "+deviceManufacturer);
        Log.e("hardware_net_response", "Identifier_Model: "+deviceModel);
        Log.e("hardware_net_response", "Identifier_Brand: "+deviceBrand);
        Log.e("hardware_net_response", "Identifier_Device: "+device);
        /**
         Mac_Address:  10:68:3f:81:ed:ff
         Serial_Num:    01b4549262d6a4a2
         Device_ID:    355136021808056
         Android_ID:    6ae48d23d1887323
         Sim_SN:    898600e6111551111111
         Manufacturer: LGE
         Model:    Nexus 4
         Brand:    google
         Device:    mako
         */

        Settings.System.putString(this.getApplicationContext().getContentResolver(),"responseStr", "abc");
        Settings.System.putInt(this.getApplicationContext().getContentResolver(),"response", 250);
        String msg = Settings.System.getString(this.getApplicationContext().getContentResolver(),"responseStr");
        int getInt = Settings.System.getInt(this.getApplicationContext().getContentResolver(),"response",0);
        Log.i("_net_response_=", msg);
        Log.i("_net_response_=", ""+ getInt);
    }

    /**
     * get App versionCode
     * @param context
     * @return
     */
    public String getVersionCode(Context context){
        PackageManager packageManager=context.getPackageManager();
        PackageInfo packageInfo;
        String versionCode="";
        try {
            packageInfo=packageManager.getPackageInfo(context.getPackageName(),0);
            versionCode=packageInfo.versionCode+"";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    /**
     * get App versionName
     * @param context
     * @return
     */
    public String getVersionName(Context context){
        PackageManager packageManager=context.getPackageManager();
        PackageInfo packageInfo;
        String versionName="";
        try {
            packageInfo=packageManager.getPackageInfo(context.getPackageName(),0);
            versionName=packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }


    /**
     * 硬件检测
     * @param intentType
     * @param cla
     */
    private void HardwareTest(final int intentType,final Class cla,String activityStr) {

        if(_IdCardCode  && _RoomCardCode == 200){//&& _PrintCode
            if (ClickUtil.isFastClick()) {
                if(action_outcard != action_outcard ){
                    int getIdcardCode = _IDCardUtils.getInstance().getSAMStatus();//身份证 开机启动获取报错
                    if (((-1)==getIdcardCode)|| (522 == getIdcardCode) ) {
                        _SToast("身份证模块异常!");
                    }
                }
                 /*
                int iStatus = _UsbPrintUtils.getInstance().getPrinterStatus();//打印机 Printer
                if (8 == iStatus) {//打印机快没纸了
                    _SToast("打印机快没纸了");
                } else if (7 == iStatus) {//打印机缺纸
                    _SToast("打印机没纸了");
                }else if(( -1) == iStatus){
                    _SToast("打印机异常");
                }*/
                SparseArray<Integer> mQueryState = _RoomCardUtils.getInstance().K720_SensorQuery();//发卡机
                if (((200 == _RoomCardCode) &&
                        (0 == mQueryState.get(0)))) {
                    String str = "";
                    switch (mQueryState.get(1)) {
                        case 0x39:// 回收箱卡满/卡箱预满
                        case 0x38://回收箱卡满
                            str += "机器状态：回收箱卡满\r\n";
                            break;
                        case 0x34://命令不能执行
                        case 0x32://准备卡失败
                            str += "机器状态：准备失败，请点击“复位”\r\n";
                            _RoomCardUtils.getInstance().Reset();
                            break;
                        case 0x31:
                            str += "机器状态：卡箱预满\r\n";
                            break;
                        case 0x30:
                            str += "机器状态：空闲\r\n";
                            break;
                    }
                    switch (mQueryState.get(2)) {
                        case 0x38://正在发卡
                        case 0x34://正在收卡
                            str += "动作状态：正在收卡发卡\r\n";
                            break;
                        case 0x32://发卡出错
                        case 0x31://收卡出错
                            str += "动作状态：出错，请点击“复位”\r\n";
                            _RoomCardUtils.getInstance().Reset();
                            break;
                        case 0x30:
                            str += "动作状态：空闲\r\n";
                            break;
                    }
                    switch (mQueryState.get(3)) {
                        case 0x39://发卡箱已满，无法再回收到发卡箱
                        case 0x38://发卡箱已满，无法再回收到发卡箱/卡箱预空
                            str += "卡箱状态：发卡箱已满，无法再回收到发卡箱\r\n";
                            break;
                        case 0x34://重叠卡
                        case 0x32://卡堵塞
                            str += "卡箱状态：重叠卡/卡堵塞\r\n";
                            break;
                        case 0x31://TODO   小于十张房卡 预空状态 提示快没卡了 给酒店提示快没房卡了
                            str += "卡箱状态：卡箱预空\r\n";
                            break;
                        case 0x30://TODO   大于十张房卡 非预空状态
                            str += "卡箱状态：卡箱为非预空状态\r\n";
                            break;
                    }
                    switch (mQueryState.get(4)) {
                        case 0x3E:
                            str += "卡片状态：只有一张卡在传感器2-3位置\r\n";
                            break;
                        case 0x3B:
                            str += "卡片状态：只有一张卡在传感器1-2位置\r\n";
                            break;
                        case 0x39:
                            str += "卡片状态：只有一张卡在传感器1位置\r\n";
                            break;
                        case 0x37:
                            str += "卡片状态：卡在传感器1-2-3的位置\r\n";
                            break;
                        case 0x36:
                            str += "卡片状态：卡在传感器2-3的位置\r\n";
                            break;
                        case 0x35:
                            str += "卡片状态：卡在传感器取卡位置\r\n";
                            break;
                        case 0x34:
                            str += "卡片状态：卡在传感器3位置\r\n";
                            break;
                        case 0x33:
                            str += "卡片状态：卡在传感器1-2位置(读卡位置)\r\n";
                            break;
                        case 0x32:
                            str += "卡片状态：卡在传感器2位置\r\n";
                            break;
                        case 0x31:
                            str += "卡片状态：卡在传感器1位置(取卡位置)\r\n";
                            break;
                        case 0x38:
                            str += "卡片状态：卡箱已空\r\n";
                            break;
                        case 0x30:
                            str += "卡片状态：空闲\r\n";
                            break;
                    }

                    _Log.d(TAG, str);
                    if( intentType == action_outcard){//003 自助退房[房卡为空可以收卡]
                        Intent intent = new Intent(LoginActivity.this, cla);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                    }else {  //001 没打手机  002 扫码订房 [房卡为空不可以订房]
                        if (0x38 == (mQueryState.get(4))) {//无卡可以收卡 不可以发卡
                            _SToast("房卡为空");
                        } else if (0x31 == mQueryState.get(3)) {
                            _SToast("房卡快没了");
                            if(intentType == action_message){
                                Intent intent = new Intent(LoginActivity.this, cla);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(_Constants.BUNDLE_REY, activityStr);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(LoginActivity.this, cla);
                                intent.putExtra(_Constants.BUNDLE_REY, activityStr);
                                startActivity(intent);
                            }
                        } else {
                            if(intentType == action_message){
                                Intent intent = new Intent(LoginActivity.this, cla);
                                intent.putExtra(_Constants.BUNDLE_REY, activityStr);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(LoginActivity.this, cla);
                                intent.putExtra(_Constants.BUNDLE_REY, activityStr);
                                startActivity(intent);
                            }
                        }
                    }
                }  else if (201 == _RoomCardCode ||
                        202 == _RoomCardCode ||
                        203 == _RoomCardCode ) {
                    _SToast("收发卡机模块异常!");
                }else {
                    _SToast("传感器状态查询失败!");
                    int mhasOrder = mShUserData.getInt("hasOrder", 0);
                    _Log.d("mhasOrder","handleMessage"+mhasOrder);
                    if(mhasOrder == 1){
                        HardwareTest(action_message,CheckInNetOrderActivity.class,_Constants.BUNDLE_NET_VALUE);
                    }else {
                        HardwareTest(action_message,HomeActivity.class,"");
                    }
                }
            }
        }else {
            _InitHardware();
        }

    }


    /**
     * okhttp
     * @param str
     */
    private synchronized void mGetHotelId(String str) {
        _Log.d("login_net_response","请求参数:"+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.NET_GET_HOSTELID_URL+str)
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {

                _LToast(_Constants.TOAST_SERVER_NULL);
            }
            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("login_net_response","HotelId/result="+result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson(); // 获取酒店id ，去查询网路订单/
                    hotelId = mgson.fromJson(result, HotelId.class);
                    if (hotelId.code == 0) {//TODO

                        mPublicEditor.putInt("public_hotelid",hotelId.data.hotelId);
                        mPublicEditor.putString("public_hotelname",hotelId.data.hotelName);
                        mPublicEditor.putString("public_mac",mDeviceId);
                        mPublicEditor.putString("public_deviceid",mDeviceId);
                        mPublicEditor.commit();

                        _Log.d("login_net_response","HotelId/result="+hotelId.data.hotelName);

                        mHandler.sendEmptyMessageDelayed(_HandlerCode.GET_PROGRAM_CODE,500);


                    }else if(hotelId.code == (-1)){
                        _LToast(hotelId.msg+"");
                    }
                    try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }


    /**
     * okhttp
     * @param str
     */
    private synchronized void mGetHotelDetial(String str) {
        _Log.d("HotelDetial_net_response","请求参数:"+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.HOTEL_DETIAL_URL+str)
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {

                _LToast(_Constants.TOAST_SERVER_NULL);
                _Log.d("HotelDetial_net_response","Call="+request.toString()+"_IOException"+e.getMessage());
                _Log.d("HotelDetial_net_response","Call="+request+"_IOException"+e.getLocalizedMessage());
            }
            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("HotelDetial_net_response","HotelId/result="+result);
                if (!TextUtils.isEmpty(result)) {
                    Gson mgson = new Gson();
                    hotelDetialsData = mgson.fromJson(result, HotelDetialsData.class);
                    if (hotelDetialsData.code == 0) {//TODO
                        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_UI_TAKE_TURN,500);
                    }else if(hotelDetialsData.code == (-1)){
                        _LToast(hotelDetialsData.msg+"");
                    }
                    try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }


    /**
     * okhttp
     * @param str
     */
    private synchronized void mGetProgramCode(String str) {
        _Log.d("getcode_net_response","请求参数="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.SMALL_PROGRAM_URL+str)
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {
                _LToast(_Constants.TOAST_SERVER_NULL);

                _Log.d("getcode_net_response","Call="+request.toString()+"_IOException"+e.getMessage());
                _Log.d("getcode_net_response","Call="+request+"_IOException"+e.getLocalizedMessage());
            }
            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("getcode_net_response","二维码/result="+result);

                if (!TextUtils.isEmpty(result)) {
                    try {
                        Gson mgson = new Gson();
                        SmallData = mgson.fromJson(result, SmallProgramData.class);
                        if (SmallData.code == 0) {
                            mHandler.sendEmptyMessage(_HandlerCode.SAMLL_PROGRAM_URL);
                        }else if(SmallData.code == (-1)){
                            _LToast(SmallData.msg+"");
                        }
                    } catch (Exception e) {
                        _LToast(_Constants.TOAST_PARSE_NULL);
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }


}
