package hk.yijian.botel;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;

import java.util.LinkedList;
import java.util.List;

import hk.yijian.baidutts._BaiduTTSUtils;
import hk.yijian.botel.bean.greenDAO.UserBeanDao;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._RoomCardUtils;
import hk.yijian.view.toast._Toast;



/**
 * @author lmh
 */

public abstract class BaseActivity extends AppCompatActivity {

    public static final List<BaseActivity> mActivities = new LinkedList<BaseActivity>();
    public BaseActivity mBaseActivity;
    //okhttp
    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("application/json; charset=utf-8");
    public static final OkHttpClient mOkHttpClient = new OkHttpClient();
    static {
        //mOkHttpClient.connectTimeoutMillis();
    }
    // 窗口管理者
    public WindowManager wManager;
    public WindowManager.LayoutParams mParams;
    public View ManagerView;
    public ImageView imageView;
    public WifiManager mWifiManager;
    //SP
    public SharedPreferences mSharePublicData  = null;
    public SharedPreferences.Editor mPublicEditor = null;

    public SharedPreferences mShareOrderData = null;
    public SharedPreferences.Editor mOrderEditor = null;

    public SharedPreferences mShUserData = null;
    public SharedPreferences.Editor mUserEditor = null;
    //
    //base data
    //public String mDeviceId =  MD5Utils.StrMd5(AppUtils.getMacAddress());
    public String mDeviceId = "220c45e4909ae2a923dad3697fcdbf3c";

    public String pkName;
    //
    public UserBeanDao mDao;
    //Hardware
    //身份证
    public boolean _IdCardCode;
    //发卡机
    public int _RoomCardCode;
    public String SignStr ;
    //打印机
    //public boolean _PrintCode;

    /**
     * 做一个lib库文件/对基础数据的检测/
     * 检测小票打印/房卡器/身份证检测是否正常
     * 网络状态/网络强弱检测/
     * 小票/房卡是否有/
     * 后台返回数据数据是否满房
     * 初始化 hardwarelibrary
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //隐藏nav bar   隐藏status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE;
            decorView.setSystemUiVisibility(option);
        }
        pkName = this.getPackageName();
        mBaseActivity = this;
        //greenDAO
        mDao = _Application.getDaoSession().getUserBeanDao();
        //init sp
        mSharePublicData = getSharedPreferences("publicdata", Context.MODE_PRIVATE);
        mPublicEditor = mSharePublicData.edit();
        mShareOrderData = getSharedPreferences("orderdata", Context.MODE_PRIVATE);
        mOrderEditor = mShareOrderData.edit();
        mShUserData = getSharedPreferences("userdata", Context.MODE_PRIVATE);
        mUserEditor = mShUserData.edit();
        //init window
        wManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        mParams = new WindowManager.LayoutParams();
        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        //
        //init method
        initView(savedInstanceState);
        initClickListener();
        initTouchListener();
        //baidu TTS
        _BaiduTTSUtils.getInstance()._InitBaiduTTS(this);
        synchronized (mActivities) {
            mActivities.add(this);
        }


        try {
            ///SignStr = Sign.appSign(Config.appid,Config.secret_id,Config.secret_key,Config.bucket,Config.expired);
            //mUserEditor.putString("signstr",SignStr);
            //mUserEditor.commit();
        }catch (Exception e){}

    }

    //protected abstract void initData();
    protected abstract void initView(Bundle savedInstanceState);
    protected abstract void initClickListener();
    protected abstract void initTouchListener();
    protected abstract void initTitleView();
    //protected abstract void initTitleTouchListener();


    @Override
    protected void onResume() {
        super.onResume();
        initTitleView();
        if(isNetConnected(this)){
            //Toast.makeText(this,"有网络",Toast.LENGTH_SHORT).show();
        }else {
            final AlertDialog.Builder mDialog = new AlertDialog.Builder(mBaseActivity);
            mDialog.setTitle("网络提示！");
            mDialog.setMessage("无网络...");
            mDialog.setCancelable(false);
            mDialog.setPositiveButton("联网", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent wifi = new Intent();
                    ComponentName wificn = new ComponentName("com.android.settings",
                            "com.android.settings.wifi.WifiSettings");
                    wifi.setComponent(wificn);
                    startActivity(wifi);
                }
            });
            mDialog.setNegativeButton("刷新", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mDialog.create().dismiss();

                    recreate();
                    /*
                    System.exit(0);
                    android.os.Process.killProcess(android.os.Process.myPid());

                    Intent intent = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(getBaseContext().getPackageName());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                   */

                }
            });
            mDialog.create().show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBaseActivity = null;
        synchronized (mActivities) {
            mActivities.remove(mBaseActivity);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    */

    public void openWifi() {
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }
    }

    /**
     *
     */
    public void _InitHardware() {

        //初始化身份证
        _IdCardCode = _IDCardUtils.getInstance().InitIDCardDevice(this);
        if(!_IdCardCode){ //iDCard创建失败
            _LToast("IdCard创建失败");
            return;
        }//iDCard创建成功

        //初始化发卡机
        _RoomCardCode = _RoomCardUtils.getInstance().InitLink();
        switch (_RoomCardCode) {
            case 203:
            case 202://设备连接失败
            case 201://串口打开错误
                _LToast("发卡机连接失败");
                break;
            case 200://设备连接成功
                break;
        }
        /*
        //初始化打印机
        _PrintCode = _UsbPrintUtils.getInstance().InitUsbPrint(this);
        if(!_PrintCode){ //Print创建失败
            _LToast("Print创建失败");
            return;
        }//Print创建成功
        */
    }



    /**
     * UI 悬浮窗
     * @param context
     * @param cla
     */
    @SuppressLint({"RtlHardcoded", "InflateParams"})
    public void mWindowBack(final Context context, final Class cla) {
        mParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        mParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        mParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mParams.gravity = Gravity.TOP | Gravity.LEFT;
        mParams.format = PixelFormat.TRANSLUCENT;
        mParams.x = 0;
        mParams.y = 0;
        ManagerView = LayoutInflater.from(context).inflate(R.layout.backlayout, null);
        imageView = ManagerView.findViewById(R.id.iv_back_window);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wManager.removeView(ManagerView);
                Intent intent = new Intent(context, cla);
                startActivity(intent);
                finish();
                //android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        wManager.addView(ManagerView, mParams);
    }

    private boolean isNetConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }
    /**
     * Toast
     * @param str
     */
    public void _SToast(final String str){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _Toast.makeText(BaseActivity.this, ""+str, 0, _Toast.WARNING,_Toast.SCALE);
            }
        });

    }



    //返回主页
    public void backLoginPage(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    /**
     * Toast
     * @param str
     */
    public void _LToast(final String str){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _Toast.makeText(BaseActivity.this, ""+str, 1, _Toast.WARNING,_Toast.SCALE);
            }
        });
    }


}
