package hk.yijian.botel.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;

/**
 * Created by young on 2017/11/25.
 */

public abstract class BaseFragment extends Fragment {

    protected Activity mActivity;
    //okhttp
    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("application/json; charset=utf-8");
    public static final OkHttpClient mOkHttpClient = new OkHttpClient();
    static {
        //mOkHttpClient.connectTimeoutMillis();
    }
    //
    public SharedPreferences mSharePublicData  = null;
    public SharedPreferences.Editor mPublicEditor = null;
    public SharedPreferences mShareOrderData = null;
    public SharedPreferences.Editor mOrderEditor = null;
    private View rootView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = (Activity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShareOrderData = getActivity().getSharedPreferences("orderdata", Context.MODE_PRIVATE);
        mOrderEditor = mShareOrderData.edit();
        mSharePublicData = getActivity().getSharedPreferences("publicdata", Context.MODE_PRIVATE);
        mPublicEditor = mSharePublicData.edit();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null != rootView) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (null != parent) {
                parent.removeView(rootView);
            }
        } else {
            rootView = inflater.inflate(getLayoutId(), container, false);
            initView(rootView, savedInstanceState);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    protected abstract int getLayoutId();

    protected abstract void initView(View view, Bundle savedInstanceState);


}
