package hk.yijian.botel.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;

import hk.yijian.botel.R;
import hk.yijian.botel.adater.HotAdapter;


public class HotFragment extends BaseFragment {

    private GridView gv_form;
    private LinearLayout ll_load_data;
    private Context mContext;

    public HotFragment() {
    }

    public static HotFragment newInstance() {
        HotFragment fragment = new HotFragment();
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_grid_layout;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mContext = getActivity();
        gv_form = view.findViewById(R.id.gv_form);
        ll_load_data = view.findViewById(R.id.ll_load_data);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ll_load_data.setVisibility(View.INVISIBLE);
                HotAdapter adapter = new HotAdapter(mContext);
                gv_form.setAdapter(adapter);
                adapter.notifyDataSetInvalidated();
            }
        },800);
    }

}
