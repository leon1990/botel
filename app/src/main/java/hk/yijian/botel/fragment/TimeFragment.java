package hk.yijian.botel.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import hk.yijian.botel.R;
import hk.yijian.botel.activity.CreateOrderActivity;
import hk.yijian.botel.bean.response.RoomListAllData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.util.ClickUtil;
import hk.yijian.hardware._Log;
import hk.yijian.view.toast._TUtils;


/**
 * Created by young on 2017/11/25.
 */

public class TimeFragment extends BaseFragment {

    private GridView gv_form;
    private LinearLayout ll_load_data;
    private int hotelId;
    private RoomListAllData timeRoomListData;
    private Gson mgson;
    private Context mContext;

    public TimeFragment() {
    }

    public static TimeFragment newInstance() {
        return new TimeFragment();
    }



    @Override
    protected int getLayoutId() {
        return R.layout.fragment_grid_layout;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mContext = getActivity();
        hotelId = mSharePublicData.getInt("public_hotelid", 0);
        gv_form = view.findViewById(R.id.gv_form);
        ll_load_data = view.findViewById(R.id.ll_load_data);
        try {
            String rooms = "{\"hotelId\": "+hotelId+"}";
            TimeNetRoomList(rooms);
        }catch (Exception e){}
    }


    /**
     *  TimeAdapter
     */
    public class TimeAdapter extends BaseAdapter {
        Context context;

        public TimeAdapter(Context context){
            this.context = context;
        }


        @Override
        public int getCount() {
            return timeRoomListData.data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            final ViewHolder holder;
            if (convertView == null) {
                final LayoutInflater inflater = (LayoutInflater) mContext.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_form_item, null);
                holder = new ViewHolder();
                holder.iv_icon = convertView.findViewById(R.id.iv_room_img);
                holder.tv_type = convertView.findViewById(R.id.tv_room_type);
                holder.tv_price = convertView.findViewById(R.id.tv_room_price);
                convertView.setTag(holder);
            }else {
                holder = (ViewHolder)convertView.getTag();
            }

            DecimalFormat df = new DecimalFormat("0.00");
            String standardFee = df.format((double)(timeRoomListData.data.get(i).standardFee)/100);
            holder.tv_type.setText("" +timeRoomListData.data.get(i).roomTypeName);
            holder.tv_price.setText("¥" + standardFee + "");
            Glide.with(mContext).load(timeRoomListData.data.get(i).deviceMainPhoto).into(holder.iv_icon);

            holder.iv_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ClickUtil.isFastClick()) {
                        mOrderEditor.putInt("ticket_deposit", timeRoomListData.data.get(i).depositFee);
                        mOrderEditor.putInt("ticket_roomFee", timeRoomListData.data.get(i).memberFee);
                        mOrderEditor.putInt("ticket_rate",timeRoomListData.data.get(i).standardFee);
                        mOrderEditor.putString("ticket_type", timeRoomListData.data.get(i).roomTypeName);
                        mOrderEditor.putString("ticket_roomtypename", timeRoomListData.data.get(i).roomTypeName);
                        mOrderEditor.putInt("ticket_roomType", timeRoomListData.data.get(i).roomTypeId);
                        mOrderEditor.putInt("ticket_breakfastNum", timeRoomListData.data.get(i).breakfastNum);
                        mOrderEditor.putInt("ticket_orderSourceType", 2);
                        mOrderEditor.putInt("ticket_stayType", 2);
                        mOrderEditor.commit();

                        try{ // 四张图
                            ArrayList<String> roomPhotos = new ArrayList<String>();
                            roomPhotos.add(""+timeRoomListData.data.get(i).devicePhotos.get(0));
                            roomPhotos.add(""+timeRoomListData.data.get(i).devicePhotos.get(1));
                            roomPhotos.add(""+timeRoomListData.data.get(i).devicePhotos.get(2));
                            roomPhotos.add(""+timeRoomListData.data.get(i).devicePhotos.get(3));

                            Intent intent = new Intent(mContext, CreateOrderActivity.class);
                            intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_TIME_VALUE);
                            intent.putStringArrayListExtra("roomPhotos", roomPhotos);
                            mContext.startActivity(intent);
                        }catch (Exception e){}
                    }
                }
            });
            return convertView;
        }
    }
    public static class ViewHolder {
        ImageView iv_icon;
        TextView tv_type;
        TextView tv_price;
    }

    /**
     * RoomList
     * @param str
     */
    private synchronized void TimeNetRoomList(String str) {
        _Log.d("TimeRoomList_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.NET_All_ROOMLIST_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        _TUtils._LToast(getActivity(),_Constants.TOAST_SERVER_NULL);
                    }
                });

            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("TimeRoomList_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    mgson = new Gson();
                    timeRoomListData = mgson.fromJson(result, RoomListAllData.class);
                    if(timeRoomListData.code == 0 ){
                        if(null == timeRoomListData.data || timeRoomListData.data.size() ==0 ){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    _TUtils._LToast(getActivity(),_Constants.TOAST_SERVER_NULL);
                                }
                            });
                        }else {


                            try{
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ll_load_data.setVisibility(View.INVISIBLE);
                                        gv_form.setAdapter(new TimeAdapter(mContext));
                                        //adapter.notifyDataSetInvalidated();
                                    }
                                });
                            }catch (Exception e){}

                        }
                    }else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                _TUtils._LToast(getActivity(),timeRoomListData.msg);
                            }
                        });
                    }
                }else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            _TUtils._LToast(getActivity(),_Constants.TOAST_DATA_NULL);
                        }
                    });
                }
            }
        });
    }
}
