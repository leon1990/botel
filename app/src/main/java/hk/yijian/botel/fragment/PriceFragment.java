package hk.yijian.botel.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import hk.yijian.botel.R;
import hk.yijian.botel.activity.CreateOrderActivity;
import hk.yijian.botel.bean.response.TimeRoomData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.util.ClickUtil;
import hk.yijian.hardware._Log;
import hk.yijian.view.toast._TUtils;


public class PriceFragment extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";
    public String mParam1;
    private PriceAdapter priceAdapter;
    private GridView gv_form;
    private LinearLayout ll_load_data;
    private Context mContext;
    //
    private TimeRoomData allRoomData;
    private Gson mgson;
    //
    private int hotelId;

    //handler
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.DO_REFRESH_PAGE:

                   try{
                       ll_load_data.setVisibility(View.INVISIBLE);
                       priceAdapter = new PriceAdapter(mContext);
                       gv_form.setAdapter(priceAdapter);
                       priceAdapter.notifyDataSetInvalidated();

                   }catch (Exception e){}

                    break;
            }
            super.handleMessage(msg);
        }
    };

    public PriceFragment() {
    }

    public static PriceFragment newInstance(String param1) {
        PriceFragment fragment = new PriceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    protected int getLayoutId() {
        return R.layout.fragment_grid_layout;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mContext = getActivity();
        gv_form = view.findViewById(R.id.gv_form);
        mParam1 = getArguments().getString(ARG_PARAM1);
        ll_load_data = view.findViewById(R.id.ll_load_data);
        try{
            hotelId = mSharePublicData.getInt("public_hotelid", 0);
            String rooms = "{\"hotelId\": "+hotelId+"}";
            if (mParam1.equals("small")) {
                HomeNetRoomList(rooms,mParam1);
            } else if (mParam1.equals("big")) {
                HomeNetRoomList(rooms,mParam1);
            }



        }catch (Exception e){}

    }


    public class PriceAdapter extends BaseAdapter {
        Context context;
        public PriceAdapter(Context context){
            this.context = context;
        }
        @Override
        public int getCount() {
            return allRoomData.getData().size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            final ViewHolder holder;
            if (convertView == null) {
                final LayoutInflater inflater = (LayoutInflater) mContext.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_form_item, null);
                holder = new ViewHolder();
                holder.iv_icon = convertView.findViewById(R.id.iv_room_img);
                holder.tv_price = convertView.findViewById(R.id.tv_room_price);
                holder.tv_type = convertView.findViewById(R.id.tv_room_type);
                convertView.setTag(holder);
            }else {
                holder = (ViewHolder)convertView.getTag();
            }
            DecimalFormat df = new DecimalFormat("0.00");
            String standardFee = df.format((double)(allRoomData.getData().get(i).getStandardFee())/100);
            holder.tv_type.setText("" +allRoomData.getData().get(i).getRoomTypeName());
            holder.tv_price.setText("¥" + standardFee + "    ");
            Glide.with(mContext).load(allRoomData.getData().get(i).getDeviceMainPhoto()).into(holder.iv_icon);

            holder.iv_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ClickUtil.isFastClick()) {

                        mOrderEditor.putInt("ticket_deposit", allRoomData.getData().get(i).getDepositFee());
                        mOrderEditor.putInt("ticket_roomFee", allRoomData.getData().get(i).getMemberFee());

                        mOrderEditor.putInt("ticket_rate",allRoomData.getData().get(i).getStandardFee());
                        mOrderEditor.putString("ticket_type", allRoomData.getData().get(i).getRoomTypeName());
                        mOrderEditor.putString("ticket_roomtypename", allRoomData.getData().get(i).getRoomTypeName());
                        mOrderEditor.putInt("ticket_roomType", allRoomData.getData().get(i).getRoomTypeId());
                        mOrderEditor.putInt("ticket_breakfastNum", allRoomData.getData().get(i).getBreakfastNum());
                        mOrderEditor.putInt("ticket_orderSourceType", 2);
                        mOrderEditor.putInt("ticket_stayType", 1);
                        mOrderEditor.commit();

                        try {
                            ArrayList<String> roomPhotos = new ArrayList<String>();
                            roomPhotos.add(""+allRoomData.getData().get(i).getDevicePhotos().get(0));
                            roomPhotos.add(""+allRoomData.getData().get(i).getDevicePhotos().get(1));
                            roomPhotos.add(""+allRoomData.getData().get(i).getDevicePhotos().get(2));
                            roomPhotos.add(""+allRoomData.getData().get(i).getDevicePhotos().get(3));

                            Intent intent = new Intent(mContext, CreateOrderActivity.class);
                            intent.putExtra(_Constants.BUNDLE_REY, _Constants.BUNDLE_IN_VALUE);
                            intent.putStringArrayListExtra("roomPhotos", roomPhotos);
                            mContext.startActivity(intent);




                        }catch (Exception e){}

                    }
                }
            });
            return convertView;
        }
    }
    public static class ViewHolder {
        ImageView iv_icon;
        TextView tv_type;
        TextView tv_price;
    }
    /**
     * RoomList
     * @param str
     */
    private synchronized void HomeNetRoomList(String str,final String param) {
        _Log.d("AllRoomList参数_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.NET_All_ROOMLIST_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override

            public void onFailure(Request request, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        _TUtils._LToast(getActivity(),_Constants.TOAST_SERVER_NULL);
                    }
                });
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("AllRoomList_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    mgson = new Gson();
                    allRoomData = mgson.fromJson(result, TimeRoomData.class);
                    if(allRoomData.getCode() == 0 ){
                        if(null == allRoomData.getData() || allRoomData.getData().size() ==0 ){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    _TUtils._LToast(getActivity(),_Constants.TOAST_SERVER_NULL);
                                }
                            });

                        }else {
                            if (param.equals("small")) {
                                Collections.sort(allRoomData.getData(), new Comparator<TimeRoomData.Data>() {
                                    @Override
                                    public int compare(TimeRoomData.Data result, TimeRoomData.Data t1) {
                                        int i = t1.getMemberFee() - result.getMemberFee();
                                        return i;
                                    }
                                });
                            } else if (param.equals("big")) {
                                Collections.sort(allRoomData.getData(), new Comparator<TimeRoomData.Data>() {
                                    @Override
                                    public int compare(TimeRoomData.Data result, TimeRoomData.Data t1) {
                                        int i = result.getMemberFee() - t1.getMemberFee();
                                        return i;
                                    }
                                });
                            }
                            mHandler.sendEmptyMessageDelayed(_HandlerCode.DO_REFRESH_PAGE,500);
                        }
                    }else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                _TUtils._LToast(getActivity(),allRoomData.getMsg());
                            }
                        });
                    }
                }else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            _TUtils._LToast(getActivity(),_Constants.TOAST_DATA_NULL);
                        }
                    });
                }

            }
        });
    }


}
