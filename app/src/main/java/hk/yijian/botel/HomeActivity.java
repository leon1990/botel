package hk.yijian.botel;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

import hk.yijian.botel.activity.CheckInAllRoomDataActivity;
import hk.yijian.botel.activity.CheckInNetOrderActivity;
import hk.yijian.botel.activity.CheckInTimeRoomActivity;
import hk.yijian.botel.bean.response.NetOrderData;
import hk.yijian.botel.constant._Constants;
import hk.yijian.botel.constant._ConstantsAPI;
import hk.yijian.botel.constant._HandlerCode;
import hk.yijian.botel.view.ProgressManager;
import hk.yijian.botel.view.TimeView;
import hk.yijian.botel.view.TitleBarManager;
import hk.yijian.hardware._IDCardUtils;
import hk.yijian.hardware._Log;



public class HomeActivity extends BaseActivity {

    private ImageView iv_back;
    private ImageView iv_delete;
    private View v_second_line;
    private ImageView iv_idcard_icon;
    private LinearLayout ll_checkin_room;
    private LinearLayout ll_time_room;
    private LinearLayout ll_net_room;
    private NetOrderData netOrder;
    private String  nameStr;
    private TimeView tv_address_countdown;

    //handler
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case _HandlerCode.CHANGE_READID_READING:
                    //mReadIDCardInfo();
                    break;
                case _HandlerCode.INTENT_NET_UI:
                    Intent intent = new Intent(HomeActivity.this,CheckInNetOrderActivity.class);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    };



    @Override
    protected void initView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        ll_checkin_room = findViewById(R.id.ll_checkin_room);
        ll_time_room = findViewById(R.id.ll_time_room);
        ll_net_room = findViewById(R.id.ll_net_room);
        iv_idcard_icon = findViewById(R.id.iv_idcard_icon);
        iv_idcard_icon.setImageResource(R.drawable.idcardframe);
        AnimationDrawable ad = (AnimationDrawable) iv_idcard_icon.getDrawable();
        ad.setOneShot(false);//循环播放
        ad.start();

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initTitleView() {
        TitleBarManager.getInstance().init(this);
        ProgressManager.getInstance().init(this);
        ProgressManager.getInstance().setProgressInvisible();
        iv_back = findViewById(R.id.iv_back);
        iv_delete = findViewById(R.id.iv_delete);
        v_second_line = findViewById(R.id.v_second_line);
        v_second_line.setVisibility(View.INVISIBLE);
        tv_address_countdown = findViewById(R.id.tv_address_countdown);
        tv_address_countdown.mStartTime();
        tv_address_countdown.setOnCountDownFinishListener(new TimeView.onCountDownFinishListener() {
            @Override
            public void onFinish() {
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        iv_back.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                backLoginPage(HomeActivity.this);
                return true;
            }
        });

        iv_delete.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                backLoginPage(HomeActivity.this);
                return true;
            }
        });
        mHandler.removeMessages(_HandlerCode.CHANGE_READID_READING);
        mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_READID_READING, 500);
    }

    @Override
    protected void initClickListener() {
        ll_checkin_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ll_time_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ll_net_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initTouchListener() {

        ll_checkin_room.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Intent intent = new Intent(HomeActivity.this,CheckInAllRoomDataActivity.class);
                intent.putExtra(_Constants.BUNDLE_REY,  _Constants.BUNDLE_IN_VALUE);
                startActivity(intent);
                finish();
                //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                return false;
            }
        });

        ll_time_room.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Intent intent = new Intent(HomeActivity.this,CheckInTimeRoomActivity.class);
                intent.putExtra(_Constants.BUNDLE_REY,  _Constants.BUNDLE_TIME_VALUE);
                startActivity(intent);
                finish();
                return false;
            }
        });
        ll_net_room.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mUserEditor.putInt("intentcode",203);
                mUserEditor.commit();//mUserEditor.apply();

                Intent intent = new Intent(HomeActivity.this,CheckInNetOrderActivity.class);
                intent.putExtra(_Constants.BUNDLE_REY,  _Constants.BUNDLE_NET_VALUE);
                startActivity(intent);
                finish();
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _Log.d("HomeActivity","onDestroy");
        tv_address_countdown.mStopTime();
        mHandler.removeMessages(_HandlerCode.INTENT_NET_UI);
        mHandler.removeMessages(_HandlerCode.CHANGE_READID_READING);
    }



    //ReadIDCardInfo();
    private void mReadIDCardInfo() {
        try {
            SparseArray<String> IDCardInfo = _IDCardUtils.getInstance().readIDCardInfo(pkName);
            int retNo = Integer.parseInt(IDCardInfo.get(0));
            _Log.d("retNo",""+retNo);
            if (retNo == 0x90) {
                nameStr = IDCardInfo.get(1);
                int status = 3;//TODO 该参数？
                String net_str ="{\"status\": "+status+",\"userName\": \""+nameStr.trim()+"\"}";
                mSearchNetOrder(net_str);
            }else {
                mHandler.removeMessages(_HandlerCode.CHANGE_READID_READING);
                mHandler.sendEmptyMessageDelayed(_HandlerCode.CHANGE_READID_READING, 500);
            }
        }catch (Exception e){
            //throw new IllegalArgumentException("The argument cannot be null");
        }
    }


    /**
     *
     * ID 刷身份证查询网络订单
     * @param str
     */
    private synchronized void mSearchNetOrder(String str) {
        _Log.d("main_net_response","str="+str);
        Request request = new Request.Builder()
                .addHeader("content-type", "application/json;charset:utf-8")
                .url(_ConstantsAPI.NET_ORDER_LIST_URL)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, str))
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {//execute
            @Override
            public void onFailure(Request request, IOException e) {

                _LToast(_Constants.TOAST_SERVER_NULL);
            }
            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                _Log.d("main_net_response","result="+result);
                if (!TextUtils.isEmpty(result)) {
                    try {
                        Gson mgson = new Gson();
                        netOrder = mgson.fromJson(result, NetOrderData.class);
                        if (netOrder.code == 0) {
                            if(netOrder.data!=null && !netOrder.data.isEmpty()){
                                mUserEditor.putInt("intentcode",201);
                                mUserEditor.putString("userName",nameStr.trim());
                                mUserEditor.commit();
                                mHandler.removeMessages(_HandlerCode.INTENT_NET_UI);
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.INTENT_NET_UI,100);
                            }else {
                                _LToast("该姓名无网络订单！");
                                mUserEditor.putInt("intentcode", 203);
                                mUserEditor.commit();
                                mHandler.removeMessages(_HandlerCode.INTENT_NET_UI);
                                mHandler.sendEmptyMessageDelayed(_HandlerCode.INTENT_NET_UI,100);
                            }
                        }else if(netOrder.code == -1){
                            _LToast(""+netOrder.msg);
                        }
                    } catch (Exception e) {
                        _LToast(_Constants.TOAST_PARSE_NULL);
                        e.printStackTrace();
                    }
                }else {
                    _LToast(_Constants.TOAST_DATA_NULL);
                }
            }
        });
    }


}
