package hk.yijian.botel.constant;

/**
 * Created by young on 2017/9/23.
 */

public class _HandlerCode {

    //Main

    public static final int SAMLL_PROGRAM_URL = 70;
    public static final int CHANGE_ROOM_RATE = 5;

    public static final int CHANGE_READID_READING = 10;
    public static final int READ_IDCARD_VOICE = 9;
    public static final int READ_IDCARD_SUCCESS = 117;

    public static final int CHANGE_POPWIN_VIEW_ROOMNO = 90;
    public static final int CHANGE_VIEW_ROOMNO = 95;

    public static final int CHANGE_SINGLE_SELETE_ROOMNO = 17;


    public static final int CHANGE_UI_SUCCESS_MESSAGE = 21;
    public static final int CHANGEUI_SUCECESS_MESSAGE = 121;
    public static final int CHANGEUI_ERROR_MESSAGE = 122;
    public static final int CHANGE_UI_ERROR_MESSAGE = 123;

    public static final int OPON_DIALOG_WINDOW = 22;

    //Net  PAY
    public static final int PAY_ORDER_API = 23;
    public static final int PAY_GOOD_API = 25;
    //out card

    public static final int OUT_CARD_VIEW = 29;
    public static final int CHANGE_OUT_CARD_INFO = 30;
    public static final int CHANGE_OUT_CARD_OK = 31;
    //youtu

    public static final int NET_ORDER_CHANGE_UI = 80;
    //out card
    //Main
    public static final int INTENT_NET_UI= 81;
    public static final int IS_LOGIN_USER = 82;

    public static final int DO_PAYMENT_OK = 91;
    public static final int CHANGE_PAY_UI = 191;


    public static final int DO_REFRESH_PAGE = 101;

    public static final int CHANGE_UI_CONTINUE_ROOM = 102;

    //
    public static final int GET_PROGRAM_CODE = 115;


    public static final int CHANGE_UI_TAKE_TURN = 103;

    //out card
    public static final int PRINT_TICKET_CARD = 106;
    //
    public static final int TICKET_CARD_CHECKIN = 107;
    public static final int TICKET_CARD_CONTINUE = 108;
    public static final int TICKET_CARD_CHECKOUT = 109;
    //
    public static final int QUERY_CARD_MACHINE_STATE = 115;
    //
    public static final int TICKET_CARD_CHECKIN_SPEAKER = 110;
    public static final int TICKET_CARD_CONTINUE_SPEAKER = 111;
    public static final int TICKET_CARD_CHECKOUT_SPEAKER = 112;


    //续房
    public static final int READ_ROOM_CARD_DATA = 116;





}

