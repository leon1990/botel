package hk.yijian.botel.constant;

/**
 * Created by young on 2018/1/19.
 */

public class _ConstantsAPI {

    //TODO  API
    //TODO public URL
    //public static final String PUBLIC_URL = "https://yj.htmlk.cn/api";   //开发环境
    public static final String PUBLIC_URL = "https://hotel.htmlk.cn/api";  //线上环境[测试环境]

    //身份证信息上存 （人脸识别）
    public static final String FACE_TEST_URL = PUBLIC_URL+"/v1.0/upload/idcard";
    //
    public static final String CREATE_ORDER_URL = PUBLIC_URL+"/v1.0/order/create";
    public static final String CREATE_PAY_URL = PUBLIC_URL+"/v1.0/qrcode/pay";
    //
    public static final String QUERY_ORDER_STATUS_URL = PUBLIC_URL+"/v1.0/order/";

    //TODO  Out Card
    public static final String CHECKIN_CARD_INFO_URL = PUBLIC_URL+"/v1.0/order/checkin";     //  入住 - 打印房卡信息
    public static final String ORDER_OUT_CONFIRM_URL = PUBLIC_URL+"/v1.0/order/checkout/confirm";

    public static final String ORDER_OUT_CARD_URL = PUBLIC_URL+"/v1.0/order/checkout";
    public static final String ORDER_FACE_HOTELID_URL = PUBLIC_URL+"/v1.0/identify/face/";
    public static final String SMALL_PROGRAM_URL = PUBLIC_URL+"/v1.0/qrcode/sapp/";
    public static final String NET_ORDER_LIST_URL= PUBLIC_URL+"/v1.0/order/list";
    public static final String NET_GET_HOSTELID_URL= PUBLIC_URL+"/v1.0/pms/hotel/getid";

    //续房
    public static final String CONTINUE_ROOM_DATA_URL= PUBLIC_URL+"/v1.0/order/renewal";

    //all roomlist
    public static final String NET_All_ROOMLIST_URL= PUBLIC_URL+"/v1.0/pms/hotel/roomtype/list";//更改
    //room no list
    public static final String DETIALS_ROOM_NO_LIST_URL= PUBLIC_URL+"/v1.0/pms/hotel/room/available";//更改

    //上传信鸽 Token
    public static final String XG_TOKEN_NO_URL= PUBLIC_URL+"/v1.0/device/upload/token";//更改

    public static final String HOTEL_DETIAL_URL= PUBLIC_URL+"/v1.0/pms/hotel/detail/";//hotel  detials

    //酒店激活接口
    public static final String HOTEL_ACTIVATION_URL= PUBLIC_URL+" /v1.0/device/active";//hotel
}
