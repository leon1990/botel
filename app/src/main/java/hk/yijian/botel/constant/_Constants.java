package hk.yijian.botel.constant;

/**
 * Created by young on 2017/9/23.
 */

public class _Constants {
    //
    public static final String BUNDLE_REY = "activityCode";

    public static final String BUNDLE_NET_VALUE = "netcard";
    public static final String BUNDLE_IN_VALUE = "incard";
    public static final String BUNDLE_TIME_VALUE = "timecard";
    public static final String BUNDLE_CONTINUE_VALUE = "continuecard";
    public static final String BUNDLE_OUT_VALUE = "outcard";
    //
    public static final String NET_CARD = "网订取卡";
    public static final String CHECK_OUT = "自助退房";
    public static final String CHECK_IN = "全天房";
    public static final String CONTINUE_ROOM = "自助续住";
    public static final String TIME_ROOM = "钟点房";
    //
    public static final int PROGRESS_ONE = 1;
    public static final int PROGRESS_TWO = 2;
    public static final int PROGRESS_THREE = 3;
    public static final int PROGRESS_FOUR = 4;
    public static final int PROGRESS_FIVE = 5;
    //
    public static final String TOAST_DATA_NULL = "请求数据为空";
    public static final String TOAST_PARSE_NULL = "数据解析出错";
    public static final String TOAST_SERVER_NULL = "服务器连接失败";



}
