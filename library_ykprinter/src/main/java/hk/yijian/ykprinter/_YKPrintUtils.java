package hk.yijian.ykprinter;

import android.content.Context;

import com.szsicod.print.escpos.PrinterAPI;
import com.szsicod.print.io.InterfaceAPI;
import com.szsicod.print.io.USBAPI;

import java.io.UnsupportedEncodingException;


/**
 * Created by young on 2017/11/8.
 */
public class _YKPrintUtils {

    private Runnable runnable;
    private PrinterAPI mPrinter = new PrinterAPI();


    private _YKPrintUtils() {
    }

    private static class _UsbPrintUtilsLoader {
        private static final _YKPrintUtils INSTANCE = new _YKPrintUtils();
    }

    public static _YKPrintUtils getInstance() {
        return _UsbPrintUtilsLoader.INSTANCE;
    }
    

    public boolean initYKPrinter(final Context context){
        boolean YKbool = false;
        InterfaceAPI io = null;
        io = new USBAPI(context);

        if (PrinterAPI.SUCCESS == mPrinter.connect(io)) {
            YKbool = true;
        } else {
            YKbool = false;
        }
        return YKbool;

    }


    public void printTicketData(final String printData) {
        try {
            if (PrinterAPI.SUCCESS == mPrinter.printString(
                    printData, "GBK")) {
                if (PrinterAPI.SUCCESS == mPrinter.cutPaper(66, 0)) {
                    if (PrinterAPI.SUCCESS == mPrinter.disconnect()) {
                    }
                }
            } else {
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
