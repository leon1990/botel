package hk.yijian.baidutts;

import android.content.Context;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.TtsMode;

/**
 * Created by young on 2017/11/14.
 */

public class _BaiduTTSUtils {

    private SpeechSynthesizer mSpeechSynthesizer;

    private _BaiduTTSUtils() {
    }
    private static class _BaiduTTSUtilsLoader {
        private static final _BaiduTTSUtils INSTANCE = new _BaiduTTSUtils();
    }

    public static _BaiduTTSUtils getInstance() {
        return _BaiduTTSUtils._BaiduTTSUtilsLoader.INSTANCE;
    }

    public void _InitBaiduTTS(Context context) {
        mSpeechSynthesizer = SpeechSynthesizer.getInstance();
        mSpeechSynthesizer.setContext(context);
        mSpeechSynthesizer.setAppId("11018747");
        mSpeechSynthesizer.setApiKey("q74EnDW1z3YaR46YC3qzAwpG", "1Vtew6TG8ytZFaWnVzFuh5wOCAIzE5Pr");
        mSpeechSynthesizer.auth(TtsMode.MIX); //离在线混合
        mSpeechSynthesizer.initTts(TtsMode.MIX);
    }

    public void _OpenBaiduTTS(String string) {
        mSpeechSynthesizer.speak(string);
    }


    public void _PauseBaiduTTS() {
        mSpeechSynthesizer.pause();
    }

    public void _StopBaiduTTS() {
        mSpeechSynthesizer.stop();
    }

    public void _ReleaseBaiduTTS() {
        mSpeechSynthesizer.release();
    }

}
